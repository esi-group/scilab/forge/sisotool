//www.cds.caltech.edu/~murray/courses/cds101/fa04/caltech/am04_ch2-3oct04.pdf
2.2.

//where M is the mass of the base, m and J are the mass and moment of
//inertia of the system to be balanced, l is the distance from the base to
//the center of mass of balanced body, b is the coeffcient of viscous
//friction, and g is the acceleration due to gravity.

d=J*(M+m)+M*m*l^2
A=[0 0 1 0 //dp
   0 0 0 1 //dtheta
   0 m^2*g*l^2/d -(J+m*l^2)*b/d 0 //d2p
   0 m*g*l*(M+m)/d -m*l*b/d 0];//d2theta

//http://renaissance.ucsd.edu/courses/mae143c/MIPdynamics.pdf
mw=0.05; //mass of the wheel
mr=0.08; //mass of the rod
R=0.015; //radius of the wheel
Tau=0;//torque from motor
LT=0.05;//length of the rod
L=LT/2;//length from end to center of mass of the rod
Ir= mr*LT^2/3;//inertia of the rod
Iw=mw*(1+0.8^2)*R^2;//inertia of the wheel
Px=0; //reaction force between wheel and rod along x axis
Py=0;//reaction force between wheel and rod along x axis
at=0;//tangential force
an=0;//normal force
g=9.81; //gravity
//theta angle de la barre avec la verticale
//phi angle de roration de la roue
//fast theta/tau
c=Iw+(mw+mr)*R^2
G1=syslin("c",(mr*R*L+c)/((-c*(Ir+mr*L^2)+mr^2*R^2*L^2)*%s^2+mr*g*L*c));

G1=syslin("c",2500/poly([-10 10],"s","r"))


//Slow phi/theta
G2=syslin("c",(-(Ir+mr*R*L)*%s^2+mr*g*L)/((c+mr*R*L)*%s^2))


//Motor theta_dot/V (see dcmotor.sce)
J=0.01;b=0.1;K=0.01;R=1;L=0.5
M=syslin('c',K/((J*%s+b)*(L*%s+R)+K^2))
