//test  marge de phase et marge de gain
demo_viewCode("d1.sce");
s=%s;
s=poly(0,'s')
num=22801+4406.18*s+382.37*s^2+21.02*s^3+s^4;
den=22952.25+4117.77*s+490.63*s^2+33.06*s^3+s^4
G=syslin('c',num/den);
k=1;
z=-1;
p=[-4+%i -4-%i];
C=syslin('c',k*real(prod(s-z)),real(prod(s-p)));

sisotool(['rlocus','bode','nichols'],G,C)
