//test  marge de phase et marge de gain
s=%s;
G=syslin('c',poly(1,'s','c'),poly(1,'s','c'));
k=277;
z=-3561;
p=[462.88+276.12*%i,462.88-276.12*%i];
C=syslin('c',k*real(prod(s-z)),real(prod(s-p)));

sisotool(['rlocus','nichols'],G,C)
