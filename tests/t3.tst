//test  marge de phase et marge de gain
s=%s;
G=syslin('c',poly(1,'s','c'),poly(0,'s'));
k=5.544582952150362e+08;
z=-3561;
p=[462.88+276.12*%i,462.88-276.12*%i,-1.21+0.73*%i,-1.21-0.73*%i ];
C=syslin('c',k*real(prod(s-z)),real(prod(s-p)));

sisotool(['rlocus','bode','nichols'],G,C)
