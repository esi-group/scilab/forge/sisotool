// D.E. Riegle and P.M. Lin. "Matrix signal flow graphs and an optimum topological
// method for evaluating their gains. "IEEE Transactions on Circuit Theory,CT-19(5):427–435, 1972. 

//Figure 1
Arcs= [6 3;3 1;1 2;2 5;2 4;4 3];
EdgeNames=["D";"C";"B";"A";"F";"E"];
RiegleRule(Arcs,EdgeNames,6,5);

//Figure 3 
Arcs=[7 4;4 3;3 2;2 5;5 6;2 3;4 1;1 4;5 1;1 5];
EdgeNames=["A";"E";"F";"G";"D";"H";"B";"J";"K";"C"];
RiegleRule(Arcs,EdgeNames,7,6);

//https://etd.ohiolink.edu/rws_etd/document/get/ohiou1257871858/inline
Arcs=     [1 2;2 3;3 4;4 5;4 2;1 4;4 3]
EdgeNames=["A";"B";"C";"D";"G";"F";"H"];
RiegleRule(Arcs,EdgeNames,1,5);
//figure 10.7
//       u1 y1 y1 y2 y2 y3 y3 x3
Arcs=    [1  2; 2  3; 3  4; 4  5; 5 6;6 4; 4 3;3 2];
EdgeNames=["E"; "P1"; "P2"; "P3"; "E";"H";"H";"H"];
RiegleRule(Arcs,EdgeNames,1,5);
