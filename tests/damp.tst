//Vector of complex number

R=[2 1+%i  2-3*%i];
[wn,zeta]=damp(R);
if ~assert_checkequal (wn,abs(R)) then pause,end
if ~assert_checkequal (zeta,-real(R)./wn) then pause,end

dt=1;
[wn,zeta]=damp(R,dt);
Rd=log(R)/dt;
if ~assert_checkequal (wn,abs(Rd)) then pause,end
if ~assert_checkequal (zeta,-real(Rd)./wn) then pause,end

R=0;
[wn,zeta]=damp(R);
if ~assert_checkequal (wn,0) then pause,end
if ~assert_checkequal (zeta,-1) then pause,end

R=0;dt=1;
[wn,zeta]=damp(R,dt);
if ~assert_checkequal (wn,%inf) then pause,end
if ~assert_checkequal (zeta,%nan) then pause,end

R=1;dt=1;
[wn,zeta]=damp(R,dt);
if ~assert_checkequal (wn,0) then pause,end
if ~assert_checkequal (zeta,-1) then pause,end

R=1;
[wn,zeta]=damp(R);
if ~assert_checkequal (wn,1) then pause,end
if ~assert_checkequal (zeta,-1) then pause,end

//Polynomial and Polynomial array
P=real([poly([2 1+%i 1-%i  2-3*%i 2+3*%i],'s'),poly(0,'s'),poly(1,'s')]);
[wn,zeta]=damp(P);
wnref=[0;1;sqrt(2);sqrt(2);2;sqrt(13);sqrt(13)];
zetaref=[-1;-1;-sqrt(2)/2;-sqrt(2)/2;-1;-2/sqrt(13);-2/sqrt(13)];
if ~assert_checkalmostequal (wn,wnref) then pause,end
if ~assert_checkalmostequal (zeta,zetaref) then pause,end

[wn,zeta]=damp(prod(P));
wnref=[0;1;sqrt(2);sqrt(2);2;sqrt(13);sqrt(13)];
zetaref=[-1;-1;-sqrt(2)/2;-sqrt(2)/2;-1;-2/sqrt(13);-2/sqrt(13)];
if ~assert_checkalmostequal (wn,wnref) then pause,end
if ~assert_checkalmostequal (zeta,zetaref) then pause,end

dt=1;
[wn,zeta]=damp(P,dt);
t1=log(1+%i)/dt;
t2=log(2-3*%i)/dt;
wnref=[0;log(2);abs(t1);abs(t1);abs(t2);abs(t2);%inf];
zetaref=[-1;-1;-real(t1)/abs(t1);-real(t1)/abs(t1);-real(t2)/abs(t2);-real(t2)/abs(t2);%nan];
if ~assert_checkalmostequal (wn,wnref) then pause,end
if ~assert_checkalmostequal (zeta,zetaref) then pause,end

dt=1;
[wn,zeta]=damp(prod(P),dt);
t1=log(1+%i)/dt;
t2=log(2-3*%i)/dt;
wnref=[0;log(2);abs(t1);abs(t1);abs(t2);abs(t2);%inf];
zetaref=[1;-1;-real(t1)/abs(t1);-real(t1)/abs(t1);-real(t2)/abs(t2);-real(t2)/abs(t2);%nan];
if ~assert_checkalmostequal (wn,wnref) then pause,end
if ~assert_checkalmostequal (zeta,zetaref) then pause,end

//transfer function
[wn,zeta]=damp(syslin('c',ones(1,3),P));
wnref=[0;1;sqrt(2);sqrt(2);2;sqrt(13);sqrt(13)];
zetaref=[-1;-1;-sqrt(2)/2;-sqrt(2)/2;-1;-2/sqrt(13);-2/sqrt(13)];
if ~assert_checkalmostequal (wn,wnref) then pause,end
if ~assert_checkalmostequal (zeta,zetaref) then pause,end

dt=1;
[wn,zeta]=damp(syslin(dt,ones(1,3),P));
t1=log(1+%i)/dt;
t2=log(2-3*%i)/dt;
wnref=[0;log(2);abs(t1);abs(t1);abs(t2);abs(t2);%inf];
zetaref=[1;-1;-real(t1)/abs(t1);-real(t1)/abs(t1);-real(t2)/abs(t2);-real(t2)/abs(t2);%nan];
if ~assert_checkalmostequal (wn,wnref) then pause,end
if ~assert_checkalmostequal (zeta,zetaref) then pause,end

//state-space
[wn,zeta]=damp(tf2ss(syslin('c',ones(1,3),P)));
wnref=[0;1;sqrt(2);sqrt(2);2;sqrt(13);sqrt(13)];
zetaref=[-1;-1;-sqrt(2)/2;-sqrt(2)/2;-1;-2/sqrt(13);-2/sqrt(13)];
if ~assert_checkalmostequal (wn,wnref) then pause,end
if ~assert_checkalmostequal (zeta,zetaref) then pause,end

dt=1;
[wn,zeta]=damp(tf2ss(syslin(dt,ones(1,3),P)));
t1=log(1+%i)/dt;
t2=log(2-3*%i)/dt;
wnref=[0;log(2);abs(t1);abs(t1);abs(t2);abs(t2);%inf];
zetaref=[1;-1;-real(t1)/abs(t1);-real(t1)/abs(t1);-real(t2)/abs(t2);-real(t2)/abs(t2);%nan];
if ~assert_checkalmostequal (wn,wnref) then pause,end
if ~assert_checkalmostequal (zeta,zetaref) then pause,end
