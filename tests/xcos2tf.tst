importXcosDiagram("tests/xcos1.zcos");
r=xcos2TF(scs_m);
assert_checkequal(r.CL,"feedback(G*C,1)");

importXcosDiagram("tests/xcos2.zcos")
r=xcos2TF(scs_m);
assert_checkequal(r.CL,"C*C1*feedback(G,C+C1");

importXcosDiagram("tests/xcos3.zcos")
r=xcos2TF(scs_m);
assert_checkequal(r.CL,"C*C1*feedback(G,C+C1)+feedback(C2,1)");
