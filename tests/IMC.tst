function r=tri(x);[s,k]=gsort([real(x),imag(x)],"lr","d");r=x(k);endfunction

//-----------------------------------------------------------------
//-------------------------continuous time-------------------------
//-----------------------------------------------------------------
//stable & MP
P=zpk(-1,[-3;-2],1,"c")
[C,Q]=IMC(P,1.2);
assert_checkequal(tri(Q.Z{1}),[-2;-3]);
assert_checkequal(tri(Q.P{1}),[-5/6;-1]);
assert_checkequal(Q.K,5/6);
assert_checkequal(tri(C.Z{1}),[-2;-3]);
assert_checkequal(tri(C.P{1}),[0;-1]);
assert_checkequal(C.K,5/6);

//for minimum phase continuous time systems the solution should not depend on V
V=zpk([],[0 1],1,"c");
[C1,Q1]=IMC(P,1.2,V);
assert_checkequal(C1.Z,C.Z);
assert_checkequal(C1.P,C.P);
assert_checkequal(C1.K,C.K);
assert_checkequal(Q1.Z,Q.Z);
assert_checkequal(Q1.P,Q.P);
assert_checkequal(Q1.K,Q.K);

P=zpk(-1,[-1+%i,-1-%i],1,"c")
[C,Q]=IMC(P,1.2);
assert_checkequal(tri(Q.Z{1}),[-1+%i;-1-%i]);
assert_checkequal(tri(Q.P{1}),[-5/6;-1]);
assert_checkequal(Q.K,5/6);
assert_checkequal(tri(C.Z{1}),[-1+%i;-1-%i]);
assert_checkequal(tri(C.P{1}),[0;-1]);
assert_checkequal(C.K,5/6);


P=zpk([-1;-1],[-3;-2],1,"c"); 
[C,Q]=IMC(P,1.2);
assert_checkequal(tri(Q.Z{1}),[-2;-3;]);
assert_checkequal(tri(Q.P{1}),[-5/6;-1;-1]);
assert_checkequal(Q.K,5/6);
assert_checkequal(tri(C.Z{1}),[-2;-3]);
assert_checkequal(tri(C.P{1}),[0;-1;-1]);
assert_checkequal(C.K,5/6);


P=zpk(-1,[-3;-3;-2],1,"c");
[C,Q]=IMC(P,1.2);
assert_checkequal(tri(Q.Z{1}),[-2;-3;-3]); 
assert_checkequal(tri(Q.P{1}),[-5/6;-5/6;-1]);
assert_checkequal(Q.K,25/36);
assert_checkequal(tri(C.Z{1}),[-2;-3;-3]);
assert_checkalmostequal(tri(C.P{1}),[0;-1;-5/3],1e-12,1e-12);
assert_checkequal(C.K,25/36);


//unstable & MP
//marginaly unstable
P=zpk(-3,[0 -2],1,"c");
V=zpk([],0,1,"c");
[C,Q]=IMC(P,1,P*V);
assert_checkalmostequal(tri(Q.Z{1}),[0;-0.5;-2]); 
assert_checkequal(tri(Q.P{1}),[-1;-1;-3]);
assert_checkequal(Q.K,2);
assert_checkalmostequal(tri(C.Z{1}),[-0.5;-2]);
assert_checkalmostequal(tri(C.P{1}),[0;-3]);
assert_checkequal(C.K,2);


P=zpk(-1,[-3;2],1,"c")
[C,Q]=IMC(P,1.2);
assert_checkalmostequal(tri(Q.Z{1}),[2;-25/132;-3]); 
assert_checkequal(tri(Q.P{1}),[-5/6;-5/6;-1]);
assert_checkequal(Q.K,11/3);
assert_checkalmostequal(tri(C.Z{1}),[-25/132;-3]);
assert_checkalmostequal(tri(C.P{1}),[0;-1]);
assert_checkequal(C.K,11/3);

//for minimum phase continuous time systems the solution should not depend on V
V=zpk([2],[0 1],1,"c");
[C1,Q1]=IMC(P,1.2,V);
assert_checkequal(Q.Z{1},Q1.Z{1}); 
assert_checkequal(Q.P{1},Q1.P{1});
assert_checkequal(Q.K,Q1.K);
assert_checkequal(C.Z{1},C1.Z{1});
assert_checkequal(C.P{1},C1.P{1});
assert_checkequal(C.K,C1.K);

P=zpk([-1;-1],[-3;2],1,"c")
[C,Q]=IMC(P,1.2);//perturbation applied in d
assert_checkalmostequal(tri(Q.Z{1}),[2;-25/132;-3]); 
assert_checkequal(tri(Q.P{1}),[-5/6;-5/6;-1;-1]);
assert_checkequal(Q.K,11/3);
assert_checkalmostequal(tri(C.Z{1}),[-25/132;-3]);
assert_checkalmostequal(tri(C.P{1}),[0;-1;-1]);
assert_checkequal(C.K,11/3);

P=zpk([-1],[2;-3;-3],1,"c") 
[C,Q]=IMC(P,1.2);
assert_checkalmostequal(tri(Q.Z{1}),[2;-125/2394;-3;-3]); 
assert_checkequal(tri(Q.P{1}),[-5/6;-5/6;-5/6;-1]);
assert_checkequal(Q.K,133/12);
assert_checkalmostequal(tri(C.Z{1}),[-125/2394;-3;-3]); 
assert_checkalmostequal(tri(C.P{1}),[0;-1;-9/2]);
assert_checkequal(C.K,133/12);


//unstable & NMP
P=zpk(2,[-1;1],1,"c"); //perturbation applied in d
[C,Q]=IMC(P,1.2);
assert_checkalmostequal(tri(Q.Z{1}),[1;-1/5;-25/96]);
assert_checkalmostequal(tri(Q.P{1}),[-5/6;-5/6;-2]);
assert_checkalmostequal(Q.K,-40/3);
assert_checkalmostequal(tri(C.Z{1}),[-1/5;-25/96;-1]);
assert_checkalmostequal(tri(C.P{1}),[0;-0.330303531;-18.66969647],1e-9,1e-9);
assert_checkalmostequal(C.K,-40/3);


V=zpk([],0,1,"c");
[C,Q]=IMC(P,1.2,P*V); //perturbation applied in r (p 93) OK
Z=tri(Q.Z{1});
assert_checkalmostequal(abs(Z(3:4))^2,[3/4;3/4]);
assert_checkalmostequal(2*real(Z(3:4)),[-13/8;-13/8]);
assert_checkalmostequal(Z([1 2 5]),[1;-125/1206;-1]); 
assert_checkalmostequal(tri(Q.P{1}),[-5/6;-5/6;-5/6;-2;-2]);
assert_checkalmostequal(Q.K,-134/9);
Z=tri(C.Z{1});
assert_checkalmostequal(abs(Z(2:3)).^2,[3/4;3/4]);
assert_checkalmostequal(2*real(Z(2:3)),[-13/8;-13/8]);
assert_checkalmostequal(Z([1 4]),[-125/1206;-1]); 
assert_checkalmostequal(tri(C.P{1}),tri([0;
        -0.8109933220551+%i*0.3012482637989;
        -0.8109933220551-%i*0.3012482637989;-20.76690224477875;]),1e-12,1e-12);
assert_checkalmostequal(C.K,-134/9);

//-----------------------------------------------------------------
//-------------------------discrete time---------------------------
//-----------------------------------------------------------------
//stable & MP
P=zpk(-0.5,[-0.2;-0.3],1,1);

[C,Q]=IMC(P,1.2);
assert_checkalmostequal(tri(real(Q.Z{1})),[-0.2 ;-0.3]); 
assert_checkalmostequal(tri(Q.P{1}),[1.2;0]);
assert_checkalmostequal(Q.K,-2/15);
assert_checkalmostequal(tri(C.Z{1}),[-0.2;-0.3]);
assert_checkalmostequal(tri(C.P{1}),[1;1/15]);
assert_checkalmostequal(C.K,-2/15);

//Check that the solution does not depends on V
V=zpk([2],[1 -2],1,1);
[C1,Q1]=IMC(P,1.2,V);
assert_checkequal(Q.Z{1},Q1.Z{1}); 
assert_checkequal(Q.P{1},Q1.P{1});
assert_checkequal(Q.K,Q1.K);
assert_checkequal(C.Z{1},C1.Z{1});
assert_checkequal(C.P{1},C1.P{1});
assert_checkequal(C.K,C1.K);

P=zpk(-0.5,[0.2;0.3],1,1);
[C,Q]=IMC(P,1.2);
assert_checkalmostequal(tri(real(Q.Z{1})),[0.3 ;0.2]); 
assert_checkalmostequal(tri(Q.P{1}),[1.2;0]);
assert_checkalmostequal(Q.K,-2/15);
assert_checkalmostequal(tri(C.Z{1}),[0.3;0.2]);
assert_checkalmostequal(tri(C.P{1}),[1;1/15]);
assert_checkalmostequal(C.K,-2/15);
//Check that the solution does not depends on V
V=zpk([2],[1 -2],1,1);
[C1,Q1]=IMC(P,1.2,V);
assert_checkequal(Q.Z{1},Q1.Z{1}); 
assert_checkequal(Q.P{1},Q1.P{1});
assert_checkequal(Q.K,Q1.K);
assert_checkequal(C.Z{1},C1.Z{1});
assert_checkequal(C.P{1},C1.P{1});
assert_checkequal(C.K,C1.K);


P=zpk([-0.5 0.5],[-0.2 -0.3],1,1)
[C,Q]=IMC(P,1.2);
assert_checkalmostequal(tri(real(Q.Z{1})),[-0.2 ;-0.3]); 
assert_checkalmostequal(tri(Q.P{1}),[1.2;0.5]);
assert_checkalmostequal(Q.K,-2/15);
assert_checkalmostequal(tri(C.Z{1}),[-0.2;-0.3]);
assert_checkalmostequal(tri(C.P{1}),[1;1/2]);
assert_checkalmostequal(C.K,-2/15);
//Check that the solution does not depends on V
V=zpk([2],[0.1 -0.2],1,1);
[C1,Q1]=IMC(P,1.2,V);
assert_checkequal(Q.Z{1},Q1.Z{1}); 
assert_checkequal(Q.P{1},Q1.P{1});
assert_checkequal(Q.K,Q1.K);
assert_checkequal(C.Z{1},C1.Z{1});
assert_checkequal(C.P{1},C1.P{1});
assert_checkequal(C.K,C1.K);


//stable & NMP 
P=zpk(-1.5,[-0.2 -0.3],1,1);//step perturbation en d
[C,Q]=IMC(P,1.2); 
assert_checkalmostequal(tri(real(Q.Z{1})),[-0.2 ;-0.3],1e-12,1e-12); 
assert_checkalmostequal(tri(Q.P{1}),[1.2;0],1e-12,1e-12); //simplification non faite
assert_checkalmostequal(Q.K,-0.08);
assert_checkalmostequal(tri(C.Z{1}),[-0.2;-0.3],1e-12,1e-12);
assert_checkalmostequal(tri(C.P{1}),[1;0.12],1e-12,1e-12); 
assert_checkalmostequal(C.K,-0.08);

V=zpk([],1,1,1); 
[C1,Q1]=IMC(P,1.2,P*V); //perturbation en r
assert_checkequal(Q.Z{1},Q1.Z{1}); 
assert_checkequal(Q.P{1},Q1.P{1});
assert_checkequal(Q.K,Q1.K);
assert_checkequal(C.Z{1},C1.Z{1});
assert_checkequal(C.P{1},C1.P{1});
assert_checkequal(C.K,C1.K);

V=zpk([2],[0.1 -0.2],1,1);
[C1,Q1]=IMC(P,1.2,V); // controller does not depend on perturbation
assert_checkequal(Q.Z{1},Q1.Z{1}); 
assert_checkequal(Q.P{1},Q1.P{1});
assert_checkequal(Q.K,Q1.K);
assert_checkequal(C.Z{1},C1.Z{1});
assert_checkequal(C.P{1},C1.P{1});
assert_checkequal(C.K,C1.K);


//unstable  & MP
P=zpk(-0.5,[-1.3 -0.2],1,1)
[C,Q]=IMC(P,1.2); // step perturbation on d
assert_checkalmostequal(tri(real(Q.Z{1})),[13/29;-1/5;-1.3]); 
assert_checkalmostequal(tri(Q.P{1}),[0;0;0],1e-12,1e-12);
assert_checkalmostequal(Q.K,29/24);
assert_checkalmostequal(tri(C.Z{1}),[13/29;-1/5;-1.3]);
assert_checkalmostequal(tri(C.P{1}),[1;0.634905800;-0.426572467],1e-9,1e-9);
assert_checkalmostequal(C.K,29/24);

//Check that the solution does not depends on V
V=zpk([],[1],1,1);
[C1,Q1]=IMC(P,1.2,V*P); //step perturbation on r
assert_checkequal(Q.Z{1},Q1.Z{1}); 
assert_checkequal(Q.P{1},Q1.P{1});
assert_checkequal(Q.K,Q1.K);
assert_checkequal(C.Z{1},C1.Z{1});
assert_checkequal(C.P{1},C1.P{1});
assert_checkequal(C.K,C1.K);

//unstable  & NMP
P=zpk(-1.5,[-1.3 -0.2],1,1);
[C,Q]=IMC(P,1.2); // perturbation en d
assert_checkalmostequal(tri(real(Q.Z{1})),[1.029003707;0.833333333;-0.2;-1.3;-5.895670373],1e-9,1e-9); 
assert_checkalmostequal(tri(Q.P{1}),[1.3;0;0;0],1e-12,1e-12);
assert_checkalmostequal(Q.K,18/5);
assert_checkalmostequal(tri(real(C.Z{1})),[1.029003707;0.833333333;-0.2;-1.3;-5.895670373],1e-9,1e-9); 
assert_checkalmostequal(tri(C.P{1}),[1;0.862704134;-1.413464957;-8.610777638],1e-9,1e-9);
assert_checkalmostequal(C.K,18/5);


V=zpk([],[1],1,1);
[C,Q]=IMC(P,1.2,P*V); // perturbation en r
assert_checkalmostequal(tri(real(Q.Z{1})),[0.858153242;0.464403415;-0.194194313;-0.2;-1.3],1e-9,1e-9); 
assert_checkalmostequal(tri(Q.P{1}),[0;0;0;0;0],1e-12,1e-12);
assert_checkalmostequal(Q.K,4.40887492);
assert_checkalmostequal(tri(real(C.Z{1})),[0.858153242;0.464403415;-0.194194313;-0.2],1e-9,1e-9); 
assert_checkalmostequal(tri(C.P{1}),[4.447447998;1;0.455690565;-0.194263642],1e-9,1e-9);
assert_checkalmostequal(C.K,Q.K);
