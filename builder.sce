mode(-1);
function main_builder()
  try
    getversion("scilab");
  catch
    error(_("Scilab 5.0 or more is required."));
  end;

  if ~with_module("development_tools") then
    error(msprintf(gettext("%s module not installed."),"development_tools"));
  end
 
  TOOLBOX_NAME = "sisotool"
  TOOLBOX_TITLE = _("sisotool");

  toolbox_dir = get_absolute_file_path("builder.sce");

  tbx_builder_macros(toolbox_dir);
  //tbx_builder_src(toolbox_dir);
  //tbx_builder_gateway(toolbox_dir);
  pathmacros=toolbox_dir+"macros"
  sisotoollib = lib(toolbox_dir+"macros");
  updatelib = lib(pathmacros+"/update")
  IMClib=lib(pathmacros+"/IMC")
  
  tbx_builder_help(toolbox_dir);
  tbx_build_loader(toolbox_dir);
  tbx_build_cleaner(toolbox_dir);
  //exec(toolbox_dir+"macros/xcos/builder.sce",-1);
endfunction
main_builder()

clear main_builder
