<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2017 - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="sisotoolNyquistAnalyser">
  <refnamediv>
    <refname>Nyquist frequency response analyser</refname>
    <refpurpose>Displays the Nyquist plot of the frequency responses
    for the selected transfer function together with the user
    requirements and main characteristics.</refpurpose>
  </refnamediv>
 
  <refsection id="NyquistAnalyserView">
      <title>Analyser view</title> 
      <para>
        <mediaobject>
          <imageobject>
            <imagedata align="center" fileref="../images/NyquistAnalyser.svg"/>
          </imageobject>
      </mediaobject>
      <!-- G=syslin("c",1/(%s+1));C=syslin("c",0.3/((%s+0.7)*(0.41+%s+%s^2))); -->
      This example represents the open loop "Out of C" nyquist plot
      for the system given by the plant <latex>G=\frac{1}{1+s}</latex>
      and the compensator
      <latex>C=\frac{0.3}{(s+0.7)(0.41+s+s^2)}</latex>. The "Min
      stability margins" characteristics (see <link
      linkref="NyquistCharacteristics">below</link>) are also
      displayed.
      </para>

    </refsection>
    <refsection>
    <title>Description</title>
    <para>
      This sub window may displays the Nyquist plot of the frequency
      responses for the selected transfer functions together with main
      characteristics. It is mainly dedicated to open loop transfer
      function for feed back stability and robustness analysis.
    </para>
    <para>
      The choice of transfer function can be made either in the <link
      linkend="sisotoolAnalyserSelector">graphical analyser
      selector</link> or throught the <link
      linkend="sisotoolAnalyserContextualMenus">contextual
      menu</link>.
    </para>
    <refsection id="NyquistCharacteristics">
    <title>Characteristics</title>
    <para>
      The choice of characteristics to be displayed can be made
      throught the <link
      linkend="sisotoolAnalyserContextualMenus">contextual
      menu</link>.
    </para>
    <para>
      The available characteristics for Nyquist analyser are:
    </para>

    <variablelist>
      <varlistentry>
            <term>Peak response</term>
            <listitem>
              <para>This charateristic is visualized with a dot mark
              located at the maximum value of the response and
              horizontal and vertical extension lines issued from this
              point.</para>
            </listitem>
          </varlistentry>
          <varlistentry>
            <term>Min stability margins</term>
            <listitem>
              <para></para>
            </listitem>
          </varlistentry>
          <varlistentry>
            <term>All stability margins</term>
            <listitem>
              <para></para>
            </listitem>
          </varlistentry>
        </variablelist>
        <note>
          <para> The selected characteristics are displayed for all
          displayed transfer functions.  The marks and extension lines
          inherits the response curve color.</para>
        </note>
        <tip>
          <para>
            if one clicks on a mark, a datatip is displayed which gives
            the transfert function name and numerical values related to
            the characteristic. A second click hides the datatip. 
            <mediaobject>
              <imageobject>
                <imagedata align="center" fileref="../images/NyquistAnalyserTip.svg"/>
              </imageobject>
            </mediaobject>
          </para>
        </tip>
    </refsection>
    <refsection>
      <title>Design Requirements</title>
      <para>
      There in no design requirement available for Nyquist analyser. 
      </para>
    </refsection>
 <refsection>
    <title>Grid</title>
    <para>
      If the user asks for Nyquist Analyser  grid (using the <link
      linkend="sisotoolAnalyserContextualMenus">contextual
      menu</link>, the <link linkend="Hallchart">Hall chart</link> is drawn at the plot background.
    </para>
    <para>
      Example
      <mediaobject>
        <imageobject>
            <imagedata align="center" fileref="../images/NyquistAnalyserGrid.svg"/>
          </imageobject>
      </mediaobject>
      The gray lines represent the iso-phase and the iso-module curves
      for feedback system.
    </para>
  </refsection>
  </refsection>
</refentry>
