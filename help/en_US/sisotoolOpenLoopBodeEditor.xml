<?xml version="1.0" encoding="UTF-8"?>
<!--
* Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
* Copyright (C) 2016 - INRIA - Serge Steer
*
* This file is hereby licensed under the terms of the GNU GPL v2.0,
* pursuant to article 5.3.4 of the CeCILL v.2.1.
* For more information, see the COPYING file which you should have received
* along with this program.
-->
<refentry xml:id="sisotoolOpenLoopBodeEditor" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>Open loop Bode editor</refname>

    <refpurpose>Interactive graphical compensator editor with dynamical
    display of the open loop Bode plot.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Description</title>

    <para>This is one of the available graphical editors. It allows to
    interactively add, modify or delete the components of the selected
    compensator block through the <link linkend="scilab.help/bode">Bode
    plot</link> display of the open loop system. It also allow to
    interactively add, modify or delete frequency requirements for the closed
    loop system.</para>

    <refsection>
      <title>Editor view</title>

      <para>The editor consists in two sub plots one for the modulus of the
      open loop system frequency response and the other one for the phase.
      User may interact with both sub plots. <mediaobject>
          <imageobject>
            <imagedata align="center" fileref="../images/OL_Bode.png"/>
          </imageobject>
        </mediaobject></para>

      <para>The red crosses represent the images of the adjustable compensator
      poles. The red crosses represent the images of the compensator's poles.
      They are located on the gain and phase plot curves at their natural
      frequencies.</para>

      <para>The red circles represent the images of the adjustable compensator
      zeros. They are located on the gain and phase plot curves at their
      natural frequencies.</para>

      <para>The brown lines represents the gain and phase margins if
      they exist while the dashed blue lines denote the frequencies
      and the gain and phase values associated with the
      margins.</para>
    </refsection>

    <refsection>
      <title>Compensator adjustment</title>

      <para>The red symbols can be moved using a press-drag-release
      action:</para>

      <itemizedlist>
        <listitem>
          <para>For real pole or zero, horizontal move controls natural
          frequency and vertical move does nothing.</para>
        </listitem>

        <listitem>
          <para>For complex pole or zero move in the gainplot sub window,
          horizontal move controls natural frequency and vertical move
          controls damping factor. For complex pole or zero move in the phase
          plot sub window, horizontal move controls natural frequency and
          vertical does noting.</para>
        </listitem>
      </itemizedlist>

      <para>To tune the open loop gain use a press-drag-release action on the
      gainplot curve. Vertical move increase or decrease the gain.</para>

      <tip>
        <para>It is also possible to add or remove compensator poles and zeros
        using the <link linkend="bodecontextual_menus">contextual
        menu</link>.</para>
      </tip>

      <caution>
        <para>If one uses the open loop bode editor for compensator
        adjustment, it is necessary to activate the refresh (see <link
        linkend="bodecontextual_menus">contextual menu</link>) to be able to
        see the evolution of the Bode plots locus during move.</para>
      </caution>
    </refsection>
  </refsection>

  <refsection id="bodecontextual_menus">
    <title>Contextual menus</title>

    <para>A left click in the Bode's sub windows opens a popup which depend on
    the underlying object:</para>

    <itemizedlist>
      <listitem>
        <para>A left click in a free area opens a popup menu which proposes
        the following actions:</para>

        <variablelist>
          <varlistentry>
            <term>Add Component</term>

            <listitem>
              <para>This sub menu opens the popup below to select the type of
              component he wants to add to the compensator.</para>

              <mediaobject>
                <imageobject>
                  <imagedata align="center"
                             fileref="../images/AddComponent.png"/>
                </imageobject>
              </mediaobject>

              <para>User selects in this popup the type of component he wants
              to add.</para>

              <itemizedlist>
                <listitem>
                  <para>If an integrator or a differentiator is selected
                  nothing more is needed.</para>
                </listitem>

                <listitem>
                  <para>For a real pole (zero), the user selects a point in
                  the sub window. The abscissa of the point gives the natural
                  frequency of the pole (zero).</para>
                </listitem>

                <listitem>
                  <para>For a complex pole (zero), the user selects a point in
                  the sub window. The abscissa of the point gives the natural
                  frequency of the pole (zero), the imaginary part is set to
                  zero.</para>
                </listitem>

                <listitem>
                  <para>For a lead filter, the user select a point in the sub
                  window. The abscissa of the point gives the natural
                  frequency of the lead filter's pole and 2/3 of the natural
                  frequency of the lead filter's zero.</para>
                </listitem>

                <listitem>
                  <para>For a lag filter, the user select a point in the sub
                  window. The abscissa of the point gives the natural
                  frequency of the lead filter zero and 3/2 of the natural
                  frequency of the lead filter's pole.</para>
                </listitem>

                <listitem>
                  <para>For a notch filter, the user select a point in the sub
                  window. The abscissa of the point gives the natural
                  frequency of the notch's pole and zero the pole's damping
                  ratio is set to 0.5 while the zero's damping ratio is set to
                  0.05.</para>
                </listitem>
              </itemizedlist>
            </listitem>
          </varlistentry>

          <varlistentry>
            <term>Design requirements</term>

            <listitem>
              <para>The design requirements allow to set and draw the
              admissible area which corresponds to the user's requirement for
              the compensated system.</para>

              <para>If the user selects this menu a popup opens which proposes
              to select the requirement to set or update:</para>

              <variablelist>
                <varlistentry>
                  <term>Upper gain limit</term>

                  <listitem>
                    <para>This sub menu allows to define a forbidden upper
                    region for the gain response. It open a GUI where user may
                    define the limit as a sequence of polyline segments.
                    <mediaobject>
                        <imageobject>
                          <imagedata align="center"
                                     fileref="../images/Bode_UpperGainLimit.png"/>
                        </imageobject>
                      </mediaobject></para>

                    <para>This setting results in the following gain plot:
                    <mediaobject>
                        <imageobject>
                          <imagedata align="center"
                                     fileref="../images/Bode_UpperGainLimit1.png"/>
                        </imageobject>
                      </mediaobject></para>
                  </listitem>
                </varlistentry>

                <varlistentry>
                  <term>Lower gain limit</term>

                  <listitem>
                    <para>This sub menu allows similarly to define a forbidden
                    lower region for the gain response.</para>
                  </listitem>
                </varlistentry>

                <varlistentry>
                  <term>Minimum phase margin</term>

                  <listitem>
                    <para>This sub menu opens a GUI where user may define the
                    desired minimum <link
                    linkend="scilab.help/p_margin">phase
                    margin</link>. This requirement is
                    displayed as an arrowed pink thick vertical segment. The
                    phase plot should not cross it to respect the
                    requirement.</para>
                  </listitem>
                </varlistentry>

                <varlistentry>
                  <term>Minimum gain margin</term>

                  <listitem>
                    <para>This sub menu opens a GUI where user may
                    define the desired minimum <link
                    linkend="scilab.help/g_margin">gain
                    margin</link>. This requirement is displayed as an
                    arrowed pink thick vertical segment. The gain plot
                    should not cross it to respect the
                    requirement.</para>
                  </listitem>

                  <para>A view of the Bode editors with the minimum gain
                  margin (20dB) and minimum phase margin (30 degrees)
                  requirements. <mediaobject>
                      <imageobject>
                        <imagedata align="center"
                                   fileref="../images/OL_BodeMarginsRequirements.svg"/>
                      </imageobject>
                    </mediaobject></para>
                </varlistentry>
              </variablelist>
            </listitem>
          </varlistentry>

          <varlistentry>
            <term>Grid</term>

            <listitem>
              <para>This menu toggles the display of gain plot or phase plot
              grid. With the default settings the grid display is
              disabled.</para>
            </listitem>
          </varlistentry>

          <varlistentry>
            <term>Refreshed</term>

            <listitem>
              <para>This menu toggles the refreshed mode. If enabled (the
              default value) the corresponding Bode plot is refreshed during
              the move operations. If disabled it is only refreshed at the end
              of the move operations.</para>
            </listitem>
          </varlistentry>
        </variablelist>
      </listitem>

      <listitem>
        <para>A left click over a red mark opens a popup which allows to
        select the Delete or Edit action for the corresponding component. The
        selection of the Edit action opens a simple GUI for component textual
        edition. The selection of the Delete action removes the selected
        component of the compensator.</para>
      </listitem>

      <listitem>
        <para>A left click over a requirement border opens a popup which
        allows to select the Delete or Edit action for the corresponding
        requirement. The selection of the Edit action opens a simple GUI for
        requirement textual edition. The selection of the Delete action
        removes the selected requirement.</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
