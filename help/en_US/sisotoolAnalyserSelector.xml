<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2017 -  Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="sisotoolAnalyserSelector">
  <refnamediv>
    <refname>Graphical system's analyser selector</refname>
    <refpurpose>This GUI can be used to select the transfer functions
    to be analyzed and the type of response viewer to be
    used.</refpurpose>
  </refnamediv>
  <refsection>
    <title>Gui view</title> 
    <para>
      <mediaobject>
        <imageobject>
          <imagedata align="center"
                     fileref="../images/AnalysersSelector.png"/>
        </imageobject>
      </mediaobject>
      
    </para>
  </refsection>
  <refsection>
    <title>Description</title>

    <para>
      The GUI consist in two frames:
    </para>
    <variablelist>
      <varlistentry>
        <term>Analysis plots</term>
        <listitem>
          <para>In this frame the user can select the type of plots
          (Step, Impulse, Bode, Nyquist, Nichols, Pole/zero) to appear
          in the sub windows of the Analysers window using the
          drop-down lists. It is possible to select up to 6 sub
          windows to be displayed.</para>

          <para>In the above example the user has selected two sub
          window, the first one will display step response and the
          second one Nichols.</para>
          <para>It is possible to dynamically change the selection
          made in this frame keeping as far as possible the selections
          made in the second frame:</para>
          <itemizedlist>
            <listitem>
              <para>
                One can change the plots type for a given sub window, using the drop-down list
              </para>
            </listitem>
          <listitem>
              <para>
                One can add a new sub window changing the "None"
                choice by another selection in the right most
                drop-down list
              </para>
            </listitem>
          <listitem>
            <para>
              One can remove a sub window selecting the "None" choice
              in the corresponding drop-down list.
              </para>
          </listitem>
          </itemizedlist>
          
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Content of plots</term> 
        <listitem>
          <para> The right part of the frame shows the list of
          available transfer function for the selected architecture
          (see <link
          linkend="sisotoolArchitectureSelector">Architecture
          Selector</link>) and the left part contains columns of check
          boxes (one for each selected sub window). These check boxes
          can be used to selected select which transfer function
          should be represented in which window.</para>
          <para>In the above example the user has choosen to display the
          step responses of the r to y and open loop transfer function
          and the Nichols plots of the open loop</para>
          <para>This frame adapts automatically to the choices made by
          the user in the first frame.</para>
        </listitem>
      </varlistentry>
    </variablelist>
    <para>The "Apply" button can be used at any time to update the
    content of the analysers window.</para>
  </refsection>
</refentry>
