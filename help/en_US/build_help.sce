mode(-1)
help_lang_dir = get_absolute_file_path("build_help.sce");
xmlfiles=[listfiles(help_lang_dir+"*.xml")
          listfiles(help_lang_dir+"internals/*.xml")]
if newest([help_lang_dir+"date_build";xmlfiles])==1 then
  clear  xmlfiles lang
  return
end
tbx_build_help("sisotool", help_lang_dir);

//tbx_build_help("sisotool_internals", help_lang_dir+"internals");
mputl(sci2exp(getdate()),help_lang_dir+"date_build")
clear help_lang_dir;
