<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolSubwinSettings" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolSubwinSettings</refname>

    <refpurpose>Defines the way the sub windows are placed in the editors and
    analysers window.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>[axes_bounds,margins]=sisotoolSubwinSettings(type_names) </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>type_names</term>

        <listitem>
          <para>A character string array which contains the types of the sub
          windows to be placed in the figure. The length of this array should
          be less or equal to 6.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>axes_bounds</term>

        <listitem>
          <para>A n by 4 array. Each row contains the axes_bounds property
          value to be set for the corresponding axes..</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>margins</term>

        <listitem>
          <para>A n by 4 array. Each row contains the margins property value
          to be set for the corresponding axes.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function first splits the window frame into a set of areas
    depending on the size (n) of the type_names array as described
    below.</para>

    <itemizedlist>
      <listitem>
        <para>Case n=1</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../../images/case1.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case n=2</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../../images/case2.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case n=3</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../../images/case3.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case n=4</para>

        <inlinemediaobject>
          <para>
            <imageobject>
              <imagedata fileref="../../images/case4.png"/>
            </imageobject>
          </para>
        </inlinemediaobject>
      </listitem>

      <listitem>
        <para>Case n=5</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../../images/case5.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case n=6</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../../images/case6.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>
    </itemizedlist>

    <para>In case of bode plots the corresponding area is then split in two
    parts.</para>
  </refsection>
</refentry>
