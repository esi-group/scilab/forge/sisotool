<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolEditorsMenu" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolEditorsMenu</refname>

    <refpurpose>Creates and displays a menu for compensator edition.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>sisotoolEditorsMenu(fig) </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>fig</term>

        <listitem>
          <para>The handle of the window where the menu will be
          created.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Creates and displays a menu for compensator edition with the
    following sub menus:</para>
    <variablelist>
      <varlistentry>
        <term>Graphical Compensator Editor selector</term>
        <listitem>
          <para>This sub menu opens a GUI where the user may select
          the graphical editor sub window he wants (see <link
          linkend="sisotoolGraphicalCompensatorEditors">Graphical
          compensator editors</link>).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Textual Compensator Editor</term>
        <listitem>
          <para>This sub menu opens a GUI where the user may enter the
          compensator elements (poles ,zeros, leads, lags, notches)
          and their numerical values.</para>
        </listitem>
      </varlistentry>
        <varlistentry>
        <term>Architecture selector</term>
        <listitem>
          <para>This sub menu opens a GUI where the user may choode
          the system architecture (see <link
          linkend="sisotoolArchitectureSelector">Architecture
          Selector</link>).</para>
        </listitem>
      </varlistentry>
    
    </variablelist>
  
  </refsection>

  <refsection>
    <title>Called functions</title>

    <informaltable border="1">
          <tr>
            <td>
              <link linkend="sisotoolEditorsGui">
                <function>sisotoolEditorsGui</function>
              </link>
            </td>

            <td>builds the GUI which allows the selection of the editors
            sub windows.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolCreateCGUI">
                <function>sisotoolCreateCGUI</function>
              </link>
            </td>

            <td>creates the graphical user interface of the textual
            compensator editor.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolArchiGui">
                <function>sisotoolArchiGui</function>
              </link>
            </td>

            <td>creates the graphical user interface used to select the
            desired dynamical system diagram.</td>
          </tr>
    </informaltable>
  </refsection>
</refentry>
