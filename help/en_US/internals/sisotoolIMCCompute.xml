<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolIMCCompute" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolIMCCompute</refname>

    <refpurpose>realizes the IMC compensator computation corresponding to the
    user settings.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>sisotoolIMCCompute() </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>gcbo</term>

        <listitem>
          <para>The handle of the calling uicontrol (see <link
          linkend="scilab.help/gcbo">gcbo</link> ). This variable is
            defined in the calling context.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Realizes the IMC compensator computation corresponding to the user
    settings.</para>
  </refsection>

  <refsection>
    <title>Called functions</title>

    <informaltable border="1">
          <tr>
            <td>
              <link linkend="sisotoolGetSession">
                <function>sisotoolGetSession</function>
              </link>
            </td>

            <td>retrieves the <link linkend="sisotoolSessionData">session data
            structure</link> from the editors window user_data field.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolGetArchiSpecs">
                <function>sisotoolGetArchiSpecs</function>
              </link>
            </td>

            <td>returns the characteristics of the selected
            architecture.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolNewController">
                <function>sisotoolNewController</function>
              </link>
            </td>

            <td>creates the <link linkend="sisotoolCompensatorData">compensator
            data structure</link>.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolC2tex">
                <function>sisotoolC2tex</function>
              </link>
            </td>

            <td>converts the <link linkend="sisotoolCompensatorData">compensator
            data structure</link> into a rational transfer function
            representation in LateX.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolSetSession">
                <function>sisotoolSetSession</function>
              </link>
            </td>

            <td>stores the <link linkend="sisotoolSessionData">session data
            structure</link> in the editors window user_data field.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolRedraw">
                <function>sisotoolRedraw</function>
              </link>
            </td>

            <td>updates the all the opened GUIs.</td>
          </tr>
    </informaltable>
  </refsection>
</refentry>
