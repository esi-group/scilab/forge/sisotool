<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolContextMenu" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolContextMenu</refname>

    <refpurpose>creates the contextual menu associated with right click in the
    Editors sub windows.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>sisotoolContextMenu(ax,x,y) </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>ax</term>

        <listitem>
          <para>the axes handle of the editor sub window.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>x</term>

        <listitem>
          <para>The abscissa of the clicked point (in pixels).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>y</term>

        <listitem>
          <para>The ordinates of the clicked point (in pixels).</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Creates the contextual menu associated with right click in the
    Editors sub windows.</para>
  </refsection>

  <refsection>
    <title>Called functions</title>

    <informaltable border="1">
      <tr>
        <td><link linkend="sisotoolGetEditorData">
        <function>sisotoolGetEditorData</function> </link></td>

        <td>retrieves the <link linkend="sisotoolEditorData">editor data
        structure</link> out of the specified editor axes user_data.</td>
      </tr>

      <tr>
        <td><link linkend="sisotoolFindSelected">
        <function>sisotoolFindSelected</function> </link></td>

        <td>finds if there is a marker near the given point in the current
        axes.</td>
      </tr>

      <tr>
        <td><link linkend="sisotoolRequirementTypes">
        <function>sisotoolRequirementTypes</function> </link></td>

        <td>defines the characteristics of the requirements for each editor
        and analyser.</td>
      </tr>

      <tr>
        <td><link linkend="sisotoolCreatePopupMenu">
        <function>sisotoolCreatePopupMenu</function> </link></td>

        <td>creates a popup menu.</td>
      </tr>
    </informaltable>
  </refsection>

  <refsection>
    <title>Callback functions</title>

    <informaltable border="1"><tr>
        <td>
          <link linkend="sisotoolRequirementsGui">
            <function>sisotoolRequirementsGui</function>
          </link>
        </td>

        <td/>
      </tr> Creates the GUI for Requirements specification. <tr>
        <td>
          <link linkend="sisotoolAddRealPole">
            <function>sisotoolAddRealPoles</function>
          </link>
        </td>

        <td>handles the graphical localization by the user and the addition of
        a real pole in the current compensator block.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolAddComplexPoles">
            <function>sisotoolAddComplexPoles</function>
          </link>
        </td>

        <td>handles the graphical localization by the user and the addition of
        a complex conjugate pole pair in the current compensator block.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolAddIntegrator">
            <function>sisotoolAddIntegrator</function>
          </link>
        </td>

        <td>add an integrator in the current compensator block.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolAddRealZero">
            <function>sisotoolAddRealZero</function>
          </link>
        </td>

        <td>handles the graphical localization by the user and the addition of
        a real zero in the current compensator block.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolAddComplexZeros">
            <function>sisotoolAddComplexZeros</function>
          </link>
        </td>

        <td>handles the graphical localization by the user and the addition of
        a complex conjugate zero pair in the current compensator block.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolAddDiff">
            <function>sisotoolAddDiff</function>
          </link>
        </td>

        <td>add a differentiator in the current compensator block.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolAddLead">
            <function>sisotoolAddLead</function>
          </link>
        </td>

        <td>handles the graphical localization by the user and the addition of
        a lead filter pole in the current compensator block.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolAddLag">
            <function>sisotoolAddLag</function>
          </link>
        </td>

        <td>handles the graphical localization by the user and the addition of
        a lag filter zero in the current compensator block.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolAddNotch">
            <function>sisotoolAddNotch</function>
          </link>
        </td>

        <td>handles the graphical localization by the user and the addition of
        a notch filter in the current compensator block.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolDeleteComponent">
            <function>sisotoolDelete</function>
          </link>
        </td>

        <td>Callback of sisotoolContextMenu for removal of the selected
        compensator component.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolEditComponent">
            <function>sisotoolEditComponent</function>
          </link>
        </td>

        <td>Callback of sisotoolContextMenu for textual edition of the
        selected compensator component.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolToggleGrid">
            <function>sisotoolToggleGrid</function>
          </link>
        </td>

        <td>Toggle the grid mode for the selected sub window.</td>
      </tr> <tr>
        <td>
          <link linkend="sisotoolToggleRefresh">
            <function>sisotoolToggleRefresh</function>
          </link>
        </td>

        <td>Toggle the refresh mode for the selected sub window.</td>
      </tr></informaltable>
  </refsection>
</refentry>
