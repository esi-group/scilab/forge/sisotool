<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolPreferencesData" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolPreferencesData</refname>

    <refpurpose>Description of the sisotool Preferences data
    structure.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Description</title>

    <para>The sisotool Preferences data structure stores the user's
    preferences like units, parameters for editors ans analysers.</para>

    <para>It is a Scilab <link linkend="scilab.help/struct">struct</link> with
    the following fields.</para>

    <variablelist>
      <varlistentry>
        <term>units</term>

        <listitem>
          <para>A struct, which contains the user selected units. It contains
          the following fields:</para>

          <variablelist>
            <varlistentry>
              <term>frequency</term>

              <listitem>
                <para>A character string. The supported values are:
                <literal>"Hz"</literal>, <literal>"rd/s"</literal>,
                <literal>"kHz"</literal>, <literal>"MHz" </literal>or
                <literal>"GHz"</literal>.</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>magnitude</term>

              <listitem>
                <para>A character string. The supported values are:
                <literal>"dB" </literal>or
                <literal>"absolute"</literal>.</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>phase</term>

              <listitem>
                <para>A character string. The supported values are:
                <literal>"degrees" </literal>or<literal> "rd"</literal></para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>time</term>

              <listitem>
                <para>A character string. The supported value is
                <literal>"s"</literal>:</para>
              </listitem>
            </varlistentry>
          </variablelist>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>timeResponsesPref</term>

        <listitem>
          <para>A struct which contains the time vector definition parameters
          for time responses. It has the following fields:</para>

          <variablelist>
            <varlistentry>
              <term>method</term>

              <listitem>
                <para>A scalar with possible values 0 or 1, 1 means automatic
                computation of the time vector, 0 means that final value and
                discretization step are given by the user.</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>final</term>

              <listitem>
                <para>A positive scalar the final time value (in seconds) for
                time responses.</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>step</term>

              <listitem>
                <para>A positive scalar the discretization step in seconds.
                This value is only used for continuous time systems.</para>
              </listitem>
            </varlistentry>
          </variablelist>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>freqResponsesPref</term>

        <listitem>
          <para>A struct which contains the frequency vector definition
          parameters for frequency responses. It has the following
          fields:</para>

          <variablelist>
            <varlistentry>
              <term>method</term>

              <listitem>
                <para>A scalar with possible values 1,2 or 3. 1 means
                automatic computation of the time vector, 2 means that
                frequency range is given by the user and 3 means that the
                frequency vector is given by the user.</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>range</term>

              <listitem>
                <para>The array <literal>[fmin, fmax]</literal>, <literal>fmin
                </literal>is the lowest frequency and<literal> fmax</literal>
                the highest one.</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>vector</term>

              <listitem>
                <para>The frequency vector.</para>
              </listitem>
            </varlistentry>
          </variablelist>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>RootLocusPref</term>

        <listitem>
          <para>A struct which contains the max gain value for root
          locus.</para>

          <varlistentry>
            <term>method</term>

            <listitem>
              <para>A scalar with possible values 0 or 1, 1 means automatic
              computation of the root locus max gain, 0 means that the maximum
              gain is given by the user.</para>
            </listitem>
          </varlistentry>

          <variablelist>
            <varlistentry>
              <term>maxGain</term>

              <listitem>
                <para>A positive scalar, the maximum gain for root
                locus.</para>
              </listitem>
            </varlistentry>
          </variablelist>
        </listitem>
      </varlistentry>
    </variablelist>

    <para>The structure is created by <link
    linkend="sisotoolNewSession">sisotoolNewSession</link> and updated by
    <link linkend="sisotoolPrefGuiOk">sisotoolPrefGuiOk</link>.</para>
  </refsection>
</refentry>
