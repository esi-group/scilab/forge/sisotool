<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolSetCGUI" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolSetCGUI</refname>

    <refpurpose>updates the GUI of the textual compensator editor with current
    values of the compensator elements.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>sisotoolSetCGUI(gui) </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>gui</term>

        <listitem>
          <para>The handle on the textual compensator editor GUI.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Updates the GUI of the textual compensator editor with current
    values with current values of the compensator elements (poles, zeros,
    leads, lags, notches).</para>
  </refsection>

  <refsection>
    <title>Called functions</title>

    <informaltable border="1">
          <tr>
            <td>
              <link linkend="sisotoolGetSession">
                <function>sisotoolGetSession</function>
              </link>
            </td>

            <td>retrieves the <link linkend="sisotoolSessionData">session data
            structure</link> from the editors window user_data field.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolC2tex">
                <function>sisotoolC2tex</function>
              </link>
            </td>

            <td>converts the <link linkend="sisotoolCompensatorData">compensator
            data structure</link> into a rational transfer function
            representation in LateX.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolFormatPZ">
                <function>sisotoolFormatPZ</function>
              </link>
            </td>

            <td>converts an array of poles or zeros into a character
            string.</td>
          </tr>

          <tr>
            <td>
              <link linkend="freqconv">
                <function>freqconv</function>
              </link>
            </td>

            <td>is used to convert frequency units.</td>
          </tr>

          <tr>
            <td>
              <link linkend="LeadLag2Properties">
                <function>LeadLag2Properties</function>
              </link>
            </td>

            <td>computes the frequency location and phase of the maximum
            phase of a lead or lag given by its pole and zero.</td>
          </tr>

          <tr>
            <td>
              <link linkend="phaseconv">
                <function>phaseconv</function>
              </link>
            </td>

            <td>is used to convert phase units.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolNotchWidth">
                <function>sisotoolNotchWidth</function>
              </link>
            </td>

            <td>computes the width of a notch given by its pole and zero
            damping factors.</td>
          </tr>

          <tr>
            <td>
              <link linkend="magconv">
                <function>magconv</function>
              </link>
            </td>

            <td>is used to convert magnitude units.</td>
          </tr>
    </informaltable>
  </refsection>
</refentry>
