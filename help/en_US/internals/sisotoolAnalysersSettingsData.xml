<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolAnalysersSettingsData" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolAnalysersSettingsData</refname>

    <refpurpose>Description of the AnalysersSettings data
    structure.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Description</title>

    <para>The AnalysersSetting data structure contains all information
    associated with the opened Analysers sub windows.</para>

    <para>It is a Scilab <link linkend="scilab.help/struct">struct</link> with
    the following fields.</para>

    <variablelist>
      <varlistentry>
        <term>subWins</term>

        <listitem>
          <para>A struct array, subWins(i) contains the data relative to the
          ith sub window in the following fields:</para>

          <informaltable border="1">
            <tr>
              <td>type</td>

              <td>The sub window type (it is also the tag property of the
              associated axes entity) the supported values are: "step",
              "impulse", "gainplot", "phaseplot" and "nyquist".</td>
            </tr>

            <tr>
              <td>transfer_names</td>

              <td>A character string array, the name of the transfer function
              represented in the sub window.</td>
            </tr>

            <tr>
              <td>expr</td>

              <td>A character string array, the symbolic expression of the
              transfer function represented in the sub window.</td>
            </tr>

            <tr>
              <td>transfer selection</td>

              <td>The index of the transfer function in the transfer function
              list</td>
            </tr>

            <tr>
              <td>characteristics_settings</td>

              <td>A boolean array, the true value indicates defined
              characteristics.</td>
            </tr>

            <tr>
              <td>reqProps</td>

              <td>A struct array, it defines the requirements available for
              the sub window <link
              linkend="sisotoolRequirementTypes">sisotoolRequirementTypes</link>.</td>
            </tr>
          </informaltable>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>handles</term>

        <listitem>
          <para>A list, handles(i) contains the handle in the ith sub window's
          axes entity. For bode editors handles(i) is an array with two
          elements: gain plot handle, phase plot handle.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>gridded</term>

        <listitem>
          <para>A boolean array, if gridded(i) is true the ith sub window's
          grid is drawn. This field is set by the <link
          linkend="sisotoolToggleGrid">sisotoolToggleGrid</link>
          function.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>refreshed</term>

        <listitem>
          <para>A boolean array, if refreshed(i) is true the ith sub window is
          updated during move operations. This field is set by the <link
          linkend="sisotoolToggleRefresh">sisotoolToggleRefresh</link>
          function.</para>
        </listitem>
      </varlistentry>
    </variablelist>

    <para>This structure is created by the <link
    linkend="sisotoolNewSession">sisotoolNewSession</link> function, updated
    by <link linkend="sisotoolDefaultSession">sisotoolDefaultSession</link>,
    <link
    linkend="sisotoolApplyAnalysers">sisotoolApplyAnalysers</link>....</para>
  </refsection>
</refentry>
