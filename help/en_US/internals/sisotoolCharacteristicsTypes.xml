<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolCharacteristicsTypes" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolCharacteristicsTypes</refname>

    <refpurpose>returns the available characteristics for the specified
    analyser sub window.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>[keys,names] = sisotoolCharacteristicsTypes(analyserType) </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>analyserType</term>

        <listitem>
          <para>A character string: the specified analyser sub window type
          (<literal>step</literal>, <literal>impulse</literal>,
          <literal>gainplot</literal>, <literal>phaseplot</literal>,
          <literal>nyquist</literal>, <literal>nichols</literal>,
          <literal>pole/zero</literal>).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>keys</term>

        <listitem>
          <para>A character string array, the short names f the available
          characteristics for the specified analyser.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>names</term>

        <listitem>
          <para>A character string array, the user friendly names of the
          available characteristics for the specified analyser.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Returns the available characteristics for the specified analyser sub
    window.</para>

    <itemizedlist>
      <listitem>
        <para>For step responses: Peak response, Settling time, Rise
        time.</para>
      </listitem>

      <listitem>
        <para>For impulse response: Peak response, Settling time.</para>
      </listitem>

      <listitem>
        <para>For gain plot, phase plot, Nyquist and Nichols: Peak response,
        Min stability margin, All stability margins.</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
