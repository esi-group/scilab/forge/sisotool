<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolMoveRootLocus" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolMoveRootLocus</refname>

    <refpurpose>handles selection and user driven movement of markers and
    requirements in the root locus sub window.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>sisotoolMoveRootLocus(mainH,editorData,entityType,i,k,coord,loc) </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>mainH</term>

        <listitem>
          <para>The handle of the Editors window.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>editorData</term>

        <listitem>
          <para>The root locus sub window <link linkend="sisotoolEditorData">editor
          data structure</link>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>entityType, i , k, coord, loc</term>

        <listitem>
          <para>The arguments returned by <link
          linkend="sisotoolFindSelected">sisotoolFindSelected</link>.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Handles user driven movement of the selected marker or requirement
    in the root locus sub window.</para>
  </refsection>

  <refsection>
    <title>Called functions</title>

    <informaltable border="1">
          <tr>
            <td>
              <link linkend="sisotoolGetSession">
                <function>sisotoolGetSession</function>
              </link>
            </td>

            <td>retrieves the <link linkend="sisotoolSessionData">session data
            structure</link> from the editors window user_data field.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolRequirementArea">
                <function>sisotoolRequirementArea</function>
              </link>
            </td>

            <td>returns the data needed to draw the limit of the valid area
            for the specified requirement.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolAddHistory">
                <function>sisotoolAddHistory</function>
              </link>
            </td>

            <td>adds a new change in the changes history.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolSetSession">
                <function>sisotoolSetSession</function>
              </link>
            </td>

            <td>stores the <link linkend="sisotoolSessionData">session data
            structure</link> in the editors window user_data field.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolGetEditorData">
                <function>sisotoolGetEditorData</function>
              </link>
            </td>

            <td>retrieves the <link linkend="sisotoolEditorData">editor data
            structure</link> out of the specified editor axes
            user_data.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolGetCElement">
                <function>sisotoolGetCElement</function>
              </link>
            </td>

            <td>returns the data and the optimization settings for a
            specified compensator element (pole, zero, ...).</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolSetSys">
                <function>sisotoolSetSys</function>
              </link>
            </td>

            <td>stores the <link linkend="sisotoolDesignsData">Designs data
            structure</link> in the Editors figure user_data property.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolUpdateResponses">
                <function>sisotoolUpdateResponses</function>
              </link>
            </td>

            <td>updates the Editors and Analysers sub windows during a
            marker move.</td>
          </tr>

          <tr>
            <td>
              <link linkend="wnzeta2complex">
                <function>wnzeta2complex</function>
              </link>
            </td>

            <td>this function converts a (natural pulsation, damping
            factor) representation into a (real part - imaginary part)
            ones.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolUpdateCElement">
                <function>sisotoolUpdateCElement</function>
              </link>
            </td>

            <td>updates compensator elements values in the textual
            compensator editor GUI.</td>
          </tr>
    </informaltable>
  </refsection>
</refentry>
