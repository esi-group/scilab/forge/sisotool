<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolRedrawEditors" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolRedrawEditors</refname>
    <refpurpose>redraws or updates the Editors sub windows.</refpurpose>
  </refnamediv>

    <refsection>
      <title>Synopsis</title>

      <synopsis>sisotoolRedrawEditors(mainH,update [,sel]) </synopsis>
    </refsection>

    <refsection>
      <title>Arguments</title>

      <variablelist>
        <varlistentry>
          <term>mainH</term>

          <listitem>
            <para>The handle of the Editors window.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>update</term>

          <listitem>
            <para>Boolean, if true the editors sub-windows are updated to
            follow move operations, else a complete redraw is made.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>sel</term>

          <listitem>
            <para>optional,indexes of the selected sub-windows. If omitted all
            sub-windows are taken into account.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsection>
  <refsection>
    <title>Description</title>
      <para>Redraws or updates the Editors sub windows.</para>
  </refsection>

    <refsection>
      <title>Called functions</title>

      <informaltable border="1">
            <tr>
              <td>
                <link linkend="sisotoolGetSession">
                  <function>sisotoolGetSession</function>
                </link>
              </td>

              <td>retrieves the <link linkend="sisotoolSessionData">session data
              structure</link> from the editors window user_data
              field.</td>
            </tr>

            <tr>
              <td>
                <link linkend="sisotoolSys2OL">
                  <function>sisotoolSys2OL</function>
                </link>
              </td>

              <td>computes the specified open loop system.</td>
            </tr>

            <tr>
              <td>
                <link linkend="sisotoolRlocusEditor">
                  <function>sisotoolRlocusEditor</function>
                </link>
              </td>

              <td>recreates or updates the root locus editor sub
              window.</td>
            </tr>

            <tr>
              <td>
                <link linkend="sisotoolOLBodeEditor">
                  <function>sisotoolOLBodeEditor</function>
                </link>
              </td>

              <td>creates or updates the open loop Bode editor sub
              window.</td>
            </tr>

            <tr>
              <td>
                <link linkend="sisotoolNicholsEditor">
                  <function>sisotoolNicholsEditor</function>
                </link>
              </td>

              <td>creates or updates the Nichols editor sub window.</td>
            </tr>

            <tr>
              <td>
                <link linkend="sisotoolClosedLoop">
                  <function>sisotoolClosedLoop</function>
                </link>
              </td>

              <td>returns the closed loop transfer function between the
              specified input and output.</td>
            </tr>

            <tr>
              <td>
                <link linkend="sisotoolCLBodeEditor">
                  <function>sisotoolCLBodeEditor</function>
                </link>
              </td>

              <td>creates or updates the closed loop Bode editor sub
              window.</td>
            </tr>
      </informaltable>
    </refsection>
</refentry>
