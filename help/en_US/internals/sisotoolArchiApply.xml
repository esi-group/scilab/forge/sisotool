<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolArchiApply" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolArchiApply</refname>

    <refpurpose>affects the architectural changes on editors and open
    GUIs.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>sisotoolArchiApply() </synopsis>

    <refsection>
      <title>Arguments</title>

      <variablelist>
        <varlistentry>
          <term>gcbo</term>

          <listitem>
            <para>The handle on the calling uicontrol (see <link
            linkend="scilab.help/gcbo">gcbo</link>), defined in the calling scope.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsection>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Affects the architectural changes on editors and open GUIs.</para>
  </refsection>

  <refsection>
    <title>Called functions</title>

    <informaltable border="1">
          <tr>
            <td>
              <link linkend="sisotoolGetSession">
                <function>sisotoolGetSession</function>
              </link>
            </td>

            <td>retrieves the <link linkend="sisotoolSessionData">session data
            structure</link> from the editors window user_data field.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolSaveSession">
                <function>sisotoolSaveSession</function>
              </link>
            </td>

            <td>saves the <link linkend="sisotoolSessionData">session data
            structure</link> into a file.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolGetArchiSpecs">
                <function>sisotoolGetArchiSpecs</function>
              </link>
            </td>

            <td>returns the characteristics of the selected
            architecture.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolNewController">
                <function>sisotoolNewController</function>
              </link>
            </td>

            <td>creates the <link linkend="sisotoolCompensatorData">compensator
            data structure</link>.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolSetSession">
                <function>sisotoolSetSession</function>
              </link>
            </td>

            <td>stores the <link linkend="sisotoolSessionData">session data
            structure</link> in the editors window user_data field.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolAnalysersGui">
                <function>sisotoolAnalysersGui</function>
              </link>
            </td>

            <td>builds the GUI which allows the selection of the analyser
            sub windows and the transfer functions.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolCreateAnalysers">
                <function>sisotoolCreateAnalysers</function>
              </link>
            </td>

            <td>creates the Analyser sub windows and the associated graphic
            entities according to the user choices.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolCreateCGUI">
                <function>sisotoolCreateCGUI</function>
              </link>
            </td>

            <td>creates the graphical user interface of the textual
            compensator editor.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolOptimDefaults">
                <function>sisotoolOptimDefaults</function>
              </link>
            </td>

            <td>creates and initialize the <link
            linkend="optim_settings_data">optimization settings data
            structure</link>.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolRedraw">
                <function>sisotoolRedraw</function>
              </link>
            </td>

            <td>updates the all the opened GUIs.</td>
          </tr>
    </informaltable>
  </refsection>
</refentry>
