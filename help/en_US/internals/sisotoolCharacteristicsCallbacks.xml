<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolCharacteristicsCallbacks" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolCharacteristicsCallbacks</refname>

    <refpurpose>Callback of the <link
    linkend="sisotoolAnalysersCMenu">sisotoolAnalysersCMenu</link>
    used to toggle the selected characteristic display on or off.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>sisotoolCharacteristicsCallback(subWinIndex,win,sel,op)</synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>subWinIndex</term>
        <listitem>
          <para>The index of the selected sub window.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>win</term>
        <listitem>
          <para>The figure id of the figure</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>sel</term>
        <listitem>
          <para>An integer, the index of the selected characteristic
          in the available characteristics list (see <link
          linkend="sisotoolCharacteristicsTypes">sisotoolCharacteristicsTypes</link>)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>op</term>
        <listitem>
          <para>A character string with possible values "delete"
          (removes the display of selected characteristic) or "add"
          (show it).</para>
        </listitem>
      </varlistentry>

    </variablelist>
  </refsection>
  <refsection>
    <title>Called functions</title>
    <informaltable border="1">
      <tr>
        <td><link linkend="sisotoolGetSession">sisotoolGetSession</link></td>
        <td>retrieves the <link linkend="sisotoolSessionData">session data
        structure</link> from the editors window user_data field.</td>
      </tr>
      <tr>
        <td><link linkend="sisotoolGetEditorData">sisotoolGetEditorData"</link></td>
        <td>retrieves the <link linkend="sisotoolEditorData">editor data
        structure</link> out of the specified editor axes user_data.</td>
      </tr>
      <tr>
        <td><link linkend="sisotoolCharacteristicsTypes">sisotoolCharacteristicsTypes</link></td>
        <td>returns the available characteristics for the specified
        analyser sub window.</td>
      </tr>
      <tr>
        <td><link linkend="sisotoolManageCharacteristics"> sisotoolManageCharacteristics</link></td>
        <td>creates, updates or delete the characteristics graphic
        entities of the specified editor.</td>
      </tr>
      <tr>
        <td><link linkend="sisotoolSetSession">sisotoolSetSession</link></td>
        <td>stores the <link linkend="sisotoolSessionData">session data structure</link> in the editors window user_data field</td>
      </tr>
    </informaltable>
  </refsection>
</refentry>
