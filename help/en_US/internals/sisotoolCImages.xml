<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolCImages" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolCImages</refname>

    <refpurpose>returns the location of the markers associated with the
    compensator elements in the editors sub windows</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>I = sisotoolCImages(Compensator,sys) </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>Compensator</term>

        <listitem>
          <para>The <link linkend="sisotoolCompensatorData">compensator data
          structure</link>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>sys</term>

        <listitem>
          <para>The representation of the considered transfer.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>I</term>

        <listitem>
          <para>A data structure, with same fields as the compensator data
          structure. Each field contains a 3 column array (freq,
          phase,magnitude). The frequencies are the natural frequencies of the
          corresponding components, phase and magnitude are the phase and
          magnitude of the considered transfer for the frequencies.</para>

          <para>Each of these column is given in the user selected
          units.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Returns the location of the markers associated with the compensator
    elements in the editors sub windows.</para>
  </refsection>

  <refsection>
    <title>Called functions</title>

    <informaltable border="1">
          <tr>
            <td>
              <link linkend="freqconv">
                <function>freqconv</function>
              </link>
            </td>

            <td>is used to convert frequency units</td>
          </tr>

          <tr>
            <td>
              <link linkend="phaseconv">
                <function>phaseconv</function>
              </link>
            </td>

            <td>is used to convert phase units</td>
          </tr>

          <tr>
            <td>
              <link linkend="magconv">
                <function>magconv</function>
              </link>
            </td>

            <td>is used to convert magnitude units</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolNotchWidthLoc">
                <function>sisotoolNotchWidthLoc</function>
              </link>
            </td>

            <td>computes the frequency position of the notch width
            markers:</td>
          </tr>
    </informaltable>
  </refsection>
</refentry>
