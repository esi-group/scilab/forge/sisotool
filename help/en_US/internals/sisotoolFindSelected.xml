<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolFindSelected" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolFindSelected</refname>

    <refpurpose>finds if there is a marker near the given point in the current
    axes.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>[entityType,i,k,coord,loc] = sisotoolFindSelected(editorData,pt) </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>editorData</term>

        <listitem>
          <para>The selected sub window <link
          linkend="sisotoolEditorData">editor data structure</link>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>pt</term>

        <listitem>
          <para>An array [x, y], The coordinates (in pixels) of the selected
          point.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>entityType</term>

        <listitem>
          <para>The type of the compensator component associated with the
          selected entity near point pt or [] if no marker is near enough of
          pt (<literal>"gain"</literal>,, <literal>"poles"</literal>,
          <literal>"zeros"</literal>, <literal>"leads"</literal>,
          <literal>"lags"</literal>, <literal>"notches"</literal>,
          <literal>"notches_width"</literal>, <literal>"loci"</literal>,
          <literal>"requirements"</literal>).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>i</term>

        <listitem>
          <para>If the selected component is a pole or a zero i is set to
          one,</para>

          <para>If the selected component is a lead, a lag or a notch,
          <literal>i </literal>is set to one if the selected marker
          corresponds to the zero and to two for the pole.</para>

          <para>If the selected marker is a notch width marker <literal>i
          </literal>is set to one if the selected marker corresponds to the
          low frequency bound and to two for the high frequency bound.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>k</term>

        <listitem>
          <para>The index of the compensator component or
          <literal>[]</literal>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>coord</term>

        <listitem>
          <para>The coordinates (in graph coordinates) of the marker.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>loc</term>

        <listitem>
          <para>This return argument is set if the selected entity is a locus
          (typ=="loci" or typ=="requirements").</para>

          <para>For <literal>typ=="loci"</literal>, <literal>loc</literal> is
          the array <literal>[ind c]</literal> where <literal>ind </literal>is
          the index of the curve point which precedes the orthogonal
          projection of pt on the curve and <literal>c</literal> the distance
          ratio of the projection on the segment <literal>[ind
          ind+1]</literal>.</para>

          <para>For<literal> typ=="requirements"</literal>,<literal>
          loc</literal> is the array <literal>[ind, c] </literal>or
          <literal>[ind, c, j]</literal>, the later case corresponds to the
          tunnel requirements composed of 2 curves. In this case <literal>j
          </literal>is the curve index.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Finds if there is a marker near the given point in the current
    axes.</para>
  </refsection>
</refentry>
