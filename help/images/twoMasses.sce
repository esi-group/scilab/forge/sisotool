function spring(x0,y0,l)
  x=x0+[0 2:13 15]*l/15;
  y=y0+[0 0 -1 1 -1 1 -1 1 -1 1 -1 1  0 0]/8;
  xpoly(x,y);e=gce(),e.thickness=2;e.foreground=1;
endfunction
function mass(x0,y0,str)
  h=1.5;
  w=4;
  xfrect(x0,y0+h,w,h);M=gce();
  M.background=color("lightgray")
  xstringb(x0,y0,"$\bf "+str+"$",w,h);e=gce();
  e.font_size=6;
endfunction
function damper(x0,y0,l)
  r=l/2.5
  xpoly(x0+[0 r],y0+[0 0])
  xpoly(x0+[r r],y0+[-1/8 1/8])
  
  xpoly(x0+[l-r*1.2 l],[y0 y0])
  xpoly(x0+[r*0.7 l-r*1.2 l-r*1.2 r*0.7],y0+[-1/6 -1/6 1/6 1/6]) 
endfunction
f=gcf();f.axes_size=[520,230];
ax=gca();
ax.margins=ones(4,1)*0.03;
ax.data_bounds=[0 0;20,5];
xpoly([0,19],1+[0 0]);
e=gce();
e.line_style=2;
e.polyline_style=4;
e.arrow_size_factor=3;
xpoly([0,0],[1 5]);
e=gce();
e.line_style=2;
e.polyline_style=4;
e.arrow_size_factor=3;

y1=5;
h=1.5;
w=4;
mass(y1,1,"M")
xpoly([y1 y1],[1 1-1/4])
xstring(y1,1-1/2,"$\bf y_1$");e=gce();
e.font_size=3;

xpoly([3,5],1+[1 1]*h/2);
e=gce();
e.polyline_style=4;
e.arrow_size_factor=3;
xstring(3,1+h/2,"$\bf u$");e=gce();
e.font_size=3;


y2=13;
mass(y2,1,"m")
xpoly([y2 y2],[1 1-1/4])
xstring(y2,1-1/2,"$\bf y_2$");e=gce();
e.font_size=3;

l=y2-y1-4;
spring(y1+4,1+h*2/3,l)
xstring(y1+4+l/2,1+h*2/3+1/8,"$\bf k$");e=gce();
e.font_size=3;

damper(y1+4,1+h/3,y2-y1-4)
xstring(y1+4+l/2,1+h/3-1/2,"$\bf b$");e=gce();
e.font_size=3;
