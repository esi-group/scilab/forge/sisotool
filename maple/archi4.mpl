eqns:={e=r-n-H*y,u=C1*e-C2*(n+H*y)+du,y=G*u+dy};

S:=eliminate(eqns,{e,y}):
U:=solve(S[2][1],u);

S:=eliminate(eqns,{e,u}):
Y:=solve(S[2][1],y);

with(CodeGeneration):
Matlab(U);
Matlab(Y);

#Open loop 1 (output of C1) with C2
eqns := {u = -C2*H*y+uo, y = G*u, yo = C1*y}:
S:=eliminate(eqns,{y,u}):
Yo:=solve(S[2][1],yo);

#Open loop 2 (output of C2) with C1
eqns := {u = -C1*H*y-uo, y = G*u, yo = C2*H*y};
S:=eliminate(eqns,{y,u});
Yo:=solve(S[2][1],yo);
