dp:=1/4:
Alpha:=depth^dp:
beta:=sqrt(zetap^2*(Alpha^2-depth^2)/(1-Alpha^2)):
width:=1 + 2*beta^2 + 2*beta*sqrt(1+beta^2);


with(CodeGeneration):
Matlab(width,resultname="width");
S:=solve(width=w,zetap): s:=S[2];
Matlab(s,resultname="zetap");

functionwidth=foow(zetap,depth)
width=1+2*zetap^2*(sqrt(depth)-depth^2)/(-sqrt(depth)+1)+2*sqrt(zetap^2*(sqrt(depth)-depth^2)/(-sqrt(depth)+1))*sqrt(zetap^2*(sqrt(depth)-depth^2)/(-sqrt(depth)+1)+1);
endfunction

functionzetap=fooz(width,depth)
zetap=sqrt(-width*((2*depth^(5/2))+(depth^(3/2))-(depth^3)-(depth^2)+sqrt(depth)-(2*depth))*((depth^(3/2))+3*sqrt(depth)-(3*depth)-1))/width/((2*depth^(5/2))+(depth^(3/2))-(depth^3)-(depth^2)+sqrt(depth)-(2*depth))*(width-1)/2;
endfunction

