#The OL transfer function H(s) wrotes K(s)/((s-r)*(s-r')
#where r is the moved pole. 

#In bode plot, The image of a root r is located at the abscissae w corresponding to its natural frequency

#let r_old the initial value of the pole and  r_new the value of the pole to be determined 

# |H_old(s)|/|H_new(s)|=|(s-r_new)*(s-r_new')|/|(s-r_old)*(s-r_old')|

#let (w,y_new) is the coordinates of the pointer location 
#let y_old=|H_old(%i*w)| y_old is obtained as the ordinates of the gain plot at w
#    y_new= |H_new(%i*w)|y_old is the ordinates of the pointer location

#y_new/y_old=|(%i*w-r_old)*(%i*w-conj(r_old))|/|(%i*w-r_new)*(%i*w-conj(r_new))|

#the complex roots can be parametrized by  w*(zeta-%i*sqrt(1-zeta^2))  
#   with w the natural pulsation and zeta the damping factor.


assume(w,'real',w>0):# selected frequency and natural frequency of the new pole
assume(wo,'real',wo>0):#  natural frequency (modulus) of the old pole
assume('zo', 'real'): assume(zo > 0, zo < 1):#damping factor of the old pole
assume('zn', 'real'): assume(zn > 0, zn < 1):#damping factor of the new pole (to be determined)
assume(yo,'real',yo>0): # old transfert function gain at w
assume(yn,'real',yn>0): # new transfert function gain at w


ro:=wo*(zo-I*sqrt(-zo^2+1));#old pole
rn:=w*(zn-I*sqrt(-zn^2+1));#new pole
q:=abs(simplify(expand((I*w-ro)*(I*w-conjugate(ro)))))/abs(simplify(expand((I*w-rn)*(I*w-conjugate(rn)))));#y_new/y_old
z:=solve(q=yn/yo,zn)

with(CodeGeneration):
Matlab(s);


ro:=ao+I*sqrt(ao^2-wo^2)
rn:=an+I*sqrt(an^2-wn^2)
q:=abs(simplify(expand((I*wn-ro)*(I*wn-conj(ro)))))/abs(simplify(expand((I*wn-rn)*(I*wn-conj(rn)))));
