assume(w > 0); assume(w0 > 0); assume(Zz, 'real'); assume(Zp, 'real');assume(r>0);
H := (1+2*Zz*s/w0+s^2/w0^2)/(1+2*Zp*s/w0+s^2/w0^2);
Hw0:=abs(eval(H,s=I*w0));
w:=sqrt(r)*w0;
Hw:=abs(eval(H,s=I*w));
R := solve(Hw^2 = Hw0^(2*df), r);
R:=eval(R, Hw0^(2*df) = t);
r1=factor(R[1]);
r2=factor(R[2]);
