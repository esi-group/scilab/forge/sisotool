#-------------------Continuous time case ------------------------------------------------
# let H=N/(Dc*Dv) where Dc contains the constant poles and Dv the moving pair
#given 2 points (w,Y0) and (w,Y1) in the gainplot space corresponding to 2 transfer functions
# H0=N/(Dc*Dv0) and H1=N/(Dc*Dv1) Y0=||H0(iw)||,  find Dv1 such as  Y1=||H1(iw)||


assume(w, 'real', 0 < w, z0, 'real', 0 < z0 and z0 < 1, z1, 'real', 0 < z1 and z1 < 1);
r0 := w*(z0+sqrt(-1)*sqrt(1-z0^2)); #old complex root given by its natural frequency (w) and damping factor (z0)
r1 := w*(z1+sqrt(-1)*sqrt(1-z1^2)); #new complex root, given by its natural frequency (w) and damping factor (z1 unknown)
Dv0 := (I*w-r0)*(I*w-conjugate(r0)); #old denominator varying part
Dv1 := (I*w-r1)*(I*w-conjugate(r1)); #new denominator varying part

NormDv0 := sqrt(Re(Dv0*conjugate(Dv0)));
NormDv1 := sqrt(Re(Dv1*conjugate(Dv1)));
#Y1/Y0=||H1(Iw)||/||H0(iw)||=NormDv0/NormDv1
solve(NormDv0/NormDv1=Y1/Y0,z1)

#-------------------ContinuousDiscrete time case ------------------------------------------------
#????
Dv0 := (exp(I*w)-r0)*(exp(I*w)-conjugate(r0)); #old denominator varying part
Dv1 := (exp(I*w-r1))*(exp(I*w)-conjugate(r1)); #new denominator varying part
NormDv0 := sqrt(Re(Dv0*conjugate(Dv0)));
NormDv1 := sqrt(Re(Dv1*conjugate(Dv1)));
solve(NormDv0/NormDv1=Y1/Y0,z1)
