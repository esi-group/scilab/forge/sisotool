importXcosDiagram(sisotoolPath()+"architectures/xcos/archi1.zcos");
archiSpecs=xcos2TF(scs_m,"C");
save(sisotoolPath()+"architectures/data/archi1.sod", "archiSpecs")

importXcosDiagram(sisotoolPath()+"architectures/xcos/archi2.zcos");
archiSpecs=xcos2TF(scs_m,"C");
save(sisotoolPath()+"architectures/data/archi2.sod", "archiSpecs")

importXcosDiagram(sisotoolPath()+"architectures/xcos/archi3.zcos");
archiSpecs=xcos2TF(scs_m,"C");
save(sisotoolPath()+"architectures/data/archi3.sod", "archiSpecs");

importXcosDiagram(sisotoolPath()+"architectures/xcos/archi4.zcos");
archiSpecs=xcos2TF(scs_m,["C1" "C2"]);
save(sisotoolPath()+"architectures/data/archi4.sod", "archiSpecs")

importXcosDiagram(sisotoolPath()+"architectures/xcos/archi5.zcos");
archiSpecs=xcos2TF(scs_m,"C");
save(sisotoolPath()+"architectures/data/archi5.sod", "archiSpecs");

importXcosDiagram(sisotoolPath()+"architectures/xcos/archi6.zcos");
archiSpecs=xcos2TF(scs_m,["C1" "C2"]);
save(sisotoolPath()+"architectures/data/archi5.sod", "archiSpecs");

