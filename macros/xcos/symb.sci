function r=symbol(e)
  select type(e)
  case 1 then
    if size(e,"*")<>1|~isreal(e) then
      error(msprintf(_("%s: Wrong type for input argument #%d: a real scalar expected.\n"),"symbolic",1))
    end
    r=tlist(["expr","v","s"],list(string(abs(e))),sign(e))
  case 10 then
    if size(e,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: a character string expected.\n"),"symbolic",1))
    end
    r=tlist(["expr","v","s"],list(e),1)
  else
    error(msprintf(_("%s: Wrong type for input argument #%d: A real or a string expected.\n"),"symbolic",1))
  end
endfunction
function r=sym_expr(varargin)
  nv=size(varargin)
  r=tlist(["expr","v","s"],varargin,ones(1,nv))
endfunction
function e=%expr_s(e)
  e.s=-e.s
endfunction
function r=sym_fact(varargin)
  r=tlist(["term","v","s"],varargin,1)
endfunction
function e=%term_s(e)
  e.s=-e.s
endfunction
function t=%expr_string(e)
  v=e.v;
  s=e.s;
  t=""
  for k=1:size(v)
    if abs(s(k))==1 then
      f=""
    else
      f=string(abs(s(k)))+"*"
    end
    vk=string(v(k))
    if part(vk,1)=="-" then
      vk=part(vk,2:length(vk))
      s(k)=-s(k)
    end
    if s(k)>0 then
      if k==1 then
        t=t+f+vk
      else
        t=t+"+"+f+vk
      end
    else
      t=t+"-"+f+vk
   end
  end
endfunction

function t=%term_string(e)
  v=e.v;nv=size(v);
  s=e.s;
  t=[];
  for k=1:nv
    if typeof(v(k))=="fb" then
        t=[t,string(v(k))]
    elseif size(v(k).v)==1|typeof(v(k))=="term" then
      t=[t,string(v(k))];
    else
      t=[t,"("+string(v(k))+")"];
    end
  end
  t=strcat(t,'*');
  if abs(s)<>1 then
    t=string(abs(s))+'*'+t
  end
  if s<0 then  
    t="-"+t
  end
endfunction

function %term_p(e)
  mprintf("  %s\n",string(e))
endfunction
function %expr_p(e)
  mprintf("  %s\n",string(e))
endfunction

function r=%expr_a_expr(e1,e2)
  t1=e1.v;s1=e1.s;
  if t1==list(0) then r=e2;return;end
  t2=e2.v;s2=e2.s;
  if t2==list(0) then r=e1;return;end
  
  t=[];s=[];
  for i2=1:size(t2)
    t=t2(i2);
    eq=0;
    for i1=1:size(t1)
      if (type(t)==type(t1(i1)))&(t==t1(i1)) then eq=i1;break;end
    end
    if eq>0 then //terms are equal or opposite
      s1(eq)=s1(eq)+s2(i2);
      if s1(eq)==0 then
        s1(eq)=[];
        t1(eq)=[];
      end
      if s1==[] then
        t1=list(0);
        s1=1;
      end
    else
      t1($+1)=t2(i2);
      s1(1,$+1)=s2(i2);
    end
  end
  for i1=1:size(t1)
    if type(t1(i1))==1 then t1(i1)=t1(i1)*s1(i1);s1(i1)=1;end
  end
  r=tlist(["expr","v","s"],t1,s1) 
endfunction
function r=%expr_s_expr(e1,e2)
  e2.s=-e2.s
  r=e1+e2
endfunction
   
function r=%expr_m_expr(e1,e2)
//produit de 2 sommes
  s=1
  if size(e1.s,"*")==1 then 
    s=e1.s*s
    e1.s=1;
  end
  if size(e2.s,"*")==1 then
     s=e2.s*s
     e2.s=1;
  end
  r=tlist(["term","v","s"],list(e1,e2),s)
endfunction

function r=%term_a_term(e1,e2)
//somme de 2 produits
//recherche de facteurs egaux
  v1=e1.v;n1=size(v1)
  v2=e2.v;n2=size(v2)
  eq1=[];eq2=[]
  for k2=1:n2
    v=v2(k2);
    for k1=1:n1
      if v1(k1)==v then
        eq1=[k1,eq1];
        eq2=[k2,eq2];
        break
      end
    end
  end
  if eq1==[] then
    //transfer terms signs
    s=[e1.s e2.s]
    e1.s=1;e2.s=1;
    r=tlist(["expr","v","s"],list(e1,e2),s)
  else
    l=list();
   
    eq1=gsort(eq1,"g","d")
    
    for k=1:size(eq1,"*")
      l(0)=v1(eq1(k));
      v1(eq1(k))=null();
      v2(eq2(k))=null();
    end
    
    r1=tlist(["term","v","s"],l,1);

    if size(v2)==0 then
      if size(v1)==0 then
        r1.s=e1.s+e2.s
        r=r1
        return
      end
      e2=tlist(["expr","v","s"],list(1),e2.s);
    elseif size(v2)==1&typeof(v2(1))<>"fb" then
      s2=e2.s
      e2=v2(1);
      e2.s=e2.s*s2
    else
      e2.v=v2;
    end
    
    if size(v1)==0 then
      e1=tlist(["expr","v","s"],list(1),e1.s);
    elseif size(v1)==1&typeof(v1(1))<>"fb" then
      s1=e1.s
      e1=v1(1);
      e1.s=e1.s*s1
    else
      e1.v=v1;
    end
    for i=1:size(l)
      if typeof(l(i))=="fb" then
        // test du cas T1*fb+T2*fb si T1+T2 est un facteur de fb.K on peut
        // simplifier

        r=l(i);//the common feedback term
        e=e1+e2;//the factor of the feedback
        if typeof(r.K)=="expr"&r.K==e then
          r.K=1;
          r.G=r.G*e;
          l(i)=r;
          r=tlist(["term","v","s"],l,1)
          return
        else
          v2=r.K.v; 
          e=e1+e2;//the factor of the feedback
          for k2=1:size(v2)
            if e==v2(k2) then //e=T1+T2 est un facteur de r.K
              r.G=r.G*e;
              r.K.v(k2)=null();
              l(i)=r;
              r=tlist(["term","v","s"],l,1)
              return
            end
            if -e==v2(k2) then //e=T1+T2 est un facteur de r.K
            
              r.G=r.G*(-e);
              r.K.v(k2)=null();
              r=tlist(["term","v","s"],list(r),-1);
              l(i)=r;
              r=tlist(["term","v","s"],l,1)
              return
            end
          end
        end
      end
    end
    r=r1*(e1+e2);
  end
endfunction
function r=%term_s_term(e1,e2)
//difference de 2 produits
//recherche de facteurs egaux
  e2.s=-e2.s
  r=e1+e2
endfunction
function r=%term_m_term(t1,t2)
  //multiplication de 2 produits
    v=t1.v
    v2=t2.v
    s=t1.s*t2.s
    for i2=1:size(v2)
      v($+1)=v2(i2)
    end
   r=tlist(["term","v","s"],v,s) 
endfunction

function r=%term_m_expr(t,e)
//multiplication d'un produit par une somme
  if e.v==list(0) then r=e;return;end
  v=t.v
  v($+1)=e
  r=tlist(["term","v","s"],v,t.s)  
endfunction
function r=%expr_m_term(e,t)
//multiplication d'une somme (e) par un produit (t)
  if e.v==list(0) then r=e;return;end
  v=t.v
  v(0)=e
  r=tlist(["term","v","s"],v,t.s)
endfunction
function r=%term_a_expr(p,s)
//ajout d'une somme s a un produit p (p+s)
  s=tlist(["term","v","s"],list(s),1) 
  r=%term_a_term(p,s);
endfunction
function r=%expr_a_term(s,p)
//ajout d'une somme s a un produit p (p+s)
  s=tlist(["term","v","s"],list(s),1) 
  r=%term_a_term(p,s);
endfunction

function r=%term_s_expr(p,s)
//soustraction d'une somme s a un produit p (p-s)
  s=tlist(["term","v","s"],list(s),-1) 
  r=%term_a_term(p,s);
endfunction

function r=%expr_a_term1(s,p)
//ajout d'une somme s a un produit p (p+s)
  s=tlist(["term","v","s"],list(s),1) 
  r=%term_a_term(s,p);
endfunction

function r=%expr_a_term1(s,p)
//ajout d'une somme s a un produit p (p+s)
 
  s=tlist(["term","v","s"],list(s),1) 
  r=%term_a_term(s,p);
endfunction

function r=%expr_s_term(s,p)
//soustraction d'un produit p �� une somme s (s-p)
  s=tlist(["term","v","s"],list(s),1) 
  p.s=-p.s
  r=%term_a_term(s,p);
endfunction
function r=%s_m_expr(s,r)
  r=tlist(["term","v","s"],list(r),s)
endfunction
function r=%expr_m_s(r,s)
  r=tlist(["term","v","s"],list(r),s)
endfunction

function r=%s_m_term(s,r)
  r.s=s*r.s
endfunction
function r=%term_m_s(r,s)
  r.s=s*r.s
endfunction
function r=%s_a_expr(s,r)
  if s<>0 then
    r.v(0)=abs(s)
    r.s=[sign(s) r.s]
  end
endfunction
function r=%s_s_expr(s,r)
  if s<>0 then
    r.v(0)=abs(s)
    r.s=[sign(s) -r.s]
  end
endfunction
function r=%expr_a_s(r,s)
  if s<>0 then
    r.v($+1)=abs(s)
    r.s=[r.s sign(s)]
  end
endfunction
function r=%expr_s_s(r,s)
  if s<>0 then
    r.v($+1)=abs(s)
    r.s=[r.s -sign(s) ]
  end
endfunction

function r=%s_a_term(s,r)
  if s<>0 then 
    s1=r.s
    r.s=1
    r=tlist(["expr","v","s"],list(abs(s),r),[sign(s),s1])
  end
endfunction
function r=%s_s_term(s,r)
   r.s=-r.s
   r=s+r
endfunction

function r=%term_a_s(r,s)
  if s==0 then return;end
  r=sym_expr(r)+s
endfunction
function r=%term_s_s(r,s)
  if s==0 then r.s=-r.s;return;end
  r=sym_expr(r)-s
endfunction
function r=%expr_n_expr(e1,e2)
  r=or(e1.s<>e2.s) 
  if r then return;end
  for k=1:size(e1.v)
    r=(e1.v(k)<>e2.v(k))
    if r then return;end
  end
endfunction
function r=%expr_o_expr(e1,e2)
  r=and(size(e1.s,"*")==size(e2.s,"*")) 
  if ~r then return;end
  for k=1:size(e1.v)
    r=(e1.v(k)==e2.v(k))&(e1.s(k)==e2.s(k))
    if ~r then return;end
  end
endfunction
function r=%expr_o_s(e1,e2)
  r=%f
endfunction
function r=%expr_o_c(e1,e2)
  r=%f
endfunction
function r=%expr_n_c(e1,e2)
  r=%t
endfunction
function r=%expr_n_s(e1,e2)
  r=%t
endfunction

function r=%term_n_term(e1,e2)
  r=or(e1.s<>e2.s) 
  if r then return;end
  for k=1:size(e1.v)
    r=or(e1.v(k)<>e2.v(k))
     if r then return;end
  end
endfunction
function r=%term_o_term(e1,e2)
  r=or(e1.s==e2.s)&size(e1.v)==size(e2.v) 
  if ~r then return;end
  for k=1:size(e1.v)
    r=or(e1.v(k)==e2.v(k))
     if ~r then return;end
  end
endfunction

function r=%term_n_c(t,s)
  r=%t
endfunction  
function r=%term_o_c(t,s)
  r=%f
endfunction  

function r=%term_n_s(t,s)
  r=%t
endfunction  
function r=%term_o_s(t,s)
  r=%f
endfunction  


function r=%s_o_expr(t,s)
  r=%f
endfunction  

function r=%s_o_term(t,s)
  r=%f
endfunction

function r=%c_o_term(e1,e2)
  r=%f
endfunction
function r=%s_n_term(t,s)
  r=%t
endfunction




function r=sym_div(e1,e2)
  if %f&typeof(e2)=="term"&type(e2.v(1))==1&e2.v(1)==1&size(e2.v)==2& typeof(e2.v(2))=="term" then //potential feedback
    t=e2.v(2)
    if typeof(e1)=="expr" then e1=tlist(["term",v,s],list(e1),1);end
    //recherche de facteurs communs
  else
    r=tlist(["div","num","den"],e1,e2)
  end
endfunction

function t=%div_string(r)
  num=r.num
  den=r.den
  tn=string(num);
  td=string(den);
  if td=="1" then
    t=tn
  else
    if typeof(num)=="expr"&size(num.v)>1 then
      tn="("+tn+")"
    end
    
    if typeof(den)=="expr"&size(den.v)>1 then
      td="("+td+")"
    end
    t=tn+"/"+td
  end
endfunction

function F=convert2feedback(e1,e2)
//transforms H*G/(1+K*G) as H*feedback(G,K) where
//e2 contains K*G and e1 contains H*G  
  
  if typeof(e1)=="constant" then 
    F=tlist(["term","v","s"],list(tlist(["fb","G","K"],1,e2)),sign(e1))
    return
  end
  if typeof(e2)=="constant" then 
    F=e1*(1/(1+e2))
    return
  end
  if typeof(e2)=="expr"&size(e2.v)==1 then
    if typeof(e2.v(1))=="constant" then 
      F=e1*(1/(1+e2.v(1)));//no feedback
      return;
    end 
    e2=tlist(["term","v","s"],list(e2),1)
  end
  
  if typeof(e1)=="expr"&size(e1.v)==1 then 
    if typeof(e1.v(1))=="constant" then 
       F=e1*tlist(["fb","G","K"],1,e2)
       return
    end 
    
    e1=tlist(["term","v","s"],list(e1),1)
  end
  if typeof(e1)<>"term" then
    e1=tlist(["term","v","s"],list(e1),1)
  end
  if typeof(e2)<>"term" then
    F=tlist(["term","v","s"],list(e1,tlist(["fb","G","K"],1,e2)),1)
    return
  else
    v1=e1.v
    v2=e2.v
    //find common factors
    eq2=[];eq1=[];
    for i1=1:size(v1)
      t1=v1(i1);
      for i2=1:size(v2)
        if and(i2<>eq2) then
          t2=v2(i2);
          if t1==t2 then //equal factor found
            eq2=[i2 eq2];
            eq1=[i1 eq1];
            break
          end
        end
      end
    end
    v=list();
    eq2=gsort(eq2,"g","d");
    for i=1:size(eq1,"*")
      v2(eq2(i))=null();
      v($+1)=v1(eq1(i));
      v1(eq1(i))=null();
    end

    //form H
    if size(v1)==0 then 
      H=e1.s
    elseif size(v1)==1 then
      H=e1.s*v1(1)
    else
      H=e1;H.v=v1
    end
    //form G
    if size(v)==0 then //no common factor
      G=1;
    elseif size(v)==1
      G=v(1);
    else
      G=e1;G.v=v;G.s=1;
    end
    //form K
    if size(v2)==0 then 
      K=e2.s;
    elseif size(v2)==1 then
      K=v2(1)
      K.s=K.s*e2.s
    else
      K=tlist(["term","v","s"],v2,e2.s)
    end
    F=H*tlist(["term","v","s"],list(tlist(["fb","G","K"],G,K)),1)
  
  end
endfunction
function t=%fb_string(e)
  t="feedback("+string(e.G)+","+string(e.K)+")"
endfunction
function %fb_p(e)
  mprintf("  %s\n",string(e))
endfunction
function r=%fb_a_fb(e1,e2)
  
    e1=tlist(["term","v","s"],list(e1),1)
    e2=tlist(["term","v","s"],list(e2),1)
    r=e1+e2
endfunction
function r=%term_a_fb(e1,e2)
  r=%term_a_term(e1,e2)
endfunction
function r=%s_a_fb(e1,e2)
  r=%s_a_term(e1,e2)
endfunction

function r=%div_a_div(e1,e2)
  r=%term_a_term(e1,e2)
endfunction
function r=%term_a_div(e1,e2)
  r=%term_a_term(e1,e2)
endfunction
function r=%s_a_div(e1,e2)
  r=%s_a_term(e1,e2)
endfunction
function r=%div_a_fb(e1,e2)
  r=%term_a_term(e1,e2)
endfunction
