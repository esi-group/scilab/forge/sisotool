function T = tree2tf(Arcs,EdgeNames,fromNodes,toNodes,Debug)
//Returns T as a sum of the transfer function associated with fromNodes to
//toNodes paths
//
//all blocks are assumed to be SISO  and consequently the product of
//transfer functions is assumed to be commutative.
// data  : a cblock data structure mlist("cblock",Arcs,)
  T=[];
  if type(Arcs)<>1|~isreal(Arcs) then
     error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),"tree2tf",1))
  end
 if type(EdgeNames)<>10 then
     error(msprintf(_("%s: Wrong type for argument %d: Character string array expected.\n"),"tree2tf",2))
  end
  if type(fromNodes)<>1|~isreal(fromNodes)|or(int(fromNodes)<>fromNodes) then
     error(msprintf(_("%s: Wrong type for first input argument: Integer matrix expected.\n"),"tree2tf",3))
  end
 if type(toNodes)<>1|~isreal(toNodes)|or(int(toNodes)<>toNodes) then
     error(msprintf(_("%s: Wrong type for first input argument: Integer matrix expected.\n"),"tree2tf",4))
  end
  nArcs=size(Arcs,1);
  if size(EdgeNames,"*")<>nArcs then
    error(msprintf(_("%s: Incompatible input arguments #%d and #%d: Same column dimensions ""expected.\n"),"tree2tf",1,2))
  end
  if fromNodes==[]|toNodes==[] then return;end

  // Determine  the  loops --------------------------------
  loopsEdgeIndices=[]; // Edge indices array, each row is relative to a loop
  loopsNodeIndices=[]; // Node  indices array, each row is relative to a loop
  for index=1:nArcs; 
    // Get the edge and node indices describing all the loops going from the
    // node numbered Arcs(index,1) to itself
    [edgeIndices,nodeIndices]=findPaths(index,index,Arcs);
    // Store in the indices array
    loopsEdgeIndices=[loopsEdgeIndices;edgeIndices];
    loopsNodeIndices=[loopsNodeIndices;nodeIndices];
  end
  // Remove duplicates loops, padding and transform arrays into lists
  [loopsEdgeIndices,loopsNodeIndices]=pathArray2List(loopsEdgeIndices,loopsNodeIndices)
  nLoops=size(loopsEdgeIndices);
  
  allLoopsEdgeIndices=list(loopsEdgeIndices);
  allLoopsNodeIndices=list(loopsNodeIndices);
  //Add n-uples of non touching loops
  n=1;
  while %t
    n=n+1;//n-uple order
    loopsNodeIndices=[];
    loopsEdgeIndices=[];
    for i1=1:nLoops
      N1=allLoopsNodeIndices(1)(i1);
      E1=allLoopsEdgeIndices(1)(i1);
      for i2=1:size(allLoopsEdgeIndices(n-1))
        N2=allLoopsNodeIndices(n-1)(i2);
        E2=allLoopsEdgeIndices(n-1)(i2);
        if intersect(N1,N2)==[] then 
          //loops i1 and i2 does not touch, create a single path with both
          //padding is used for quick removal of duplicates
          padding=zeros(1,nArcs-size(N1,"*")-size(N2,"*"));
          loopsNodeIndices=[loopsNodeIndices;[N1,N2,padding]];
          loopsEdgeIndices=[loopsEdgeIndices;[E1,E2,padding]];
        end
      end
    end
    if loopsNodeIndices<>[] then 
      [loopsEdgeIndices,loopsNodeIndices]=pathArray2List(loopsEdgeIndices,loopsNodeIndices)
      allLoopsEdgeIndices(n)=loopsEdgeIndices;
      allLoopsNodeIndices(n)=loopsNodeIndices;
    else
      //no more n_uple of non touching loops
      break,
    end
  end
  maxTupleOrder=n-1;
  //Delta is equal to 1 + the sum of all paths gains
  //look for common terms in path gains
  f=allLoopsEdgeIndices(1)(1);
  for i=1: maxTupleOrder
    loopsEdgeIndices=allLoopsEdgeIndices(i);
    nPaths=size(loopsEdgeIndices);
    for j=1:nPaths
      f=intersect(f,loopsEdgeIndices(j));
    end
    disp(f)
  end
  pause
   // loop on the (input,output) pairs -------------------------------
  for j=1:size(fromNodes,"*")
    for i=1:size(toNodes,"*")
      //Find all the paths connecting the nodes fromNodes(j) and toNodes(i), 
      // such as  no node is crossed more than once
      [pathsEdgeIndices,pathsNodeIndices]=findPaths(fromNodes(j),toNodes(i),Arcs);
      // Remove padding and transform arrays into lists
      [pathsEdgeIndices,pathsNodeIndices]=pathArray2List(pathsEdgeIndices,pathsNodeIndices);
      nPaths=size(pathsEdgeIndices);
      
      // Determine the transfer as the sum of the loop's transfer  Gpath/(1+Dpath)
      // for all paths connecting the nodes fromNodes(j) and toNodes(i),
       
      if nPaths==0 then continue;end
      touch(nLoops,nPaths)=%f;
      pause
      for n=1:nLoops,
        for path_index=1:nPaths,
          touch(n,path_index)=intersect(pathsNodeIndices(path_index),loopsNodeIndices(n))<>[];
        end;
      end
      disp(touch)
      Switch=%f//faux pour xcos3 vrai pour les autres ????
      Tij=0; // fromNodes(j) to toNodes(i) transfer funtion initial value
      for path_index=1:nPaths
        //Symbolic expression of the current path transfer function

        v=0
         //faux pour xcos3 vrai pour les autres ????
         if Switch then
         for n=1:nLoops
          if ~touch(n,path_index) then
            //loop n does not touch current path, substract its path
            if modulo(n,2)==1 then
              v= v-pathTransfer(EdgeNames(loopsEdgeIndices(n)))
            else
              v= v+pathTransfer(EdgeNames(loopsEdgeIndices(n)))
            end
          end
         end
         end
        G_path=(v+1)*pathTransfer(EdgeNames(pathsEdgeIndices(path_index)))
        
        KG_path=0; //initialize Dpath
        for n=1:nLoops           
          // Perform symbolic addition of the loop contribution
          if touch(n,path_index) then
            KG_path=KG_path-pathTransfer(EdgeNames(loopsEdgeIndices(n)))
          end
        end

        //Symbolically convert Gpath/(1+Dpath) as a feedback expression
        T_path=convert2feedback(G_path,KG_path)
        //if i==1&j==2 then pause,end
        //Symbolically add a term to the current transfer function
        Tij=Tij+T_path
      end
      T(i,j)=string(Tij)
    end
  end
endfunction

function [pathsEdgeIndices,pathsNodeIndices]=findPaths(fromNode,toNode,Arcs,Path,Nodes)
//Recursive function to find all oriented paths between fromNode and toNode. 
// a path connects  fromNode to toNode using a node at most one time
//
//  Arcs              :the array with the network list in it. 
//  Path, Nodes       :arguments used during recursion
//  pathsEdgeIndices  :a matrix, each row lists the number of the coefficients of all
//                     paths terminated below that node that are sucesfull. Each row
//                     is padded with zeros to make them the same length.
//  pathsNodeIndices :a matrix, each row the list of nodes traversed. Each row 
//                     is padded with zeros to make them the same length.

// Determine number of coefficients in net
  if argn(2)==3 then Path=[];Nodes=[];end
  nArcs=size(Arcs,1);

  pathsEdgeIndices=[];
  pathsNodeIndices=[];

  // Terminate branch and return nothing if the Nodes to date contains repetitions.
  for index=1:nArcs
    if Nodes<>[] then // Check if the Path has data in it
      if (sum(Nodes==index)>1) then
        pathsEdgeIndices=[];
        return
      end
    end
  end

  // Terminate branch and return path if start and stop nodes are the same
  if (fromNode==toNode) & (length(Path>1)) then
    pathsEdgeIndices=Path;
    pathsNodeIndices=Nodes;
    return
  end
  // Check for all branches leaving fromNode, and call another iteration for them
  for index=find(Arcs(:,1)==fromNode)
  //for index=1:nArcs
    //if fromNode==Arcs(index,1) then
      // Iterate with appended edge to path and new startnode
      [FoundEdges,FoundNodes]=findPaths(Arcs(index,2),toNode,Arcs,[Path,index],[Nodes,fromNode]);
      if FoundEdges<>[]  then  
        padding=zeros(1,nArcs-length(FoundEdges))
        pathsEdgeIndices=[pathsEdgeIndices;[FoundEdges,padding]];
        pathsNodeIndices=[pathsNodeIndices;[FoundNodes,padding]];
      end
    //end
  end
endfunction

function [E,N]=pathArray2List(pathEdgeIndices,pathNodeIndices)
// Remove duplicates, padding and transform arrays into lists
// Remove duplicate n-uples
  LL=gsort(pathEdgeIndices,"c","d") //do not take edges order into account
  [LL,ku]=unique(LL,"r");//find unique rows
  pathEdgeIndices=loopsEdgeIndices(ku,:);
  pathNodeIndices=loopsNodeIndices(ku,:);
  // Remove  padding and transform arrays into lists
  E=list();N=list();
  for index=1:size(pathEdgeIndices,1)                  
    edgeIndices=pathEdgeIndices(index,:); 
    n=find(edgeIndices==0,1)-1;
    E(index)=edgeIndices(1:n);         
    nodeIndices=pathNodeIndices(index,1:n);
    N(index)=[nodeIndices,nodeIndices(1)];
  end
endfunction

function m=pathTransfer(EdgeNames)
// Computes the symbolic product of all given edges
// EdgeNames is an array of character strings containing the symbolic
// names of the edges
  m=1;
  for k=1:size(EdgeNames,'*')
    c=EdgeNames(k);
    if c=="1" then
    elseif c=="-1" then
     m=(-1)*m
    else
      m=m*sym_expr(c);
    end
  end
endfunction

function pathEdges2pathNodes(edges,arcs)
  nodes=arcs(edges,:)'
  nodes=nodes(1:2:$)'
endfunction
