function r=sym_div(num,den)
  if %f&typeof(den)=="expr"&type(den.v(1))==1&den.v(1)==1&size(den.v)==2& typeof(den.v(2))=="term" then //potential feedback
    den=den.v(2)
    if typeof(num)=="expr" then num=tlist(["term",v,s],list(num),1);end
    //recherche de facteurs communs a droite de num et den 
    nn=size(num.v)
    nd=size(den.v)
    kmax=-1;
    for k=0:min(nn,nd)-1
      if num.v(nn-k)<>den.v(nd-k) then kmax=k-1;break;end
    end
  else
    r=tlist(["div","num","den"],num,den)
  end
endfunction
