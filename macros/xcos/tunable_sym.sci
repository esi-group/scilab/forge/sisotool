function [sciblk, s1, s2]=tunable_sym(job, sciblk, e1)
  s1= []; s2 = [];// arguments requis mais non utilisés
  select job
  case "set" then
    exprs = sciblk.graphics.exprs;
    model = sciblk.model;
    [ok, name, desc,exprs] = scicos_getvalue(_("Set tunable block name"),...
                                        [_("name");_("Description")],...
                                        list("str",1,"str",1),exprs);
    if ~ok then return,end
    sciblk.model.opar=list(name,desc);
    sciblk.graphics.exprs=exprs;

    style_properties=["blockWithLabel";
                      "verticalLabelPosition=middle";
                      "verticalAlign=middle";
                      "displayedLabel="+name;
                      "fontStyle=1";
                      "fontSize=20";
                      "fillColor=red"
                      "strokeColor=black"
                      "strokeWidth=0.5"
                      "gradientColor=white" 
                      "gradientDirection=south"
                     ];
    sciblk.graphics.style=strcat(style_properties,";"); 
   case "define" then
    model = scicos_model();
    model.sim = list("tunable", 5);
    model.intyp = 1;
    model.outtyp = 1;
    model.in = 1;
    model.in2 =1;
    model.out =1;
    model.out2 =1;
    model.ipar=4;
    model.opar=list("C","")
    model.dep_ut = [%f %f];
    sciblk = standard_define([2 2], model, []);
    style_properties=["blockWithLabel";
                      "verticalLabelPosition=middle";
                      "verticalAlign=middle";
                      "displayedLabel="+model.opar(1)
                      "fontStyle=1";
                      "fontSize=20";                                          
                      "fillColor=red"
                      "strokeColor=black"
                      "strokeWidth=0.5"
                      "gradientColor=white" 
                      "gradientDirection=south"
                     ];
    sciblk.graphics.style=strcat(style_properties,";")
    sciblk.graphics.exprs=[model.opar(1);model.opar(2)]
  end
endfunction
