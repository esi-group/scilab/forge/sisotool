function [sciblk, s1, s2]=symbolic(job, sciblk, e1)
  s1= []; s2 = [];// arguments requis mais non utilisés
  select job
  case "set" then
    exprs = sciblk.graphics.exprs;
    [ok, name, nin,nout,exprs] = scicos_getvalue(_("Set symbolic block properties"),...
                                             [_("name");_("number of inputs");_("number of outputs")],...
                                             list("str",1,"row",1,"row",1),exprs);                                                 
    sciblk.model.opar=list(name);
   
    [model,graphics,ok] = check_io(sciblk.model,sciblk.graphics,ones(1,nin),ones(1,nout),[],[]);

    sciblk.graphics.exprs=exprs;
    style_properties=tokens(sciblk.graphics.style,";")
    style_properties(6)="displayedLabel="+name
   
    sciblk.graphics.style=strcat(style_properties,";"); 
    sciblk.model=model
   case "define" then
    model = scicos_model();
    model.sim = list("G", 5);
    model.intyp = 1;
    model.outtyp = 1;
    model.in = 1;
    model.in2 =1;
    model.out =1;
    model.out2 =2;
    model.opar=list("G")
    model.ipar=1
    model.dep_ut = [%f %f];
    sciblk = standard_define([2 2], model, []);
    style_properties=["blockWithLabel";
                      "verticalLabelPosition=middle";
                      "verticalAlign=middle";
                      "displayedLabel="+model.opar(1)
                      "fillColor=green"
                      "strokeColor=black"
                      "strokeWidth=0.5"
                      "gradientColor=white" 
                      "gradientDirection=south"
                     ];
    sciblk.graphics.style=strcat(style_properties,";")
    sciblk.graphics.exprs=[model.opar(1);sci2exp(model.in,0);sci2exp(model.out,0)]
  end
endfunction
