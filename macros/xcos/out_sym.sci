function [sciblk, s1, s2]=out_sym(job,sciblk, e1)
  s1= []; s2 = [];// arguments requis mais non utilisés
  select job
  case "set" then
    exprs = sciblk.graphics.exprs;
    [ok, name, exprs] = scicos_getvalue(_("Set output name"),_("name"),list("str",1),exprs);
    if ~ok then return,end
    sciblk.model.opar=list(name);
    sciblk.model.sim=list("output",5);
    sciblk.graphics.exprs=exprs;
    style_properties=tokens(sciblk.graphics.style,";")
    if size(style_properties,"*")==13 then
      style_properties=[style_properties(1:6);"fontStyle=1";style_properties(7:$)]
    end
    style_properties(6)="displayedLabel="+name
    c=part(style_properties(8),11:length(style_properties(8)))
    in_style=tokens(sciblk.graphics.in_style,";");
    in_style(6)="fillColor="+c
    in_style(7)="strokeColor="+c
    sciblk.graphics.in_style=strcat(in_style,";");
    sciblk.graphics.style=strcat(style_properties,";") ;
  case "define" then
        model=scicos_model()
        model.sim=list("output",5);
        model.in=1;
        model.in2=1;
        model.intyp=1;
        model.ipar=2;
        model.opar=list("y");
        model.blocktype="c"
        model.dep_ut=[%f %f]
        sciblk=standard_define([1 1],model,[]);
        style_properties=["blockWithLabel";
                          "shape=hexagon";   
                          "verticalLabelPosition=middle";
                          "verticalAlign=middle";
                          "displayedLabel="+model.opar(1)
                          "fillColor=orange"
                          "strokeColor=black"
                          "strokeWidth=0.5"
                          "gradientColor=white" 
                          "gradientDirection=south"
                     ];
        sciblk.graphics.exprs=model.opar(1)
        sciblk.graphics.style=strcat(style_properties,";")
        sciblk.graphics.in_style="shape=triangle;fillColor=orange;strokeColor=orange"
  end
endfunction
