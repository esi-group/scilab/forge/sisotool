function [sciblk, s1, s2]=in_sym(job,sciblk, e1)
  s1= []; s2 = [];// arguments requis mais non utilisés
  select job
  case "set" then
    exprs = sciblk.graphics.exprs;
    model = sciblk.model;
    [ok, name, exprs] = scicos_getvalue(_("Set input name"),_("name"),list("str",1),exprs);
    if ~ok then return,end
    sciblk.model.opar=list(name);
    sciblk.model.sim=list("input",5);
    sciblk.graphics.exprs=exprs;
   
    style_properties=tokens(sciblk.graphics.style,";")
    if size(style_properties,"*")==13 then
      style_properties=[style_properties(1:6);"fontStyle=1";style_properties(7:$)]
    end
    style_properties(6)="displayedLabel="+name
    
    c=part(style_properties(8),11:length(style_properties(8)))
    out_style=tokens(sciblk.graphics.out_style,";");
    out_style(6)="fillColor="+c
    out_style(7)="strokeColor="+c
    sciblk.graphics.out_style=strcat(out_style,";");
    sciblk.graphics.style=strcat(style_properties,";") ;
    
  case "define" then
    model=scicos_model()
    model.sim=list("input",5);
    model.out=1;
    model.out2=1;
    model.outtyp=1;
    model.ipar=2;
    model.opar=list("u");
    model.blocktype="c"
    model.dep_ut=[%f %f]
    gr_i=[]
    sciblk=standard_define([1 1],model,[]);
    style_properties=["blockWithLabel";
                      "shape=hexagon"
                      "verticalLabelPosition=middle";
                      "verticalAlign=middle";
                      "displayedLabel="+model.opar(1)
                      "fillColor=blue"
                      "strokeColor=black"
                      "strokeWidth=0.5"
                      "gradientColor=white" 
                      "gradientDirection=south"
                     ];
    sciblk.graphics.style=strcat(style_properties,";")
    sciblk.graphics.exprs=model.opar(1)
    sciblk.graphics.out_style="shape=triangle;fillColor=blue;strokeColor=blue"
  end
endfunction
