// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function T=sisotoolFormatPZ(v)
  if v==[] then T=[],return,end
  if isreal(v) then
    t=tostring(v)
  else
    kreal=find(imag(v)==0)
    kcmplx=find(imag(v)<0);
    n=size(kreal,'*')+size(kcmplx,'*')
    if n==0 then
      t=[]
    else
      t=emptystr(n,1)
      if kreal<>[] then
        t(kreal)=tostring(real(v(kreal)));
      end
      if kcmplx<>[] then
        t(kcmplx)=tostring(real(v(kcmplx)))+"±"+tostring(abs(imag(v(kcmplx))))+"i";
      end
    end
  end
  T=t(:,1)
  for k=2:size(t,2)
    T=T+", "+t(:,k)
  end
endfunction

function s=tostring(v)
  s=string(round(100*v)/100)
endfunction

