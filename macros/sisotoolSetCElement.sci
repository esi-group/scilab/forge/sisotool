// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function S=sisotoolSetCElement(S,compensatorName,entityType,value,index)
  C=S.Designs(compensatorName);
  optSettings=S.OptimSettings.Controller(compensatorName)(entityType);
  if argn(2)<5 then //add
    //index=size(optSettings.style,'*')+1;
    index=size(C(entityType),2)+1
  end
  if type(value)==15 then //for undo/redo
    opt=value(2);
    value=value(1);
    m=size(value,1);
    C(entityType)(1:m,index)=value;
    optSettings.style(1,index)=opt.style;
    optSettings.enabled(1,index)=opt.enabled;
    m=size(opt.initial,"*");
    optSettings.initial(1:m,index)=opt.initial;
    optSettings.lowbound(1:m,index)=opt.lowbound;
    optSettings.highbound(1:m,index)=opt.highbound;
  else
    m=size(value,"*")
    C(entityType)(1:m,index)=value(:);
    optSettings.style(1,index)=1;
    optSettings.enabled(1,index)=%f;
    
    if or(entityType==["poles" "zeros"]) then
      optSettings.initial(1:2,index)=[real(value);imag(value)];
      optSettings.lowbound(1:2,index)=[-%inf;-%inf];
      optSettings.highbound(1:2,index)=[%inf;0];
    elseif entityType=="leadslags" then
      optSettings.initial(1:2,index)=value(:);
      optSettings.lowbound(1:2,index)=[-%inf;-%inf];
      optSettings.highbound(1:2,index)=[%inf;%inf];
    elseif entityType== "notches" then
      z=value(1);p=value(2);
      R=abs(z);zetaz=abs(real(z))/R; zetap=abs(real(p))/R;
      optSettings.initial(1:3,index)=[R;zetaz;zetap];
      optSettings.lowbound(1:3,index)=[0;-1;-1];
      optSettings.highbound(1:3,index)=[%inf;1;1];
    end
  end
  S.Designs(compensatorName)=C;
  S.OptimSettings.Controller(compensatorName)(entityType)=optSettings;
endfunction
