// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [axes_bounds,margins]=sisotoolSubwinSettings(types)
//Defines the configuration of the subwindows for Editors ans Analysers
//types       : the array of subwindow types
//axes_bounds : the axes_bounds(l,:) is the axes_bounds property value for
//              the lth subwindow
//margins     : the margins(l,:) is the margins property value for
//              the lth subwindow
  
//bode plots will count for one area
  kbode=find(types=="olbm"|types=="clbm"|types=="gainplot")
  n=size(types,"*")-size(kbode,"*")
  select n
  case 0
    axes_bounds=[],margins=[]
  case 1
    axes_bounds=[0   0   1   1];
    margins=[0.12 0.12 0.12 0.12];
  case 2//[1 2]
    axes_bounds=[0   0   1/2 1;
                 1/2 0   1/2 1];
    margins=[0.12 0.12 0.12 0.12;
             0.16 0.1 0.12 0.12];
  case 3//[1 2/3]
    axes_bounds=[0   0   1/2 1;
                 1/2 0   1/2 1/2
                 1/2 1/2 1/2 1/2];
    margins=[0.16 0.1 0.12 0.12;
             0.16 0.1 0.15 0.15;
             0.16 0.1 0.15 0.19];
  case 4//[1/2 3/4]
    axes_bounds=[0   0   1/2 1/2;
                 0   1/2 1/2 1/2;
                 1/2 0   1/2 1/2
                 1/2 1/2 1/2 1/2];
    margins=[0.16 0.1 0.15 0.19;
             0.16 0.1 0.15 0.19;
             0.16 0.1 0.15 0.19;
             0.16 0.1 0.15 0.19]
  case 5//[1/2 3/4/5]    
    axes_bounds=[0   0   1/2 1/2;
                 0   1/2 1/2 1/2;
                 1/2 0   1/2 1/3
                 1/2 1/3 1/2 1/3
                 1/2 2/3 1/2 1/3];
    margins=[0.16 0.1 0.15 0.19;
             0.16 0.1 0.15 0.19;
             0.16 0.1 0.14 0.3;
             0.16 0.1 0.14 0.3;
             0.16 0.1 0.14 0.3];
  case 6//[1/2/3 4/5/6]
    axes_bounds=[0   0   1/2 1/3;
                 0   1/3 1/2 1/3;
                 0   2/3 1/2 1/3;
                 1/2 0   1/2 1/3
                 1/2 1/3 1/2 1/3
                 1/2 2/3 1/2 1/3];
    margins=[0.16 0.1 0.14 0.3;
             0.16 0.1 0.14 0.3;
             0.16 0.1 0.14 0.3;
             0.16 0.1 0.14 0.3;
             0.16 0.1 0.14 0.3;
             0.16 0.1 0.14 0.3];
  end
  //split the area allocated for bode plots in 2 sub-windows

  for k=kbode
    mk=margins(k,:)
    bk=axes_bounds(k,:)
    mkm=mk;
    mkm(3)=mk(3)+2/8;
    mkm(4)=0.05;
    bkm=bk;bkm(4)=bk(4)/2
    mkp=mk;mkp(3)=0.05;mkp(4)=mk(4)+2/8;
    bkp=bk;bkp(4)=bk(4)/2;bkp(2)=bkp(2)+bk(4)/2
    axes_bounds=[axes_bounds(1:k-1,:)
                 bkm
                 bkp
                 axes_bounds(k+1:$,:)]
    margins=[margins(1:k-1,:)
             mkm
             mkp
             margins(k+1:$,:)]
    
  
  end

  
endfunction
