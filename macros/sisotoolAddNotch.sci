// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAddNotch(win)
//      s^2/wn^2 + (2*Zeta1^2)*s/wn + 1
//      --------------------------
//      s^2/wn^2 + (2*Zeta2^2)*s/wn + 1
  pixDistTol=4;
  mainH=get_figure_handle(win);
  mainH.event_handler_enable = "off";
  mainH.info_message=_("Click left where you want to add this noth");
  ax=gca()//current axes has been seta by sisotoolEditorsEvents
  [btn,pt]=getPointInSubwin(ax); 
  mainH.info_message="";
  
  if pt==[] then 
    mainH.event_handler_enable = "on";
    return,
  end
  typ=ax.tag;
  editorData=sisotoolGetEditorData(ax);
  compensatorName=editorData.tunable;
  S=sisotoolGetSession(mainH);
  dt=S.Designs(compensatorName).dt;

  ok=%f;
  if typ=='rlocus' then
    //put the notch poles at the selected point, set the zeros to  achieve 20 dB drop
     p=pt(1)-imult(abs(pt(2)));// pole at the selected point
     [wn,zetap]=damp(p,dt);
     zetaz=zetap/10; //zero damping factor achieve 20 dB drop
     z=wnzeta2complex(dt,wn,zetaz)
     v=[z;p];
     ok=%t;
  elseif or(typ==["olbm" "olbp" "clbm" "clbp"]) then
    //the selected point gives the notch natural frequency
    zetaz=0.05;zetap=0.5; 
    u=S.Preferences.units.frequency;
    wn=freqconv(u,"rd/s",pt(1));//natural frequency (rd/s)
    z=wnzeta2complex(dt,wn,zetaz)
    p=wnzeta2complex(dt,wn,zetap)    
    v=[z;p];
    ok=%t;
  elseif typ=="nichols" then
    //the selected point gives the notch natural frequency interpolated
    //on the Nichols curve.
    zetaz=0.05;zetap=0.5;
    curve=editorData.loci;
   
    [d,ptp,ind,c]=orthProj(curve.data,pt);
    if ind<>[] then
      [xx,yy]=xchange(ptp(1),ptp(2),'f2i');
      [x,y]=xchange(pt(1),pt(2),'f2i');
      [d,k]=max([abs(xx-x) abs(yy-y)]);
      if d<=pixDistTol then 
        frq=curve.display_function_data.freq;
        u=S.Preferences.units.frequency;
        wn=freqconv(u,"rd/s",frq(ind)+(frq(ind+1)-frq(ind))*c);
        z=wnzeta2complex(dt,wn,zetaz)
        p=wnzeta2complex(dt,wn,zetap)    
        v=[z;p];
        ok=%t;
      end
    end
  end
  if ok then
    S=sisotoolAddHistory(S,"Compensator","add",compensatorName,"notches",v);
    S=sisotoolSetCElement(S,compensatorName,"notches",v);
    sisotoolSetSession(S,mainH);  
    sisotoolRedraw(mainH);
    sisotoolUpdateOptimGui(mainH);
  end
  mainH.event_handler_enable = "on";  
 endfunction
