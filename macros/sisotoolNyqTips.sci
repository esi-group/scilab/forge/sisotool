// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function str=sisotoolNyqTips(h)

  c=h.parent;
//  h.background=9; //to fix a bug
  ri=complex(h.data(1),h.data(2));
 
  dfd=c.display_function_data;
 
  frequencyUnit=dfd.units.frequency;
  magnitudeUnit=dfd.units.magnitude;
  phaseUnit=dfd.units.phase;

  tipType=dfd.tipType
  if tipType==[] then
    //find corresponding freq
    frq=dfd.freq;
    frq=[frq -frq($:-1:1)]
    
    RI=complex(c.data(:,1),c.data(:,2))
    [m,k]=min(abs(RI-ri));
    if real(RI(k))>real(ri)|k==size(RI,1) then k=k-1;end
    frq=frq(k)+(frq(k+1)-frq(k))*abs(ri-RI(k))/abs(RI(k+1)-RI(k))
    frq=freqconv("Hz",frequencyUnit,frq);
    str=msprintf(_("%s\nReal part: %.3g\nImaginary part: %.3g\nFrequency: %.3g%s"),...
                 dfd.TF_name,real(ri),imag(ri),frq,frequencyUnit) 
  elseif tipType=="peak" then
      g=magconv("absolute",magnitudeUnit,abs(ri))
      frq=freqconv("Hz",frequencyUnit,dfd.freq)
      str=msprintf(_("%s\nPeak Gain: %.3g%s\nFrequency: %.3g%s"),...
                   dfd.TF_name,g,magnitudeUnit,frq,frequencyUnit) 
  elseif tipType=="gainMargin" then
    i=h.user_data;
    if i<>[]&dfd.gmargin<>[]  then
      frq=freqconv("Hz",frequencyUnit,dfd.gmargin(1,i))
      gm=magconv("dB",magnitudeUnit,dfd.gmargin(2,i))
      str=msprintf(_("%s\nGain margin: %.3g%s\nFrequency: %.3g%s"),...
                   dfd.TF_name,gm,magnitudeUnit,frq,frequencyUnit) 
    else
      str=""
    end
  elseif tipType=="phaseMargin" then
    i=h.user_data;
    if i<>[]&dfd.pmargin<>[] then
      frq=freqconv("Hz",frequencyUnit,dfd.pmargin(1,i))
      pm=phaseconv(_("degrees"),phaseUnit,dfd.pmargin(2,i))
      str=msprintf(_("%s\nPhase margin: %.3g%s\nFrequency: %.3g%s"),...
                   dfd.TF_name,pm,phaseUnit,frq,frequencyUnit) 
    else
      str=""
    end
  end
endfunction
