// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [typ,i,k,pt,loc]=sisotoolFindSelected(editorData,pt)
//find the entity selected by the user in the current axes (see sisotoolEditorsEvents)
//editorData:  the data structure associated with the  current axes
//pt        :  the coordinates in pixels of the oint selected by the user
//the current axes handle is ax
  pixDistTol=8;
  //
  xi=pt(1);
  yi=pt(2);
  i=1;ok=%f,loc=[]
  //look if it is a marker
  for typ=["leadslags","notches"]
    if isfield(editorData,typ) then
      h=editorData(typ)
      
      for i=1:2
        //i=1 -->zeros, i=2 -->poles,
        data=h(i).data;
        if data<>[] then
          [xx,yy]=xchange(data(:,1),data(:,2),'f2i')
          [d,k]=min(max(abs([xx-xi ,yy-yi]),'c'))
          if d<=pixDistTol then 
            pt=data(k,:);
            ud=h.user_data
            if  ud<>[] then
              //complex case, only the value with negative imaginary part
              //is stored
              k=ud(k)
            end
            ok=%t;return,end
        end
      end
    end
  end
  for typ=["gain","poles","zeros"]
    if isfield(editorData,typ) then
      h=editorData(typ)
      if h<>[] then
        data=h.data;
        if data<>[] then
          [xx,yy]=xchange(data(:,1),data(:,2),'f2i')
          [d,k]=min(max(abs([xx-xi ,yy-yi]),'c'))
          if d<=pixDistTol then 
            ud=h.user_data
            pt=data(k,:);
            if  ud<>[] then
              //complex case, only the value with negative imaginary part
              //is stored
              k=ud(k)
            end
            ok=%t;
            return,
          end
        end
      end
    end
  end

  // notches width markers
  typ="notches_width"
  if isfield(editorData,typ) then
    h=editorData(typ)
    for i=1:size(h,'*')
      //i=1 -->low frequency bound, i=2 -->high frequency bound,
      data=h(i).data;
      if data<>[] then
        [xx,yy]=xchange(data(:,1),data(:,2),'f2i')
        [d,k]=min(max(abs([xx-xi ,yy-yi]),'c'))
        if d<=pixDistTol then pt=data(k,:);ok=%t;return,end
      end
    end
  end
  //Check for loci selection
  h=editorData.loci
  [xu,yu]=xchange(pt(1),pt(2),'i2f')
  
  for i=1:size(h,'*')
    data=h(i).data
    if data<>[] then
      if h(i).line_mode=="on" then
        [xx,yy]=xchange(data(:,1),data(:,2),'f2i')
        [d,ptp,ind,c]=orthProj([xx yy],[xi,yi])
        if d<=pixDistTol then 
          [xx,yy]=xchange(ptp(1),ptp(2),'i2f')
          pt=[xx yy];
          typ="loci";
          loc=[ind,c];
          k=ind
          ok=%t;
          return,
        end
      else
        [xx,yy]=xchange(data(:,1),data(:,2),'f2i')
        [d,k]=min(max(abs([xx-xi ,yy-yi]),'c'))
        if d<=pixDistTol then pt=data(k,:);ok=%t;return,end
      end
    end
  end
  //check for requirements selection
  if editorData.requirements<>[] then
    h=editorData.requirements.children
    for i=1:size(h,"*")
      if h(i).type=="Compound" then //tunnel
        for j=1:size(h(i).children,'*')
          data=h(i).children(j).data
          if data<>[] then
            [xx,yy]=xchange(data(:,1),data(:,2),'f2i')
            [d,ptp,ind,c]=orthProj([xx yy],[xi,yi])
            if d<=pixDistTol then 
              [xx,yy]=xchange(ptp(1),ptp(2),'i2f')
              pt=[xx yy];
              typ="requirements";
              loc=[ind,c,j];
              k=ind
              ok=%t;
              return,
            end
          end
        end
      else //other requirement types
        data=h(i).data
        if data<>[] then
          [xx,yy]=xchange(data(:,1),data(:,2),'f2i')
          [d,ptp,ind,c]=orthProj([xx yy],[xi,yi])
          if d<=pixDistTol then 
            [xx,yy]=xchange(ptp(1),ptp(2),'i2f')
            pt=[xx yy];
            typ="requirements";
            loc=[ind,c];
            k=ind
            ok=%t;
            return,
          end
        end
      end
    end
  end
  if ~ok then
    typ=[];i=[];k=[],pt=[]
  end
endfunction
