function cleanmacros()
  libpath = get_absolute_file_path("cleanmacros.sce");
  F = [ls(libpath+"/*.bin");
       ls(libpath+"/names")
       ls(libpath+"/lib")
       ls(libpath+"/gui/*.bin");
       ls(libpath+"/gui/names")
       ls(libpath+"/gui/lib")]
  for f = F', mdelete(f);;end

endfunction

cleanmacros();
clear cleanmacros; // remove cleanmacros on stack

