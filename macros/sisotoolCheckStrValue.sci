// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function r=sisotoolCheckStrValue(valid)
  h=gcbo
  r=or(h.string==valid)
  if r then 
    sisotoolSetError(h,%f)
    r=%t
  else
    msg=msprintf(_("Expected value must be in {%s}"),strcat(valid,","))
    sisotoolSetError(h,%t,msg)
  end
endfunction
