// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolUpdateAnalysers(mainH)
// Updates Analysers curves , characteristics, special grids and datatips 
// during move operations (without recomputing discretization nor graph bounds)
//  
  S=sisotoolGetSession(mainH)
  units=S.Preferences.units
  winHandle=S.Analysers
  if winHandle==[] then return;end
  sys=S.Designs;
  subWins=S.AnalysersSettings.subWins;
  nsubwins=size(subWins,'*');
  AnalysersHandles=S.AnalysersSettings.handles
  winHandle.immediate_drawing="off"
  refreshed=S.AnalysersSettings.refreshed
  for subWinIndex=1:nsubwins
    if ~refreshed(subWinIndex) then continue;end
    subWin=subWins(subWinIndex);
    TF=evstr(subWin.expr)
    TF_names=subWin.transfer_names
    n=size(TF_names,"*")
    ax=AnalysersHandles(subWinIndex)
    ad=sisotoolGetAnalyserData(ax); //analyser data
    //TBD: mettre a jour les tips? et les mainGrids
    select subWin.type //type names are defined in sisotoolAnalysersGui
    case "step"
      loci=ad.loci
      mainGrid=ad.mainGrid;
      t=loci(1).data(:,1);
      if TF.dt<>"c" then
        y=flts(ones(t'),TF);
      else
        y=csim("step",t,TF);
      end
      for k=1:n; 
        loci(k).data(:,2)=y(k,:)'; 
        finalValue=sisotoolFinalValue(TF(k,1),"step");
        loci(k).user_data=finalValue;
        updateDatatips(loci(k))
        if finalValue<>[] then
          mainGrid(k).visible="on"
          mainGrid(k).data(:,2)=[finalValue;finalValue];
        else
          mainGrid(k).visible="off"
        end
      end
    case "impulse"
      loci=ad.loci
      mainGrid=ad.mainGrid;
      t=loci(1).data(:,1);
      if TF.dt<>"c" then
        y=flts(eye(t'),TF);
      else
        y=csim("impulse",t,TF)
      end
      for k=1:n; 
        loci(k).data(:,2)=y(k,:)'; 
        finalValue=sisotoolFinalValue(TF(k,1),"step");
        loci(k).user_data=finalValue;
        updateDatatips(loci(k))
        if finalValue<>[] then
          mainGrid(k).visible="on"
          mainGrid(k).data(:,2)=[finalValue;finalValue];
        else
          mainGrid(k).visible="off"
        end
      end
    case "gainplot"  //TBD: mettre a jour  les mainGrids?
      //Gain plot
      loci=ad.loci
      frq=loci(1).data(:,1)
      [frq,repf]=repfreq(TF,frq);  
      [phi,db]=phasemag(repf);
      for k=1:n; 
        loci(k).data(:,2)=db(k,:)'; 
        dfd=loci(k).display_function_data
        [g,fg]=g_margin(TF(k,1),"all")
        dfd.gmargin=[fg;g];//for charateristics display
        loci(k).display_function_data=dfd
        updateDatatips(loci(k))
      end
         
      //TBD c'a ne marche pas lorsqu'il y a plusieurs courbes
      //cela depend de la TF qui sera choisie dans optimGui!!
     
      [gm,frg]=g_margin(TF)
      if frg<>[] then
        frg=freqconv("Hz",units.frequency,frg);
        gm=magconv("dB",units.magnitude,gm);
        ad.gmargin=[frg,gm] 
        if subWin.reqProps(3).data<>[] then //minimum gain margin requirement is defined
          g=magconv("dB",units.magnitude,subWin.reqProps(3).data);                                
          v0=magconv("dB",units.magnitude,0)
          ad.requirements.children(3).data=[frg v0;frg v0-g]
          ad.requirements.children(3).user_data=subWin.reqProps(3).data
        else
          ad.requirements.children(3).data=[]
        ad.requirements.children(3).user_data=[]
        end
      else
        ad.gmargin=[] 
        ad.requirements.children(3).data=[]
        ad.requirements.children(3).user_data=[]
      end
      
    case "phaseplot"
      loci=ad.loci
      for k=1:n; 
        loci(k).data(:,2)=phi(k,:)'; 
        dfd=loci(k).display_function_data
        [p,fp]=p_margin(TF(k,1),"all")
        dfd.pmargin=[fp;p];//for charateristics display
        loci(k).display_function_data=dfd
        updateDatatips(loci(k))
      end
           
      //TBD c'a ne marche pas lorsqu'il y a plusieurs courbes
      //cela depend de la TF qui sera choisie dans optimGui!!
      [phm,frp]=p_margin(TF)
    
      if frp<>[] then
        frp=freqconv("Hz",units.frequency,frp);
        phm=phaseconv(_("degrees"),units.phase,phm);
        ad.pmargin=[frp,phm] 
        if subWin.reqProps(1).data<>[] then //minimum gain margin requirement is defined
         v180=phaseconv(_("degrees"),units.phase,180);  
          p= phaseconv(_("degrees"),units.phase,subWin.reqProps(1).data)
           ad.requirements.children(1).data=[frp -v180;frp -v180+p]  
           ad.requirements.children(1).user_data=subWin.reqProps(1).data
        else
          ad.requirements.children(1).data=[]
          ad.requirements.children(1).user_data=[]
        end
      else
        ad.pmargin=[] 
      end
 
    case "nyquist"  
      loci=ad.loci
      frq=loci(1).display_function_data.freq
      [frq,repf]=repfreq(TF,frq);
      repf=[conj(repf(:,$:-1:1)) repf];
      for k=1:n; 
        loci(k).data=[real(repf(k,:))' imag(repf(k,:))']; 
        dfd=loci(k).display_function_data
        dfd.freq=frq
        [g,fg]=g_margin(TF(k,1),"all")
        rf=repfreq(TF(k,1),fg)
        kept=find(fg>frq(1)&fg<frq($))
        dfd.gmargin=[fg(kept);g(kept);rf(kept)];
        [p,fp]=p_margin(TF(k,1),"all")
         kept=find(fp>frq(1)&fp<frq($))
        rf=repfreq(TF(k,1),fp)
        dfd.pmargin=[fp(kept);p(kept);rf(kept)]
        loci(k).display_function_data=dfd
        updateDatatips(loci(k))
      end 
    case "nichols"
      loci=ad.loci
      frq=loci(1).display_function_data.freq
      [frq,repf]=repfreq(TF,frq);
      [phi,db]=phasemag(repf);
      phi=phaseconv(_("degrees"),units.phase,phi);
      db=magconv("dB",units.magnitude,db);
    
      for k=1:n; 
        loci(k).data=[phi(k,:)',db(k,:)']; 
        dfd=loci(k).display_function_data
        dfd.freq=frq
        [g,fg]=g_margin(TF(k,1),"all")
        kept=find(fg>frq(1)&fg<frq($))
        dfd.gmargin=[fg(kept);g(kept)];
        [p,fp]=p_margin(TF(k,1),"all")
         kept=find(fp>frq(1)&fp<frq($))
        dfd.pmargin=[fp(kept);p(kept)]
        loci(k).display_function_data=dfd
        updateDatatips(loci(k))
      end
      
    case "pole/zero"  
      loci=ad.loci
      for k=1:n
        if typeof(TF)=="state-space" then
          [z,p]=ss2zp(TF(k,1))
        else
          [z,p]=tf2zp(TF(k,1))
        end
        loci(k).data=[real(p),imag(p)]
        loci(n+k).data=[real(z),imag(z)]
        updateDatatips(loci(k))
        updateDatatips(loci(n+k))
      end
     
    end
    // update characteristics
    analyserType=ax.tag; 
    
    handles=ad.characteristics_handles
    cKeys=sisotoolCharacteristicsTypes(analyserType)
    for cKey= cKeys
      handles=sisotoolManageCharacteristics(subWinIndex,cKey,handles,"update")
    end
    ad.characteristics_handles=handles
    sisotoolSetEditorData(ad,ax)
  end
  winHandle.immediate_drawing="on"
endfunction
