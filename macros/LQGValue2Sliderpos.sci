// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function LQGValue2Sliderpos(bounds)
  // hv handle on edition zone
  hv=gcbo;// handle on the edition zone
  gui=hv.parent.parent;// handle on the gui
  hc=gui.children(1);//handle on the button
  hs=hv.userdata; //handle on the slider

  [ok,v]=sisotoolCheckNumValue(bounds)
  if ~ok then hc.Enable=%f;return;end
  hc.Enable=%t
  if v>0 then
    hs.value=1000*((log10(v)+8)/16);
  else
    hs.value=0;
  end
endfunction
