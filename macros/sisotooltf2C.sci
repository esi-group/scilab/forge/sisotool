// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function C=sisotooltf2C(s)
  C=sisotoolNewController([]);
  select typeof(s)
  case "constant" then
    C.gain=s
  case "state-space" then
    [z,p,k]=ss2zp(s)
    C.zeros=orderedroots(z);
    C.poles=orderedroots(p);,
    C.gain=k
    C.dt=s.dt;
  case "rational" then
    [z,p,k]=tf2zp(s)
    C.zeros=orderedroots(z);
    C.poles=orderedroots(p);,
    C.gain=k
    C.dt=s.dt;//TBD a voir pour le cas discret
  case "zpk" then
    C.zeros=orderedroots(s.Z{1});
    C.poles=orderedroots(s.P{1});
    C.gain=s.K;
    C.dt=s.dt;
  end
endfunction
function r=orderedroots(r)
  r=matrix(r,1,-1);
  r=r(imag(r)<=0); //keep only one element of the complex pairs
  [s,k]=gsort([real(r);imag(r)],'lc','i')
  r=r(k)
endfunction
