// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function cTypes=sisotoolCharacteristicsTypes(analyserType)
  cTypes=[];
  select analyserType
  case "step"
    cTypes=[_("Peak response"),_("Settling time"),_("Rise time"),_("Steady state")]
  case "impulse"
    cTypes=[_("Peak response"),_("Settling time")]
  case "bode"
    cTypes=[_("Peak response"),_("Min stability margin"),_("All stability margins")]
  case "nyquist"
    cTypes=[_("Peak response"),_("Min stability margin"),_("All stability margins")]
  case "nichols"
    cTypes=[_("Peak response"),_("Min stability margin"),_("All stability margins")]
  case "pole/zero"
    cTypes=[]
  end
endfunction
