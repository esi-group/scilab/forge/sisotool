// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [transfer_names,expr,exportAs]=sisotoolTransfer(archi)
//transfer_names: the labels to appear in AnalysersGui
//expr:           the expressions which can be used to compute the
//                corresponding transfer functions
  archiSpecs=sisotoolGetArchiSpecs(archi);
  //Closed loops
  in=archiSpecs.in; //input ports
  out=archiSpecs.out; //output ports
  nin=size(in,"*");
  nout=size(out,"*");
  //Open loops
  out_of=archiSpecs.out_of; //cutting point for open loop definition
  nol=size(out_of,'*');
  
  tunable=archiSpecs.tunable;// tunable blocs (short name)
  tunable_desc=archiSpecs.tunable_desc; //(long name)
  plant=archiSpecs.plant; //plant blocs (short name)
  plant_desc=archiSpecs.plant_desc;  //(long name)

  IN=(in((1:nin).*.ones(out)))';
  OUT=(out(ones(in).*.(1:nout)))';

  transfer_names=[msprintf(_("Closed loop %s to %s\n"),IN,OUT);
                  msprintf(_("Open loop, out of %s\n"),out_of(:))
                  msprintf(_("Transfer function of %s\n"),[tunable_desc+" "+tunable, plant_desc+" "+plant]')
                 ];
  
  expr=[msprintf("sisotoolClosedLoop(sys,[""%s"",""%s""])\n",IN,OUT);
        msprintf("sisotoolSys2OL(sys,""%s"")\n",out_of(:))
        msprintf("sisotoolC2ss(sys.%s) \n",tunable(:))
        msprintf("sys.%s \n",plant(:))
       ];
  exportAs=[msprintf("T_%s2%s\n",IN,OUT);
            strsubst(out_of(:),"C","OL")
            tunable(:)
            plant(:)];
endfunction
