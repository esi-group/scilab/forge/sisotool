// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolUpdateOptSetting()
  frame=gcbo.parent;
 
  
  if frame.tag=="element" then
    guiHandle=frame.parent.parent;
    ud=guiHandle.user_data;
    mainH=ud.mainH;
    S=sisotoolGetSession(mainH);
    data=frame.user_data;
    kind=data.kind;
    tunable=data.tunable;
    typ=data.type;
    index=data.index;
    tag=gcbo.tag;
    if tag=="enabled" then
      v=gcbo.value==1
      cur=S.OptimSettings(kind)(tunable)(typ)(tag)(1,index)
      S=sisotoolAddHistory(S,"OptimSettings",tag,kind,tunable,typ,index,cur)
      S.OptimSettings(kind)(tunable)(typ)(tag)(1,index)=v;
    elseif tag=="style" then //representation change
      style=gcbo.value
      cur=S.OptimSettings(kind)(tunable)(typ)(tag)(1,index)
      if cur<>style then
        //style initial lowbound highbound
        rtyp=frame.children(4).user_data
        S=sisotoolAddHistory(S,"OptimSettings",tag,kind,tunable,typ,index,cur)
        initial=S.OptimSettings(kind)(tunable)(typ).initial(:,index);
        lowbound=S.OptimSettings(kind)(tunable)(typ).lowbound(:,index);
        highbound=S.OptimSettings(kind)(tunable)(typ).highbound(:,index);
        [initial,lowbound,highbound]=sisotoolChangeEltFormat(rtyp,style,initial,lowbound,highbound);
        S.OptimSettings(kind)(tunable)(typ).initial(:,index)=initial;
        frame.children(3).string=sci2exp(initial,0);
        S.OptimSettings(kind)(tunable)(typ).lowbound(:,index)=lowbound;
        frame.children(2).string=sci2exp(lowbound,0);
        S.OptimSettings(kind)(tunable)(typ).highbound(:,index)=highbound;
        frame.children(1).string=sci2exp(highbound,0);
        S.OptimSettings(kind)(tunable)(typ)(tag)(1,index)=style;
      end
    else
      v=evstr(gcbo.string)
      if size(v,"*")==1&typ<>"gain" then v=[v;0];end
      cur=S.OptimSettings(kind)(tunable)(typ)(tag)(:,index)
      if or(v<>cur) then
        S=sisotoolAddHistory(S,"OptimSettings",tag,kind,tunable,typ,index,cur)
        S.OptimSettings(kind)(tunable)(typ)(tag)(:,index)=v;
      end
    end
  else
    guiHandle=gcbo.parent.parent;
    ud=guiHandle.user_data;
    mainH=ud.mainH;
    data=gcbo.user_data;
    v=gcbo.value==1
    kind=data.kind;
    sub=data.subwinIndex;
    req_type=data.req_type;
    requirementIndex=data.tf_index;
    S=sisotoolGetSession(mainH);
    cur=S.OptimSettings(kind)(sub)(req_type)(requirementIndex)
    S=sisotoolAddHistory(S,"OptimSettings","req",kind,sub,req_type,requirementIndex,cur)
    S.OptimSettings(kind)(sub)(req_type)(requirementIndex)=v;
  end
  sisotoolSetSession(S,mainH);
endfunction
