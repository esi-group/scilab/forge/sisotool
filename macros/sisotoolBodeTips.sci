// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function str=sisotoolBodeTips(h)
  c=h.parent;
  dfd=c.display_function_data;
//  h.background=9; //to fix a bug
  tipType=dfd.tipType
  
  
  frequencyUnit=dfd.units.frequency;
  magnitudeUnit=dfd.units.magnitude;
  phaseUnit=dfd.units.phase;

  if tipType==[] then //standard tip along the curve
    frq=h.data(1);m=h.data(2);
    if or(c.parent.tag==["gainplot","olbm","clbm"]) then
   
      str=msprintf(_("%s\nGain: %.3g%s\nFrequency: %.3g%s"),...
                     dfd.TF_name,m,magnitudeUnit,frq,frequencyUnit); 
    else
      str=msprintf(_("%s\nPhase: %.3g%s\nFrequency: %.3g%s"),...
                   dfd.TF_name,m,phaseUnit,frq,frequencyUnit); 
 
    end
  elseif tipType=="peak" then
    frq=h.data(1);m=h.data(2)
    str=msprintf(_("%s\nFrequency: %.3g%s\nPeak gain: %.3g%s"),...
                 dfd.TF_name,frq,frequencyUnit,m,magnitudeUnit) ;
  elseif tipType=="gainMargin" then
    i=h.user_data;
    if i<>[]&dfd.gmargin<>[] then
      gm=magconv("dB",magnitudeUnit,dfd.gmargin(2,i))
      frq=freqconv("Hz",frequencyUnit,dfd.gmargin(1,i))
      str=msprintf(_("%s\nGain margin: %.3g%s\nFrequency: %.3g%s"),...
                   dfd.TF_name,gm,magnitudeUnit,frq,frequencyUnit) ;
    else
      str=""
    end
    elseif tipType=="phaseMargin" then
    i=h.user_data;
    if i<>[]&dfd.pmargin<>[] then
      pm=phaseconv(_("degrees"),phaseUnit,dfd.pmargin(2,i))
      frq=freqconv("Hz",frequencyUnit,dfd.pmargin(1,i))
      str=msprintf(_("%s\nPhase margin: %.3g%s\nFrequency: %.3g%s"),...
                   dfd.TF_name,pm,phaseUnit,frq,frequencyUnit); 
    else
      str=""
      //pause
    end
  end
    
endfunction
