// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function r=pt2px()
  px=get(0,"screensize_px")-1
  pt=get(0,"screensize_pt")
  r=mean(px(3:4)./pt(3:4))
endfunction
