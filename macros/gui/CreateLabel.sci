// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function l=CreateLabel(parent,x,y,w,h,label)
   l=uicontrol( ...
      "parent"              , parent,...
      "style"               , "text",...
      "string"              , label,...
      "position"            , [x,y,w,h ],....
      "fontsize"            , 12,...
      "horizontalalignment" , "left", ...
      "background"          , lightgray(),...
      "visible"             , "on");
endfunction
