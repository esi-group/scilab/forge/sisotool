// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function  sisotoolCLBodeEditor(CL,Tunable,EditorHandle,gridded,update,refreshed)
//S and units are defined in the calling scope
 //Redraw the editor display if update=%f
//or 
//Update the display during a move if update=%t
 
  if argn(2)<6 then refreshed=%f;end
  if argn(2)<5 then update=%f;end
  if update&~refreshed then return;end
  F=sisotoolC2ss(Tunable); 
  IF=sisotoolCImages(Tunable,F);
  
  //margins
  v180=phaseconv(_("degrees"),units.phase,180);

  [gm,frg]=g_margin(F)
  frg=freqconv("Hz",units.frequency,frg);
  gm=magconv("dB",units.magnitude,gm);
  
  [phm,frp]=p_margin(F)
  frp=freqconv("Hz",units.frequency,frp);
  phm=phaseconv(_("degrees"),units.phase,phm);
  if update then
    editorData=sisotoolGetEditorData(EditorHandle(1))
    curve=editorData.loci(1)
    frq=curve.data(:,1)'
    frq=freqconv(units.frequency,"Hz",frq);
    [frq,repf]=repfreq([F;CL],frq)
  else
    select freqResponsesPref.method
    case 1
      [Fmin,Fmax]=sisotoolFreqBounds(CL)
      [frq,repf]=repfreq([F;CL],Fmin,Fmax)// Hz  
    case 2
      range=freqResponsesPref.range
      [frq,repf]=repfreq([F;CL],range(1),range(2))// Hz  
    case 3
      [frq,repf]=repfreq([F;CL],freqResponsesPref.vector)
    end
  end
  [phi,db]=phasemag(repf);
  frq=freqconv("Hz",units.frequency,frq);
  Fmin=frq(1);Fmax=frq($);
  phi=phaseconv(_("degrees"),units.phase,phi);
  db=magconv("dB",units.magnitude,db);

  if EditorHandle.tag=="clbm" then //closed loop gain plot editor
    axclbm=EditorHandle
    editorData=sisotoolGetEditorData(axclbm);
    editorData.loci(1).data=[frq; db(1,:)]';//F gain locus
    editorData.loci(2).data=[frq; db(2,:)]';//CL gain locus
    if db<>[] then
      ddb=(max(db)-min(db))/8
      try
      axclbm.data_bounds=[Fmin,min(db)-ddb;Fmax,max(db)+ddb];
    catch
      disp pb;pause;
    end
    else
      axolbm.data_bounds=[Fmin,-1;Fmax,1];
    end
    //poles and zeros handles
    editorData.zeros.data=[IF.zeros(1,:);IF.zeros(3,:)]'
    editorData.zeros.display_function_data.data=Tunable.zeros;
    editorData.poles.data=[IF.poles(1,:);IF.poles(3,:)]';
    editorData.poles.display_function_data.data=Tunable.poles;
    editorData.leadslags(1).data=[IF.leadslags(1,:);IF.leadslags(5,:)]';//zeros
    editorData.leadslags(1).display_function_data.data=Tunable.leadslags;
    editorData.leadslags(2).data=[IF.leadslags(2,:);IF.leadslags(6,:)]';//poles
    editorData.leadslags(2).display_function_data.data=Tunable.leadslags;
    editorData.notches(1).data=[IF.notches(1,:);IF.notches(5,:)]';//zeros
    editorData.notches(1).display_function_data.data=Tunable.notches;
    editorData.notches(2).data=[IF.notches(2,:);IF.notches(6,:)]';//poles
    editorData.notches(2).display_function_data.data=Tunable.notches;
 
    sisotoolManageComponentsDatatips(editorData.zeros,update)
    sisotoolManageComponentsDatatips(editorData.poles,update)
    sisotoolManageComponentsDatatips(editorData.leadslags(1),update)
    sisotoolManageComponentsDatatips(editorData.leadslags(2),update)
    sisotoolManageComponentsDatatips(editorData.notches(1),update)
    sisotoolManageComponentsDatatips(editorData.notches(2),update)

    //Draw the gain and phase margins
    fmin=axclbm.data_bounds(1,1)
    fmax=axclbm.data_bounds(2,1)
    gmin=axclbm.data_bounds(1,2)
    gmax=axclbm.data_bounds(2,2)
    
    //gain margin
    editorData.gmargin(1).data=[fmin 0;fmax 0]
    if frg<>[] then
      editorData.gmargin(2).data=[frg gmin;frg gmax];
      editorData.gmargin(3).data=[frg -gm;frg 0];
      //drawing gain margin requirement. See also sisotoolDrawRequirement
      //this requirement depends on the crossover frequency so it cannot be
      //treated as usual 
      
      if subWin.reqProps(3).data<>[] then //minimum gain margin requirement is defined
        g=magconv("dB",units.magnitude,subWin.reqProps(3).data);
        editorData.requirements.children(3).data=[frg 0;frg -g]
        editorData.requirements.children(3).user_data=subWin.reqProps(3).data
      else
        editorData.requirements.children(3).data=[];
        editorData.requirements.children(3).user_data=[];
      end
    else
      editorData.gmargin(2).data=[];
      editorData.gmargin(3).data=[];
      editorData.requirements.children(3).data=[];
      editorData.requirements.children(3).user_data=[];
    end
    //phase margin
    if frp<>[] then
      editorData.pmargin.data=[frp gmin;frp gmax];
    else
      editorData.pmargin.data=[];
    end
    
    if gridded then
      axclbm.grid=ones(1,2)*color("gray");
    else
      axclbm.grid=-ones(1,2)
    end
    sisotoolSetEditorData(editorData,axclbm);
    if ~update then 
      sisotoolUpdateReqs(axclbm);
    end
    
  elseif update|EditorHandle.tag=="clbp" then 
    //closed loop phase plot editor  
    //----------------
    v180=phaseconv(_("degrees"),units.phase,180);
    axclbp=EditorHandle
    editorData=sisotoolGetEditorData(axclbp)
    editorData.loci(1).data=[frq; phi(1,:)]';//F phase locus
    editorData.loci(2).data=[frq; phi(2,:)]';//CL phase locus
    mnphi=min(phi)
    mxphi=max(phi)
    if mxphi-mnphi>=v180 then dphi=v180/2;else dphi=v180/6;end
    mnphi=dphi*floor(mnphi/dphi)
    mxphi=dphi*ceil(mxphi/dphi)
    phi_ticks=axclbp.y_ticks
    phi_ticks.locations=mnphi:dphi:mxphi
    if units.phase=="rd" then
      phi_ticks.labels=string(phi_ticks.locations/%pi)+"π";
    else
      phi_ticks.labels=string(phi_ticks.locations)
    end
    axclbp.y_ticks=phi_ticks;
    axclbp.data_bounds=[Fmin,mnphi;Fmax,mxphi];
    axclbp.tight_limits(2)="off"
    phi1=sisotoolShiftPhase(IF.zeros(1,:),IF.zeros(2,:),frq,phi(1,:),2*v180)
    editorData.zeros.data=[IF.zeros(1,:);phi1]'
    editorData.zeros.display_function_data.data=Tunable.zeros;
    phi1=sisotoolShiftPhase(IF.poles(1,:),IF.poles(2,:),frq,phi(1,:),2*v180)
    editorData.poles.data=[IF.poles(1,:);phi1]';
    editorData.poles.display_function_data.data=Tunable.poles;

    phi1=sisotoolShiftPhase(IF.leadslags(1,:),IF.leadslags(3,:),frq,phi(1,:),2*v180)
    editorData.leadslags(1).data=[IF.leadslags(1,:);phi1]';//zeros
    editorData.leadslags(1).display_function_data.data=Tunable.leadslags;
    phi1=sisotoolShiftPhase(IF.leadslags(2,:),IF.leadslags(4,:),frq,phi(1,:),2*v180)
    editorData.leadslags(2).data=[IF.leadslags(2,:);phi1]';//poles
    editorData.leadslags(2).display_function_data.data=Tunable.leadslags;
  
  
    
    phi1=sisotoolShiftPhase(IF.notches(1,:),IF.notches(3,:),frq,phi(1,:),2*v180)
    editorData.notches(1).data=[IF.notches(1,:);phi1]';//zeros
    phi1=sisotoolShiftPhase(IF.notches(2,:),IF.notches(4,:),frq,phi(1,:),2*v180)
    editorData.notches(2).data=[IF.notches(2,:);phi1]';//poles
    editorData.notches(2).display_function_data.data=Tunable.notches;

    sisotoolManageComponentsDatatips(editorData.zeros,update)
    sisotoolManageComponentsDatatips(editorData.poles,update)
    sisotoolManageComponentsDatatips(editorData.leadslags(1),update)
    sisotoolManageComponentsDatatips(editorData.leadslags(2),update)
    sisotoolManageComponentsDatatips(editorData.notches(2),update)
 
    fmin=axclbp.data_bounds(1,1);
    fmax=axclbp.data_bounds(2,1);
    pmin=axclbp.data_bounds(1,2);
    pmax=axclbp.data_bounds(2,2);

    //gain margin
    if frg<>[] then
      editorData.gmargin.data=[frg pmin;frg pmax];
      
    else
      editorData.gmargin.data=[]
      editorData.requirements.children(1).data=[]
    end
    
    //phase margin
    editorData.pmargin(1).data=[fmin -v180;fmax -v180];

    if frp<>[] then
      editorData.pmargin(2).data=[frp pmin;frp pmax];
      //drawing phase margin requirement. See also sisotoolDrawRequirement
      //this requirement depends on the crossover frequency so it cannot be
      //treated as usual 
      if subWin.reqProps(1).data<>[] then //minimum phase margin requirement is defined
        p= phaseconv(_("degrees"),units.phase,subWin.reqProps(1).data)
        editorData.requirements.children(1).data=[frp -v180;frp -v180+p]
        editorData.requirements.children(1).user_data=subWin.reqProps(1).data
      else
        editorData.requirements.children(1).data=[]
        editorData.requirements.children(1).user_data=[]
      end
      
      data=editorData.loci.data;
      frq=data(:,1)
      ind=find(frq(1:$-1)<=frp&frq(2:$)>frp);
      if ind<>[] then 
        phi=sisotoolShiftPhase(frp,phm+v180,frq,data(:,2),2*v180)
        editorData.pmargin(3).data=[frp -v180;frp phi];
      else
        editorData.pmargin(3).data=[]
        editorData.pmargin(3).data=[] 
      end
    else
      editorData.pmargin(3).data=[]
      editorData.pmargin(3).data=[]
    end
    
    xt=axclbp.x_ticks;xt.labels=emptystr(xt.labels)
    axclbm.x_ticks=xt
    
    if gridded then
      axclbp.grid=ones(1,2)*color("gray");
    else
      axclbp.grid=-ones(1,2)
    end
    sisotoolSetEditorData(editorData,axclbp);
    if ~update then 
      sisotoolUpdateReqs(axclbp);
    end

  end
endfunction
