// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolNicholsEditor(OL,Tunable,EditorHandle,gridded,update,refreshed)
//S, units and freqResponsesPref are defined in the calling scope
  if argn(2)<6 then refreshed=%f;end
  if argn(2)<5 then update=%f;end
  if update&~refreshed then return;end

  v180=phaseconv(_("degrees"),units.phase,180);
  I=sisotoolCImages(Tunable,OL)
  editorData=sisotoolGetEditorData(EditorHandle)
  kgrid=find(EditorHandle.children.tag=="grid")
  if kgrid<>[]  then //grid exist
    h_grid=EditorHandle.children(kgrid)
  else
    h_grid=[]
  end
  if update then
    curve=editorData.loci
    [frq,repf]=repfreq(OL,curve.display_function_data.freq)
    [phi,db]=phasemag(repf);
    phi=phaseconv(_("degrees"),units.phase,phi);
    db=magconv("dB",units.magnitude,db);
  else
    select freqResponsesPref.method
    case 1
      [Fmin,Fmax]=sisotoolFreqBounds(OL)// in Hz
      [frq,repf]=repfreq(OL,Fmin,Fmax)// Hz  
    case 2
      range=freqResponsesPref.range// in Hz
      [frq,repf]=repfreq(OL,range(1),range(2)); 
    case 3
      [frq,repf]=repfreq(OL,freqResponsesPref.vector)
    end
    [phi,db]=phasemag(repf);
    cphi=nicholsCriticalPoints(min(phi),max(phi))
    phi=phaseconv(_("degrees"),units.phase,phi);
    cphi=phaseconv(_("degrees"),units.phase,cphi);
    db=magconv("dB",units.magnitude,db);
    frq=freqconv("Hz",units.frequency,frq);
    
    mnphi=min(cphi(1)-v180/2,min(phi))
    mxphi=max(max(phi),cphi($)+v180/2)
    if mxphi-mnphi>=v180 then dphi=v180/2;else dphi=v180/6;end
    mnphi=dphi*floor(mnphi/dphi)
    mxphi=dphi*ceil(mxphi/dphi)
    phi_ticks=EditorHandle.x_ticks
    phi_ticks.locations=mnphi:dphi:mxphi
    if units.phase==_("degrees") then
      phi_ticks.labels=string(phi_ticks.locations)
    else
      phi_ticks.labels=string(phi_ticks.locations/%pi)+"π";
    end
    EditorHandle.x_ticks=phi_ticks
    
    EditorHandle.tight_limits(1)="off"
    db20=magconv("dB",units.magnitude,20)
    dbm20=magconv("dB",units.magnitude,-20)
    EditorHandle.data_bounds=[mnphi min(dbm20,min(db));
                    mxphi max(max(db),db20)];
    editorData.loci.display_function_data.freq=frq;
  end
 
  editorData.loci.data=[phi(:),db(:)];
  updateDatatips(editorData.loci)
  //Poles and zeros handles
  d=[frq;editorData.loci.data'];

  
  //zeros
  phi1=sisotoolShiftPhase(I.zeros(1,:),I.zeros(2,:),frq,phi,2*v180)
  editorData.zeros.data=[phi1;I.zeros(3,:)]'
  Z=Tunable.zeros;k=find(imag(Z)<>0);
  editorData.zeros.display_function_data.data=[Z,conj(Z(k))];
  updateDatatips(editorData.zeros)

   //poles
  phi1=sisotoolShiftPhase(I.poles(1,:),I.poles(2,:),frq,phi,2*v180)
  editorData.poles.data=[phi1;I.poles(3,:)]';
  P=Tunable.poles;k=find(imag(P)<>0);
  editorData.poles.display_function_data.data=[P,conj(P(k))];
  updateDatatips(editorData.poles)
  
  //leads and lags
  phi1=sisotoolShiftPhase(I.leadslags(1,:),I.leadslags(3,:),frq,phi,2*v180)
  editorData.leadslags(1).data=[phi1;I.leadslags(5,:)]';//zeros
  editorData.leadslags(1).display_function_data.data=Tunable.leadslags   
  phi1=sisotoolShiftPhase(I.leadslags(2,:),I.leadslags(4,:),frq,phi,2*v180)
  editorData.leadslags(2).data=[phi1;I.leadslags(6,:)]';//poles
  editorData.leadslags(2).display_function_data.data=Tunable.leadslags     

  //Notches
  N=Tunable.notches
  phi1=sisotoolShiftPhase(I.notches(1,:),I.notches(3,:),frq,phi,2*v180)
  editorData.notches(1).data=[phi1;I.notches(5,:)]';//zeros
  editorData.notches(1).display_function_data.data=[N(1,:),conj(N(1,:));
                    [N(2,:),conj(N(2,:))]];
  phi1=sisotoolShiftPhase(I.notches(2,:),I.notches(4,:),frq,phi,2*v180)
  editorData.notches(2).data=[I.notches(4,:);I.notches(6,:)]';//poles
//  editorData.notches(2).display_function_data.data=[N(1,:),conj(N(1,:));
//                    [N(2,:),conj(N(2,:))]];
  //Notch width markers
  phi1=sisotoolShiftPhase(I.notches(7,:),I.notches(9,:),frq,phi,2*v180)
  editorData.notches_width(1).data=[phi1;I.notches(11,:)]';
  phi1=sisotoolShiftPhase(I.notches(8,:),I.notches(10,:),frq,phi,2*v180)
  editorData.notches_width(2).data=[phi1;I.notches(12,:)]';
 
  sisotoolManageComponentsDatatips(editorData.zeros,update)
  sisotoolManageComponentsDatatips(editorData.poles,update)
  sisotoolManageComponentsDatatips(editorData.leadslags(1),update)
  sisotoolManageComponentsDatatips(editorData.leadslags(2),update)
  sisotoolManageComponentsDatatips(editorData.notches(1),update)
//  sisotoolManageComponentsDatatips(editorData.notches(2),update)

  //Gain and phase margins 
   curve=editorData.loci
  //margins
  [gm,frg]=g_margin(OL)
  [phim,w]=phasemag(repfreq(OL,frg));
   gm=magconv("dB",units.magnitude,gm);
  [phm,frp]=p_margin(OL)
   phm=phaseconv(_("degrees"),units.phase,phm);
   phim=phaseconv(_("degrees"),units.phase,phim);
   //Gain margin
   pi=phaseconv(_("degrees"),units.phase,180)
   if frg<>[] then
     phim=sisotoolShiftPhase(frg,phim,frq,curve.data(:,1),2*v180)
     editorData.gmargin.data=[phim 0;phim -gm];
   else
     editorData.gmargin.data=[];
   end

   //phase margin
 
  if frp<>[] then
    phi=sisotoolShiftPhase(frp,phm-pi,frq,curve.data(:,1),2*v180)
    kpi=phi-(phm-pi)-pi
    editorData.pmargin.data=[kpi 0;phi 0];
  else
    editorData.pmargin.data=[]
  end

  if ~update then
    // mainGrid
    bounds=EditorHandle.data_bounds
    dbnd=diff(bounds,1,1)
    phi=phaseconv(_("degrees"),units.phase,-180);
    db=magconv("dB",units.magnitude,0);
    mainGrid=editorData.mainGrid;
    mainGrid(1).data=[bounds(1,1)-dbnd(1) db;
                      bounds(2,1)+dbnd(1) db];
    mainGrid(2).data=[phi bounds(1,2)-dbnd(2);
                      phi bounds(2,2)+dbnd(2)];
    mainGrid(3).data=[phi,db];
    //Requirements
    sisotoolUpdateReqs(EditorHandle)
    editorData.loci.user_data=OL.dt
    
    //Grid       
    if EditorHandle.children($).type=="Compound" then
      delete(h_grid)//delete the grid if it is currently displayed
    end
    if gridded then
      sca(EditorHandle)
      bounds=EditorHandle.data_bounds
      nicholschart()
      h_grid=EditorHandle.children($)
      if units.phase<>_("degrees")| units.magnitude<>"dB" then
        scaleNicholsChart(h_grid,units.phase,units.magnitude)
      end
      h_grid.tag="grid"
      EditorHandle.data_bounds= bounds
      //put the grid over the requirements
      swap_handles(EditorHandle.children($),EditorHandle.children($-1))
    end
  end
  sisotoolSetEditorData(editorData,EditorHandle);
endfunction
function e=sisoDrawMargin(x,y)
  xpoly(x,y,"lines")
  set(gce(),"foreground",27,"thickness",7)//brown
endfunction
