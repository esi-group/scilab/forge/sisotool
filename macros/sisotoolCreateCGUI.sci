// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolCreateCGUI(mainWin)
  mainH=get_figure_handle(mainWin);
  S=sisotoolGetSession(mainH)
  units=S.Preferences.units
  archi=S.Designs.archi
  guiHandle=S.CompensatorEditor

  sisoguilib=lib(sisotoolPath()+"/macros/gui")
  //Creates the compensator editor GUI 
  margin_x     = 5;      // Horizontal margin between each elements
  margin_y     = 5;      // Vertical margin between each elements
  button_w     = 100;
  button_h     = 30;
  label_h      = 20;
  frame0_h     = 90;
  frame0_w     = 500;
  frame1_h     = 180;
  frame1_w     = 500;
  frame2_h     = 6*label_h+7*margin_y;
  frame2_w     = 500;

  list_h       = frame1_h-1.5*label_h-2*margin_y;
  list_w       = (frame1_w-margin_x)/4;
  list_incr    = list_w;
  axes_w       = 4*margin_x+frame1_w;
  axes_h       = 8*margin_y+frame0_h+frame1_h+frame2_h+button_h;// Frame height
  defaultfont  = "arial"; // Default Font

   if guiHandle<>[] then 
    ud=guiHandle.user_data
    if ud.archi==archi then 
      return,
    else
      delete(guiHandle.children)
      guiHandle.axes_size=[axes_w,axes_h]
    end
   else
     [path, fname] = fileparts(S.Path)
     guiHandle=figure("dockable", "off", ...
                      "menubar", "none", ...
                      "toolbar", "none", ...
                      "infobar_visible", "off",  ...
                      "axes_size", [axes_w,axes_h],...
                      "figure_name", msprintf(_("%s: Compensator Editor (%%d)"),fname),...
                      "default_axes","off",...
                      "event_handler_enable", "off",...
                      "visible","off",...
                      "resize","off",...
                      "icon",sisotoolPath()+"/icons/sisotool32.png",...       
                      "closerequestfcn","sisotoolGuiClose");
     S.CompensatorEditor=guiHandle
   end
 
  gui=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "units"               , "pixels",...
      "position"            , [1 1 axes_w-2 axes_h-2],...
      "background"          , lightgray(), ...
      "visible"             , "on");
  yo=axes_h-2*margin_y;
  x=margin_x;
  y1=yo-frame0_h//-margin_y
  
  Frame0=uipanel('Parent',gui,"Title",_("Compensator"),...
                       'Units', 'pixels', 'Position',[x,y1,frame0_w,frame0_h],...
                       'BackgroundColor',lightgray());   
  
  y0=frame0_h-7*margin_y
  w0=y0-margin_y;
  CreateLabel(Frame0,margin_x,y0,100,label_h,_("Parts"))
  y0=y0-w0
  compensatorsList=sisotoolGetArchiSpecs(archi,"tunable");

  
  compensatorParts=CreateList(Frame0,margin_x,y0,80,w0,compensatorsList,"sisotoolSetCGUI()")
  x1=80+4*margin_x;
  compensatorDisplay=CreateLabel(Frame0,x1,margin_y,frame0_w-x1-margin_x,w0," ")

  yf1=y1-frame1_h-2*margin_y
  Frame1=uipanel('Parent',gui,"Title",_("Components"),...
                       'Units', 'pixels', 'Position',[x,yf1,frame1_w,frame1_h],...
                       'BackgroundColor',lightgray());   

  yf2=yf1-frame2_h-2*margin_y
  Frame2=uipanel('Parent',gui,"Title",_("Component Editor"),...
                       'Units', 'pixels', 'Position',[x,yf2,frame1_w,frame2_h],...
                       'BackgroundColor',lightgray());   
  
  //Creating 4 vertical lists [Type Location Location Damping Frequency]
  //  these  lists are synchronized by sisotoolEditSelected
  callback="sisotoolEditSelected()"
  y1=frame1_h-1.5*label_h-margin_y;
  
  x1=margin_x/1.5;
  CreateLabel(Frame1,x1,y1,list_w,label_h,_("Type"))
  types=CreateList(Frame1,x1,y1-list_h,list_w,list_h,[],callback)

  x1=x1+list_incr;
  CreateLabel(Frame1,x1,y1,list_w,label_h,_("Location")) 
  locations=CreateList(Frame1,x1,y1-list_h,list_w,list_h,[],callback)

  x1=x1+list_incr;
  CreateLabel(Frame1,x1,y1,list_w,label_h,_("Damping"))
  dampings=CreateList(Frame1,x1,y1-list_h,list_w,list_h,[],callback)
   
  x1=x1+list_incr;
  CreateLabel(Frame1,x1,y1,list_w,label_h,msprintf(_("Frequency (%s)"),units.frequency))
  frequencies=CreateList(Frame1,x1,y1-list_h,list_w,list_h,[],callback)
  
  viewHandles=[types,locations,frequencies,dampings];
  yb=margin_y;
  x1=margin_x;
   
  callback="sisotoolAddComponent()"
  CreateButton(gui,x1,yb,button_w,button_h,_("Add component"),callback);
  x1=x1+button_w+margin_x;
  callback="sisotoolDeleteSelected()"
  CreateButton(gui,x1,yb,button_w,button_h,_("Delete component"),callback);
  ud=struct("mainH",mainH,"selectionHandles",tlist(['CGUI','views', ...
                    'editor','display'],viewHandles,Frame2,Frame0,compensatorParts))
  
  set(gui,"user_data", ud)
  set(guiHandle,"user_data",struct("mainH",mainH,"type","CompensatorEditor","archi",S.Designs.archi ))
  sisotoolSetSession(S,mainH)
  guiHandle.axes_size=[axes_w,axes_h];
  guiHandle.visible="on";
  sisotoolSetCGUI(gui)

endfunction

