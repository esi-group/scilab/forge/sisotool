// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [tfinal, dt] =sim_horizon(sys, tfinal, Ts)
// code translated from Octave __sim_horizon__ 
  if argn(2)<3 then Ts=[];end
  if argn(2)<2 then tfinal=[];end
  TOL = 1.0e-10;// values below TOL are assumed to be zero
  N_MIN = 50;   // min number of points
  N_MAX = 2000; // max number of points
  N_DEF = 1000; // default number of points
  T_DEF = 10;   // default simulation time

  continuous = sys.dt=='c';
  if typeof(sys)=="rational" then
    den=sys.den
    ev=[];
    for k=1:size(den,'*')
      ev=[ev; roots(den(k))]
    end
  else
    ev=spec(sys.A)
  end
  n = size(ev,"*"); // number of poles
  
  if ~continuous then
    dt = sys.dt
    Ts = dt
    // perform bilinear transformation on poles in z
    ev(abs(ev+1)<TOL)=[]
    ev=2 / Ts * (ev - 1) / (ev + 1);
  end
  
  // remove poles near zero
  ev(abs (real (ev)) < TOL) = [];
  
  if (ev == [])
    if tfinal==[] then tfinal = T_DEF;;end
    if (continuous) then dt = tfinal / N_DEF;end
  else
    ev = ev(ev<>0);
    ev_max = max (abs (ev));
    if continuous then dt = 0.2 * %pi / ev_max;end

    if tfinal==[] then
      ev_min = min (abs (real (ev)));
      tfinal = 6.0 / ev_min;
      // round up
      yy = 10^(ceil (log10 (tfinal)) - 1);
      tfinal = yy * ceil (tfinal / yy);
    end

    if continuous then
      N = tfinal / dt;
      if N < N_MIN then dt = tfinal / N_MIN;end
      if N > N_MAX then dt = tfinal / N_MAX;end
    end
  end
  if continuous & Ts<>[] then // continuous system with dt specified
    dt = Ts;
  end
endfunction
