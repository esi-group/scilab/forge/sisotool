// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAddGUIComponent(win,componentType)
  guiHandle=get_figure_handle(win)
  gui=guiHandle.children($); //handle on the naim frame
  ud=gui.user_data
  mainH=ud.mainH //main editor handle
  selectionHandles=ud.selectionHandles;//[types,locations,frequencies,dampings,Frame2])
  DisplayHandle=selectionHandles.display;
  compensatorDisplay=DisplayHandle.children(1);
  compensatorParts=DisplayHandle.children(2)
  compensatorName=compensatorParts.string(compensatorParts.value);
  
  S=sisotoolGetSession(mainH);
  S=sisotoolAddHistory(S,compensatorName,S.Designs(compensatorName))
  if S.Designs(compensatorName).dt=="c" then
    select componentType
    case "Real Pole" then
      S=sisotoolSetCElement(S,compensatorName,"poles",-1);
    case "Complex Pole" then
      S=sisotoolSetCElement(S,compensatorName,"poles",-1-%i);
    case "Integrator" then
      S=sisotoolSetCElement(S,compensatorName,"poles",0);
    case "Real Zero" then
      S=sisotoolSetCElement(S,compensatorName,"zeros",-1);
    case "Complex Zero" then
      S=sisotoolSetCElement(S,compensatorName,"zeros",-1-%i);
    case "Differentiator" then
      S=sisotoolSetCElement(S,compensatorName,"zeros",0);
    case "Lead" then
      S=sisotoolSetCElement(S,compensatorName,"leadslags",[-1;-10]);
    case "Lag" then
      S=sisotoolSetCElement(S,compensatorName,"leadslags",[-10;-1]);
    case "Notch" then
      S=sisotoolSetCElement(S,compensatorName,"notches",[-0.1-sqrt(0.99)*%i;-1-0*%i]);
    end
  else
    dt= S.Designs(compensatorName).dt
    select componentType
    case "Real Pole" then
      S=sisotoolSetCElement(S,compensatorName,"poles",exp(-1*dt));
    case "Complex Pole" then
      S=sisotoolSetCElement(S,compensatorName,"poles",exp((-1-%i)*dt));
    case "Integrator" then
      S=sisotoolSetCElement(S,compensatorName,"poles",1);
    case "Real Zero" then
      S=sisotoolSetCElement(S,compensatorName,"zeros",exp(-1*dt));
    case "Complex Zero" then
      S=sisotoolSetCElement(S,compensatorName,"zeros",exp((-1-%i)*dt));
    case "Differentiator" then
      S=sisotoolSetCElement(S,compensatorName,"zeros",1);
    case "Lead" then
      S=sisotoolSetCElement(S,compensatorName,"leadslags",exp([-1;-10]*dt));
    case "Lag" then
      S=sisotoolSetCElement(S,compensatorName,"leadslags",exp([-10;-1]*dt));
    case "Notch" then
      S=sisotoolSetCElement(S,compensatorName,"notches",exp([-0.1-sqrt(0.99)*%i;-1-0*%i]*dt));
    end
  end
  sisotoolSetSession(S,mainH);
  sisotoolRedraw(mainH)
endfunction
