// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function  updateDatatips(C)
//bug 14788 workaround
//update the display of the c polyline datatips  
  for l=1:size(C,"*")
    c=C(l);
    if c.datatips<>[] then 
      df=c.display_function;
      for k=1:size(c.datatips,"*")
        d=c.datatips(k);
        execstr("d.text="+df+"(d);");
      end

    end
  end
 
endfunction
