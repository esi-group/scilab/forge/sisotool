// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolRlocusEditor(OL,Tunable,EditorHandle,gridded,update,refreshed)
//redraw/update the rlocus editor
//  if degree(OL.num)==0&degree(OL.den)==0 then return;end
  if argn(2)<6 then refreshed=%f;end
  if argn(2)<5 then update=%f;end
  if update&~refreshed then return;end
  rootLocusPref=S.Preferences.rootLocusPref
  sca(EditorHandle);
  if EditorHandle.children==[] then return;end
  editorData=sisotoolGetEditorData(EditorHandle)
 
  //the entities must be drawn in this order: requirements, grid,
  //evans,marks
  n=size(editorData.mainGrid,"*")
  h_reqs=EditorHandle.children($) 
  h_grid=EditorHandle.children($-1)
  h_mainGrid=EditorHandle.children($-2:-1:$-1-n)
  h_evans=EditorHandle.children($-n-2)
  //group temporary the sisotool marks
  h_marks=glue(EditorHandle.children($-n-3:-1:1));//9 handles

  //Preserve data_bounds as evans may change them
  data_bounds=EditorHandle.data_bounds;
  //remove  previous Evans plot entities
  delete(h_evans)
  nold=size(EditorHandle.children,"*")
  //Recreate the Evans entities
  if OL.A<>[] then 
    if rootLocusPref.method==1 then
      evans(OL/Tunable.gain)
    else
      evans(OL/Tunable.gain,rootLocusPref.maxGain)
    end
  else
    //if OL is a simple gain evans create nothing
    //create 2 entities  to mimic evans regular entity structure
    xpoly([],[])
    xpoly([],[])
  end
  EditorHandle.children(1).visible='off';//hide legend
  loci=EditorHandle.children(2).children;
  //group the entities created by evans
  h_evans=glue(EditorHandle.children($-nold:-1:1)); 

  //put the sisotool marks display above evans
  swap_handles(h_marks,h_evans);
  // ungroup the sisotool marks
  unglue(h_marks)

  editorData.loci=loci;

  //controller Poles and Zeros handles
  Z=Tunable.zeros(:);
  k=find(imag(Z)<>0)
 
  editorData.zeros.data=[real(Z) imag(Z);
                    real(Z(k)) -imag(Z(k))];
  editorData.zeros.display_function_data.data=[Z;conj(Z(k))].';
  //preserve zeros order for sisotoolFindSelected.
  editorData.zeros.user_data=[1:size(Z,'*'),k];
  
  P=Tunable.poles(:);
  k=find(imag(P)<>0)
  editorData.poles.data=[real(P),imag(P)
                    real(P(k)) -imag(P(k))];
  editorData.poles.display_function_data.data=[P;conj(P(k))].';
  //preserve  poles order for sisotoolFindSelected.
  editorData.poles.user_data=[1:size(P,'*'),k];

  editorData.leadslags(1).data   = [Tunable.leadslags(1,:);0*Tunable.leadslags(1,:)]'; 
  editorData.leadslags(1).display_function_data.data=Tunable.leadslags    
  editorData.leadslags(2).data   = [Tunable.leadslags(2,:);0*Tunable.leadslags(2,:)]';
  editorData.leadslags(2).display_function_data.data=Tunable.leadslags
      
 
  
  N=Tunable.notches.'
  editorData.notches(1).data = [real(N(:,1)) imag(N(:,1));real(N(:,1)) -imag(N(:,1))];
  editorData.notches(1).display_function_data.data=[[N(:,1);conj(N(:,1))],[N(:,2);conj(N(:,2))]].';
  editorData.notches(2).data = [real(N(:,2)) imag(N(:,2));real(N(:,2)) -imag(N(:,2))];
  editorData.notches(2).display_function_data.data=[[N(:,1);conj(N(:,1))],[N(:,2);conj(N(:,2))]].';

 
  ord=1:size(N,1)
  editorData.notches(1).user_data=[ord ord];
  editorData.notches(2).user_data= [ord ord];
  
  //Image of the gain
  if typeof(OL)=="state-space" then
    Gain_Image=spec(OL.A-(OL.B*OL.C))
  else
    Gain_Image=roots(OL.den+OL.num,"e")
  end

  editorData.gain.data=[real(Gain_Image) imag(Gain_Image)] ;
  editorData.gain.display_function_data.loci=loci;
  sisotoolManageComponentsDatatips(editorData.gain,update)
  sisotoolManageComponentsDatatips(editorData.zeros,update)
  sisotoolManageComponentsDatatips(editorData.poles,update)
  sisotoolManageComponentsDatatips(editorData.leadslags(1),update)
  sisotoolManageComponentsDatatips(editorData.leadslags(2),update)
  sisotoolManageComponentsDatatips(editorData.notches(1),update)
  sisotoolManageComponentsDatatips(editorData.notches(2),update)
 
  //update data_bounds for sgrid
  if ~update then
    if typeof(OL)=="state-space" then
      p=spec(OL.A);
      z=trzeros(OL);
    else
      p=roots(OL.den,"e");
      z=roots(OL.num,"e");
    end
    pz=[Tunable.zeros(:);Tunable.poles(:);Tunable.leadslags(:);
        Tunable.notches(:);Gain_Image;p;z]
    if pz<>[] then
      mxx=max([real(pz);0]);mnx=min([real(pz);0]);dx=max(0.5,(mxx-mnx)/4);
      mxy=max([imag(pz);0]);mny=min([imag(pz);0]);dy=max(0.5,(mxy-mny)/4);
      data_bounds=[mnx-dx mny-dy;mxx+dx mxy+dy]
    end
    mainGrid=editorData.mainGrid
    mainGrid(1).data=[data_bounds(:,1),[0;0]]
    mainGrid(2).data=[[0;0] data_bounds(:,2)]
  end
  EditorHandle.data_bounds=data_bounds;      
 
  //Grid
  if ~update then
    sisotoolUpdateReqs(EditorHandle)
    if  gridded then 
      delete(h_grid)
      if OL.dt=="c" then sgrid();else zgrid();end
      //sgrid put the grid in background of all previously defined entities
      h_grid=EditorHandle.children($);
      //put the grid display above the requirements    
      swap_handles(h_reqs, h_grid)
    else
      delete(h_grid.children)
    end
     //force again the data_bounds because sgrid may have enlarged them     
    EditorHandle.data_bounds=data_bounds;   
  end
   
  sisotoolSetEditorData(editorData,EditorHandle);
endfunction
