// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C)  1985-2016 - INRIA - Serge Steer
//
// This file is hereby licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// This file was originally licensed under the terms of the CeCILL v2.1,
// and continues to be available under such terms.
// For more information, see the COPYING file which you should have received
// along with this program.

function [h,num,den]=ss2tf(sl,rmax)
// State-space to transfer function.
// Syntax:
//   h=ss2tf(sl)
//   h=ss2tf(sl,'b')
//   h=ss2tf(sl,rmax)
//
//   sl   : linear system (syslin list)
//   h    : transfer matrix
//   rmax : optional parameter controlling the conditioning in
//          block diagonalization method is used (If 'b' is entered
//          a default value is used)
//   Methods: 
//   -The default method ('p') is based on the Matrix determinant lemma 
//    see https://en.wikipedia.org/wiki/Matrix_determinant_lemma
//    if B is a colum vector and C a row vector
//    det(sI-A+BC)=(1+C(sI-A)^(-1)B)det(sI-A)
//    so
//    C(sI-A)^(-1)B =det(sI-A+BC)-det(sI-A)
//
//  -The other method  is based on  block-diagonalization and leverrier
//   algorithm (this may gives a less accurate result).
//
//!

    if type(sl)==1|type(sl)==2 then
        h=sl
        return
    end
    if typeof(sl)<>"state-space" then
        error(msprintf(_("%s: Wrong type for input argument #%d: State-space form expected.\n"),"ss2tf",1));
    end
    [A,B,C,D]=abcd(sl)
    //Handle special cases (no input, no output)
    if D==[] then 
      h=[];num=[];den=[];
      return;
    end
    //Handle special cases (no  state)
    if size(sl.A,"*")==0 then 
      h=syslin(sl.dt,D,ones(D))
      return
    end

    //Determine the rational fraction formal variable name
    domain=sl.dt
    if domain==[] then
      if type(sl.D)==2 then 
        var=varn(sl.D);
      else
        var="s";
      end
    elseif domain=="c" then 
      var="s";
    else
      var="z";
    end

    //Determine the algorithm
    [lhs,rhs]=argn(0);
    meth="p";
    if rhs==2 then
        if type(rmax)==10 then
            meth=part(rmax,1);
            if and(meth<>["p","b"]) then
                error(msprintf(_( "%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),"ss2tf",1,"''p'',''b''"));
            end
            rhs=1;
        else
            meth="b";
        end
    end

    select meth
    case "b" // Block diagonalization + Leverrier method
        a=sl.A;
        [n1,n1]=size(a);
        z=poly(0,var);
        //block diagonal decomposition of the state matrix
        if rhs==1 then
            [a,x,bs]=bdiag(a);
        else
            [a,x,bs]=bdiag(a,rmax);
        end
        k=1;m=[];v=ones(1,n1);den=1;
        for n=bs' //loop on blocks
            k1=k:k-1+n;
            // Leverrier algorithm
            h=z*eye(n,n)-a(k1,k1);
            f=eye(n,n);
            for kl=1:n-1,
                b=h*f;
                d=-sum(diag(b))/kl;
                f=b+eye()*d;
            end
            d=sum(diag(h*f))/n;
            //
            den=den*d;
            l=[1:k-1,k+n:n1];
            if l<>[] then v(l)=v(l)*d;end
            m=[m,x(:,k1)*f];
            k=k+n;
        end

        if lhs==3 then
            h=sl.D,
            num=real(sl.C*m*diag(v)*(x\sl.B));
        else
            m=real(sl.C*m*diag(v)*(x \ sl.B)+sl.D*den);
            [m,den]=simp(m,den*ones(m))
            h=syslin(domain,m,den)
        end

    case "p" then //Adjugate matrix method
      [A,B,C,D]=abcd(sl);
      Den=poly(A,var) //common denominator
      nz=find(coeff(Den)<>0)-1
      
      
      [m,n]=size(D)
      den=[];
      num=[];
      for l=1:m //loop on outputs
        if norm(C(l,:),'inf')==0 then
          num(l,1:n)=D(l,:);
          den(l,1:n)=ones(1,n);
        else
          for k=1:n //loop on inputs
            if norm(B(:,k),'inf')==0 then
              num(l,k)=D(l,k);
              den(l,k)=1;
            else
              if lhs==1 then
                Num=real(poly(A-B(:,k)*C(l,:),var)+(D(l,k)-1)*Den);
                //clean the numerator coefficients which are not
                //significant
                if %f&D(l,k)<>1 then
                  kz=find(abs(coeff(Num,nz)./((D(l,k)-1)*coeff(Den,nz)))<2*%eps);
                  if kz<>[] then
                    Num=coeff(Num)
                    Num(kz(kz<=size(Num,'*')))=0;
                    Num=poly(Num,var,"c")
                  end
                end
                [num(l,k),den(l,k)]=simp(Num,Den);
              else
                num(l,k)=real(poly(A-B(:,k)*C(l,:),var)-Den);
              end
            end
          end
        end
      end
      if lhs==3 then
        den=Den
        h=D;
      else
        if and(degree(num)==0)&and(degree(den)==0)
          h=coeff(num)./coeff(den);   //degenerate case
        else
          h=syslin(domain,num,den);
        end
      end
   
    end
endfunction
