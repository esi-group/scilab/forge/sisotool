// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - INRIA - Serge Steer <serge.steer@inria.fr>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at;
// http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt

function [d,H,ind,c]=orthProj(BC,A)
// computes minimum distance from a point A to a polyline (data)
//d    minimum distance of the point to the nearest polyline data point
//H  projected point coordinates
//ind  projection lies on segment [ind ind+1]
//c    orthogonal projection coefficient
// https://fr.wikipedia.org/wiki/Projection_orthogonale
  
//                                         +
//                                         A
//                                         |
//                                         |
//                                         |
//                                         |         
//  --------------+------------------------+----------+------------(D)
//                B                        H          C
d=[];H=[];ind=[],c=[];
  if argn(2)<>2 then
        error(msprintf(_("%s: Wrong number of input argument(s): %d expected.\n"),"orthProj",2))
    end


    d = [];ptp = [];ind = [],c = []
    [n,m] = size(BC)
    A = matrix(A,1,-1) //make A a row vector
    if n<2 then return,end
    //the orthogonal projection coefficient of the vector y on the vector x;
    //is given by  <x,y>/||x||
    //shift origin to (0,0) for each segment defined by BC
    
    //V: the direction vectors of the lines D on which the projections are done
    //   The lines D are given by the segments of the polyline
    V = (BC(2:$,:)-BC(1:$-1,:)) 
  
    //BA the vector from The point B (first point of the segments) to the given point (A)
    B=BC(1:$-1,:);
    BA=ones(n-1,1)*A-B
    
    //make direction vector of unit length
    L=sqrt(sum(V.*V,2));// the length of the directions vectors
    //remove zero length segments
    kz=find(L==0)
    if kz<>[] then
      L(kz)=[];V(kz,:)=[];BA(kz,:)=[],BC(kz,:)=[]
    end
    V=V./[L L];//normalize direction vectors

    //BH : the projections of the vectors BA on lines D  
    BH=(sum(BA.*V,2)*ones(1,2)).*V;
    //H : the coordinates of the projected points
    H=BH+BC(1:$-1,:);
    //find projections with H point inside the corresponding segment
    P=sqrt(sum(BH.*BH,2))./L

    i_in = find(P<1)

    if i_in<>[] then
      //HA: the othogonal vector from the point H to the projection on D
      HA=BA(i_in,:)-BH(i_in,:)
      //find the HA vector of minimum length
      LHA=sqrt(sum(HA.*HA,2));
      [d,k]=min(LHA)
      ind = i_in(k) //index of the first bound of the segment in BC
      H = H(ind,:) //the projected point
      c = P(ind)  // the orthogonal projection coefficient
      if kz<>[] then //make ind relative to the initial data
        nz=1:n;nz(kz)=[];
        ind = nz(ind) 
      end
    end
endfunction
