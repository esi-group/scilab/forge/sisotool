// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) INRIA -
//
// Copyright (C) 2012 - 2016 - Scilab Enterprises
//
// This file is hereby licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// This file was originally licensed under the terms of the CeCILL v2.1,
// and continues to be available under such terms.
// For more information, see the COPYING file which you should have received
// along with this program.

function [K,r]=obscont(P,Kc,Kf)
    //Returns the observer-based controller associated with a
    //plant P=[A,B,C,D]. The full-state control gain is Kc and filter
    //gain is Kf. A+B*Kc and A+C*Kf are assumed stable.
    if argn(1)==1 then //K=obscont(P,Kc,Kf)
      K=syslin(P.dt,P.A+P.B*Kc+Kf*P.C+Kf*P.D*Kc,-Kf,Kc)
    else //[K,r]=obscont(P,Kc,Kf)
      //Parametrisation of all the stabilizing feedbacks
      [nu,ny]=size(P)
      zro=0*Kc*Kf;I1=eye(Kc*P.B);I2=eye(P.C*Kf);
      K=syslin(P.dt,P.A+P.B*Kc+Kf*P.C+Kf*P.D*Kc,[-Kf,P.B+Kf*P.D],[Kc;-P.C-P.D*Kc],[zro,I1;I2,-P.D]);
      r=[ny nu];
    end
endfunction
