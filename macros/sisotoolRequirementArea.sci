// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function d=sisotoolRequirementArea(editorType,requirementType,requirementData,bnds)
//returns the polyline data used to show the requirement constraints for
//each editor type
  units=S.Preferences.units
  dt=S.Designs.dt
  editorType=convstr(editorType)
  if or(editorType==["olbm","clbm","gainplot"]) then
    select requirementType
    case "gmax" 
      d=[requirementData(1,1) bnds(2,2);
         requirementData;     
         requirementData($,1) bnds(2,2);
         requirementData(1,1) bnds(2,2)];
      d(:,1)=freqconv("Hz",units.frequency,d(:,1))
    case "gmin" 
      d=[requirementData(1,1) bnds(1,2);
         requirementData;
         requirementData($,1) bnds(1,2);
         requirementData(1,1) bnds(1,2)];
       d(:,1)=freqconv("Hz",units.frequency,d(:,1))
    case "gmargin" 
      d=[]//min gain margin requirement is specialy treated (see sisotoolOLBodeEditor)
    else
      d=[];
      messagebox(msprintf(_("Requirement %s not available for %s editor"),requirementType,editorType),"modal")
    end
  elseif or(editorType==["olbp","clbp","phaseplot"]) then
     d=[];//req(k).data //min phase margin requirement is specialy treated (see sisotoolOLBodeEditor)
  elseif or(editorType==["rlocus" "pole/zero"])then
    select requirementType
    case "settlingtime"
      Ts=requirementData
      R=-4/Ts
      if dt=="c" then
        d=[R,         R,         bnds(2,1), bnds(2,1),R
           bnds(1,2), bnds(2,2), bnds(2,2), bnds(1,2),bnds(1,2)]';
      else
        R=exp(-4*dt/Ts)
        xmax=max(max(abs(bnds(:,1))),R)*1.5
        ymax=max(max(abs(bnds(:,2))),R)*1.5
        t=linspace(0,2*%pi,200);
        x=[xmax R*cos(t) xmax xmax,-xmax -xmax xmax xmax];
        y=[0 R*sin(t) 0 -ymax -ymax ymax ymax,0];
        d=[x;y]'
      end
    case "damping"
      zeta=requirementData
      t=acos(zeta)
      if dt=="c" then
        r=norm(bnds(1,:));
        if abs(r*sin(t))>=bnds(2,2) then
          d=-r*[0 cos(t) 0        0      cos(t) 0
                0 sin(t) sin(t) -sin(t) -sin(t) 0]';
        else
          by=bnds(:,2)/r;
          d=-r*[0 cos(t) cos(t) 0      0   cos(t)    cos(t) 0
                0 sin(t) by(2)  by(2) by(1) by(1)   -sin(t) 0]';
        end
        
      else
        t=acos(zeta);a=%pi/sin(t)
        x=linspace(0,-a*cos(t),200);
        y=linspace(0,-a*sin(t),200);
        z=exp(complex(x,y));
        x=real(z($:-1:1)) ;x=[x($:-1:1) x];
        y=imag(z($:-1:1)) ;y=[-y($:-1:1) y];
        xmax=max(max(abs(bnds(:,1))),max(x))*1.5
        ymax=max(max(abs(bnds(:,2))),max(y))*1.5
        x=[xmax x xmax xmax,-xmax -xmax xmax xmax];
        y=[0 y 0 -ymax -ymax ymax ymax,0];
        d=[x;y]'
      end
    case "wmax" 
      wn=requirementData
      b=max(abs(bnds))
      t=linspace(%pi/2,3*%pi/2,200);
      d=[bnds(1,1),0,wn*cos(t), 0,bnds(1,1),bnds(1,1)
         b        ,b,wn*sin(t),-b,-b,       bnds(2,2)]';
     
    case "wmin" 
      wn=requirementData
      t=linspace(%pi/2,3*%pi/2,200);
      t=[t t(1)];
      d=[wn*cos(t)
         wn*sin(t)]';
    else
      d=[]
      messagebox(msprintf(_("Requirement %s not available for %s editor"),requirementType,editorType),"modal") 
    end
   elseif editorType=="nichols" then 
    select requirementType
    case "pmargin" //Phase margin
      v180=phaseconv("degrees",units.phase,180);
      requirementData=phaseconv("degrees",units.phase,requirementData);
      d=[-v180-requirementData 0 
         -v180+requirementData 0]
    case "gmargin" //Gain margin
      v180=phaseconv("degrees",units.phase,180);
      requirementData=magconv("dB",units.magnitude,requirementData);
      d=[-v180 requirementData 
         -v180 -requirementData]
    case "clpeak" //Closed loop peak gain 
      att=requirementData //dB
      //see nicholschart (dB curves)
      w=linspace(-%pi,0,300)';
      y=10^(att/20)*exp(%i*w);
      y(y==1)=[];//remove singular point if any
      rf=y./(1-y);
      [module, phi]=dbphi(rf) ;
      phi=phaseconv("degrees",units.phase,phi);
      module=magconv("dB",units.magnitude,module);
      v360=phaseconv("degrees",units.phase,360);

      phi=[-v360-phi($:-1:2);phi];
      module=[module($:-1:2);module];
      d=[phi,module];
      n=size(d,1);
      //save("/tmp/d1.dat","d","bnds")
      
      while bnds(1,1)<d(1,1)
        d=[[d(1:n-1,1)-v360, d(1:n-1,2)];d];
      end
      while bnds(2,1)>d($,1) then
        d=[d;[d($-n+2:$,1)+v360, d($-n+2:$,2)]];
      end
      //save("/tmp/d.dat","d")
      if att<0 then
        d=[d(1,1),bnds(2,2);
           d
           d($,1),bnds(2,2)];
      end
 
 
    case "gplow" //Gain-phase low bound
      d=[requirementData(1,1) bnds(1,2);
         requirementData;
         requirementData($,1) bnds(1,2);
         requirementData(1,1) bnds(1,2)];
      d(:,1)=phaseconv("degrees",units.phase,d(:,1))
    case "gphigh" //Gain-phase high bound
      d=[requirementData(1,1) bnds(2,2);
         requirementData;     
         requirementData($,1) bnds(2,2);
         requirementData(1,1) bnds(2,2)];
      d(:,1)=phaseconv("degrees",units.phase,d(:,1))
    else
      d=[]
      messagebox(msprintf(_("Requirement %s not available for %s editor"),requirementType,editorType),"modal")
    end
  elseif or(editorType==["step" "impulse"]) then

    select requirementType
    case "upbound"
       d=[requirementData(1,1) bnds(2,2);
         requirementData;     
         requirementData($,1) bnds(2,2);
         requirementData(1,1) bnds(2,2)];
       d(:,1)=timeconv("s",units.time,d(:,1));
    case "lowbound"
      d=[requirementData(1,1) bnds(1,2);
         requirementData;
         requirementData($,1) bnds(1,2);
         requirementData(1,1) bnds(1,2)];
      d(:,1)=timeconv("s",units.time,d(:,1));
    case "tunnel"
      final=requirementData(1);
      riseTime=timeconv("s",units.time,requirementData(2))
      rise=requirementData(3)*final/100
      settlingTime=timeconv("s",units.time,requirementData(4))
      settling=requirementData(5)*final/100
      overshoot=final+requirementData(6)*final/100
      undershoot=requirementData(7)*final/100
      tmax=max(max(bnds(2,1),riseTime),settlingTime)
      d.low=[bnds(1,1), bnds(1,2)
            bnds(1,1), -undershoot
            riseTime,  -undershoot
            riseTime, rise
            settlingTime, rise
            settlingTime, final-settling 
            tmax, final-settling
            tmax,  bnds(1,2)
            bnds(1,1), bnds(1,2) ]
      d.high=[bnds(1,1), bnds(2,2)
             bnds(1,1), overshoot
             settlingTime, overshoot
             settlingTime, final+settling
             tmax, final+settling
             tmax,  bnds(2,2)
             bnds(1,1), bnds(2,2)]
    else
      d=[]
      messagebox(msprintf(_("Requirement %s not available for %s analyser"),requirementType,editorType),"modal")
    end
    
  end
endfunction
