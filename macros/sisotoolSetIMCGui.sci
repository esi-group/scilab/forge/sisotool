// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolSetIMCGui(gui)
//resets the LQG GUI values if it is opened
  if type(gui)<>9 then return;end
  ud=get(gui,"user_data")
  handles=ud.handles //[C,Input,Lambda,D]
  
  S=sisotoolGetSession(ud.mainH);
  sys=S.Designs;
  out_of=sisotoolGetArchiSpecs(sys.archi,"out_of");
  IMCSettings=S.IMCSettings
 
  handles(1).value=IMCSettings(1) //C
  handles(4).String=""; //bug 14578 workaround
  handles(4).String=sisotoolC2tex(sys(out_of(IMCSettings(1)))); //D
  handles(2).value=IMCSettings(2) //input
  handles(3).String=string(IMCSettings(3))//lambda
endfunction
