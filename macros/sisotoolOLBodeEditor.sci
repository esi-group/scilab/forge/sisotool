// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolOLBodeEditor(OL,Tunable,EditorHandle,gridded,update,refreshed)
//S, subWin,  units and freqResponsesPref are defined in the calling scope
  if argn(2)<6 then refreshed=%f;end
  if argn(2)<5 then update=%f;end
  ieee_save=ieee();ieee(2)
  I=sisotoolCImages(Tunable,OL)
  ieee(ieee_save)
  //margins
  v180=phaseconv(_("degrees"),units.phase,180);

  [gm,frg]=g_margin(OL)
  frg=freqconv("Hz",units.frequency,frg);
  gm=magconv("dB",units.magnitude,gm);
  
  [phm,frp]=p_margin(OL)
  frp=freqconv("Hz",units.frequency,frp);
  phm=phaseconv(_("degrees"),units.phase,phm);
  
  if update then
    if ~refreshed then return;end
    editorData=sisotoolGetEditorData(EditorHandle)
    curve=editorData.loci
    frq=curve.data(:,1)
    frq=freqconv(units.frequency,"Hz",frq);
    [frq,repf]=repfreq(OL,frq)
  else
    select freqResponsesPref.method
    case 1
      [Fmin,Fmax]=sisotoolFreqBounds(OL)
      [frq,repf]=repfreq(OL,Fmin,Fmax)// Hz  
    case 2
      range=freqResponsesPref.range
      [frq,repf]=repfreq(OL,range(1),range(2)); 
    case 3
      [frq,repf]=repfreq(OL,freqResponsesPref.vector)
    end
  end
  kz=find(abs(repf)==0)
  frq(kz)=[];
  repf(kz)=[];
  
  [phi,db]=phasemag(repf);
  frq=freqconv("Hz",units.frequency,frq);
  Fmin=frq(1);Fmax=frq($);
  phi=phaseconv(_("degrees"),units.phase,phi);
  db=magconv("dB",units.magnitude,db);
  
  if EditorHandle.tag=="olbm" then //gain plot editor
    axolbm=EditorHandle;
    edm=sisotoolGetEditorData(axolbm);
    edm.loci.data=[frq(:) db(:)];//gain locus
    if ~update then
      if db<>[] then
        ddb=(max(db)-min(db))/8
        axolbm.data_bounds=[Fmin,min(db)-ddb;Fmax,max(db)+ddb];
      else
        axolbm.data_bounds=[Fmin,-1;Fmax,1];
      end
      axolbm.auto_ticks(1)="on";
      axolbm.x_ticks.labels=emptystr(axolbm.x_ticks.labels);
      axolbm.auto_ticks(1)="off";
    end
    
    //poles and zeros handles
    edm.zeros.data=[I.zeros(1,:);I.zeros(3,:)]'
    edm.zeros.display_function_data.data=Tunable.zeros;
    
    edm.poles.data=[I.poles(1,:);I.poles(3,:)]';
    edm.poles.display_function_data.data=Tunable.poles;

    //leads and lags
    edm.leadslags(1).data=[I.leadslags(1,:);I.leadslags(5,:)]';//zeros
    edm.leadslags(1).display_function_data.data=Tunable.leadslags;

    edm.leadslags(2).data=[I.leadslags(2,:);I.leadslags(6,:)]';//poles
    edm.leadslags(2).display_function_data.data=Tunable.leadslags;
    
   // Notches
    edm.notches(1).data=[I.notches(1,:);I.notches(5,:)]';//zeros
    edm.notches(1).display_function_data.data=Tunable.notches;
    
    edm.notches(2).data=[I.notches(2,:);I.notches(6,:)]';//poles
    edm.notches(2).display_function_data.data=Tunable.notches;
    
    edm.notches_width(1).data=[I.notches(7,:);I.notches(11,:)]';//left markers
    edm.notches_width(2).data=[I.notches(8,:);I.notches(12,:)]';//right markers
    
    sisotoolManageComponentsDatatips(edm.zeros,update)
    sisotoolManageComponentsDatatips(edm.poles,update)
    sisotoolManageComponentsDatatips(edm.leadslags(1),update)
    sisotoolManageComponentsDatatips(edm.leadslags(2),update)
    sisotoolManageComponentsDatatips(edm.notches(1),update)
    sisotoolManageComponentsDatatips(edm.notches(2),update)

    //Draw the gain and phase margins
    fmin=axolbm.data_bounds(1,1)
    fmax=axolbm.data_bounds(2,1)
    gmin=axolbm.data_bounds(1,2)
    gmax=axolbm.data_bounds(2,2)
    
    //gain margin
    edm.gmargin(1).data=[fmin 0;fmax 0]
    if frg<>[] then
      edm.gmargin(2).data=[frg gmin;frg gmax];
      edm.gmargin(3).data=[frg -gm;frg 0];
      //drawing gain margin requirement. See also sisotoolDrawRequirement
      //this requirement depends on the crossover frequency so it cannot be
      //treated as usual 
      
      if subWin.reqProps(3).data<>[] then //minimum gain margin requirement is defined
        g=magconv("dB",units.magnitude,subWin.reqProps(3).data);
        edm.requirements.children(3).data=[frg 0;frg -g]
        edm.requirements.children(3).user_data=subWin.reqProps(3).data
      else
        edm.requirements.children(3).data=[]
        edm.requirements.children(3).user_data=[]
      end
    else
      edm.gmargin(2).data=[];
      edm.gmargin(3).data=[];
      edm.requirements.children(3).data=[]
      edm.requirements.children(3).user_data=[]
    end
    //phase margin
    if frp<>[] then
      edm.pmargin.data=[frp gmin;frp gmax];
    else
      edm.pmargin.data=[];
    end
    
    if gridded then
      axolbm.grid=ones(1,2)*color("gray");
    else
      axolbm.grid=-ones(1,2)
    end
    if ~update then 
      sisotoolUpdateReqs(axolbm);
    end

  else
    //phase plot editor
    //-----------------
    axolbp=EditorHandle
    edp=sisotoolGetEditorData(axolbp)
    edp.loci.data=[frq(:) phi(:)];//phase locus
    
    if ~update then
      if phi<>[] then
        mnphi=min(phi)
        mxphi=max(phi)
      else
        
        mnphi=0
        mxphi=v180/2
      end
      if mxphi-mnphi>=v180 then dphi=v180/2;else dphi=v180/6;end
      mnphi=dphi*floor(mnphi/dphi)
      mxphi=dphi*ceil(mxphi/dphi)
      phi_ticks=axolbp.y_ticks
      phi_ticks.locations=mnphi:dphi:mxphi
      if units.phase=="rd" then
        phi_ticks.labels=string(phi_ticks.locations/%pi)+"π";
      else
        phi_ticks.labels=string(phi_ticks.locations)
      end
      axolbp.y_ticks=phi_ticks
      axolbp.data_bounds=[Fmin,mnphi;Fmax,mxphi];
      axolbp.tight_limits(2)="off"
    end
    //poles and zeros handles
    phi1=sisotoolShiftPhase(I.zeros(1,:),I.zeros(2,:),frq,phi,2*v180)
    edp.zeros.data=[I.zeros(1,:);phi1]'
    edp.zeros.display_function_data.data=Tunable.zeros;
    phi1=sisotoolShiftPhase(I.poles(1,:),I.poles(2,:),frq,phi,2*v180)
    edp.poles.data=[I.poles(1,:);phi1]';
    edp.poles.display_function_data.data=Tunable.poles;
    
    phi1=sisotoolShiftPhase(I.leadslags(1,:),I.leadslags(3,:),frq,phi,2*v180)
    edp.leadslags(1).data=[I.leadslags(1,:);phi1]';//zeros
    edp.leadslags(1).display_function_data.data=Tunable.leadslags;
    phi1=sisotoolShiftPhase(I.leadslags(2,:),I.leadslags(4,:),frq,phi,2*v180)
    edp.leadslags(2).data=[I.leadslags(2,:);phi1]';//poles
    edp.leadslags(2).display_function_data.data=Tunable.leadslags;
    
  
    
    phi1=sisotoolShiftPhase(I.notches(1,:),I.notches(3,:),frq,phi,2*v180)
    edp.notches(1).data=[I.notches(1,:);phi1]';//zeros
    phi1=sisotoolShiftPhase(I.notches(2,:),I.notches(4,:),frq,phi,2*v180)
    edp.notches(2).data=[I.notches(2,:);phi1]';//poles
    edp.notches(2).display_function_data.data=Tunable.notches;
  
    sisotoolManageComponentsDatatips(edp.zeros,update)
    sisotoolManageComponentsDatatips(edp.poles,update)
    sisotoolManageComponentsDatatips(edp.leadslags(1),update)
    sisotoolManageComponentsDatatips(edp.leadslags(2),update)
    sisotoolManageComponentsDatatips(edp.notches(2),update)
   
    //Draw the gain and phase margins
    fmin=axolbp.data_bounds(1,1);
    fmax=axolbp.data_bounds(2,1);
    pmin=axolbp.data_bounds(1,2);
    pmax=axolbp.data_bounds(2,2);

    //gain margin
    if frg<>[] then
      edp.gmargin.data=[frg pmin;frg pmax];
      
    else
      edp.gmargin.data=[]
      edp.requirements.children(1).data=[]
    end
    
    //phase margin
    edp.pmargin(1).data=[fmin -v180;fmax -v180];

    if frp<>[] then
      edp.pmargin(2).data=[frp pmin;frp pmax];
      
      //drawing phase margin requirement. See also sisotoolDrawRequirement
      //this requirement depends on the crossover frequency so it cannot be
      //treated as usual 
      if subWin.reqProps(1).data<>[] then //minimum phase margin requirement is defined
        p= phaseconv(_("degrees"),units.phase,subWin.reqProps(1).data)
        edp.requirements.children(1).data=[frp -v180;frp -v180+p]
        edp.requirements.children(1).user_data=subWin.reqProps(1).data
      else
        edp.requirements.children(1).data=[]
        edp.requirements.children(1).user_data=[]
      end
      
      data=edp.loci.data;
      frq=data(:,1)
      ind=find(frq(1:$-1)<=frp&frq(2:$)>frp);
      if ind<>[] then 
        phi=sisotoolShiftPhase(frp,phm+v180,frq,data(:,2),2*v180)
        edp.pmargin(3).data=[frp -v180;frp phi];
      else
        edp.pmargin(3).data=[]
        edp.pmargin(3).data=[] 
      end
    else
      edp.pmargin(3).data=[]
      edp.pmargin(3).data=[]
    end
    xt=axolbp.x_ticks;xt.labels=emptystr(xt.labels)
    axolbm.x_ticks=xt
    if gridded then
      axolbp.grid=ones(1,2)*color("gray");
    else
      axolbp.grid=-ones(1,2)
    end
    if ~update then 
      sisotoolUpdateReqs(axolbp);
    end
  end
endfunction

