// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function hs=sisotoolHiliteSelected(sys,entityType,i,k)
  //TBD : not yet used nor finished
  S=sisotoolGetSession(fig) //fig comes from sisotoolEditorsEvents 
  EditorsSettings=S.EditorsSettings
  C=sys.C(entityType)
  hs=[];
  for ke=1:size(EditorsSettings)
    EditorType=EditorsSettings.type(ke)
    EditorHandle=EditorsSettings.handle(ke)
    sca(EditorHandle)
    select EditorType
    case "rlocus"
      select entityType
      case "poles"
        p=C(k)
        xpoly(real(p),imag(p),"marks")
        e=gce();
        e.mark_size_unit="point";
        e.mark_size=7;
        e.mark_style=2;
        e.mark_foreground=5;
        e.thickness=4;
        hs=[hs e];
      case "zeros"
      case "leadslags"
      case "notches"
      end
    case "bode"
    case "nichols"
    end
  end
  
endfunction
