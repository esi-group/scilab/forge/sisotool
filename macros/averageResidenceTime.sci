function Tar=averageResidenceTime(G)
//Reference: "Advanced PID Control"   ,K. J. Åström and T. Hägglund.
//(6th edition 2006) p 24
  dt=G.dt
  if typeof(G)=="rational" then
    if G.dt=="c" then z=0;f=1;else z=1;f=dt;end
    G1=horner(G,z);
    dG1=horner(derivat(G),z);
  elseif typeof(G)=="state-space" then
    [A,B,C,D]=abcd(G);
    if G.dt=="c" then z=zeros(A);f=1;else z=eye(A);f=dt;end
    X=C/(z-A);
    G1=X*B+D;
    X=X/(z-A);
    dG1=-X*B;
  elseif typeof(G)=="zpk" then
    if G.dt=="c" then z=0;f=1;else z=1;f=dt;end
    G=zpk2tf(G)
    G1=horner(G,z);
    dG1=horner(derivat(G),z);
  elseif typeof(G)=="iodelay" then
    L=G.iodelay
    G=G.H
    if G.dt=="c" then z=0;f=1;else z=1;f=dt;end
    G1=horner(G,z);
    dG1=horner(derivat(G),z)-L*G1;
  end
  Tar=-f*dG1/G1
endfunction
