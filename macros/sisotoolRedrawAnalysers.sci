// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolRedrawAnalysers(mainH,update,sel)
// Updates Analysers curves (recomputing discretization)
  if argn(2)<2 then update =%f;end
  S=sisotoolGetSession(mainH)
  units=S.Preferences.units
  winHandle=S.Analysers
  if winHandle==[] then return;end
  scf(winHandle) //for color setting
  sys=S.Designs;
  subWins=S.AnalysersSettings.subWins;
  if argn(2)<3 then sel=1:size(subWins,"*");end
  AnalysersHandles=S.AnalysersSettings.handles
  gridded=S.AnalysersSettings.gridded

  winHandle.immediate_drawing="off"
  for sub=sel
    subWin=subWins(sub);
    TF=evstr(subWin.expr)
    TF_names=subWin.transfer_names
    n=size(TF_names,"*")
    ax=AnalysersHandles(sub)
    ad=sisotoolGetAnalyserData(ax); //analyser data
    select subWin.type //type names are defined in sisotoolAnalysersGui
      
    case "step"
      if S.Preferences.timeResponsesPref.method==1 then
        [tfinal,dt] =sim_horizon(TF);
         t=0:dt:tfinal;
      else
        if TF.dt=="c" then 
          dt=S.Preferences.timeResponsesPref.step;
        else
          dt=TF.dt
        end
        t=0:dt:S.Preferences.timeResponsesPref.final;
      end
      if TF.dt<>"c" then
        u=ones(t);
        y=flts(u,TF);
      else
        y=csim("step",t,TF)
      end
      t=timeconv("s",units.time,t);
      dy=(max(y)-min(y))/10
      ax.data_bounds=[min(t),min(y)-dy;max(t),max(y)+dy];
      loci=ad.loci
      mainGrid=ad.mainGrid;
      bounds=ax.data_bounds
      db=diff(bounds,1,1)
      for k=1:n; 
        loci(k).data=[t' y(k,:)']; 
        finalValue=sisotoolFinalValue(TF(k,1),"step")
        loci(k).user_data=finalValue
        updateDatatips(loci(k))
        if finalValue<>[] then
          mainGrid(k).data=[[bounds(1,1)-db(1); bounds(2,1)+db(1)], ...
                            [finalValue;finalValue]]
          mainGrid(k).visible="on"
        else
          mainGrid(k).visible="off"
        end
      end
      ad.mainGrid=mainGrid;
      if gridded(sub) then
        ax.grid=ones(1,2)*color("gray");
      else
        ax.grid=-ones(1,2)
      end
      
    case "impulse"
      if S.Preferences.timeResponsesPref.method==1 then
        [tfinal,dt] =sim_horizon(TF);
         t=0:dt:tfinal;
      else
        if TF.dt=="c" then 
          dt=S.Preferences.timeResponsesPref.step;
        else
          dt=TF.dt
        end
        t=0:dt:S.Preferences.timeResponsesPref.final;
      end
      
      if TF.dt<>"c" then
        u=eye(t);
        y=flts(u,TF);
      else
        y=csim("impulse",t,TF)
      end  
      t=timeconv("s",units.time,t);
      dy=(max(y)-min(y))/10
      ax.data_bounds=[min(t),min(y)-dy;max(t),max(y)+dy];
      loci=ad.loci
      mainGrid=ad.mainGrid;
     
      bounds=ax.data_bounds
      db=diff(bounds,1,1)
      for k=1:n; 
        loci(k).data=[t' y(k,:)']; 
        finalValue=sisotoolFinalValue(TF(k,1),"impulse")
        loci(k).user_data=finalValue;
        updateDatatips(loci(k))
        if finalValue<>[] then
          mainGrid(k).data=[[bounds(1,1)-db(1); bounds(2,1)+db(1)], ...
                            [finalValue;finalValue]]
          mainGrid(k).visible="on"
        else
          mainGrid(k).visible="off"
        end
      end
   
      if gridded(sub) then
        ax.grid=ones(1,2)*color("gray");
      else
        ax.grid=-ones(1,2)
      end
    case "gainplot"
      select S.Preferences.freqResponsesPref.method
      case 1 then
        [Fmin,Fmax]=sisotoolFreqBounds(TF)
        [frq,repf]=repfreq(TF,Fmin,Fmax); 
      case 2 then
        range=S.Preferences.freqResponsesPref.range
        [frq,repf]=repfreq(TF,range(1),range(2)); 
      case 3 then
        [frq,repf]=repfreq(TF,S.Preferences.freqResponsesPref.vector)
      end
      [phi,db]=phasemag(repf);
      frq=freqconv("Hz",units.frequency,frq);
      db=magconv("dB",units.magnitude,db);
      ddb=(max(db)-min(db))/10
      ax.data_bounds=[min(frq),min(db)-ddb;max(frq),max(db)+ddb];
      ax.auto_ticks(1)="on";
      ax.x_ticks.labels=emptystr(ax.x_ticks.labels);
      ax.auto_ticks(1)="off";
      loci=ad.loci
      for k=1:n; 
        loci(k).data=[frq(:) db(k,:)']; 
        dfd=loci(k).display_function_data
        [g,fg]=g_margin(TF(k,1),"all")
        kept=find(fg>frq(1)&fg<frq($))
        dfd.gmargin=[fg(kept);g(kept)];
        loci(k).display_function_data=dfd
        updateDatatips(loci(k))
      end
      
      //TBD c'a ne marche pas lorsqu'il y a plusieurs courbes
      //cela depend de la TF qui sera choisie dans optimGui!!
      [gm,frg]=g_margin(TF)
      
      if frg<>[] then
        //For Bode type plots, Gain and phase margin requirements are
        //located at the crossover frequency if it exists, 
        //so so they cannot be treated as usual. See also
        //sisotoolDRawRequirements
        frg=freqconv("Hz",units.frequency,frg);
        gm=magconv("dB",units.magnitude,gm);
        ad.gmargin=[frg,gm] 
        if subWin.reqProps(3).data<>[] then //minimum gain margin requirement is defined
          g=magconv("dB",units.magnitude,subWin.reqProps(3).data); 
          v0=magconv("dB",units.magnitude,0)   
          ad.requirements.children(3).data=[frg v0;frg v0-g]
          ad.requirements.children(3).user_data=subWin.reqProps(3).data
        else
          ad.requirements.children(3).data=[]
          ad.requirements.children(3).user_data=[]
        end
      else
        ad.gmargin=[] 
        ad.requirements.children(3).data=[]
        ad.requirements.children(3).user_data=[]
      end
      
      if gridded(sub) then
        ax.grid=ones(1,2)*color("gray");
      else
        ax.grid=-ones(1,2)
      end
    case "phaseplot"
      select S.Preferences.freqResponsesPref.method
      case 1 then
        [Fmin,Fmax]=sisotoolFreqBounds(TF)
        [frq,repf]=repfreq(TF,Fmin,Fmax); 
      case 2 then
        range=S.Preferences.freqResponsesPref.range
        [frq,repf]=repfreq(TF,range(1),range(2)); 
      case 3 then
        [frq,repf]=repfreq(TF,S.Preferences.freqResponsesPref.vector)
      end
      [phi,db]=phasemag(repf);
      frq=freqconv("Hz",units.frequency,frq);
      phi=phaseconv(_("degrees"),units.phase,phi);
      v180=phaseconv(_("degrees"),units.phase,180);
      mnphi=min(phi)
      mxphi=max(phi)
      if mxphi-mnphi>=v180 then dphi=v180/2;else dphi=v180/6;end
      mnphi=dphi*floor(mnphi/dphi)
      mxphi=dphi*ceil(mxphi/dphi)

      ax.data_bounds=[min(frq),mnphi;max(frq),mxphi];
      phi_ticks=ax.y_ticks
      phi_ticks.locations=mnphi:dphi:mxphi
      if units.phase=="rd" then
        phi_ticks.labels=string(phi_ticks.locations/%pi)+"π";
      else
        phi_ticks.labels=string(phi_ticks.locations)
      end      
      ax.y_ticks=phi_ticks
      ax.tight_limits(2)="off"

      loci=ad.loci
      for k=1:n; 
        loci(k).data=[frq(:) phi(k,:)']; 
        dfd=loci(k).display_function_data
        [p,fp]=p_margin(TF(k,1),"all")
        kept=find(fp>frq(1)&fp<frq($))
        dfd.pmargin=[fp(kept);p(kept)]
        loci(k).display_function_data=dfd
        updateDatatips(loci(k))
      end
    
      //TBD c'a ne marche pas lorsqu'il y a plusieurs courbes
      //cela depend de la TF qui sera choisie dans optimGui!!!
      [phm,frp]=p_margin(TF)
     
      if frp<>[] then
        frp=freqconv("Hz",units.frequency,frp);
        phm=phaseconv(_("degrees"),units.phase,phm);
        ad.pmargin=[frp,phm] 
        if subWin.reqProps(1).data<>[] then //minimum gain margin requirement is defined
           p= phaseconv(_("degrees"),units.phase,subWin.reqProps(1).data)
           ad.requirements.children(1).data=[frp -v180;frp -v180+p]  
           ad.requirements.children(1).user_data=subWin.reqProps(1).data
        else
          ad.requirements.children(1).data=[]
          ad.requirements.children(1).user_data=[]
        end
      else
        ad.pmargin=[] 
      end
      if gridded(sub) then
        ax.grid=ones(1,2)*color("gray");
      else
        ax.grid=-ones(1,2)
      end

    case "nyquist"
      kgrid=find(ax.children.tag=="grid")
      if kgrid<>[]  then //grid exist
        h_grid=ax.children(kgrid)
      else
        h_grid=[]
      end
      select S.Preferences.freqResponsesPref.method
      case 1 then
        [Fmin,Fmax]=sisotoolFreqBounds(TF)
        [frq,repf]=repfreq(TF,Fmin,Fmax); 
      case 2 then
        range=S.Preferences.freqResponsesPref.range
        [frq,repf]=repfreq(TF,range(1),range(2)); 
      case 3 then
        [frq,repf]=repfreq(TF,S.Preferences.freqResponsesPref.vector)
      end
     
      repf=[conj(repf(:,$:-1:1)) repf];
      ax.data_bounds=[min(real(repf)),min(imag(repf));
                      max(real(repf)),max(imag(repf))];
      loci=ad.loci
      for k=1:n; 
        loci(k).data=[real(repf(k,:))' imag(repf(k,:))']; 
        dfd=loci(k).display_function_data
        dfd.freq=frq
        [g,fg]=g_margin(TF(k,1),"all")
        rf=repfreq(TF(k,1),fg)
        kept=find(fg>frq(1)&fg<frq($))
        dfd.gmargin=[fg(kept);g(kept);rf(kept)];
        [p,fp]=p_margin(TF(k,1),"all")
        kept=find(fp>frq(1)&fp<frq($))
        rf=repfreq(TF(k,1),fp)
        dfd.pmargin=[fp(kept);p(kept);rf(kept)]
        loci(k).display_function_data=dfd
        updateDatatips(loci(k))
      end
      
      sca(ax);bounds=ax.data_bounds
      bounds(1,1)=min(-1,bounds(1,1))
      ax.data_bounds=bounds
      db=diff(bounds,1,1)
      mainGrid=ad.mainGrid;
      mainGrid(1).data=[bounds(1,1)-db(1) 0;
                        bounds(2,1)+db(1) 0];
      mainGrid(2).data=[0 bounds(1,2)-db(2);
                        0  bounds(2,2)+db(2)];
      if gridded(sub) then
        delete(h_grid);
        hallchart(colors=color('light gray')*[1 1])
        h_grid=ax.children($)
        h_grid.tag="grid"
        ax.data_bounds= bounds
        //there is no requirement for this analyser.
        //swap_handles(ax.children($),ax.children($-1))
      else
        if kgrid<>[] then delete(h_grid);end
      end
    case "nichols"
      kgrid=find(ax.children.tag=="grid")
      if kgrid<>[]  then //grid exist
        h_grid=ax.children(kgrid)
      else
        h_grid=[]
      end
      select S.Preferences.freqResponsesPref.method
      case 1 then
        [Fmin,Fmax]=sisotoolFreqBounds(TF)
        [frq,repf]=repfreq(TF,Fmin,Fmax); 
      case 2 then
        range=S.Preferences.freqResponsesPref.range
        [frq,repf]=repfreq(TF,range(1),range(2)); 
      case 3 then
        [frq,repf]=repfreq(TF,S.Preferences.freqResponsesPref.vector)
      end
 
      [phi,db]=phasemag(repf);
      cphi=nicholsCriticalPoints(min(phi),max(phi))
      phi=phaseconv(_("degrees"),units.phase,phi);
      cphi=phaseconv(_("degrees"),units.phase,cphi);
      db=magconv("dB",units.magnitude,db);
      
      if ~update then
        v180=phaseconv(_("degrees"),units.phase,180)
        if phi<>[] then
          mnphi=min(cphi(1)-v180/2,min(phi))
          mxphi=max(max(phi),cphi($)+v180/2)
        else
          mnphi=0
          mxphi=v180/2
        end
        if mxphi-mnphi>=v180 then dphi=v180/2;else dphi=v180/6;end
        mnphi=dphi*floor(mnphi/dphi)
        mxphi=dphi*ceil(mxphi/dphi)
        db20=magconv("dB",units.magnitude,20)
        dbm20=magconv("dB",units.magnitude,-20)
        ax.data_bounds=[mnphi min(dbm20,min(db));
                                    mxphi max(max(db),db20)];
         
        phi_ticks=ax.x_ticks
        phi_ticks.locations=mnphi:dphi:mxphi
        if units.phase==_("degrees") then
          phi_ticks.labels=string(phi_ticks.locations)
        else
          phi_ticks.labels=string(phi_ticks.locations/%pi)+"π";
        end
       
        ax.x_ticks=phi_ticks
        ax.tight_limits(2)="off"
      end

      loci=ad.loci
      for k=1:n; 
        loci(k).data=[phi(k,:)',db(k,:)']; 
        dfd=loci(k).display_function_data
        dfd.freq=frq
        [g,fg]=g_margin(TF(k,1),"all")
        kept=find(fg>frq(1)&fg<frq($))
        dfd.gmargin=[fg(kept);g(kept)];
        [p,fp]=p_margin(TF(k,1),"all")
        kept=find(fp>frq(1)&fp<frq($))
        dfd.pmargin=[fp(kept);p(kept)]
        loci(k).display_function_data=dfd
        updateDatatips(loci(k))
      end
      sca(ax);bounds=ax.data_bounds
      dbnd=diff(bounds,1,1)
      phi=phaseconv(_("degrees"),units.phase,-180);
      db=magconv("dB",units.magnitude,0);
      mainGrid=ad.mainGrid;
      mainGrid(1).data=[bounds(1,1)-dbnd(1) db;
                        bounds(2,1)+dbnd(1) db];
      //mainGrid(2).data=[phi bounds(1,2)-dbnd(2);
      //                  phi bounds(2,2)+dbnd(2)];
      bndl=bounds(1,2)-dbnd(2) ;bndh=bounds(2,2)+dbnd(2);
      mainGrid(2).data=[matrix([cphi;cphi],-1,1),...
                        matrix([bndl*ones(cphi) ;bndh*ones(cphi)],-1,1)]
                    
      mainGrid(3).data=[cphi',db*ones(cphi')];
      
      delete(h_grid)
      if gridded(sub) then
         nicholschart()
         h_grid=ax.children($)
         h_grid.tag="grid"
         if units.phase<>_("degrees")| units.magnitude<>"dB" then
           scaleNicholsChart(h_grid,units.phase,units.magnitude)
         end
         ax.data_bounds= bounds
         //put the grid over the requirements
         swap_handles(ax.children($),ax.children($-1))
      end
    case "pole/zero"  
      kgrid=find(ax.children.tag=="grid")
      if kgrid<>[]  then //grid exist
        h_grid=ax.children(kgrid)
      else
        h_grid=[]
      end

      loci=ad.loci
      data_bounds=[%inf,%inf;-%inf,-%inf];
      mxx=-%inf;mnx=%inf;mxy=-%inf;
      for k=1:n
        TFk=evstr(subWin.expr(k)) //evaluate each TF individually to avoid numerical problems
        if typeof(TFk)=="state-space" then
          [z,p]=ss2zp(TFk)
        elseif typeof(TFk)=="rational" then
          [z,p]=tf2zp(TFk)
        end
        if p<>[] then
          mnx=min(mnx,min([real(p);0]));
          mxx=max(mxx,max([real(p);0]));
          mxy=max(mxy,max([imag(p);0]));
        end
        loci(k).data=[real(p),imag(p)]
        mark_style=loci(k).mark_style;
        mark_size=loci(k).mark_size
        for l=1:size(p,"*")
           d=sisotoolDatatipCreate(loci(k),l);
           d.mark_style=mark_style
           d.mark_size=mark_size
        end
        updateDatatips(loci(k))
        if z<>[] then
          mnx=min(mnx,min([real(z);0]));
          mxx=max(mxx,max([real(z);0]));
          mxy=max(mxy,max([imag(z);0]));
        end
        loci(n+k).data=[real(z),imag(z)]
        mark_style=loci(n+k).mark_style;
        mark_size=loci(k).mark_size
        for l=1:size(z,"*")
           d=sisotoolDatatipCreate(loci(n+k),l);
           d.mark_style=mark_style
           d.mark_size=mark_size
        end
        updateDatatips(loci(n+k))
      end
      dx=max(0.5,(mxx-mnx)/4);
      dy=max(0.5,mxy/2);
      data_bounds=[mnx-dx -mxy-dy;mxx+dx mxy+dy]
      
      mainGrid=ad.mainGrid
      mainGrid(1).data=[data_bounds(:,1),[0;0]]
      mainGrid(2).data=[[0;0] data_bounds(:,2)]
      if  gridded(sub) then 
        delete(h_grid)
        if S.Designs.dt=="c" then sgrid();else zgrid();end
        h_grid=ax.children($)
        h_grid.tag="grid"
        //put the grid over the requirements
        swap_handles(ax.children($),ax.children($-1))
      end
      if ~or(isinf(data_bounds)) then
        ax.data_bounds=data_bounds;
      end
    end
    sisotoolSetAnalyserData(ad,ax)
  end
   for sub=sel
    // update characteristics
    ax=AnalysersHandles(sub)
    sca(ax)
    analyserType=ax.tag; 
    analyserData=sisotoolGetEditorData(ax)
    handles=analyserData.characteristics_handles
    cKeys=sisotoolCharacteristicsTypes(analyserType)
    characteristics_settings=subWin.characteristics_settings(1:size(cKeys,"*"));
    cKeys=cKeys(characteristics_settings)
   
    for  cKey=cKeys
      if and(fieldnames(handles)<>cKey) then
        handles=sisotoolManageCharacteristics(sub,cKey,handles,"create")
      end
      handles=sisotoolManageCharacteristics(sub,cKey,handles,"update")
    end
    analyserData.characteristics_handles=handles
    sisotoolSetEditorData(analyserData,ax)
  end
  winHandle.immediate_drawing="on"
 
endfunction
