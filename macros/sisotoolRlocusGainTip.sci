function str=sisotoolRlocusGainTip(h)
  try
  h.mark_size_unit="point";
  h.mark_size=6;
  h.mark_style=11;
  h.mark_foreground=6;
  h.mark_background=6;
  c=h.parent
  dfd=c.display_function_data;
  loci=dfd.loci
  dt=dfd.dt
  u=dfd.units.frequency
  //find the branch
  p=h.data(1:2)*[1;%i]
  [wn,z] = damp(p,dt)
  n=size(loci,"*");
  m=zeros(1,n);k=zeros(1,n);
  for l=1:n
    [m(l),k(l)]=min(abs(loci(l).data*[1;%i]-p));
  end
  [m,l]=min(m);//l is the branch index
  k=k(l);
  if k>size(loci(l).display_function_data,"*") then
    K=loci($).display_function_data(k);
  else
    K=loci(l).display_function_data(k);
  end
  wn=freqconv("rd/s",u,wn);
  str=msprintf(_("Loop gain: %.3g\nClosed loop pole\nDamping:%.3g\nNatural pulsation %.3g%s"),K,z,wn,u)
catch
  disp(lasterror());pause
  end
endfunction
