// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [Ts,Tf,StableFlag] = timegrid(sys,Tf)

  if typeof(sys)=="rational" then
    sys=tf2ss(sys)
  end

NoTf = argn(2)<2
StableFlag = %t;
[ny,nu] = size(d);
nx = size(a,1);
Tfmin = 0

// Parameters
toljw = sqrt(%eps);
Nsmin = 100;// min number of time steps
Nsmax = 2000;// max number of time steps till settling
Nsu = 400;// number of points for unstable sim.
Nslimit = 50000;// max number of samples no matter what
SettlingThresh = 0.005;// settling threshold
DominantThresh = 0.01;// threshold to pick dominant modes

// Compute eigendecomposition
[v,p] = spec(a);
p = diag(p);// system poles 
rp = real(p);

if norm(sys.c)==0 then
  Ts = 0.1;
  if NoTf then Tf = max(1,Tfmin);end;
  return;
end;

// Treat unstable, marginally stable, and stable cases separately
if or(rp>toljw) then
  // Unstable system or infinite dc gain (pole at s=0)
  rp(rp<=toljw) = 1e-2;  % integrators
  if NoTf then
    // Simulate until exp(m*t)=1d15 if no Tf specified
    Tf = max(log(1d15)/max(rp),Tfmin);
  end;
  Ts = Tf/Nsu;
  StableFlag = %f;
else
  // Stable or marginally stable
  // Treat together to be robust to cancelling pseudo-integrators (g338843)
  gamma = abs(c*v);
  // RE: Do not make TOL too large, affects ability to estimate DC contribution
  // REVISIT: use rank-revealing QR instead of SVD
  beta = abs(pinv(v,0.00000001)*x0);

  // Estimate amplitude of Y variation for each I/O pair
  DeltaY = max(abs(d),0.01*(abs(d)+abs(c)*abs(x0)));

  // Find dominant modes 
  isDominant = zeros(nx,1)==1;
  for j = 1:nu
    for i = 1:ny
      contrib = gamma(i,:) .*((beta(:,j)).');
      // RE: First term to protect against bad dc value
      domthresh = min(max(contrib)/10,DominantThresh*DeltaY(i,j)); 
      isDominant(contrib >= domthresh) = %t;

    end;
  end;
  p = p(isDominant);
  rp = rp(isDominant);

  // Marginally stable dominant modes
  pms = p(rp>=(-toljw));
  StableFlag = isempty(pms);

  // Time constants for stable dominant modes
  timct = log(SettlingThresh) ./rp(rp<(-toljw));  // time constants
  MinTC = min(timct); // min time constant
  MaxTC = max(timct);  // max time constant

  // Final time
  if NoTf then
    // Show contribution of slowest stable mode and marginally
    // stable mode with largest period
    Tf = max([30 ./(1+abs(pms)) ; MaxTC ; Tfmin]); // always >0
  end;

  // Sample time
  // At least 5 points before settling for fastest mode, 50 points for
  // slowest mode
  Ts = min([%inf , MinTC/5 , MaxTC/50]);
  // At least 20 points/period for underdamped modes

  pRes = p(abs(rp)<(abs(p)/2));
  if ~isempty(pRes) then
    Ts = min(Ts,pi/10/max(abs(pRes)));
  end;
  // Use NSU points for models with only marginally stable modes
  if isinf(Ts) then
    Ts = Tf/Nsu;
  end;

  // Out of memory safeguards
  if NoTf then
    // Unspecified final time: protect against ultrafast dynamics or very HF modes
    Ts = max(Ts,Tf/(10*Nsmax));
  else
    // Specified final time: protect against final time much larger than
    // system''s time constant (can happen in FILLRESP, see g258770), and
    // ensure at least NSMIN steps in simulation
    Ts = max(min(Ts,Tf/Nsmin),Tf/Nslimit);
  end;

end;
endfunction
