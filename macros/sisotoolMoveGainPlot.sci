// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolMoveGainPlot(mainH,editorData,entityType,i,k,pt,loc)
  ax=gca();//current axes has been set by sisotoolEventHandler
  figureHandle=ax.parent
  S=sisotoolGetSession(mainH)
  sys=S.Designs
  units=S.Preferences.units

  if entityType=="requirements" then
    //see sisotoolRequirementArea
    req=ax.children($).children(i)
    ds=req.data//keep it in case of no move
    if or(req.tag==["gmin" "gmax"]) then
      d=ds
      np=size(d,1)
      ind=loc(1)
      if loc(2)<0.1|loc(2)>0.9 then
        //a vertex has been selected move it
        if loc(2)>0.9 then ind=ind+1;end//right point
         while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          d(ind,1:2)=rep(1:2)
          //insure we have a function
          if ind>2 then d(ind,1)=max(d(ind,1),d(ind-1,1));end
          if ind<np-2 then d(ind,1)=min(d(ind,1),d(ind+1,1));end
          
          if ind==2 then d(1,1)=d(ind,1);end
          if ind==np-2 then d(np-1,1)=d(ind,1);end
          req.data=d;
         end 
         d=d(2:$-2,:)
         req.user_data=d
      elseif d(ind,1)==d(ind+1,1) then
        // a vertical segment selected translate it horizontally
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          d(ind:ind+1,1)=rep(1)
          req.data=d;
        end 
        d=d(2:$-2,:)
        req.user_data=d
      else
        //a  horizontal segment selected translate it vertically
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          dd=rep(2)-d(ind,2)
          d(ind:ind+1,2)= d(ind:ind+1,2)+dd //vertical translation
          req.data=d;
        end 
        d=d(2:$-2,:)
        req.user_data=d
      end
      if or( req.data<>ds) then
        requirementIndex=i
        subWinIndex=editorData.subWinIndex
        windowType=ax.parent.tag
        Settings= windowType+"Settings"
        subWin= S(Settings).subWins(subWinIndex)
        reqProp=subWin.reqProps(requirementIndex)
        opt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type);
        S=sisotoolAddHistory(S,"Requirements","update",...
                             windowType,subWinIndex,requirementIndex,reqProp,opt);
        d(:,1)=freqconv(units.frequency,"Hz",d(:,1));
        d(:,2)=magconv(units.magnitude,"dB",d(:,2));
        reqProp.data=d;
        subWin.reqProps(requirementIndex)=reqProp;
        S(Settings).subWins(subWinIndex)=subWin
        sisotoolSetSession(S,mainH)
      end
    elseif req.tag=="gmargin" then //TBD?

      if loc(2)>0.5 then
        z=magconv("dB",units.magnitude,0);
        //mprintf("Move %s not yet implemented\n",req.tag)
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          req.data(2,2)=min(z,rep(2));
        end
        d=req.data(2,2)
        req.user_data(2,2)=req.data(2,2);
      end
      if or( req.data<>ds) then
        requirementIndex=i
        subWinIndex=editorData.subWinIndex
        windowType=ax.parent.tag
        Settings= windowType+"Settings"
        subWin= S(Settings).subWins(subWinIndex)
        reqProp=subWin.reqProps(requirementIndex)
        opt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type);
        S=sisotoolAddHistory(S,"Requirements","update",...
                             windowType,subWinIndex,requirementIndex,reqProp,opt);
        
        d=-magconv(units.magnitude,"dB",d);
        reqProp.data=d;
        subWin.reqProps(requirementIndex)=reqProp;
        S(Settings).subWins(subWinIndex)=subWin
        sisotoolSetSession(S,mainH)
      end
    end
    
    figureHandle.info_message="";
    return
  end

  sys=S.Designs
  editorData=sisotoolGetEditorData(ax)
  compensatorName=editorData.tunable
  C=sys(compensatorName)
 
  dt=C.dt
  if entityType=="loci" then
    entityType="gain",k=1
  end
  if entityType=="notches_width" then
    entityName="notches"
  else
    entityName=entityType
  end
  cur=sisotoolGetCElement(S,compensatorName,entityName,k);
  S=sisotoolAddHistory(S,"Compensator","update",compensatorName,entityName,k,cur);
  if or(entityType==["zeros" "poles"]) then
    pz=1-2*(entityType=="poles")
    
    if entityType=="zeros" then
      info_message=_("Drag this zero  to the desired location.")+" "+ ...
          _("Current value:%s")
    else
      info_message=_(" Drag this pole to the desired location.")+" "+ ...
          _("Current value:%s")
    end
    if imag(C(entityName)(k))==0 then //real  case
      figureHandle.info_message=msprintf(info_message,string(C(entityName)(k)))
      //horizontal move controls natural frequency, vertical move does nothing
      while %t
        set("current_axes", ax);
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        wn=freqconv("Hz","rd/s",rep(1))
        z=wn
        if dt<>"c" then z=exp(z*dt);end
        z=z*sign(C(entityName)(k))
        C(entityName)(k)=z;
        figureHandle.info_message=msprintf(info_message,string(z))
        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
      end
    else //complex pair case
      //horizontal move controls natural frequency, vertical moves controls damping factor

      //The OL transfer function H(s) wrotes H1(s)/((s-r)*(s-r') where r is
      //the moved pole.
      //In bode plot, the image of a root r is located on the gain locus at
      //the abscissae wr corresponding to its natural frequency

      //let ro the initial value of the pole and  rn the value of the
      //pole to be determined according to the mouse displacement. Let
      //similarily Ho(s) the value of the transfer function before the
      //move and Hn(s) the value of the transfer function after the move.
      // |Ho(s)|/|Hn(s)|=|(s-rn)*(s-rn')|/|(s-ro)*(s-ro')| assuming the
      // gain plot of Hn passes by the pointer location 

      //let (wn,yn) is the coordinates of the pointer location 
      //we have yo=|Ho(%i*wn)| and  yn=|Hn(%i*wn)|
      //        yn/yo=|(%i*wn-ro)*(%i*wn-ro')|/|(%i*wn-rn)*(%i*wn-rn')|

      //let ro=wo*(zo-%i*sqrt(1-zo^2)) and rn=wn*(zn-%i*sqrt(1-zn^2)) 
      //    where zo and zn are the damping factors of ro and rn
      //let q=(yn/yo)
      // we have the equation in zn
      // (sqrt((-2*w*wo*zo)^2+(-w^2+wo^2)^2))=q*(2*w^2*zn)

      h=editorData.loci(1)
      data=h.data
      ro=C(entityName)(k);
      [wo,zo]=damp(ro,dt)
      yo=magconv(units.magnitude,"absolute",...
                 interpln(h.data',freqconv("rd/s",units.frequency,wo)))
      while %t
        set("current_axes", ax);
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        wn=freqconv(units.frequency,"rd/s",rep(1));
        yn=magconv(units.magnitude,"absolute",rep(2));
        q=(yn/yo)^pz
        zn=min(1,q*sqrt(4*(wn*wo*zo)^2+(wo^2-wn^2)^2)/(2*wn^2))
        rn=wnzeta2complex(dt,wn,sign(real(ro))*zn)
        C(entityName)(k)=rn
        if abs(ro-rn)/abs(ro) >1e-3 then
          figureHandle.info_message=msprintf(info_message,string(rn))
          sys(compensatorName)=C;
          sisotoolSetSys(sys,mainH);
          sisotoolUpdateResponses(mainH);
          yo=yn;
          ro=rn
          wo=wn,zo=zn;
        end
      end
    end
    if rep==-1000 then sys=[];end //the tool has been closed
    figureHandle.info_message=""
  elseif entityType=="gain" then
    //entityType="gain";k=1;
    info_message=_("Drag Gain curve up or down to adjust the loop gain.")+" "+ ...
          _("Current gain:%s")
    figureHandle.info_message=msprintf(info_message,string(C.gain))
    h=editorData.loci(i);
    f=h.data(:,1);
    while %t
      set("current_axes", ax);
      g=h.data(:,2)//the gain curve discretization
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      frq=rep(1);
      new_g=rep(2) //desired  gain value at frequency frq (in dB)
      old_g=interpln(h.data',frq) //current gain value in dB
      figureHandle.info_message=msprintf(info_message,string(sys(compensatorName).gain))
      C=sys(compensatorName)
      C.gain=real(sys(compensatorName).gain*(10^((new_g-old_g)/20)));
      sys(compensatorName)=C
      sisotoolSetSys(sys,mainH);
      sisotoolUpdateResponses(mainH);
    end
    figureHandle.info_message=""
    if rep==-1000 then sys=[];end //the tool has been closed
  
  elseif entityType=="leadslags" then
    if i==1 then
      info_message=_("Drag this zero  to the desired location.")+" "+ ...
          _("Current value:%s")
    else
      info_message=_(" Drag this pole  to the desired location.")+" "+ ...
          _("Current value:%s")
    end
    figureHandle.info_message=msprintf(info_message,string(C(entityName)(i,k)))
    while %t
      set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      wn=freqconv("Hz","rd/s",rep(1))
      z=wn
      if dt<>"c" then z=exp(z*dt);end
      z=z*sign(C(entityName)(k))
      C(entityName)(i,k)=z;
      figureHandle.info_message=msprintf(info_message,string(z))
      sys(compensatorName)=C;
      sisotoolSetSys(sys,mainH);
      sisotoolUpdateResponses(mainH);
    end
    figureHandle.info_message=""
    if rep==-1000 then sys=[];end //the tool has been closed
 
    sys(compensatorName)=C;    
  elseif entityType=="notches" then
    //Horizontal move changes only affects the pole/zero natural frequency wn
    //Vertical move affects the notch depth through the zero damping factor zetaz
    
    //H(s)=Hr(s)*N(s) 
    //N(s)=(s^2/wn^2+2*zetaz/wn*s+1)/(s^2/wn^2+2*zetap/wn*s+1)
    //The notch image is at (wn, |H(%i*wn)|) = (wn,m=|Hr(%i*wn)|*zetaz/zetap)
    //if one has selected a point (wn1,m1=|H1(%i*wn1)|) 
    //the notch filter that produce an image at this point is given by 
    // wn1, zetap and zetaz1=m1*zetap/|Hr(%i*w1)|
     info_message=_("Drag Notch  to adjust its depth and natural frequency.")+" "+ ...
        _("Current depth:%s, wn:%s")
    
    z=C(entityName)(1,k);
    p=C(entityName)(2,k);
    [wn0,zetaz]=damp(z,C.dt)
    [wn0,zetap]=damp(p,C.dt)
    
    
    Hr=sisotoolSys2OL(sys,compensatorName,list(entityName,k));
    figureHandle.info_message=msprintf(info_message,string(wn0),string(zetaz/zetap))
    while %t
      set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      w1=freqconv("Hz","rd/s",rep(1));//new natural frequency
     
      m1=10^(rep(2)/20);// from dB to linear
    
      Hr_w1=repfreq(Hr,rep(1))
      zetaz1=min(1,m1*zetap/abs(Hr_w1)); 
      figureHandle.info_message=msprintf(info_message,string(w1), ...
                                         string(zetaz1/zetap))
      z=wnzeta2complex(C.dt,w1,zetaz1)
      p=wnzeta2complex(C.dt,w1,zetap)
      C(entityName)(:,k)=[z;p];

      sys(compensatorName)=C;
      sisotoolSetSys(sys,mainH);
      sisotoolUpdateResponses(mainH);
    end
    figureHandle.info_message=""
  elseif entityType=="notches_width" then
    cur=sisotoolGetCElement(S,compensatorName,entityName,k);
    info_message=_("Drag the mark along the frequency axis to adjust notch width.")+" "+_("Current width:%s")
    df=0.25;
    z=C.notches(1,k);
    p=C.notches(2,k);
    [wn,zetaz]=damp(z,C.dt)
    [wn,zetap]=damp(p,C.dt)
    depth=zetaz/zetap;
    dr=depth.^(df*2);
    t1=(-depth^2+dr);
    t2=t1*(dr-1);

    width=sisotoolNotchWidth(zetaz,zetap)
    figureHandle.info_message=msprintf(info_message,string(width))
    lf=editorData(entityType)(1).data(k,1)
    rf=editorData(entityType)(2).data(k,1)
    xpoly([lf;lf],ax.data_bounds(:,2));pl=gce();pl.foreground=11,pl.line_style=2;
    xpoly([rf;rf],ax.data_bounds(:,2));pr=gce();pr.foreground=11,pr.line_style=2;
    while %t
      set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      r=freqconv("Hz","rd/s",rep(1))/wn; //the new P polynomial root value
      b=-(r+1/r); //the new degree 1 coefficient of the 2nd order polynomial
      zetap=min(1,sqrt(t2*(b+2))/t1);
      zetaz=depth*zetap
      width=sisotoolNotchWidth(zetaz,zetap)
      figureHandle.info_message=msprintf(info_message,string(width))
      z=wnzeta2complex(C.dt,wn,zetaz)
      p=wnzeta2complex(C.dt,wn,zetap)  
      C.notches(:,k)=[z;p];
      sys(compensatorName)=C;
      sisotoolSetSys(sys,mainH);
      sisotoolUpdateResponses(mainH);
      lf=editorData(entityType)(1).data(k,1);pl.data(:,1)=lf
      rf=editorData(entityType)(2).data(k,1);pr.data(:,1)=rf
    end
    delete(pl);delete(pr)
    figureHandle.info_message=""
  end
  //update optimSettings
  S=sisotoolUpdateCElement(S,compensatorName,entityName,k,C(entityName)(:,k))
  sisotoolSetSession(S,mainH);
endfunction
