// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolUpdateComponent()

  sisoguilib=lib(sisotoolPath()+"/macros/gui")
  [ok,value]=sisotoolCheckNumValue();
  if ~ok then return;end
  editedHandle=gcbo
 
  gui=editedHandle.parent.parent;
  ud=get(gui,"user_data");
  mainH=ud.mainH;
  
  selectionHandles=ud.selectionHandles;
  DisplayHandle=selectionHandles.display;
  compensatorDisplay=DisplayHandle.children(1);
  compensatorParts=DisplayHandle.children(2)
  compensatorName=compensatorParts.string(compensatorParts.value);

  //set(editedHandle,"BackgroundColor",lightgray());
  EditorFrame=editedHandle.parent
  ComponentData=get(EditorFrame,"user_data")
  entityIndex=ComponentData.entityIndex
  entityType=ComponentData.type
  // data relative to the selection GUI
  selectionIndex=ComponentData.selectionIndex
  //data relative to the edition gui 
  ed_index=editedHandle.user_data; //which field has been modified
  S=sisotoolGetSession(mainH);
  units=S.Preferences.units
  
  cur=sisotoolGetCElement(S,compensatorName,entityType,entityIndex);
  S=sisotoolAddHistory(S,"Compensator","update",compensatorName,entityType,entityIndex,cur);

  sys=S.Designs;
  C=sys(compensatorName);
  dt=C.dt
  if entityType=="gain" then
    C.gain=value
  elseif or(entityType==["zeros" "poles"]) then
    z=C(entityType)(entityIndex);
    if imag(z)==0 then //real case
      z=value;
      C(entityType)(entityIndex)=z;
    else
      select ed_index //which value has been modified
      case 1 then //real part
        z=value+imult(imag(z));
      case 2 then //imaginary part
        z=real(z)-imult(abs(value));
      case 3 then // frequency
        if sisotoolSetError(editedHandle,value<=0,_("Positive value expected")) then return;end
        [wn,zeta]=damp(z,dt)
        wn=freqconv(units.frequency,"rd/s",value);
        z=wnzeta2complex(dt,wn,zeta)
      case 4 then //damping
        msg=msprintf(_("Expected value must be in the interval [%g %g]"),-1,1)
        if sisotoolSetError(editedHandle,value<-1|value>1,msg) then return;end
        [wn,zeta]=damp(z,dt)
        z=wnzeta2complex(dt,wn,value)
      end
      C(entityType)(entityIndex)=z;
    end
  elseif entityType=="leadslags" then
    z=C(entityType)(1,entityIndex);
    p=C(entityType)(2,entityIndex);
    if dt=="c" then
      if abs(z)>abs(p) then 
        Type="lags";
        t=-1
        msg=_("|zero|>|pole| expected")

      else
        Type="leads";
        t=1
        msg=_("|zero|<|pole| expected")
      end
  
    else
      if abs(p)>abs(z) then 
        Type="lags";
        t=-1
        msg=_("0<|pole|<|zero|<1 expected")
      else
        Type="leads";
        t=1
        msg=_("0<|zero|<|pole|<1 expected")
       end
    end
   
    select ed_index //which value has been modified
    case 1 then //zero
      z=value
      if dt=="c" then
        if Type=="lags" then
          msg=_("|zero|>|pole| expected")
          if sisotoolSetError(editedHandle,damp(z,dt)<=damp(p,dt),msg) then return;end
        else
          msg=_("|zero|<|pole| expected")
          if sisotoolSetError(editedHandle,damp(z,dt)>=damp(p,dt),msg) then return;end
        end
      else
        if Type=="lags" then
          msg=_("0<zero<pole<1 expected")
          if sisotoolSetError(editedHandle,z>=p|z<=0,msg) then return;end
        else
          msg=_("0<pole<zero<1 expected")
          if sisotoolSetError(editedHandle,z>=1|z<=p,msg) then return;end
        end
      end
    case 2 then //pole
      p=value
      if dt=="c" then
        if Type=="lags" then
          msg=_("|zero|>|pole| expected")
          if sisotoolSetError(editedHandle,damp(z,dt)<=damp(p,dt),msg) then return;end
        else
          msg=_("|zero|<|pole| expected")
          if sisotoolSetError(editedHandle,damp(z,dt)>=damp(p,dt),msg) then return;end
        end
      else //discrete time
        if Type=="lags" then
          msg=_("0<zero<pole<1 expected")
          if sisotoolSetError(editedHandle,p<=0|p<=z,msg) then return;end
        else
          msg=_("0<pole<zero<1 expected")
          if sisotoolSetError(editedHandle,p>z|p>=1,msg) then return;end
        end
      end
    case 3 then //max phase
      if Type=="leads" then
        limit=phaseconv(_("degrees"),units.phase,90)
        msg=msprintf(_("Expected value must be in the interval [%g %g]"),0,limit)
        if sisotoolSetError(editedHandle,value<=0|value>limit) then return;end
      else
        limit=phaseconv(_("degrees"),units.phase,-90)
        msg=msprintf(_("Expected value must be in the interval [%g %g]"),limit,0)
        if sisotoolSetError(editedHandle,value<limit|value>0) then return;end
      end
      [wmax,phimax]=LeadLag2Properties(z,p,dt);
      phimax=phaseconv(units.phase,"rd",value);//convert in rd
      [z,p]=Properties2LeadLag(wmax,phimax,dt)
    case 4 then // frequency  
      msg=_("Expected value must be positive")
      if sisotoolSetError(editedHandle,value<=0,msg) then return;end
      [wmax,phimax]=LeadLag2Properties(z,p,dt)
      wmax=freqconv(units.frequency,"rd/s",value);
      [z,p]=Properties2LeadLag(wmax,phimax,dt)
      // alpha=abs(z/p);
      // z=-wn*sqrt(alpha);
      // p=z/alpha
    end
    C(entityType)(1,entityIndex)=z
    C(entityType)(2,entityIndex)=p
  elseif or(entityType=="notches") then
    z=C(entityType)(1,entityIndex);
    p=C(entityType)(2,entityIndex);
    
    [wn,zetaz]=damp(z,dt)
    [wn,zetap]=damp(p,dt)
    select ed_index //which value has been modified
    case 1 then //frequency (rd/s)
      msg=_("Positive value expected");
      if sisotoolSetError(editedHandle,value<=0,msg) then return;end
      wn=freqconv(units.frequency,"rd/s",value);
    case 2 then //zetaz
      msg=msprintf(_("Expected value must be in the interval [%g %g]"),0,min(1,zetap));
      if sisotoolSetError(editedHandle,value<=0|value>=zetap,msg) then return;end
      zetaz=value
    case 3 then //zetap
      msg=msprintf(_("Expected value must be in the interval [%g %g]"),zetaz,1);
      if sisotoolSetError(editedHandle,value<=zetaz|value>1,msg) then return;end
      zetap=value
    case 4 then //depth (dB)
      limit=magconv("absolute",units.magnitude,1);
      msg=msprintf(_("Expected value must be less than %g"),limit)
      if sisotoolSetError(editedHandle,value>=limit,msg) then return;end
      value=magconv(units.magnitude,"absolute",value)
      // Calculate  width from old zetap and depth
      depth=value
      width=sisotoolNotchWidth(zetaz,zetap)
      //compute zetap from new width and new depth value
      zetap=notchzetap(depth,width)
      //compute zetaz
      zetaz=depth*zetap
    case 5 then //width (log10)
      // Calculate  depth from old zetap
      depth=zetaz/zetap;
      vlimit=log10(notchwidthlimit(depth))
      msg=msprintf(_("Expected value must be less than %g"),vlimit)
      if sisotoolSetError(editedHandle,value>vlimit,msg) then return;end
      width=10^value
      //compute new zetap with given width 
      zetap=notchzetap(depth,width)
      zetap=max(min(zetap,1),0)
      //compute zetaz
      zetaz=depth*zetap
    end
    z=wnzeta2complex(dt,wn,zetaz)
    p=wnzeta2complex(dt,wn,zetap)
    C(entityType)(1,entityIndex)=z;
    C(entityType)(2,entityIndex)=p;
  end
  
  S.Designs(compensatorName)=C
  sisotoolSetSession(S,mainH);
  sisotoolSetCGUI(gui)
  sisotoolSetLQGGui(S.LQGGui)
  sisotoolRedraw(mainH)

endfunction

function wlim=notchwidthlimit(depth)
//compute max  notch width  that insure zetap<1 for a given depth
//code generated with Maple:
//  dp:=1/4:
//  Alpha:=depth^dp:
//  beta:=sqrt(zetap^2*(Alpha^2-depth^2)/(1-Alpha^2)):
//  width:=1 + 2*beta^2 + 2*beta*sqrt(1+beta^2);
//  wlim:=eval(width,zetap=1);
//  with(CodeGeneration):Matlab(wlim,optimize);
  t1=sqrt(depth);
  t2=depth^2;
  t6=1/(-t1+1)*(t1-t2);
  wlim=2*sqrt(t6+1)*sqrt(t6)+2*t6+1;
endfunction
function zetap=notchzetap(depth,width)
 //generated by the following Maple code
// dp:=1/4:
// Alpha:=depth^dp:
// beta:=sqrt(zetap^2*(Alpha^2-depth^2)/(1-Alpha^2)):
// w:=1 + 2*beta^2 + 2*beta*sqrt(1+beta^2):
// with(CodeGeneration):
// S:=solve(w=width,zetap): s:=S[2];
// Matlab(s,resultname="zetap",optimize);
   t1=depth^2;
   t2=sqrt(depth);
   t5=t2*depth;
   t8=-t1*depth+2*t2*t1-2*depth-t1+t2+t5;
   zetap=min(1,abs(sqrt(-width*t8*(t5+3*t2-3*depth-1)) / width/t8*(width-1)/2));
endfunction

