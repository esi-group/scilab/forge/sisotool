// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [z,p]=Properties2LeadLag(wmax,phimax,dt)
//Continuous time case
//  assume(w>0);assume(z<0);assume(p<z);
//  solve({wmax=sqrt(z*p),tan(phimax)=-wmax*(p-z)/(2*z*p)},{z,p}); 
//Discrete time case
// assume(w>0);assume(0<z,z<1);assume(0<p,p<1);assume(p>z);
// z2:=z^2;p2:=p^2;zp:=z*p;t:=sqrt(p2*z2-p2-z2+1);
// S:=solve({tw=t/(p+z),tphi=(-(p-z)*t/(z*p^3-zp-p2+1))/((z2-1)/(zp-1))},{z,p});  
  if dt=="c" then
    tphi=tan(phimax);
    R=tphi-sqrt((tphi^2+1))
    z=R*wmax;
    p=wmax/R;
  else
    tphi=tan(phimax);
    tw=tan(wmax*dt);
    R=sqrt(real(roots((tphi*tw-1)^2*%z^2 -2*((tphi*tw)^2+2*tw^2+1)*%z+(tphi*tw+1)^2)))
    z=R(R<1)
    t=tphi*(z^2-1)
    p=-(tphi*tw+1)*(t+tw*z^2)/(tphi*tw-1)/(t+tw)/z;
    //if phimax>0 then [z,p]=(p,z);end
  end
endfunction
