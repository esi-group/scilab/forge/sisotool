// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAddComplexZeros(win)
  mainH=get_figure_handle(win);
  mainH.event_handler_enable = "off";
  mainH.info_message=_("Click left where you want to add this zero");
  ax=gca()//current axes has been set by sisotoolEditorsEvents
  [btn,pt]=getPointInSubwin(ax) ;

  mainH.info_message="";;
  if pt==[] then 
    mainH.event_handler_enable = "on";
    return,
  end
  typ=ax.tag;
  editorData=sisotoolGetEditorData(ax);
  compensatorName=editorData.tunable;
  S=sisotoolGetSession(mainH);
  dt=S.Designs(compensatorName).dt;

  ok=%f;
  if typ=='rlocus' then
    v= pt(1)-imult(abs(pt(2)));
    ok=%t
  elseif or(typ==["olbm" "olbp" "clbm" "clbp"]) then
    //add a pair of real stable zeros 
    u=S.Preferences.units.frequency;
    wn=freqconv(u,"rd/s",-pt(1));
    zetaz=0.05
    v=wnzeta2complex(dt,wn,zetaz)

    ok=%t;
  elseif typ=="nichols" then
    h=editorData.loci
    [d,ptp,ind,c]=orthProj(h.data,pt);
    if ind<>[] then
      u=S.Preferences.units.frequency;
      frq=h.display_function_data.freq;
      wn=-freqconv(u,"rd/s",frq(ind)+(frq(ind+1)-frq(ind))*c);
      zetaz=0.05
      v=wnzeta2complex(dt,wn,zetaz)
      ok=%t;
    end
  end

  if ok then
    S=sisotoolAddHistory(S,"Compensator","add",compensatorName,"zeros",v);
    S=sisotoolSetCElement(S,compensatorName,"zeros",v);
    sisotoolSetSession(S,mainH);  
    sisotoolRedraw(mainH);
    sisotoolUpdateOptimGui(mainH);
  end
  mainH.event_handler_enable = "on";
endfunction
