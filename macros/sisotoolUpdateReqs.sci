// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolUpdateReqs(ax)
  //update requirements forbidden areas to take into account the boundary changes
  req=ax.children($).children;
  for k=1:size(req,"*")
    if req(k).user_data<>[] then 
      d=sisotoolRequirementArea(ax.tag,req(k).tag,req(k).user_data, axesbounds(ax))
      if d<>[] then
        req(k).data=d;
      end
    else
      req(k).data=[];
    end
  end
endfunction
