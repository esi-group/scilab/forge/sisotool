function r=pade(T,n)
// The coefficients of the Pade approximation are given by the 
// recursion   h[l+1] = (n-l)/(2*n-l)/(l+1) * h[l],  h[0] = 1
// and  exp(-T*s) ~ sum( h[l] (-T*s)^l ) / sum(h[l] (T*s)^l)
   h=1;
   for l = 0:n-1,
     h = [h, h($)*(n-l)/(2*n-l)/(l+1)];
   end
   num=sum(h.*(-T*%s).^(0:n));
   den=sum(h.*(T*%s).^(0:n));
   f=coeff(den,n)
   r=syslin("c",num/f,den/f);
endfunction
