// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function str=sisotoolNicTips(h)
  c=h.parent;
  i=h.user_data

  dfd=c.display_function_data;
  frq=dfd.freq 
  frequencyUnit=dfd.units.frequency;
  magnitudeUnit=dfd.units.magnitude;
  phaseUnit=dfd.units.phase;
  TF_name=dfd.TF_name
  tipType=dfd.tipType;
  
  if tipType==[] then
    //find corresponding freq
    pm=complex(h.data(1),h.data(2))
    PM=complex(c.data(:,1),c.data(:,2));
    [m,k]=min(abs(PM-pm));
    if real(PM(k))>real(pm)|k==size(PM,1) then k=k-1;end
    h.user_data=frq; //for update
    frq=frq(k)+(frq(k+1)-frq(k))*abs(pm-PM(k))/abs(PM(k+1)-PM(k));
    frq=freqconv("Hz",frequencyUnit,frq)
    str=msprintf(_("%s\nMagnitude: %.3g%s\nPhase: %.3g%s\nFrequency: %.3g%s"),...
                 TF_name,h.data(2),magnitudeUnit,h.data(1),phaseUnit,frq,frequencyUnit);
  elseif tipType=="peak" then
    g=h.data(2)
    frq=freqconv("Hz",frequencyUnit,frq)
    str=msprintf(_("%s\nPeak Gain: %.3g%s\nFrequency: %.3g%s"),...
                 TF_name,g,magnitudeUnit,frq,frequencyUnit) ;
  elseif tipType=="gainMargin" then
    i=h.user_data;
    if i<>[]&dfd.gmargin<>[] then
      frq=freqconv("Hz",frequencyUnit,dfd.gmargin(1,i))
      gm=magconv("dB",magnitudeUnit,dfd.gmargin(2,i))
      gm=dfd.gmargin(2,i)
      str=msprintf(_("%s\nGain margin: %.3g%s\nFrequency: %.3g%s"),...
                   TF_name,gm,magnitudeUnit,frq,frequencyUnit) ;
    else
      //display may be called before i has been set
      str=""
    end
  elseif tipType=="phaseMargin" then
    i=h.user_data;
    if i<>[]&dfd.pmargin<>[]  then
      frq=freqconv("Hz",frequencyUnit,dfd.pmargin(1,i))
      pm=phaseconv(_("degrees"), phaseUnit,dfd.pmargin(2,i))
      str=msprintf(_("%s\nPhase margin: %.3g%s\nFrequency: %.3g%s"),...
                   TF_name,pm,phaseUnit,frq,frequencyUnit); 
    else
      str=""
    end
  end

endfunction
