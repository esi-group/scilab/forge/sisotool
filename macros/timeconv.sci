// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function r=timeconv(from,to,r)
  if r==[] then return;end
  if from==to then return;end
  choices=["ns";"µs";"ms";"s";"mn";"h";"d"];
  if and(from<>choices)|and(to<>choices) then
    error(msprintf(_("%s: Conversion from %s to %s not implemented\n"),"timeconv",from,to))
  end  
  select [from to]
  case ["s" "ms"]
    r=1e3*r
  case ["s" "µs"]
    r=1e6*r
  case ["s" "ns"]
    r=1e9*r
  case ["s" "mn"]
    r=r/60
  case ["s" "h"]
    r=r/3600
  case ["s" "d"]
    r=r/86400
  case ["ms" "s"]
    r=1e-3*r
  case ["µs" "s"]
    r=1e-6*r
  case ["ns" "s"]
    r=1e-9*r
  case ["mn" "s"]
    r=r*60
  case ["h" "s"]
    r=r*3600
  case ["d" "s"]
    r=r*86400
  end
endfunction
