// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolPrefGuiOk
  tag=gcbo.parent
  guiHandle=tag.parent.parent
  
  ud=guiHandle.user_data
  mainH=ud.mainH

  S=sisotoolGetSession(mainH)
  select tag.string
  case _("Units")
    units=S.Preferences.units
    units_old=units
    Handles=tag.user_data

    //frq
    upd=%f
    frq=Handles(1);v=frq.string(frq.value)
    if v<>units.frequency then
      units.frequency=v;
      upd=%t
    end
    //db
    db=Handles(2);v=db.string(db.value)
    if units.magnitude<>v then
      units.magnitude=v;
      upd=%t
    end
    //phi
    phi=Handles(3);v=phi.string(phi.value)
    if units.phase<>v then
      units.phase=v;
      upd=%t
    end
    //time
    time=Handles(4);v=time.string(time.value)
    if units.time<>v then
      units.time=v;
      upd=%t
    end
    if upd then //update units for datatips
      S.Preferences.units=units
      updateAxesLabels()
      //Editors
      //update units for poles zero, ... markers
      handles=S.EditorsSettings.handles;
      for k=1:size(handles)
        ed=sisotoolGetEditorData(handles(k));
        for c=["loci","gain","poles","zeros"]
          if ed(c)<>[] then
            ed(c).display_function_data.units=units
            //updateDatatips(ed(c))
          end
        end
        for c=["leadslags","notches"]
          ed(c)(1).display_function_data.units=units
          //updateDatatips(ed(c)(1))
          ed(c)(2).display_function_data.units=units
          //updateDatatips(ed(c)(2))
        end
        //sisotoolSetEditorData(ed,handles(k))
      end
      
      //Analysers
      handles=S.AnalysersSettings.handles;
      for k=1:size(handles)
        ad=sisotoolGetAnalyserData(handles(k));
        for l=ad.loci 
          l.display_function_data.units=units
        end
        for f=fieldnames(ad.characteristics_handles)'
          
          for c=ad.characteristics_handles(f)
            if c.type=="Polyline"&c.display_function<>"" then
              c.display_function_data.units=units
            end
          end
        end
      end
    end
  case _("Time responses")
    timeResponsesPref=S.Preferences.timeResponsesPref
    check_time=tag.user_data
    timeResponsesPref.method=check_time.value
    if check_time.value==0 then
      timeResponsesPref.final=evstr(check_time.user_data(2).string)
      timeResponsesPref.step=evstr(check_time.user_data(4).string)
    end
    S.Preferences.timeResponsesPref=timeResponsesPref
  case _("Frequency responses")
    freqResponsesPref=S.Preferences.freqResponsesPref
    check_freq=tag.user_data
    freqResponsesPref.method=check_freq.value
    if check_freq.value==2 then
      freqResponsesPref.range=evstr(check_freq.user_data(2).string)
    elseif check_freq.value==3 then
      freqResponsesPref.vector=evstr(check_freq.user_data(4).string)
    end
    S.Preferences.freqResponsesPref=freqResponsesPref
  case _("Root locus")
    rootLocusPref=S.Preferences.rootLocusPref
    check_rl=tag.user_data
    
    rootLocusPref.method=check_rl.value
    if check_rl.value==0 then
      rootLocusPref.maxGain=evstr(check_rl.user_data(2).string)
    end
    S.Preferences.rootLocusPref=rootLocusPref
  end
  sisotoolSetSession(S,mainH)
  sisotoolRedraw(mainH)
  //TBD update optimgui?
endfunction
function updateAxesLabels()
//S, units and units_old are defined in the calling context
   if S.CompensatorEditor<>[] then
        Frames=S.CompensatorEditor.children.children(3:$-1)
        //Components header
        Frames(2).children(2).string=msprintf(_("Frequency (%s)"),units.frequency);
        //Component Editor labels
        labels=Frames(1).children(2:2:$)
        k=find(labels.string==msprintf(_("Frequency (%s)"),units_old.frequency))
        if k<>[] then
          labels(k).string=msprintf(_("Frequency (%s)"),units.frequency)
        end
        k=find(labels.string==msprintf(_("at Frequency (%s)"),units_old.frequency))
        if k<>[] then
          labels(k).string=msprintf(_("at Frequency (%s)"),units.frequency)
        end
        k=find(labels.string==msprintf(_("Max Phase shift (%s)"),units_old.phase))
        if k<>[] then
          labels(k).string=msprintf(_("Max Phase shift (%d)"),units.phase)
        end
        k=find(labels.string==msprintf(_("Natural Frequency (%s)"),units_old.frequency))
        if k<>[] then
          labels(k).string=msprintf(_("Natural Frequency (%s)"),units.frequency);
        end
        k=find(labels.string==msprintf(_("Depth (%s)"),units_old.magnitude))
        if k<>[] then
          labels(k).string=msprintf(_("Depth (%s)"),units.magnitude)
        end
      end
      
      //see also sisotoolCreateEditors
      for k=1:size(S.EditorsSettings.subWins,"*")
        ax=S.EditorsSettings.handles(k)
        select S.EditorsSettings.subWins(k).type
        case "rlocus"
        case "olbm"
          ax.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
        case "olbp"  
          ax.y_label.text=msprintf(_("Phase (%s)"),units.phase);
          ax.x_label.text=msprintf(_("Frequency (%s)"),units.frequency);
        case "clbm"
          ax.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
        case "clbp"  
          ax.y_label.text=msprintf(_("Phase (%s)"),units.phase);
          ax.x_label.text=msprintf(_("Frequency (%s)"),units.frequency);
        case "nichols"
          ax.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
          ax.x_label.text=msprintf(_("Phase (%s)"),units.phase);
          [phi,db]=nicholsIsoModuleCurve(2.3)
          ed=sisotoolGetEditorData(ax);
          ed.mainGrid.data=[phaseconv(_("degrees"),units.phase,phi),magconv("dB",units.magnitude,db)];
        end
      end
      for k=1:size(S.AnalysersSettings.subWins,"*")
        ax=S.AnalysersSettings.handles(k)
        select convstr(S.AnalysersSettings.subWins(k).type)
        case "step"
          ax.x_label.text=msprintf(_("Time (%s)"),units.time);
        case "impulse"
          ax.x_label.text=msprintf(_("Time (%s)"),units.time);
        case "gainplot"
          ax.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
        case "phaseplot"
          ax.y_label.text=msprintf(_("Phase (%s)"),units.phase);
          ax.x_label.text=msprintf(_("Frequency (%s)"),units.frequency);
        case "nyquist"
        case "nichols"
          ax.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
          ax.x_label.text=msprintf(_("Phase (%s)"),units.phase);
        case "pole/zero"
        end
      end
endfunction
