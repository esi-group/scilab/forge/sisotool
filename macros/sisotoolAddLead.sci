// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAddLead(win)
  pixDistTol=4;
  mainH=get_figure_handle(win);
  mainH.event_handler_enable = "off";
  mainH.info_message=_("Click left where you want to add this lead");
  ax=gca()//current axes has been set by sisotoolEditorsEvents
  [btn,pt]=getPointInSubwin(ax) ;
  mainH.info_message="";
  if pt==[] then 
    mainH.event_handler_enable = "on";
    return,
  end
  typ=ax.tag;
  editorData=sisotoolGetEditorData(ax);
  compensatorName=editorData.tunable;
  S=sisotoolGetSession(mainH);
  dt=S.Designs(compensatorName).dt;
//gerer le cas discret
  ok=%f;
  if typ=="rlocus" then 
    R= min(pt(1),-1e-5);
    v=[R/1.5;R]; //|z|<|p|
    if dt<>"c" then v=exp(v*dt); end
    ok=%t;
  elseif or(typ==["olbm" "olbp" "clbm" "clbp"]) then
    //assume the added pole is stable  
    u=S.Preferences.units.frequency;
    R=-freqconv(u,"rd/s",pt(1));
    v=[R/1.5;R]
    if dt<>"c" then v=exp(v*dt); end
    ok=%t;
  elseif typ=="nichols" then
    curve=editorData.loci;
    [d,ptp,ind,c]=orthProj(curve.data,pt);
    
    if ind<>[] then
      [xx,yy]=xchange(ptp(1),ptp(2),'f2i');
      [x,y]=xchange(pt(1),pt(2),'f2i');
      [d,k]=max([abs(xx-x) abs(yy-y)]);
      if d<=pixDistTol then 
        frq=curve.display_function_data.freq;
        u=S.Preferences.units.frequency;
        R=-freqconv(u,"rd/s",frq(ind)+(frq(ind+1)-frq(ind))*c);
        v=[R/1.5;R]
        if dt<>"c" then v=exp(v*dt); end
        ok=%t;
      end
    end
  end
  if ok then
    S=sisotoolAddHistory(S,"Compensator","add",compensatorName,"leadslags",v);
    S=sisotoolSetCElement(S,compensatorName,"leadslags",v);
    sisotoolSetSession(S,mainH);  
    sisotoolRedraw(mainH);
    sisotoolUpdateOptimGui(mainH)
  end
  mainH.event_handler_enable = "on";  
 endfunction
