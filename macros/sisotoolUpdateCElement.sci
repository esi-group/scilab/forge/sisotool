// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function S=sisotoolUpdateCElement(S,compensatorName,entityType,ind,value)
  if entityType=="gain" then
    v=value
  elseif or(entityType==["poles" "zeros"]) then
    if type(value)==15 then
      value=value(1)
    end
    v=[real(value);imag(value)]
  elseif entityType=="leadslags" then
    v=value(:)
  elseif entityType== "notches" then
    z=value(1);p=value(2);
    R=abs(z);zetaz=abs(real(z))/R; zetap=abs(real(p))/R;
    v=[R;zetaz;zetap];
  end
  S.OptimSettings.Controller(compensatorName)(entityType).initial(:,ind)=v;
  S.Designs(compensatorName)(entityType)(:,ind)=value(:);
endfunction
