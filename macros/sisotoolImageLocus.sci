// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [rf,Hr]=sisotoolImageLocus(frq,sys,compensatorName,entityType,i,k)

//Continuous time systems
//  Given a transfer function H(s), such as H(s)=Hr(s)*1/(s-p) or
//  H(s)=Hr(s)*1/((s-p)*(s-conj(p)))  this function compute the C plane locus of
//  the image of p. The image is the point at H(%i*wn), where wn=abs(p) is the
//  natural frequency of resp 1/(s-p) or *1/((s-p)*(s-conj(p)))
//Discrete  time systems
 //  Given a transfer function H, such as H=Hr*1/(z-p) or
//  H=Hr*1/((z-p)*(z-conj(p)))  this function compute the C plane locus of
//  the image of p.  The image is the point at H(exp(%i*wn)), where wn=abs(log(p)/dt) is the
//  natural frequency of resp 1/(z-p) or *1/((z-p)*(z-conj(p)))
 
//Hr    : SISO transfer function
//p    : a pole of H, the reference value.
  
//frq  : real vector, the parameter of the locus
//rf  : a complex vector, the locus coordinates in the C plane
  C=sys(compensatorName);
  dt=C.dt
  npts=500; // number of points of the locus discretization
  Hr=[];
  out_of=compensatorName
  Hr=sisotoolSys2OL(sys,out_of,list(entityType,k));
  if entityType=="poles" then
    //iso zeta curve
    p=C(entityType)(k)
    if imag(p) == 0 then   //real pole
      if dt<>"c" then
        // 1/(z-p), z=exp(%i*wn*dt),p=sgn*exp(wn*dt)
        [wr,zeta]=damp(p,dt)
        pw=wnzeta2complex(dt,2*%pi*frq,zeta)
        Z=exp(2*%i*%pi*frq*dt)
        //rf = repfreq(Hr, frq).*((1-pw)./(Z-pw));
        rf = repfreq(Hr, frq)./(Z-pw);
      else
        // 1/(s-p), s=2*%i*%pi*frq, p=sgn*2*%pi*frq,
        rf = repfreq(Hr, frq)./((1-%i*sign(real(p)))*2*%pi*frq)
      end
    else   
      //complex pole
      //zeta fixed, natural frequency varying
      [wr,zeta]=damp(p,dt)
      if dt<>"c" then
        pw=wnzeta2complex(dt,2*%pi*frq,zeta) 
        Z=exp(2*%i*%pi*frq*dt)
        //rf = repfreq(Hr, frq).*((abs(pw).^2-2*real(pw)+1)./(Z.^2-2*real(pw).*Z+abs(pw).^2))
        rf = repfreq(Hr, frq)./(Z.^2-2*real(pw).*Z+abs(pw).^2)      
      else
        // 1/(s^2+2*zeta*wr*s+wr^2), s=%i*wr ==> 1/(2*%i*zeta*wr^2)
        wr=(2*%pi*frq);
        rf = repfreq(Hr, frq)./(2*%i*zeta*wr.^2)
      end
    end,
  elseif entityType=="zeros" then
    //iso zeta curve
    z=C(entityType)(k)
    if imag(z)==0 then //real zero
      if dt<>"c" then
        [wr,zeta]=damp(z,dt)
        zw=wnzeta2complex(dt,2*%pi*frq,zeta)
        Z=exp(2*%i*%pi*frq*dt)
        //        rf = repfreq(Hr, frq).*((Z-zw)./(1-zw));
        rf = repfreq(Hr, frq).*(Z-zw);
      else   
        //(s-p), s=2*%i*%pi*frq, p=sgn*2*%pi*frq,
        rf = repfreq(Hr, frq).*((1-%i*sign(real(z)))*2*%pi*frq)
      end
    else //complex zero
      [wr,zeta]=damp(z,dt)
      if dt<>"c" then
        zw=wnzeta2complex(dt,2*%pi*frq,zeta) 
        Z=exp(2*%i*%pi*frq*dt)
        //rf = repfreq(Hr, frq).*((Z.^2-2*real(zw).*Z+abs(zw).^2)./(abs(zw).^2-2*real(zw)+1))
        rf = repfreq(Hr, frq).*(Z.^2-2*real(zw).*Z+abs(zw).^2)
      else
        rf = repfreq(Hr, frq).*(2*zeta*%i)
      end
    end
  elseif entityType=="leadslags" then
    //Adapt the natural frequency of the selected item (pole or zero)
    //warning both lead components have been removed out of Hr
    //(1-s/z)/(1-s/p)
    z=C(entityType)(1,k);
    p=C(entityType)(2,k);
    [wn,zeta]=damp(z,dt) //z and p have identical damping factor zeta
    if i==2 then //pole s=%i*|p|
      if dt<>"c" then
        pw=wnzeta2complex(dt,2*%pi*frq,zeta)//keep zeta constant
        Z=exp(2*%i*%pi*frq*dt)
        //rf = repfreq(Hr, frq).*((Z-z).*(1-pw))./((Z-pw).*(1-z));
        rf = repfreq(Hr, frq).*((Z-z))./((Z-pw));
      else
        rf = repfreq(Hr, frq).*(2*%i*%pi*frq-z)./((1-%i*sign(real(p)))*2*%pi*frq)
      end
    else //zero s=%i*|z|
      if dt<>"c" then
        //g=(1-p)/(1-z);
        zw=wnzeta2complex(dt,2*%pi*frq,zeta)
        Z=exp(2*%i*%pi*frq*dt)
        //rf = repfreq(Hr, frq).*((Z-zw).*(1-p))./((Z-p).*(1-zw));
        rf = repfreq(Hr, frq).*((Z-zw))./((Z-p));
      else
        rf = repfreq(Hr, frq).*((1-%i*sign(real(z)))*2*%pi*frq)./(2*%i*%pi*frq-p);
      end
    end
  elseif entityType=="notches" then
    //(s^2+2*zetaz*w*s+w^2)/((s^2+2*zetap*w*s+w^2)
    // iso zetaz curve
    //Adapt the common natural frequency and zetaz letting zetap fixed
    z=C(entityType)(1,k);
    p=C(entityType)(2,k);
    [wn,zetap]=damp(p,dt)
    [wn,zetaz]=damp(z,dt)
    r=zetaz/zetap
    rf=repfreq(Hr,frq)*r;
  
  end
endfunction
