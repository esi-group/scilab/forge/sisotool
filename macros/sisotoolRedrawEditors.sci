// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function  sisotoolRedrawEditors(mainH,update,sel)
//Redraw the editors display if update=%f
//or 
//Update the display during a move if update=%t
  if argn(2)<2 then update =%f;end
  S=sisotoolGetSession(mainH)
  sys=S.Designs
  if sys==[] then return,end
  mainH.immediate_drawing = "off";
  units=S.Preferences.units
  freqResponsesPref=S.Preferences.freqResponsesPref
  subWins=S.EditorsSettings.subWins
  if argn(2)<3 then sel=1:size(subWins,"*");end
  gridded=S.EditorsSettings.gridded;
  handles=S.EditorsSettings.handles;
  refreshed=S.EditorsSettings.refreshed;
  // redraw  editors
  for sub=sel
    subWin=subWins(sub);
    EditorType=subWin.type
    if and(EditorType<>["clbm" "clbp"]) then //open loop
      out_of=subWin.out_of
      [OL,K]=sisotoolSys2OL(sys,out_of)
      if EditorType=="rlocus" then
        sisotoolRlocusEditor(OL,sys(out_of),handles(sub),gridded(sub),update,refreshed(sub))
      elseif or(EditorType==["olbm","olbp"]) then
        sisotoolOLBodeEditor(OL,sys(out_of),handles(sub),gridded(sub),update,refreshed(sub))
      elseif EditorType=="nichols" then
        sisotoolNicholsEditor(OL,sys(out_of),handles(sub),gridded(sub),update,refreshed(sub))
      end
    else  //closed loop
      CL=sisotoolClosedLoop(sys,[subWin.in subWin.out]);
      sisotoolCLBodeEditor(CL,sys(subWin.tunable),handles(sub),gridded(sub),update,refreshed(sub))
    end
  end
  mainH.immediate_drawing = "on";
endfunction
