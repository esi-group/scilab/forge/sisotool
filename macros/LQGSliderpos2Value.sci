// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function LQGSliderpos2Value()
  //hs handle on the slider
  hs=gcbo //handle on the slider
  gui=hs.parent.parent// handle on the button
  hc=gui.children(1)
  hv=hs.userdata //handle on edition zone
  fact=hs.Max// scale factor
  if hs.value>0 then
    v=16*hs.value/fact-8//-8 to 8
    n=int(v)
    if n==0 then
      hv.string=msprintf("%.2g",10^(v))
    else
      hv.string=msprintf("%.2ge%d",10^(v-n),n)
    end
  else
    hv.string="1e-8"
  end
  hc.Enable=%t
endfunction
