// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAddDiff(win)
  mainH=get_figure_handle(win);
  mainH.event_handler_enable = "off";
  editorData=sisotoolGetEditorData(gca());//current axes has been set by sisotoolEditorsEvents
  compensatorName=editorData.tunable;
  S=sisotoolGetSession(mainH);
  if S.Designs(compensatorName).dt<>"c" then
    z=1
  else
    z=0
  end
  S=sisotoolAddHistory(S,"Compensator","add",compensatorName,"zeros",z);
  S=sisotoolSetCElement(S,compensatorName,"zeros",z);
  sisotoolSetSession(S,mainH);
  sisotoolRedraw(mainH);
  sisotoolUpdateOptimGui(mainH);
  mainH.event_handler_enable = "on";
endfunction
