// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolApplyTransfer()
  guiHandle=gcbo.parent
  ud=guiHandle.user_data
  transferSelectionHandles=ud.transferSelectionHandles
  l=find(transferSelectionHandles.value==1)

  Analysers=ud.Analysers //handle on the Analysers window if opened
  if Analysers==[] then //the  Analysers window  has been closed
    return
  end
  mainH=Analysers.user_data.mainH 
  S=sisotoolGetSession(mainH);
 
  C=Analysers.children(Analysers.children.type=="Axes")
  //retrieve the subWin index
  ax=C(ud.axesIndex)
  sub=ax.user_data.subWinIndex
  if ax.tag=="gainplot" then
    
    sub=[sub sub+1]
  elseif ax.tag=="phaseplot" then
    sub=[sub-1 sub]
  end
   [transfer_names,expr]=sisotoolTransfer(S.Designs.archi)
  plants=sisotoolGetArchiSpecs(S.Designs.archi,"plant");
  dt=S.Designs(plants(1)).dt
  TF_names=transfer_names(l)

  for k=1:size(sub,"*")
    subWin=S.AnalysersSettings.subWins(sub(k))
    tfsel_old=subWin.transferSelection
  
    subWin.transfer_names=TF_names
    subWin.expr=expr(l)
    subWin.transferSelection=l
    S.AnalysersSettings.subWins(sub(k))=subWin
  
    //Import OptimSettings
    [v,iold,inew]=intersect(tfsel_old,l)
    sold=S.OptimSettings.Analysers(sub(k));
    s=sold
    for f=fieldnames(sold)'
      s(f)=%f(ones(l));
      s(f)(inew)=sold(f)(iold)
    end
    S.OptimSettings.Analysers(sub(k))=s
    sisotoolSetSession(S,mainH);
    ax=S.AnalysersSettings.handles(sub(k));
    sisotoolManageAnalyserLoci(ax,subWin,dt,"update")
  end
  sisotoolRedrawAnalysers(mainH) 


  AnalysersSelector=S.AnalysersSelector
  if AnalysersSelector<>[] then 
    //the AnalyserGui is opened update it
    transferSelectionHandles=AnalysersSelector.user_data.transferSelectionHandles
    for k=1:size(sub,"*")
      transferSelectionHandles(:,sub(k)).value=0;
      transferSelectionHandles(l,sub(k)).value=1;
    end
  end
endfunction


