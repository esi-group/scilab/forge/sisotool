// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotool(varargin)
//sisotool()
//sisotool(archi,dt)
//sisotool(sessionfile)
//sisotool(session))
 
  if getscilabmode() == "NWNI" then
    error(msprintf(_("%s: can not use this function in this mode: %s.\n"), "sisotool", getscilabmode()));
  end
  sisoguilib=lib(sisotoolPath()+"/macros/gui")
  narg=size(varargin)
  if narg==0 then
    S=sisotoolDefaultSession("archi1","c")
  elseif narg==1 then
    if type(varargin(1))==10 then
      sessionFile=varargin(1)
      if size(sessionFile,"*")<>1 then
        error(msprintf(_("%s: wrong size of input arguments: single string expected"),"sisotool"))
      end
      if ~isfile(sessionFile) then
        error(msprintf(_("%s: File ""%s"" does not exist.\n"),"sisotool",sessionFile))
      end
      try
        load(sessionFile)
      catch
        error(msprintf(_("%s: Invalid file ""%s"":a sisotool session file expected"),"sisotool",sessionFile))
      end
      if ~exists("S","local") then
        error(msprintf(_("%s: Invalid file ""%s"":a sisotool session file expected"),"sisotool",sessionFile))
      end
      if typeof(S)<>"st"&and(fieldnames(S)<>"type")&S.type<>"sisotoolSession" then
        error(msprintf(_("%s: Invalid file ""%s"":a sisotool session file expected"),"sisotool",sessionFile))
      end
      S=sisotoolUpdateVersion(S)
      S.Path=sessionFile
    else
      S=varargin(1)
      if typeof(S)<>"st"|and(fieldnames(S)<>"type")|S.type<>"sisotoolSession" then  
        error(msprintf(_("%s: unexpected argument"),"sisotool"))
      end
      S=sisotoolUpdateVersion(S)
    end
  elseif narg==2 then 
    archi=varargin(1)
    dt=varargin(2)
    if and(archi<>"archi"+string(1:6)) then 
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),...
                     "sisotool",1,strcat("archi"+string(1:6),",")))
    end
    if ~(dt=="c"||(type(dt)==1&(size(dt,"*")==1&isreal(dt)&dt>0))) then
      error(msprintf(_("%s: Wrong value for input argument #%d: positive scalar or ""c"" expected.\n"),...
                   "sisotool",2,))
    end
    S=sisotoolDefaultSession(archi,dt)
  end
  //Create the editors window which is also the sisotool root
  [path, fname] = fileparts(S.Path)
  //"default_axes", "off",... removed to workaround bug14908 
  mainH = figure("menubar", "none", ...
                "toolbar","none", ...
                 "infobar_visible", "on",  ...
                 "default_axes", "off",...
                 "figure_name",msprintf(_("%s: Graphical compensator editors (%%d)"),fname),...
                 "visible","on",...
                 "background",color("lightgray"),...//  "immediate_drawing", "off",...
                 "event_handler","sisotoolEditorsEvents",...
                 "event_handler_enable", "off",...
                 "tag","Editors",...
                 "icon",sisotoolPath()+"/icons/sisotool32.png",...
                 "closerequestfcn","sisotoolCloseSession");
 
  datatipManagerMode("off")
  sisotoolMenuBar(mainH);
  sisotoolToolBar(mainH);
  mainH.immediate_drawing = "off";
  
  S=sisotoolCreateEditors(mainH,S)
  if S.Analysers<>[] then  S=sisotoolCreateAnalysers(mainH, S);end
  sisotoolSetSession(S,mainH);

  sisotoolRedraw(mainH)
  if S.OptimGui<>[] then sisotoolOptimGui(mainH);end
  if S.PrefGui<>[] then sisotoolPrefGui(mainH);end
  if S.EditorsSelector<>[] then sisotoolEditorsGui(mainH.figure_id);end
  if S.AnalysersSelector<>[] then  sisotoolAnalysersGui(mainH.figure_id);end
  if S.ArchitectureSelector<>[] then 
    S.ArchitectureSelector=sisotoolArchiGui(mainH)
    sisotoolSetSession(S,mainH);
  end
  mainH.immediate_drawing = "on";
  mainH.axes_size=mainH.axes_size+1; //to fix a bug (the subwindows are
                                     //not shown)
  mainH.event_handler_enable = "on";
  //mainH.resizefcn="sisotoolResizeEditors()"
endfunction

