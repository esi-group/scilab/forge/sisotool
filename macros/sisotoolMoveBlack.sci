// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function  sisotoolMoveBlack(mainH,editorData,entityType,i,k,pt,loc)
//retreive the frequency discretization out of datatips data structure
 
  ax=gca();//current axes has been set by sisotoolEditorsEvents
  figureHandle=ax.parent

  S=sisotoolGetSession(mainH)
  units=S.Preferences.units
  if entityType=="requirements" then
    //see sisotoolRequirementsArea
    req=ax.children($).children(i)
    ud=req.user_data;
    d=ud; //keep it in case of no move
    update=%f
    if or(req.tag==["gplow" "gphigh"]) then
      d=req.data
      np=size(d,1)
      ind=loc(1)
      if loc(2)<0.1|loc(2)>0.9 then
        //a vertex has been selected move it
        if loc(2)>0.9 then ind=ind+1;end//right point
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          d(ind,1:2)=rep(1:2)
          //insure we have a function
          if ind>2 then d(ind,1)=max(d(ind,1),d(ind-1,1));end
          if ind<np-2 then d(ind,1)=min(d(ind,1),d(ind+1,1));end
          if ind==2 then d(1,1)=d(ind,1);end
          if ind==np-2 then d(np-1,1)=d(ind,1);end
          req.data=d;
        end 
        
      elseif d(ind,1)==d(ind+1,1) then
        // a vertical segment selected translate it horizontally
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          d(ind:ind+1,1)=rep(1)
          req.data=d;
        end 
        
      else
        //a  segment selected translate it vertically
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          dd=rep(2)-d(ind,2)
          d(ind:ind+1,2)= d(ind:ind+1,2)+dd //vertical translation
          req.data=d;
        end 
      end
      d=d(2:$-2,:)
      req.user_data=d
      if  or(d<>ud) then
        update=%t
        d(:,1)=phaseconv(units.phase,"degrees",d(:,1))
        d(:,2)=magconv(units.magnitude,"dB",d(:,2))
      end
    elseif req.tag=="pmargin" then 
      if loc(2)<0.2|loc(2)>0.8 then 
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          d=max(0,abs(phaseconv(units.phase,_("degrees"),rep(1))+180))
          req.data(:,1)=[-180-d;-180+d]
          req.user_data=d
          figureHandle.info_message=msprintf(_("Phase margin=%.3g (degrees)"),d,units.phase)
        end
        if or(d<>ud) then
          update=%t
          d=phaseconv(units.phase,"degrees",d)
        end
      end
    elseif req.tag=="gmargin" then
      if loc(2)<0.2|loc(2)>0.8 then 
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          d=magconv(units.magnitude,"dB",abs(rep(2)))
          req.data(:,2)=[-d;d]
          figureHandle.info_message=msprintf(_("Gain margin=%.3g (dB)"),d)
        end
        if or(d<>ud) then
          update=%t
          d=MAGconv(units.magnitude,"dB",d)
        end
      end
      req.user_data=d
    elseif req.tag=="clpeak"  then
      while %t
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        frq=freqconv(units.frequency,"rd/s",rep(1))
        rf=rep(2)*exp(2*%i*%pi*frq)
        y=rf/(1+rf)
        d=20*log10(abs(y)) //TBD conversion d'unité?
        req.data=sisotoolRequirementArea("nichols",req.tag,d,axesbounds(ax))
        figureHandle.info_message=msprintf(_("Closed loop peak gain=%.3g (dB)"),d)
      end 
      req.user_data=req.data
      if or(d<>ud) then
        update=%t
      end
    end
    figureHandle.info_message=""
    if update then
      requirementIndex=i;
      subWinIndex=editorData.subWinIndex
      windowType=ax.parent.tag
      Settings= windowType+"Settings"
      reqProp= S(Settings).subWins(subWinIndex).reqProps(requirementIndex)
      opt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type);
      S=sisotoolAddHistory(S,"Requirements","update",...
                         windowType,subWinIndex,requirementIndex, ...
                           reqProp,opt);
      S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=d;
      sisotoolSetSession(S,mainH)
    end
  
    return
  end
  bounds=axesbounds(ax);
  editorData=sisotoolGetEditorData(ax)
  frq=editorData.loci.display_function_data.freq;
  compensatorName=editorData.tunable
  sys=S.Designs
  C=sys(compensatorName)
  dt=C.dt
  if entityType=="loci" then
    entityType="gain";k=1;
  end
  if  entityType<>"notches_width" then
    cur=sisotoolGetCElement(S,compensatorName,entityType,k);
    S=sisotoolAddHistory(S,"Compensator","update",compensatorName, entityType,k,cur);
  
  else
    cur=sisotoolGetCElement(S,compensatorName,"notches",k);
    S=sisotoolAddHistory(S,"Compensator","update",compensatorName, "notches",k,cur);
  end

  ////////////////////////////////////////////////////////////////////////
  if or(entityType==["zeros" "poles"]) then
    //Compute and Draw the locus of the moving entity
    
    if entityType=="zeros" then entityName=_("zero");else entityName=_("pole");end
    info_message=_("Drag this %s along the red curve to the desired location.")+" "+ ...
        _("Current location:%s")
    
    z=C(entityType)(k)
    realflag=imag(z)==0;
    [wn,zeta]=damp(z,dt);
    if dt<>"c"&wn>%pi/dt then return;end
    figureHandle.info_message=msprintf(info_message,entityName,string(z))
    rf=sisotoolImageLocus(frq,sys,compensatorName,entityType,i,k)
 
    [locus_p,locus_m]=phasemag(rf);
    locus_p= phaseconv(_("degrees"),units.phase,locus_p)
    locus_m= magconv("dB",units.magnitude,locus_m);

    //shift the locus to make it pass by the pole/zero image
    f=wn/(2*%pi);
    ind=find(frq(1:$-1)<=f&frq(2:$)>f);
    if ind==[] then
      [m,ind]=min(abs([frq(1) frq($)]-f))
      if ind==2 then ind=size(frq,"*");end
    end
      
    twopi=phaseconv(_("degrees"),units.phase,360)
    shift=locus_p(ind)-sisotoolShiftPhase(frq(ind),locus_p(ind),frq,editorData.loci.data(:,1),twopi/2)
  
    locus_p=locus_p-shift;
 
    //draw locus keeping axes bounds
    figureHandle.immediate_drawing="off";
    xpoly(locus_p,locus_m);
  
    locus=gce();locus.line_style=4;locus.foreground=5;
    bounds=ax.data_bounds;
    figureHandle.immediate_drawing="on";
    
    while %t
      set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      //[xr,yr]=xchange(rep(1),rep(2),"f2i")
      //[d,ptp,ind,c]=orthProj([locus_p locus_m],[xr,yr])
      [d,ptp,ind,c]=orthProj(locus.data,rep(1:2))
      if ind<>[] then
        w=freqconv("Hz","rd/s",frq(ind)+(frq(ind+1)-frq(ind))*c)
        z=wnzeta2complex(dt,w,zeta)// keep zeta constant
        if realflag then z=real(z);end
        C(entityType)(k)=z;
        figureHandle.info_message=msprintf(info_message,entityName,string(z))
        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
      end
    end
    figureHandle.info_message=""
    if rep==-1000 then sys=[];end //the tool has been closed
    delete(locus);
    
    //////////////////////////////////////////////////////////////////////// 
  elseif entityType=="gain" then
    k=1;
    h=editorData.loci(i);
    info_message=_("Drag Nichols curve up or down to adjust the loop gain.")+" "+ ...
          _("Current gain:%s")
    figureHandle.info_message=msprintf(info_message,string(C.gain))
    while %t
      set("current_axes", ax);
      g=h.data(:,2)//the gain curve discretization
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      [d,ptp,ind,c]=orthProj(h.data,rep(1:2))
      if ind<>[] then
        //interpolate
        old_g=ptp(2)//old gain value  (in dB)
        new_g=rep(2) //desired  value (in dB)
        C.gain=C.gain*(10^((new_g-old_g)/20))
        figureHandle.info_message=msprintf(info_message,string(C.gain))
        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
      end
    end
    if rep==-1000 then sys=[];end //the tool has been closed
     ////////////////////////////////////////////////////////////////////////
  elseif entityType=="leadslags" then
    if i==1 then entityName=_("zero");else entityName=_("pole");end
    info_message=_("Drag this %s along the red curve to the desired location.")+" "+ ...
        _("Current location:%s")
    z=C(entityType)(i,k)
    
    figureHandle.info_message=msprintf(info_message,entityName,string(z))
    //Adapt natural frequency of the  zero or pole pair keeping the
    //common damping factor fixed
   
    rf=sisotoolImageLocus(frq,sys,compensatorName,entityType,i,k)
    [locus_p,locus_m]=phasemag(rf);
    locus_p= phaseconv(_("degrees"),units.phase,locus_p)
    locus_m= magconv("dB",units.magnitude,locus_m)

    //shift the locus to make it pass by the pole/zero image
    [wn,zeta]=damp(z,dt);
    f=wn/(2*%pi);
    ind=find(frq(1:$-1)<=f&frq(2:$)>f);
    twopi=phaseconv(_("degrees"),units.phase,360)
    shift=locus_p(ind)-sisotoolShiftPhase(frq(ind),locus_p(ind),frq,editorData.loci.data(:,1),twopi/2)
    locus_p=locus_p-shift;
 
     //draw locus keeping axes bounds
    figureHandle.immediate_drawing="off";
    xpoly(locus_p,locus_m);
    locus=gce();locus.line_style=4;locus.foreground=5;
    ax.data_bounds=bounds
    figureHandle.immediate_drawing="on";
    [wr,zeta]=damp(z,C.dt)
    while %t
      set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      [d,ptp,ind,c]=orthProj(locus.data,rep(1:2))
      if ind<>[] then
        w=freqconv("Hz","rd/s",frq(ind)+(frq(ind+1)-frq(ind))*c);
        if w>0 then 
          C(entityType)(i,k)=real(wnzeta2complex(dt,w,zeta))
          figureHandle.info_message=msprintf(info_message,entityName,string(C(entityType)(i,k)));
          sys(compensatorName)=C;
          sisotoolSetSys(sys,mainH);
          sisotoolUpdateResponses(mainH);
        end
      end
    end
    delete(locus)
    figureHandle.info_message=""
    if rep==-1000 then sys=[];end //the tool has been closed

    sys(compensatorName)=C;
    
    ////////////////////////////////////////////////////////////////////////
  elseif entityType=="notches" then
    //Adapt the natural frequency and zero damping zetaz letting zetap
    //fixed
    entityName=_("notch")
     info_message=_("Drag this %s along the red curve to the desired location.")+" "+ ...
         _("Current depth:%s, wn:%s")
    [rf,Hr]=sisotoolImageLocus(frq,sys,compensatorName,entityType,i,k)
    [locus_p,locus_m]=phasemag(rf);
    locus_p= phaseconv(_("degrees"),units.phase,locus_p)
    locus_m= magconv("dB",units.magnitude,locus_m)

    //shift the locus to make it pass by the pole/zero image
     z=C(entityType)(1,k);
    [wn,zeta]=damp(z,dt);
    f=wn/(2*%pi);
    ind=find(frq(1:$-1)<=f&frq(2:$)>f);
    twopi=phaseconv(_("degrees"),units.phase,360)
    shift=locus_p(ind)-sisotoolShiftPhase(frq(ind),locus_p(ind),frq,editorData.loci.data(:,1),twopi)
    locus_p=locus_p-shift;
 
    
    ax.parent.immediate_drawing='off'
    xpoly(locus_p,locus_m)
    locus=gce();locus.line_style=4;locus.foreground=5;
   
    ax.data_bounds=bounds;
    ax.parent.immediate_drawing='on'
   
 
    z=C(entityType)(1,k);
    p=C(entityType)(2,k);
    [wn,zeta]=damp(C(entityType)(:,k),C.dt)
    zetaz0=zeta(1)
    zetap=zeta(2)
    figureHandle.info_message=msprintf(info_message,entityName,string(wn(1)),string(zeta(1)/zeta(2)))
    while %t
      set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      [d,ptp,ind,c]=orthProj(locus.data,rep(1:2))
      if ind<>[] then
        f=frq(ind)+(frq(ind+1)-frq(ind))*c;
        m=10^(rep(2)/20);a=rep(1)*%pi/180;
        depth=real(complex(m*cos(a),m*sin(a))/repfreq(Hr,f));
        wn=2*%pi*f

        figureHandle.info_message=msprintf(info_message,entityName,string(wn),string(depth))
        zetaz=depth*zetap
        z=sign(real(z))*wnzeta2complex(C.dt,wn,zetaz)
        p=sign(real(p))*wnzeta2complex(C.dt,wn,zetap)  
        C(entityType)(:,k)=[z ;p];

        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
        //locus.data(:,2)=locus.data(:,2)+20*log10(zetaz/zetaz0);
        zetaz0=zetaz;
      end
    end
    delete(locus)
    figureHandle.info_message=""
  elseif entityType=="notches_width" then
    entityType="notches";
    //the notch width marker frequency positions are given by a second order
    //polynomial P(x)=1+b*x+x^2; where x is the ratio w/wn (see sisotoolNotchWidthLoc)
    //where 
    //  b=(-2+4*depth^2*Zetap^2-(4*Zetap^2-2)*depth^(2*df))/(1-depth^(2*df))
    // b is also equal to -(r+1/r) if r is a root of P
    // so Zetap =
    // sqrt((-depth^2+depth^(2*df))*(b+2)*(depth^(2*df)-1))/(-depth^2+depth^(2*df))
    info_message=_("Drag the mark along the Nichols curve to adjust notch width.")+" "+ ...
        _("Current width:%s")
    h=editorData.loci;
    frq=h.display_function_data.freq

    df=0.25;

    [wn,zeta]=damp(C.notches(:,k),C.dt)
    wn=wn(1)
    zetap=zeta(2)
    depth=abs(zeta(1)/zeta(2));
    dr=depth.^(df*2);
    t1=depth^2-dr;
    t2=t1*(dr-1);
    t3=t1/(dr-1)
    Beta =sqrt(zetap^2*t3);
    width = (1 + 2*Beta^2 + 2*Beta*sqrt(1+Beta^2));
   
    figureHandle.info_message=msprintf(info_message,string(width))
    //Compute new values for zetaz and zetap keeping natural frequency and
    //depth constant
   
    //zetap=-sqrt((dr-1)*(deph^2-dr)*r)*abs(r-1)/(2*(dr-depth^2)*r)
    //zetap=sqrt(t2*r)*abs(r-1)/(2*t1*r)
    while %t
      set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      [d,ptp,ind,c]=orthProj(h.data,rep(1:2))
      if ind<>[] then
        r=(freqconv("Hz","rd/s",frq(ind)+(frq(ind+1)-frq(ind))*c)/wn)^2;
        b=-(r+1/r); //the new degree 1 coefficient of the 2nd order polynomial
        zetap=min(1,sqrt(t2*r)*abs(r-1)/(2*t1*r));
        Beta =sqrt(zetap^2*t3);
        width = (1 + 2*Beta^2 + 2*Beta*sqrt(1+Beta^2));
        figureHandle.info_message=msprintf(info_message,string(width))
        zetaz=depth*zetap
       
        C.notches(:,k)=wnzeta2complex(C.dt,wn,[zetaz;zetap]);
        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
      end
    end
    figureHandle.info_message=""
  end
  //update optimSettings
  S=sisotoolUpdateCElement(S,compensatorName,entityType,k,C(entityType)(:,k))
  sisotoolSetSession(S,mainH);
endfunction
