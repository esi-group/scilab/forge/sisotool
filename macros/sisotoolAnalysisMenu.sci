// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAnalysisMenu(fig,mainH)
  if argn(2)==1 then mainH=fig;end
  w=string(mainH.figure_id);
  analysis  = uimenu("parent", fig, ...
                     "label", _("Analysers"),...
                     "Handle_Visible", "off",...
                     "callback" ,list(4,"sisotoolAnalysersGui("+w+")"))
endfunction
