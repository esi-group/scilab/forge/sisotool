// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [keys,names]=sisotoolCharacteristicsTypes(analyserType)
//names is used for dialogs
//keys is used for programs
  names=[];keys=[];
  select analyserType
  case "step"
    names=[_("Peak response"),_("Settling time"),_("Rise time")]
    keys=["peak_response","settling_time","rise_time"]
  case "impulse"
    names=[_("Peak response"),_("Settling time")]
    keys=["peak_response","settling_time"]
  case "gainplot" 
    names=[_("Peak response"),_("Min stability margin"),_("All stability margins")]
    keys=["peak_response","min_margin","all_margins"]
  case "phaseplot" 
    names=[_("Peak response"),_("Min stability margin"),_("All stability margins")]
    keys=["peak_response","min_margin","all_margins"]
  case "nyquist"
    names=[_("Peak response"),_("Min stability margin"),_("All stability margins")]
    keys=["peak_response","min_margin","all_margins"]
  case "nichols"
    names=[_("Peak response"),_("Min stability margin"),_("All stability margins")]
    keys=["peak_response","min_margin","all_margins"]
  case "pole/zero"
    names=[]
    keys=[]
  end
endfunction
