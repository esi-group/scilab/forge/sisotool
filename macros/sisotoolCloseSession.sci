// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolCloseSession(mainWin)
  if argn(2)==1 then
     mainH=get_figure_handle(mainWin);
  else
    c=gcbo();while c.type<>"Figure" then c=c.parent;end
    mainH=c
  end
  S=sisotoolGetSession(mainH)
    
  if S.History.current<> S.History.ref then
    if messagebox(_("System has been modified do you want to save it?"),...
                  "Sisotool message","info",[_("Yes"),_("No")],"modal")==1 then
      sisotoolSaveSession(mainH)
    end
  end
  if S.ArchitectureSelector<>[] then delete(S.ArchitectureSelector),end
  if S.CompensatorEditor<>[] then delete(S.CompensatorEditor),end
  if S.AnalysersSelector<>[] then delete(S.AnalysersSelector),end
  if S.Analysers<>[] delete(S.Analysers);end
  if S.EditorsSelector<>[] then delete(S.EditorsSelector),end
  if type(S.OptimGui)==9 then delete(S.OptimGui),end
  if type(S.PrefGui)==9 then delete(S.PrefGui),end
  if type(S.LQGGui)==9 then delete(S.LQGGui),end
  if type(S.IMCGui)==9 then delete(S.IMCGui),end
  if type(S.PIDGui)==9 then delete(S.PIDGui),end
 delete(mainH)
endfunction

