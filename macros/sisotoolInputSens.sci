// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function CL=sisotoolSensitivity(sys,typ)
  if argn(2)<2 then typ="Input",end

  C=sisotoolC2ss(sys.C);
  select sys.archi
  case 1 then
    if typ=="Input" then
    elseif typ=="Output" then
    elseif typ=="Noise" then
    end
  case 2 then
    if typ=="Input" then
    elseif typ=="Output" then
    elseif typ=="Noise" then
    end
  case 3 then
    if typ=="Input" then
    elseif typ=="Output" then
    elseif typ=="Noise" then
    end
  else
    CL=C
    warning('Sensitivity #'+string(sys.archi)+' is not yet implemented')
  end
  
endfunction
