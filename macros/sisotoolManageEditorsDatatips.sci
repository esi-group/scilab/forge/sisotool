function  sisotoolManageComponentsDataTips(C,update)
  if ~update then
    nold=size(C.datatips,"*")
    n=size(C.data,1);
    if nold>n then
      for i=n+1:nold,datatipRemove(C,i);end
    elseif n>nold then
      for i=nold+1:n,sisotoolDatatipCreate(C,i);end
    end
  end
  updateDatatips(C)
endfunction
