// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function Ft=sisotoolFinalTime(S)
//Computes a final simulation time for a SISO linear dynamical system.
//the computation is based on settling time for step response.
  SettlingThreshold  = 0.005;
  StabilityThreshold = 1e-4;
  if typeof(S)=="state-space" then
    poles=spec(S.A)
  elseif typeof(S)=="rational" then
    poles=roots(S.den,"e")
  else
    error(msprintf(_("%s: Wrong type for input argument #%d: Admitted types should be in %s.\n"),..
                   "sisotool",1,"{""state-space"",""rational""}"))
  end
  
   //Determine final time for step response.
  eps=0.01 //settling tolerance
  //for stable real pole, the step response is given by
  //y=tau*(1-exp(-t/tau))
  //the settling time to reach tau-eps is -tau*log(eps/tau)
  //http://sites.estvideo.net/btssepallam/BS1SEcours/C106reponsesaunechelon.pdf
  //For stable complex pairs p=w*(-zeta+-%i*sqrt(1-zeta^2) the step
  //response is given by
  //y := 1-exp(-zeta*w*t)*cos(w*sqrt(1-zeta^2)*t+arcsin(-zeta))/sqrt(1-zeta^2)
  //the curve envelope is   
  //e :=1-exp(-zeta*w*t)/sqrt(1-zeta^2), with zeta<1
  //the settling time to reach abs(e-1)=eps is given by -(1/2)*log(eps^2*(1-zeta^2))/(zeta*w)

  //check for stability
  Ft=0;
  [mxrp,k]=max(real(poles))
  if mxrp>StabilityThreshold then 
    Ft=log(10)/mxrp
  elseif mxrp>=StabilityThreshold then 
    //similar to an integrator
    Ft=30.0/(1+abs(poles(k)))
  end

  stable=poles(real(poles)<-StabilityThreshold)
  
  //real poles
  kreal=find(abs(imag(stable)./real(stable))<1d-6)
  if kreal<>[] then
    tau=1 ./abs(stable(kreal))
    Ft=max(Ft,max(-tau.*log( SettlingThreshold./tau)))
  end
  //complex poles
  cmplx=stable;cmplx(kreal)=[];
  if cmplx<>[] then
    w=abs(cmplx);
    zeta=-real(cmplx)./w;
    Ft=max(Ft,max(-(1/2)*log(SettlingThreshold^2*(1-zeta^2))./(zeta.*w)))
  end
endfunction

