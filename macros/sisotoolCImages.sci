// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function I=sisotoolCImages(Controller,sys)

//poles zeros images location in bode, black,.. are given by their
//natural frequency H(%i*wn) where wn stands for their natural frequency
//(see http://www-hadoc.lag.ensieg.inpg.fr/hadoc/continu/n09/r09-02.htm)
   
//computes the image (phase and magnitude) of all components defined in
//Controller in sys frequency response 
  if Controller.zeros<>[] then
    fzeros=damp(Controller.zeros,Controller.dt)/(2*%pi);
    fzeros=fzeros(fzeros<>0);
    [pzeros,mzeros]=phasemag(repfreq(sys,fzeros));
    I.zeros=[freqconv("Hz",units.frequency,fzeros);
             phaseconv(_("degrees"),units.phase,pzeros);
             magconv("dB",units.magnitude,mzeros)];
  else
    I.zeros=[]
  end
  if Controller.poles<>[] then
    fpoles=damp(Controller.poles,Controller.dt)/(2*%pi);
    fpoles=fpoles(fpoles<>0);
    [ppoles,mpoles]=phasemag(repfreq(sys,fpoles));
    I.poles=[freqconv("Hz",units.frequency,fpoles);
             phaseconv(_("degrees"),units.phase,ppoles);
             magconv("dB",units.magnitude,mpoles)];
  else
    I.poles=[];
  end
  
  if Controller.leadslags<>[] then
    fleads=damp(Controller.leadslags,Controller.dt)/(2*%pi);
    [pleads,mleads]=phasemag(repfreq(sys,fleads));
    I.leadslags=[freqconv("Hz",units.frequency,fleads);
             phaseconv(_("degrees"),units.phase,matrix(pleads,2,-1));
             magconv("dB",units.magnitude,matrix(mleads,2,-1))];
  else
    I.leadslags=[];
  end
  if Controller.notches<>[] then
    fnotches=damp(Controller.notches,Controller.dt)/(2*%pi);
    [pnotches,mnotches]=phasemag(repfreq(sys,fnotches));
    //compute notches width marker frequency locations
    fmarkers=sisotoolNotchWidthLoc(Controller.notches,Controller.dt);
    [pmarkers,mmarkers]=phasemag(repfreq(sys,fmarkers));
    I.notches=[freqconv("Hz",units.frequency,matrix(fnotches,2,-1));
               phaseconv(_("degrees"),units.phase,matrix(pnotches,2,-1));
               magconv("dB",units.magnitude,matrix(mnotches,2,-1));
               freqconv("Hz",units.frequency,matrix(fmarkers,2,-1));
               phaseconv(_("degrees"),units.phase,matrix(pmarkers,2,-1));
               magconv("dB",units.magnitude,matrix(mmarkers,2,-1))];
  else
    I.notches=[]
  end
endfunction
