// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolSetCLPGUI(sys,gui)
//populates the compensator editor GUI with current values
  CL=sisotoolClosedLoop(sys);
  p=roots(CL.den);
  ud=get(gui,"user_data")
  propertyHandles=ud.propertyHandles;//[locations,frequencies,dampings])

  locations=[];
  dampings=[];
  frequencies=[];

  //poles
  if p<>[] then
    k=find(imag(p)<=0);
    [wn,zeta]=damp(p(k),CL.dt);
    l=sisotoolFormatPZ(p(k));
    locations=[locations;l];
    dampings=[dampings;sisotoolFormatPZ(zeta)];
    frequencies=[frequencies;sisotoolFormatPZ(freqconv("rd/s","Hz",wn))];
  end

   //locations
   propertyHandles(1).string=locations
   // frequencies
   propertyHandles(2).string=frequencies
   //damping
   propertyHandles(3).string=dampings
 
endfunction
