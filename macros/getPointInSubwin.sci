// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [btn,pt]=getPointInSubwin(ax)
  sca(ax);
  bnds=ax.data_bounds
  [btn,x,y]=xclick()
  if prod(bnds(:,1)-x)<=0&prod(bnds(:,2)-y) then
    pt=[x,y];
  else
    pt=[]
  end
endfunction
