// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function p=poles(sys)
  if typeof(sys)=="rational" then 
    if size(sys,"*")==1 then
      p=roots(sys.den)
    else
      sys=tf2ss(sys)
      p=spec(sys.A)
    end
  elseif typeof(sys)=="state-space" then 
     p=spec(sys.A)
  elseif typeof(sys)=="zpk" then 
    p=sys.P
  elseif typeof(sys)=="constant" then 
    p=[]
  else
    error("%s: Wrong type for input argument: Linear dynamical system expected.\n","pole")
  end
endfunction
