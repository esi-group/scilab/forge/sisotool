// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function r=magconv(from,to,r)
  if r==[] then return;end
  if from==to then return;end
  choices=["dB";"absolute"];
  if and(from<>choices)|and(to<>choices) then
    error(msprintf(_("%s: Conversion from %s to %s not implemented\n"),"magconv",from,to))
  end  
  select [from to]
  case ["dB","absolute"]
    r=10.0.^(r/20)
  case ["absolute","dB"]
    r=20*log10(r)
  end
endfunction
