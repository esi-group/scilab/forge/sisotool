// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolDeleteSelected()
  gui=gcbo.parent;
  ud=gui.user_data
  mainH=ud.mainH
  selectionHandles=ud.selectionHandles;
  EditorFrame=selectionHandles.editor;
  ComponentData=EditorFrame.user_data
  viewHandles=selectionHandles.views;
  
  DisplayHandle=selectionHandles.display;
  compensatorParts=DisplayHandle.children(2)
  compensatorName=compensatorParts.string(compensatorParts.value);
  index=viewHandles(1).value;
  if index==[] then return;end
  componentTypes=viewHandles(1).string
  componentType=componentTypes(index)

  S=sisotoolGetSession(mainH);
  C=S.Designs(compensatorName);
  S=sisotoolAddHistory(S,compensatorName,C)
  componentIndex=find(find(componentType==componentTypes)==index)
 
  select componentType
  case "Zero"
    entityType="zeros"
  case "Pole"
    entityType="poles"
  case "Lead"
    entityType="leadslags"
  case "Lag"
    entityType="leadslags"
  case "Notch"
    entityType="notches"
  else
    disp("Problem"),pause
  end
  S=sisotoolDeleteCElement(S,compensatorName,entityType,componentIndex)
  sisotoolSetSession(S,mainH);
  //remove component editor widgets
  delete(EditorFrame.children);
  //Assume there is no currently edited component
  ComponentData=[];
  EditorFrame.user_data=ComponentData
  //redraw
  sisotoolUpdateOptimGui(mainH)
  sisotoolSetCGUI(gui)
  sisotoolRedraw(mainH)
endfunction
