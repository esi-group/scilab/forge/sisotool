// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolEditSelected()
  //callback of Compensator Editor Gui
  sisoguilib=lib(sisotoolPath()+"/macros/gui")
  margin_x     = 5;      // Horizontal margin between each elements
  margin_y     = 5;      // Horizontal margin between each elements
  label_h      = 20;
  Item=gcbo.Value;
  gui=gcbo.parent.parent

  ud=gui.user_data
  mainH=ud.mainH //main editor handle 
  selectionHandles=ud.selectionHandles;
  selectionHandles=ud.selectionHandles;
  DisplayHandle=selectionHandles.display;
  compensatorDisplay=DisplayHandle.children(1);
  compensatorParts=DisplayHandle.children(2)
  compensatorName=compensatorParts.string(compensatorParts.value);
  
  EditorFrame=selectionHandles.editor;
  viewHandles=selectionHandles.views;
  for k=1:4
     viewHandles(k).value=Item;
  end
  selectionIndex=viewHandles(1).value;
  componentTypes=viewHandles(1).string

  componentType=componentTypes(selectionIndex);
  ind1=find(find(componentTypes==componentType)==selectionIndex);
  S=sisotoolGetSession(mainH);
  units=S.Preferences.units
  C=S.Designs(compensatorName);
  
  ComponentData=tlist(["componentdata","type","selectionIndex","entityIndex","handle"],...
                      [],selectionIndex,[],[]);
  callback="sisotoolUpdateComponent()";
  delete(EditorFrame.children);
  set(EditorFrame,"user_data",[]);
  y=margin_y;
  if componentType=="Gain"  then
    K=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Gain"),string(C.gain),callback,1);
    ComponentData.handle=K;
    entityType="gain"
    entityIndex=ind1
    ComponentData.type=entityType
  elseif or(componentType==["Zero" "Pole"]) then
    entityType=convstr(componentType)+'s';
    //ind2=find(imag(C(entityType))<=0);
    //entityIndex=ind2(ind1);
    entityIndex=ind1
    z=C(entityType)(entityIndex);
    if imag(z)==0 then
      Re=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Position"),string(real(z)),callback,1);
      ComponentData.handle=Re;
    else
      [wn,zeta]=damp(z,C.dt);
      wn=freqconv("rd/s",units.frequency,wn);
      Re=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Real Part"),string(real(z)),callback,1);
      y=y+margin_y+label_h;
      Im=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Imaginary Part"),string(imag(z)),callback,2);
      y=y+margin_y+label_h;
      Frq=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,...
                              msprintf(_("Frequency (%s)"),units.frequency),string(wn),callback,3);
      y=y+margin_y+label_h;
      Dmp=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Damping"),string(zeta),callback,4);
      ComponentData.handle=[Re Im Frq Dmp];
    end
    ComponentData.type=entityType;
  elseif or(componentType==["Lead" "Lag"]) then
    //www.cats.rpi.edu/~wenj/ECSE4962S04/leadlag.pdf
    entityType="leadslags";
    entityIndex=ind1;
    z=C(entityType)(1,entityIndex);
    p=C(entityType)(2,entityIndex);
    [wmax,phimax]=LeadLag2Properties(z,p,C.dt)
    wmax=freqconv("rd/s",units.frequency,wmax);//TBD a verifier
    phimax=phaseconv(_("rd"),units.phase,phimax);
    
    Z=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Zero"),...
                            string(z),callback,1);
    y=y+margin_y+label_h;
    P=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Pole"),...
                            string(p),callback,2);
    y=y+margin_y+label_h;
    Phi=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,...
    msprintf(_("Max Phase shift (%s)"),units.phase),...
                            string(phimax),callback,3);
    y=y+margin_y+label_h;
    Frq=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,...
                            msprintf(_("at Frequency (%s)"),units.frequency),...
                            string(wmax),callback,4);
    ComponentData.handle=[Z P Phi Frq];
    ComponentData.type=entityType;
    
  elseif componentType=="Notch" then
    entityType="notches"
    entityIndex=ind1;

    z=C(entityType)(1,entityIndex);
    p=C(entityType)(2,entityIndex);
    //compute wn zetaz zetap depth width
   
    [wn,zetaz] = damp(z,C.dt)
    [wn,zetap] = damp(p,C.dt)
    //Zetaz should be less than zetap
    
    depth=zetaz/zetap;
    width=sisotoolNotchWidth(zetaz,zetap);
    
    wn=freqconv("rd/s",units.frequency,wn)
    depth=magconv("absolute",units.magnitude,depth);
    
    Frq=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,...
    msprintf(_("Natural Frequency (%s)"),units.frequency),...
             string(wn),callback,1);
    y=y+margin_y+label_h;
    Zetaz=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Damping (Zero)"),string(zetaz),callback,2);
    y=y+margin_y+label_h;
    Zetap=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Damping (Pole)"),string(zetap),callback,3);
    y=y+margin_y+label_h;
    
    Depth=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,...
                              msprintf(_("Depth (%s)"),units.magnitude),...
                              string(depth),callback,4);
    y=y+margin_y+label_h;
    Width=sisotoolValueEditor(mainH,EditorFrame,margin_x,y,_("Width (log10)"),string(width),callback,5); 
    
    ComponentData.handle=[Frq Zetaz Zetap Depth Width];
    ComponentData.type=entityType;
  elseif componentType==[] then
    return
  else
    warning(_("Unknown component type"))
    return
  end
  ComponentData.entityIndex=entityIndex;
  
  set(EditorFrame,"user_data",ComponentData);
  //s-0.0326214isotoolSetCGUI(gui)

endfunction

function ed=sisotoolValueEditor(mainH,parent,x,y,label,initialvalue,callback,index)
  margin_x     = 5;      // Horizontal margin between each elements
  label_h      = 20;
  defaultfont  = "arial"; 
  //Assume the y coordinate origin is given relatively to the top of the frame
  pos=parent.position;
  h=pos(4)
  y=h-y-1.5*label_h
  label=uicontrol( ...
      "parent"              , parent,...
      "style"               , "text",...
      "string"              , label,...
      "units"               , "pixels",...
      "position"            , [x y 170 label_h],...
      "fontname"            , defaultfont,...
      "fontunits"           , "points",...
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "horizontalalignment" , "left", ...
      "background"          , lightgray(), ...
      "visible"             , "on");
  ed=uicontrol( ...
      "parent"              , parent,...
      "style"               , "edit",...
      "string"              , initialvalue,...
      "units"               , "pixels",...
      "position"            , [x+margin_x+170 y 250 label_h],...
      "fontname"            , defaultfont,...
      "fontunits"           , "points",...
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "horizontalalignment" , "left", ...
      "background"          , [1 1 1], ...
      "callback"            , callback,...
      "userdata"            , index,...
      "visible"             , "on")
endfunction


