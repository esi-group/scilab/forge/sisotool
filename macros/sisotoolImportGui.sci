// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolImportGui(win)
  mainH=get_figure_handle(win)
  sisoguilib=lib(sisotoolPath()+"/macros/gui")
  S=sisotoolGetSession(mainH)
  guiHandle= S.ImportSelector
  //Creates the compensator editor GUI 
  margin_x     = 5;      // Horizontal margin between each elements
  margin_y     = 5;      // Vertical margin between each elements

  button_h     = 30;
  label_h      = 20;
  label_w      = 80;
  rbutton_w    = 20;
  
  list_h       = 90;
  list_w       = 180;
  //  axes_w       =margin_x+list_w+4*margin_x+label_w+margin_x+rbutton_w+margin_x
  axes_w       = margin_x+list_w+margin_x+list_w+margin_x
  axes_h       = 8*margin_y+2*label_h+3*rbutton_w+list_h+button_h
  defaultfont  = "arial"; // Default Font
  button_w     = (axes_w-4*margin_x)/3;
  edit_w       = axes_w-5*margin_x-label_w-rbutton_w-label_h;
  
  if  guiHandle<>[] then
    ud=guiHandle.user_data
    if ud.archi==S.Designs.archi then 
      return,
    else
      delete(guiHandle.children)
      guiHandle.axes_size=[axes_w,axes_h]
    end
  else
    [path, fname] = fileparts(S.Path)
    guiHandle=figure("dockable", "off", ...
                     "menubar", "none", ...
                     "toolbar", "none", ...
                     "infobar_visible", "off",  ...
                     "axes_size", [axes_w,axes_h],...
                     "figure_name", msprintf(_("%s: Import manager(%%d)"),fname),...
                     "default_axes","off",...
                     "event_handler_enable", "off",...
                     "visible","on",...
                     "resize","off",...
                     "icon",sisotoolPath()+"/icons/sisotool32.png",...
                     "closerequestfcn","sisotoolGuiClose");
  end
  archiSpecs=sisotoolGetArchiSpecs(S.Designs.archi)
  
  y1=axes_h-margin_y-label_h;
  x1=margin_x;
  xm=x1
  ym=y1
  
  //subsystem selection
  CreateLabel(guiHandle,xm,ym,list_w,label_h,_("Which subsystem ?"))
  Model=CreateList(guiHandle,xm,ym-list_h,list_w,list_h,...
                   [archiSpecs.plant_desc+" "+archiSpecs.plant,...
                    archiSpecs.tunable_desc+" "+archiSpecs.tunable],""); 
  Model.user_data=  [archiSpecs.plant  archiSpecs.tunable]              
  
  //Available systems selection
  xa=xm+margin_x+list_w
  ya=ym
  CreateLabel(guiHandle,xa,ya,list_w,label_h,_("Available systems?"))
  Avail=CreateList(guiHandle,xa,ya-list_h,list_w,list_h,"","")
  Avail.value=0;
 

  //Source selection
  //radio buttons
  dt=S.Designs.dt
  Rcallback=msprintf("sisotoolImportCallbacks(""%s"",%d,%s)","Radio",win,sci2exp(dt))
  Ecallback=msprintf("sisotoolImportCallbacks(""%s"",%d,%s)","Edit",win,sci2exp(dt))
  Bcallback=msprintf("sisotoolImportCallbacks(""%s"",%d,%s)","Browse",win,sci2exp(dt))
  Icallback=msprintf("sisotoolImportCallbacks(""%s"",%d,%s)","Import",win,sci2exp(dt))
  xs=margin_x
  ys=ym-list_h-margin_y-label_h
  CreateLabel(guiHandle,xs,ys,label_w+margin_x+rbutton_w,label_h,_("Import from ?"))
 
  xr=xs;xl=xr+margin_x+rbutton_w;xe=xl+margin_x+label_w;xb=xe+margin_x+edit_w
  
  yr=ys-label_h-margin_y

  R1=CreateRadio(guiHandle,xr,yr,rbutton_w,rbutton_w,Rcallback,"Import");
  R1.tag="File";
  xl=xr+margin_x+rbutton_w
  CreateLabel(guiHandle,xl,yr,label_w,label_h,_("File"))
  
  E1=CreateEdit(guiHandle,xe,yr,edit_w,label_h,"",Ecallback)
  E1.tag="File";
  
  //browse files button

  Browse=CreateButton(guiHandle,xb,yr,label_h,label_h,_("..."),Bcallback)
  
  yr=yr-label_h-margin_y
  R2=CreateRadio(guiHandle,xr,yr,rbutton_w,rbutton_w,Rcallback,"Import");
  R2.tag="Workspace";
  CreateLabel(guiHandle,xl,yr,label_w,label_h,_("Workspace"))
  E2=R2;
  
  yr=yr-label_h-margin_y
  R3=CreateRadio(guiHandle,xr,yr,rbutton_w,rbutton_w,Rcallback,"Import")
  R3.tag="Expression";
  CreateLabel(guiHandle,xl,yr,label_w,label_h,_("Expression"))
  E3=CreateEdit(guiHandle,xe,yr,edit_w,label_h,"",Ecallback)
  E3.tag="Expression";
  //buttons
  yb=yr-margin_y-button_h
  xb=margin_x

  import=CreateButton(guiHandle,xb,yb,button_w,button_h,_("Import"),Icallback)
  import.enable=%f
  xb=xb+button_w+margin_x
  CreateButton(guiHandle,xb,yb,button_w,button_h,_("Help"),...
                      "help sisotoolImportManager")

  
  ud=struct("mainH",mainH,"type","ImportSelector","handles",[R1 R2 R3 E1 E3 Browse Avail Model import])
  set(guiHandle,"user_data", ud)
  guiHandle.axes_size=[axes_w,axes_h];
  
  R3.value=1
  E1.enable='off';
  E3.enable='on';
  Browse.enable='off'
endfunction
