// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolSelectComponent()
  Item=gcbo.Value;
  gui=gcbo.parent.parent
  ud=gui.user_data
  mainH=ud.mainH //main editor handle
  //The gui is formed by 4  vertical lists [Type Location Damping Frequency]
  //  see sisotoolCreateCGUI
  //mprintf("win=%s,gui_id=%s Item=%s\n",sci2exp(win,0),sci2exp(Item,0),sci2exp(gui_id,0))
  selectionHandles=ud.selectionHandles;
  viewHandles=selectionHandles.views;
  for k=1:4
     viewHandles(k).value=Item;
  end
  sisotoolEditSelected(gui,mainH,selectionHandles)
endfunction




