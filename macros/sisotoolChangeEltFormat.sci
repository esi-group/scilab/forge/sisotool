// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [initial,lowbound,highbound]=sisotoolChangeEltFormat(dt,rtyp,style,initial,lowbound,highbound)
//callback of sisotoolOptimGui, called when complex element or lead lag
//reprepresntation is changed
  if rtyp==1 then //complex pole or zero
    if style==1 then //(Wn,Zeta) -> (R,I)
      wn=initial(1);zeta=initial(2)
      z=wnzeta2complex(dt,wn,zeta)
      initial=[real(z);imag(z)]
      lowbound=[-%inf; -%inf]
      highbound=[%inf; 0]
    else // (R,I)-> (Wn,Zeta)
      z=complex(initial(1),initial(2))
      [wn,zeta] = damp(z,dt);
      initial=[wn;zeta];
      lowbound=[0;-1]
      highbound=[%inf; 1]
    end
  else //lead or lag
    if style==1 then //(Phasemax,Wmax) -> (Zero,Pole)
      Phasemax=initial(1);Wmax=initial(2)
      alpha=(1-sind(Phasemax))/(1+sind(Phasemax))
      z=-Wmax*sqrt(alpha)
      p=z/alpha
      initial=[z;p];
      lowbound=[-%inf;-%inf]
      highbound=[%inf;%inf]
    else //(Zero,Pole)  -> (Phasemax,Wmax)
      z=initial(1);p=initial(2)
      alpha=abs(z/p);
      Wmax=abs(z)/sqrt(alpha);
      Phasemax=-asind((alpha-1)/(alpha+1));
      initial=[Phasemax;Wmax];
      lowbound=[0;90];
      highbound=[%inf;90];
    end
  end
endfunction

