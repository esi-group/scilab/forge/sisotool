// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function r=sisotoolIsSISO(T,dt)
  //checks if the variable T is a siso dynamical system with a time
  //domain compatible with dt
  r=%f
  
 if (((or(typeof(T)==["rational","state-space","zpk"]))&T.dt==dt)|...
       (type(T)==1&isreal(T)))&size(T,'*')==1 then
   r =%t
 end
endfunction
