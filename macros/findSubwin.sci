// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [ax,k]=findSubwin(x,y)
  fig=gcf();
  sz=fig.axes_size;
  ax=[];
  C=fig.children;
  C=C(C.type=="Axes")
  for k=1:size(C,'*')
    ax=C(k)
    bounds=ax.axes_bounds.*[sz sz]
    if x>=bounds(1)&x<(bounds(1)+bounds(3))&y>=bounds(2)&y<(bounds(2)+bounds(4)) then
      sca(ax)
      break
    end
  end
endfunction
