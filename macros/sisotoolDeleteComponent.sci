// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolDeleteComponent(entityType,k) 
  ax=gca()//current axes has been set by sisotoolEditorsEvents
  editorData=sisotoolGetEditorData(ax)
  compensatorName=editorData.tunable
  mainH=ax.parent
  S=sisotoolGetSession(mainH)
  cur=sisotoolGetCElement(S,compensatorName,entityType,k);
  S=sisotoolAddHistory(S,"Compensator","delete",compensatorName,entityType,k,cur)
  S=sisotoolDeleteCElement(S,compensatorName,entityType,k)
  sisotoolSetSession(S,mainH);
  sisotoolRedraw(mainH);
  sisotoolUpdateOptimGui(mainH)
endfunction
