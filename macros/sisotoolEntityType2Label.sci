// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function lab=sisotoolEntityType2Label(t)
  //converts a controller entity field name into a gui label ( for OptimGui)
  select t //see sisotoolOptimGui
  case "gain"
    lab=_("Gain")
  case "poles"
    lab=_("Pole");
  case "zeros"
    lab=_("Zero");
  case "leadslags"
    lab=_("Lead or Lag");
  case "lags"
    lab=_("Lag");
  case "notches"
    lab=_("Notch");
  case "notches_width"
    lab=_("Notch width marker");
  case "requirements"
    lab=_("Requirement")
  case "loci"
    lab=_("Locus")
  end
endfunction
