// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolApplyEditors()
//callback of the "Apply" button created by sisotoolEditorsGui
 
  guiHandle=gcbo.parent; //the handle on the Editors selector GUI
  ud=get(guiHandle,"user_data")
  mainH=ud.mainH
  S=sisotoolGetSession(mainH);
 
  //Retreive selected editors

  OL_h=ud.OL_h; //array of open loop editors controls
  CL_h=ud.CL_h; //2D array of closed loop editors controls, last column
                //gives tunable index

  [l_OL,k_OL]=find(matrix(OL_h.value,size(OL_h))==1)
  [l_CL,k_CL]=find(matrix(CL_h(:,1:$-1).value,size(CL_h)-[0 1])==1)

  n_CL=size(l_CL,"*");
  n_OL=size(l_OL,"*");
  if n_CL>0 then 
    tunable_index=CL_h(l_CL,$).value
  else
    tunable_index=[]
  end
  if n_OL+n_CL>6 then
     messagebox(_("Too many selected editors (max=6)"),"modal")
     return
  end

  [tunable,out_of,in,out]=sisotoolGetArchiSpecs(S.Designs.archi,["tunable","out_of","in","out"])
  //Check if currently opened editors needs to be changed
  subWins=S.EditorsSettings.subWins
  nsubwins=size(subWins,'*')
  
  //Retrieve old selection
  old_l_OL=[];old_k_OL=[];
  old_l_CL=[];old_k_CL=[];old_tunable=[]
  EditorsSpecs=sisotoolGetEditorsSpecs()
  OL_editor_types=EditorsSpecs.OL_editor_types
  CL_editor_types=EditorsSpecs.CL_editor_types
  
  for sub=1:nsubwins
    subWin=subWins(sub);
    select subWin.type
    case "olbm" //part of a bode plot
      old_l_OL=[old_l_OL,find("bode"==OL_editor_types)];
      old_k_OL=[old_k_OL,find(subWin.out_of==out_of)];
    case "clbm" //part of closed loop bode plot
      old_l_CL=[old_l_CL find(subWin.in==in)];
      old_k_CL=[old_k_CL find(subWin.out==out)];
      old_tunable=[old_tunable find(subWin.tunable==tunable)];
    case "olbp"
    case "clbp"
    else
      old_l_OL=[old_l_OL,find(subWin.type==OL_editor_types)];
      old_k_OL=[old_k_OL,find(subWin.out_of==out_of)];
    end
  end

  if and(l_OL==old_l_OL)&size(l_CL,"*")==size(old_l_CL,'*')then
    //same editors
    //only transfert functions may have changed

    for sub=1:size(subWins,"*")-2*n_CL //update open loop transfer functions
      subWins(sub).name=out_of(k_OL(sub))
    end
    
    //the closed loop editors are the last subwindows
    sub=size(subWins,"*")-2*n_CL
    for i=1:n_CL
      sub=sub+1
      tunables=CL_h(l_CL(i),$).string
      subWins(sub).type="clbm"
      subWins(sub).in=in(l_CL(i));
      subWins(sub).out=out(k_CL(i));
      subWins(sub).tunable=tunables(tunable_index(i));
      sub=sub+1      
      subWins(sub).type="clbp"
      subWins(sub).in=in(l_CL(i));
      subWins(sub).out=out(k_CL(i));
      subWins(sub).tunable=tunables(tunable_index(i));
    end
    S.EditorsSettings.subWins=subWins
    sisotoolSetSession(S,mainH)
    sisotoolRedrawEditors(mainH)

    return
  end
  
  //At least one subwindow changes all subwindows are erased,
  //selection is modified
  
  mainH.immediate_drawing="off";
  //remove the axes associated with currently opened editors
  for i=1:size(S.EditorsSettings.handles)
    delete(S.EditorsSettings.handles(i));
  end
  subWins_old=S.EditorsSettings.subWins;
  optSettings_old=S.OptimSettings.Editors;
  gridded_old=S.EditorsSettings.gridded
  refreshed_old=S.EditorsSettings.refreshed;
  subWins=struct();
  optSettings=list();
  gridded=[];
  refreshed=[];
  //Open loop editors
  sub=1;
  [w,kept,kept_old]=intersect(l_OL,old_l_OL)
  for i=1:size(l_OL,'*')
    sub_old=kept_old(i==kept)
    if sub_old<>[] then 
      subWins(sub)=subWins_old(sub_old)
      gridded(sub)=gridded_old(sub_old);
      optSettings(sub)=optSettings_old(sub_old);
      refreshed(sub)=refreshed_old(sub_old);
      if OL_editor_types(l_OL(i))=="bode" then
        //update also the phase plot properties
        sub=sub+1
        subWins(sub)=subWins_old(sub_old+1)
        gridded(sub)=gridded_old(sub_old+1);
        refreshed(sub)=refreshed_old(sub_old+1);
        optSettings(sub)=optSettings_old(sub_old+1);
      end
    elseif OL_editor_types(l_OL(i))=="bode" then
      subWins(sub).type="olbm"
      subWins(sub).out_of=out_of(k_OL(i));
      subWins(sub).reqProps=sisotoolRequirementTypes("olbm");
      optSettings(sub)=struct();
      gridded(sub)=%f;
      refreshed(sub)=%t;
      sub=sub+1
      subWins(sub).type="olbp"
      subWins(sub).out_of=out_of(k_OL(i));
      subWins(sub).reqProps=sisotoolRequirementTypes("olbp");
      optSettings(sub)=struct();
      gridded(sub)=%f;
      refreshed(sub)=%t;
    else
      subWins(sub).type=OL_editor_types(l_OL(i));
      subWins(sub).out_of=out_of(k_OL(i));
      subWins(sub).reqProps=sisotoolRequirementTypes(subWins(sub).type);
      optSettings(sub)=struct();
      gridded(sub)=%f;
      refreshed(sub)=%t;
    end
    sub=sub+1;
  end
  //Closed loop editors (they are at the end)
  
  [w,kept,kept_old]=intersect(l_CL,old_l_CL)
  if isempty(subWins_old) then
    kb=[]
  else
    kb=find(list2vec(subWins_old.type)=="clbm");
  end
 // if kb<>[] then kept_old=kept_old+kb-1;end 
  for i=1:size(l_CL,'*')
    //sub_old=kept_old(i==kept)
    sub_old=kb(1==kept)

    tunables=CL_h(l_CL(i),$).string
    if sub_old<>[] then 
      // This editor is kept but out or tunable may have changed
      //clbm
      subWins(sub)=subWins_old(sub_old)
      subWins(sub).out=out(k_CL(i));
      subWins(sub).tunable=tunables(tunable_index(i));
      gridded(sub)=gridded_old(sub_old);
      optSettings(sub)=optSettings_old(sub_old);
      refreshed(sub)=refreshed_old(sub_old);
      
      //clbp
      sub=sub+1;
     
      subWins(sub)=subWins_old(sub_old+1)
      subWins(sub).out=out(k_CL(i));
      subWins(sub).tunable=tunables(tunable_index(i));
      gridded(sub)=gridded_old(sub_old+1);
      optSettings(sub)=optSettings_old(sub_old+1);
      refreshed(sub)=refreshed_old(sub_old+1);
    else 
      //New closed loop editor
      subWins(sub).type="clbm"
      subWins(sub).in=in(l_CL(i));
      subWins(sub).out=out(k_CL(i));
      subWins(sub).tunable=tunables(tunable_index(i));
      subWins(sub).reqProps=sisotoolRequirementTypes("clbm");
      gridded(sub)=%f
      optSettings(sub)=struct();
      sub=sub+1
      subWins(sub).type="clbp"
      subWins(sub).in=in(l_CL(i));
      subWins(sub).out=out(k_CL(i));
      subWins(sub).tunable=tunables(tunable_index(i));
      subWins(sub).reqProps=sisotoolRequirementTypes("clbp");
      gridded(sub)=%f
      optSettings(sub)=struct();
    end
    sub=sub+1
  end
  nsubwins=size(subWins,"*");
  S.OptimSettings.Editors=optSettings;
  S.EditorsSettings.refreshed=%t(ones(nsubwins,1));
  S.EditorsSettings.handles=list();
  S.EditorsSettings.subWins=subWins;
  S.EditorsSettings.gridded=gridded;
  S=sisotoolCreateEditors(mainH,S);
  sisotoolSetSession(S,mainH);
  sisotoolRedrawEditors(mainH);
  sisotoolRedrawReqs(mainH);
  mainH.immediate_drawing="on";
  if size(subWins_old,"*")==0 then 
    //to fix a bug (the axes is not drawn)
    mainH.figure_size(2)=mainH.figure_size(2)+1;
  end
endfunction
