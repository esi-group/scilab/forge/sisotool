// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolHelpMenu(fig)
   w=string(fig.figure_id);
   Help      = uimenu("parent", fig,...
                      "label", _("Help"))
   About     = uimenu("parent", tools,...
                      "label", _("About"),...
                      "callback",list(4,"execstr(""sisotoolAbout("+w+")"")"));

endfunction

function sisotoolAbout(win)
  messagebox(["Sisotool for Scilab"
              "Copyright 2010 - INRIA - Serge Steer"],"About sisotool","info")
              
endfunction
