// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function S=sisotoolDefaultSession(archi,dt)
//initializes the session data structure.
  if argn(2)<2 then dt="c";end
  if argn(2)<1 then archi="archi1";end
  if and(archi<>"archi"+string(1:6)) then 
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),...
                   "sisotoolDefaultSession",1,strcat("archi"+string(1:6),",")))
  end
  if ~(dt=="c"||(type(dt)==1&(size(dt,"*")==1&isreal(dt)&dt>0))) then
    error(msprintf(_("%s: Wrong value for input argument #%d: positive scalar or ""c"" expected.\n"),...
                   "sisotoolDefaultSession",2,))
  end
  [tunables,plants]=sisotoolGetArchiSpecs(archi, ["tunable","plant"])
  S=sisotoolNewSession()
  S.Designs.archi=archi
  for i=1:size(tunables,"*")
    S.Designs(tunables(i))=sisotoolNewController(dt);
  end
   for i=1:size(plants,"*")
    S.Designs(plants(i))=syslin(dt,1,1);
  end
  S.Designs.dt=dt
  subWins=struct();
  subWins(1)=struct("type","rlocus","out_of","C","reqProps",sisotoolRequirementTypes("rlocus"));

  S.EditorsSettings.subWins=subWins
  nsub=size(subWins,"*")
  S.EditorsSettings.gridded=%f(ones(1,nsub))
  S.EditorsSettings.refreshed=%t(ones(1,nsub))
  S.OptimSettings=sisotoolOptimDefaults(S.Designs,S.EditorsSettings,S.AnalysersSettings)
  
endfunction
