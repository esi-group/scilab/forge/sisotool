// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function s=sisotoolC2tf(C,exclude)
  if argn(2)==2 then
    entityType=exclude(1);
    sel=exclude(2);
    C(entityType)(:,sel)=[];
  end
  
  Z=[C.zeros   C.leadslags(1,:)  C.notches(1,:)];
  P=[C.poles C.leadslags(2,:), C.notches(2,:)];
  Zr=real(Z(imag(Z)==0));
  Zc=Z(imag(Z)<>0);
  Pr=real(P(imag(P)==0));nPr=size(Pr,'*');
  Pc=P(imag(P)<>0);nPc=size(Pc,'*');
 
  s=zp2tf([Zr Zc conj(Zc)].',[Pr Pc conj(Pc)].',C.gain,C.dt)
endfunction
