// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function C=PIDTuning(sys,PIDtype,method,N,varargin)

//Reference: "Advanced PID Control"   ,K. J. Åström and T. Hägglund.
//(6th edition 2006)
  
//IMC p 193
//SIMC 195
//cohen p
//PP (p 175-177)

//TBD check if sys contains more than one integrator
  methods=["pp","znsr","znfr","chr","amigosr","amigofr"]
  
  select method
  case "pp"
     C=PP(sys,PIDtype,N,varargin(:))//wn,zeta
  case "znsr"
    //TBD check if sys  is stable
    C=ZNSR(sys,PIDtype,N)
  case "znfr"
    //TBD check if sys  is stable
    //TBD check if sys order is at least 2
    C=ZNFR(sys,PIDtype,N)
  case "chr"
    //TBD check if sys  is stable
    C=CHR(sys,PIDtype,N,varargin(:))//in,overshoot
  case "cc"
    //Cohen-Coon (p 167)
  case "amigosr"
    //TBD check if sys  is stable
    C=AMIGOSR(sys,PIDtype,N)
  case "amigosfr"
    //TBD check if sys  is stable
    C=AMIGOFR(sys,PIDtype,N)
  case "rptuned"
    C=RPTuned(sys,PIDtype,N,varargin(:))
  else
    error(msprintf("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n",...
                   "PIDTuning",3,sci2exp(methods)))
  end
  if C<>[]&sys.dt<>"c" then
    C=dscr(C,sys.dt)
  end
endfunction

function [a,L]=IPD(sys)
//approximate sys by an integrator plus delay
//K*exp(-L*s)/s 
//with K=a/L:
//L : delay  
//a : intersection of the steepest tangeant with vertical axis
//Reference: "Advanced PID Control"  ,K. J. Åström and T. Hägglund.
  if typeof(sys)=="iodelay" then
    iodelay=sys.iodelay
    sys=sys.H
  else
    iodelay=0
  end
  [tfinal,dt] =sim_horizon(sys);
  t=0:dt:2*tfinal;
  if sys.dt=="c" then
    y=csim("step",t,sys)
  else
    y=flts(ones(t),sys)
  end
  dy=diff(y)/dt;
  [p,k]=max(abs(dy));// max slope point
  p=dy(k);
  a=y(k)-p*t(k);
  L=-a/p+iodelay; //delay
endfunction


function C=CC(sys,PIDtype,N)
//From "Linear Feedback Control" by Dingyu Xue, YangQuan Chen, and Derek
//P. Atherton. -p201

  [K,T,L]=FOPD(sys)
  a=K*L/T;tau=L/(L+T)  
  select PIDtype
  case "P"
    C=PID((1+0.35*tau/(1-tau))/a,[],[],[])
  case "PI"
    C=PID(0.9*(1+0.92*tau/(1-tau))/a,(3.3-3*tau)*L/(1+1.2*tau),[],[])
  case "PID"
    C=PID(1.35*(1+0.18*tau/(1-tau))/a,(2.5-2*tau)*L/(1-0.39*tau),...
          0.37*(1-tau)*L/(1-0.87*tau),[])
  case "PIDF"
    C=PID(1.35*(1+0.18*tau/(1-tau))/a,(2.5-2*tau)*L/(1-0.39*tau),...
          0.37*(1-tau)*L/(1-0.87*tau),N)
  end
  
endfunction
function C=ZNSR(sys,PIDtype,N)
//Ziegler Nichols step response
//Reference: "Advanced PID Control" ,K. J. Åström and T. Hägglund. (p160)
  [Kp,T,L]=FOPD(sys)
  a=Kp*L/T;
  // p160 (6.1)
  select PIDtype
  case "P"
    C=PID(1/a,[],[],[])
  case "PI"
    C=PID(0.9/a,3*L,[],[])
  case "PID"
    C=PID(1.2/a,2*L,L/2,[])
  case "PIDF"
    C=PID(1.2/a,2*L,L/2,N)
  end
  
endfunction

function C=ZNFR(sys,PIDtype,N)
//Ziegler Nichols frequency reponse
//Reference: "Advanced PID Control" ,K. J. Åström and T. Hägglund. (p 161)
  [g,f]=g_margin(sys)
  if g==[]|f==0 then C=[];return;end
  Ku=10^(g/20);
  Tu=1/f;
  // p161 (6.2)
  select PIDtype
  case "P"
    C=PID(Ku/2,[],[],[])
  case "PI"
    C=PID(0.4*Ku,0.8*Tu,[],[])
  case "PID"
    C=PID(0.6*Ku,0.5*Tu,0.125*Tu,[])
  case "PIDF"
    C=PID(0.6*Ku,0.5*Tu,0.125*Tu,N)
  end
  
endfunction



function C=CHR(sys,PIDtype,N,in,overshoot)
//Chien Hrones Reswick method
//Reference: "Advanced PID Control" ,K. J. Åström and T. Hägglund. (p166)
//From "Linear Feedback Control" by Dingyu Xue, YangQuan Chen, and Derek P. Atherton (p 199)
//https://electrical2005.files.wordpress.com/2016/11/linear-feedback-control-analysis-and-design-with-matlab.pdf  
  
  [K,T,L]=FOPD(sys)
  a=K*L/T;
  if argn(2)<5 then 
    overshoot=%f;
  else
    //TBD test of overshoot parameter 
  end
  if argn(2)<4 then 
    in="setpoint"
  else
    //TBD test of in parameter 
  end
  
 // [a,L]=IPD(sys); 
  select in
  case "disturbance" 
    //load disturbance response
    if ~overshoot then
      select PIDtype
      case "P"
        C=PID(0.3/a,[],[],[])  
      case "PI"
         C=PID(0.6/a,4*L,[],[])  
      case "PID"
         C=PID(0.95/a,2.4*L,0.42*L,[])  
      case "PIDF"
         C=PID(0.95/a,2.4*L,0.42*L,N)  
      end
    else //20% overshoot
      select PIDtype
      case "P"
        C=PID(0.7/a,[],[],[])  
      case "PI"
        C=PID(0.7/a,2.3*L,[],[])  
      case "PID"
        C=PID(1.2/a,2*L,0.42*L,[])  
      case "PIDF"
        C=PID(1.2/a,2*L,0.42*L,N)  
      end
    end
  case "setpoint" //Astrom p167, Atherton p199
    if ~overshoot then
      select PIDtype
      case "P"
        C=PID(0.3/a,[],[],[])  
      case "PI"
        C=PID(0.35/a,1.2*T,[],[])
      case "PID"
         C=PID(0.6/a,T,0.5*L,[])  
      case "PIDF"
         C=PID(0.6/a,T,0.5*L,N)  
      end
    else  //20% overshoot
      select PIDtype
      case "P"
        C=PID(0.7/a,[],[],[])  
      case "PI"
        C=PID(0.6/a,T,[],[])
      case "PID"
         C=PID(0.95/a,1.4*T,0.47*L,[])  
     case "PIDF"
         C=PID(0.95/a,1.4*T,0.47*L,N)  
      end
    end
  end

endfunction
function C=AMIGOSR(sys,PIDtype,N)
  [Kp,T,L]=FOPD(sys)
  if T==%inf then //system contains an integrator  (p 233)
    select  PIDType
    case "P" 
      C = PID(0.3/(Kp*L),[],[],[]);
    case "PI" //p 229 (7.6)
      C = PID(0.35/(Kp*L),13.4*L,[],[]);
    case "PID" //p 233  (7.8)
      C = PID(0.45/Kp,8*L,0.5*L,[]);
    case "PIDF" //p 233  (7.8)
      C = PID(0.45/Kp,8*L,0.5*L,N);
    end
  else
    select  PIDType
    case "P" 
      C = PID(0.3/(Kp*L/T),[],[],[]);
    case "PI" //p 228 (7.5)
      Ti=0.35*L+13*L*T^2/(T^2+12*L*T+7*L^2)
      K=0.15/Kp+(0.35-L*T/(L+T)^2)*T/Kp/L
      C = PID(K,Ti,[],[]);
    case "PID" //p 233 (7.7)
      K=1/Kp*(0.2+0.45*T/L)
      Ti=(0.4*L+0.8*T)/(L+0.1*T)*L;
      Td=0.5*L*T/(0.3*L+T);
      C = PID(K,Ti,Tp,[])
    case "PIDF" //p 233 (7.7)
      K=1/Kp*(0.2+0.45*T/L)
      Ti=(0.4*L+0.8*T)/(L+0.1*T)*L;
      Td=0.5*L*T/(0.3*L+T);
      C = PID(K,Ti,Tp,N)
    end
  end
endfunction
function C=AMIGOFR(sys,PIDtype,N)
//Approximate MIGO frequency response
//Reference: "Advanced PID Control" ,K. J. Åström and T. Hägglund.
  [g,f]=g_margin(sys)
  if g==[]|f==0 then C=[];return;end
  Ku=10^(g/20);
  Tu=1/f;
  kappa=1/(Ku*K);
  select PIDtype
  case "P"
    C=PID(Ku/2,[],[],[])
  case "PI" //p239
    C=PID(0.16*Ku,Tu/(1+4.5*kappa),[],[])
  case "PID" //p 241
    K=(0.3-0.1*kappa^4)*Ku
    Ti=0.6/(1+2*kappa)*Tu
    Td=0.15*(1-kappa)/(1-0.95*kappa)*Tu
    C=PID(K,Ti,Td,[])
 case "PIDF" //p 241
    K=(0.3-0.1*kappa^4)*Ku
    Ti=0.6/(1+2*kappa)*Tu
    Td=0.15*(1-kappa)/(1-0.95*kappa)*Tu
    C=PID(K,Ti,Td,N)
  end
endfunction
function C=RPTuned(G,PIDtype,N,p)
//G the plan model
//C the compensator model
//reference: "Linear Systems", Henry Bourlès, Wiley 2010
  select PIDtype
  case "P"
    //p gives the frequency in Hz where |C*G|=1
    K=1/abs(repfreq(G,p))
    C=PID(K,[],[],[])
  case "PI" //p 135
    //p(1) gives the frequency in Hz where |C*G|=1
    //p(2) gives the phase margin in degree
    w=2*%pi*p(1);
    pm=p(2)*%pi/180;
    //K and Ti are the solutions of the following two equations.
    //|C(iw)*G(iw)|=1
    //arg(C(iw)*G(iw))=pm-%pi
    r=repfreq(G,p(1))
    k1=1/abs(r) //k1*G has  unit gain at w
    a=atan(imag(r),real(r)) //the plant argument
    if pm-%pi>=a then
      //requested phase margin cannot be obtained (too big)
      //pm must be less than a+%pi (take one degree less)
      pm=a+%pi-%pi/180;
    end
    if a<pm-%pi/2 then
      Ti=-1/(w*tan(pm-%pi-a));
      K=Ti*w/sqrt(1+((w*Ti))^2);
      C=PID(k1*K,Ti,[],[]);
    else
      //requested phase margin cannot be obtained (too small) => Ti<=0
      //switch to a I compensator such as |C(iw)*G(iw)|=1
      Ki=1/abs(repfreq(G/%s,p(1)))
      C=Ki/%s
    end
  case "PID" //inspired by p 138
    //p(1) gives the frequency in Hz where |C*G|=1
    //p(2) gives the phase margin  in degree
 
    w=2*%pi*p(1);
    pm=p(2)*%pi/180;
    r=repfreq(G,p(1))
    a=atan(imag(r),real(r)) //the plant argument
    if pm-3*%pi/2<a&a<pm-%pi/2 then
      c=-%pi+mp-a;
      S=%pi/2;
      //phase lead
      phi_d=(S+c)/2;
      
      alpha=(1+sin(phi_d))/(1-sin(phi_d))
      tau=1/(w*sqrt(alpha))
      //L=(1+α*τ*s)/(1+τ*s)

      //PI
      phi_i=(S-c)/2;
      Ti=1/(w*tan(phi_i)) //5.12
      gamma=(Ti*w)/sqrt(1+(Ti*w)^2)
      //PI= γ(1+1/(Ti*s))
      delta=1+(alpha-1)*tau1/tau2 //5.19
      N=alpha/delta-1
      Td=N*tau1
      K=k*delta
      Ti=delta*tau2
      C=PID(K,Ti,Td,N)
    else //no solution
      C=[]
    end
  case "PIDF" 
     //p(1) gives the frequency in Hz where |C*G|=1
    //p(2) gives the phase margin  in degree
 
    w=2*%pi*p(1);
    pm=p(2)*%pi/180;
    r=repfreq(G,p(1));
    a=atan(imag(r),real(r)); //the plant argument
    if pm-3*%pi/2<a&a<pm-%pi/2 then
      c=-%pi+pm-a;
      S=%pi/2;
      //phase lead L=(1/sqrt(α))*(1+α*τ1*s)/(1+τ1*s) -(5.12)
      phi_d=(S+c)/2;
      alpha=(1+sin(phi_d))/(1-sin(phi_d));//α 5.6
      tau1=1/(w*sqrt(alpha));//τ1
      
      //PI PI= γ(1+1/(τ2*s))
      phi_i=(S-c)/2;
      tau2=1/(w*tan(phi_i));
      gamma=(tau2*w)/sqrt(1+(tau2*w)^2);//5.13  γ
      //
      kappa=gamma/sqrt(alpha)/abs(r);
      //compute PIDF coefficients from L*PI
      delta=1+(alpha-1)*tau1/tau2; //5.19
      N=alpha/delta-1
//Td=N*tau1
      Td=-(tau1-tau2)*(alpha-1)*tau1/((alpha-1)*tau1+tau2)
      K=kappa*delta
      Ti=delta*tau2
      C=PID(K,Ti,Td,N)

    else //no solution
      C=[]
    end
   
  case "PDF" //p 134
  end
  
endfunction

function C=PP(sys,PIDtype,N,wn,zeta,alpha)
//Reference: "Advanced PID Control"   ,K. J. Åström and T. Hägglund. (p 175-177)
//sys must be:
//   either a proper first order system --> PI
//   or a strictly proprer second order system.-->PID
  if typeof(sys)=="state-space" then 
    sys=ss2tf(sys);
  elseif typeof(sys)=="zpk" then 
    sys=zpk2tf(sys)
  end
  select degree(sys.den)
  case 1 then //proper first order system p 175 6.6
    //G=(a+b*s)/(c+s); C=K*(1+1/(s*Ti))
    d=coeff(sys.den,1);
    a=coeff(sys.num,0)/d
    b=coeff(sys.num,1)/d
    c=coeff(sys.den,0)/d
    select PIDType
    case "P"
      //closed loop demominator: (w+s)
      K = -(c-wn)/(-b*wn+a)
    case "PI"      
      K=-(-2*a*wn*zeta+b*wn^2+a*c)/(-2*a*b*wn*zeta+b^2*wn^2+a^2)
      Ti=(-2*a*wn*zeta+b*wn^2+a*c)/((-b*c+a)*wn^2)
      Td=[]
    case "PID"
      if b==0 then
        K=1// ou tout autre valeur
        Td=(1/2)*(K*a-2*wn*z+c)/(K*a*wn*z)
        Ti=2*K*a*z/(wn*(K*a+c))
      else
        //G=(a+b*s)/(c+s) and b<>0
        //closed loop demominator: (alpha*wn+s)*(2*s*wn*z+s^2+wn^2)
        K = -(2*a*alpha*b*wn^2*z-alpha*b^2*wn^3-a*alpha*b*c*wn-2*a*b*c*wn*z+a*b*wn^2+a^2*c)/...
            (2*a*alpha*b^2*wn^2*z-alpha*b^3*wn^3-a^2*alpha*b*wn-2*a^2*b*wn*z+a*b^2*wn^2+a^3)
        Td = a*(-b*c+a)/(2*a*alpha*b*wn^2*z-alpha*b^2*wn^3-...
                         a*alpha*b*c*wn-2*a*b*c*wn*z+a*b*wn^2+a^2*c)
        Ti = (2*a*alpha*b*wn^2*z-alpha*b^2*wn^3-a*alpha*b*c*wn-...
              2*a*b*c*wn*z+a*b*wn^2+a^2*c)/((-b*c+a)*alpha*b*wn^3)
      end
      
    end
  case 2 then //strictly proper second order system  p180
    //G = (b1*s+b2)/(s^2+a1*s+a2));C = K*(Td*Ti*s^2+Ti*s+1)/(s*Ti)
    a=coeff(sys.den,1:2)
    b=coeff(sys.num,1:2)
    
    d=((alpha*b(1)*wn-b(2))*(b(1)^2*wn^2-2*b(1)*b(2)*wn*zeta+b(2)^2))
    
    Ki=alpha*wn^3*(a(1)*b(1)*b(2)-a(2)*b(1)^2-b(2)^2)/d
    Kd=(-alpha*b(1)^2*wn^3+...
       2*alpha*b(1)*b(2)*wn^2*zeta-...
         alpha*b(2)^2*wn+...
         b(1)*b(2)*wn^2-...
        2*b(2)^2*wn*zeta+...
        a(1)*b(2)^2-...
        a(2)*b(1)*b(2))/d
    Ti=K/Ki
    Td=Kd/K

  end
  C=PID(K,Ti,Td,N)
endfunction

function C=PID(Kp,Ti,Td,N)
//creates a continuous-time PID controller with a first order derivative
//filter.
//Kp : real scalar, 
//     the proportionnal term
//Ti : positive scalar or [] if no integral term, 
//     the integral time constant
//Td : non negative scalar or [] if no derivative term, 
//     the derivativetime constant
//N  : a non negative scalar or [] if no derivative filter, 
//     the filter divisor  
//C  : PID transfer function 
  if Ti==[]&Td==[] then
    C=syslin("c",Kp,1)
    return
  end
  if Ti<>[] then 
    Ci=1/(Ti*%s)
  else
    Ci=0
  end
  if Td<>[] then
    if N==[] then
      Cd=Td*%s
    else
      //with derivative filtering "Advanced PID Control" p 73
      Cd=Td*%s/(1+Td*%s/N)
    end
  else
    Cd=0
  end
  C=syslin("c",Kp*(1+Ci+Cd))
endfunction
