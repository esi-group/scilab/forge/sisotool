// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function S=sisotoolUpdateVersion(S)
  if or(S.Version<>sisotoolCurrentVersion()) then
    messagebox(_("Old sessions cannot be loaded yet"),"modal")
    S=[]
    return
  end
  upd=%f
  F=fieldnames(S.Designs)

  F=matrix(F(F<>"archi"&F<>"dt"),1,-1)
  u=%f(ones(F))
  dt=[]
  for k=1:size(F,"*")
    if type(S.Designs(F(k)))==1 then
      u(k)=%t
    elseif dt==[] then
      dt=S.Designs(F(k)).dt
    elseif dt<>S.Designs(F(k)).dt then
      mprintf("Incompatible time domains")
      return
    end
  end
  for k=find(u)
    upd=%t
    S.Designs(F(k))=syslin(dt,S.Designs(F(k)),1)
  end
  if and(fieldnames(S.Designs)<>"dt") then
    S.Designs.dt=dt;
    upd=%t
  end
  
  tunables=sisotoolGetArchiSpecs(S.Designs.archi, "tunable")
  for i=1:size(tunables,"*")
    C=S.Designs(tunables(i))
    if or(fieldnames(C)=="leads") then
      Cn=sisotoolNewController(C.dt);
      for c=["gain","poles","zeros","notches"]
        Cn(c)=C(c);
      end
      Cn.leadslags=[C.leads C.lags];
      S.Designs(tunables(i))=Cn
      upd=%t
    end
  end
  for i=1:size(tunables,"*")
    C=S.OptimSettings.Controller(tunables(i))
    if or(fieldnames(C)=="leads") then
      leadslags=C.leads;
      for ff=fieldnames(C.leads)'
        leadslags(ff)=[C.leads(ff) C.lags(ff)];
      end
      S.OptimSettings.Controller(tunables(i))=struct("gain",C.gain,"poles",C.poles,"zeros",C.zeros,"leadslags",leadslags,"notches",C.notches)
      upd=%t
    end
  end
  
  S.PIDSettings=struct("PIDType","P", "method","","out_of_index",1,"N",[],"parameters",list());
  F=fieldnames(S.OptimSettings)
  if and(F<>"Options") then
    Options=struct("Display","off","FunValCheck",[],...
                   "MaxFunEvals",300,"MaxIter" ,300,...
                   "OutputFcn","sisotoolOptimTrace","PlotFcns",[],...
                   "TolFun",1e-8,"TolX",1e-4)
    S.OptimSettings.Options=Options
    upd=%t
  end
  
  if upd then
    mprintf("Session has been updated")
  end
endfunction
