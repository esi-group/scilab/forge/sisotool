// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolSetLQGGui(gui)
  //resets the LQG GUI values if it is opened
  if type(gui)<>9 then return;end
  ud=get(gui,"user_data")
  frameC=gui.children(4);
  frameLQE=gui.children(3);
  frameLQI=gui.children(2);
  S=sisotoolGetSession(ud.mainH);
  sys=S.Designs;
  out_of=sisotoolGetArchiSpecs(sys.archi,"out_of");
  LQGSettings=S.LQGSettings
  Popup=frameC.children(2)
  Popup.string=out_of(:)
  Popup.value=LQGSettings(6)
  Tdisp=frameC.children(1);
  Tdisp.String=""; //bug 14578 workaround
  Tdisp.String=sisotoolC2tex(sys(out_of(LQGSettings(6))));
 
  He=ud.handles;//[Pe,Me,Xe,Ue,Ie]
  
  Pe.string=string(LQGSettings(1))
  Me.string=string(LQGSettings(2))
  Xe.string=string(LQGSettings(3))
  Ue.string=string(LQGSettings(4))
  Ie.string=string(LQGSettings(5))
endfunction
