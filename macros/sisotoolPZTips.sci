// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function str=sisotoolPZTips(h)
  c=h.parent
  h.mark_style=c.mark_style
  h.mark_foreground=c.mark_foreground
  h.mark_background=c.mark_background
  dfd=c.display_function_data
  h.background=9; //to fix a bug
  R=h.data(1);I=h.data(2);
  dt=c.user_data;
  dfd=c.display_function_data
  [wn,zeta]=damp(complex(R,I),dt);
  o=100*exp(-zeta*%pi/sqrt(1-zeta^2))
  frq=freqconv("Hz",dfd.units.frequency,wn/(2*%pi))
  if I==0 then
    str=msprintf(_("%s\n%s: %.3gs−¹\nDamping: %.3g\nOvershoot: %.3g%%\nFrequency: %.3g%s"),...
                 dfd.TF_name,dfd.pz,R,zeta,o,frq,dfd.units.frequency) 
  else
    o=100*exp(-zeta*%pi/sqrt(1-zeta^2))
    str=msprintf(_("%s\n%s: %.3g%+gi (s−¹)\nDamping: %.3g\nOvershoot: %.3g%%\nFrequency: %.3g%s"),...
                 dfd.TF_name,dfd.pz,R,I,zeta,o, frq,dfd.units.frequency) 
  end
endfunction
