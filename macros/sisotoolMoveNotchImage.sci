// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [w1,a]=sisotoolMoveNotchImage(Hr,X,Y)
//Moving Notch image in Black diagram
//Transfert function with moving notch can be written
// H(s)=Hr(s)*N(s)
// N(s)=(s^2-2w*zetaz*s+w^2)/(s^2-2w*zetap*s+w^2)
//The notch image is the point given by H(%i*w)
//Let XY=X+%i*Y the new desired  location for the notch Image. 
// One must have Hr(%i*w1)*N1(%i*w1)=XY
// N1(%i*w1)=zetaz1/zetap (zetap supposed to be fixed)
// that gives the equations
// zetaz1/zetap=XY/Hr(%i*w1). 
// with the constraints zetaz1 must be real and zetaz1/zetap<1. This leads to
// imag(XY/Hr(%i*w1))=0 (1) and 0<real(XY/Hr(%i*w1))<1 (2)
// denoting XY/Hr(%i*w1)=Num(w1)/Den(w1) the equation (1) gives
// imag(Num(w1)*conj(Den(w1)))=0. The positive real root of the polynomial 
// imag(Num(w1)*conj(Den(w1))) give a set of admissible values for w1.
//The zetaz1 can then be computed using zetaz1=XY*zetap/Hr(%i*w1) and (2)
// is used to restrict the w1 values set.
  
  w=poly(0,'w');
  XY=(X+%i*Y)
  h=(XY)/horner(Hr,%i*w);
  num=h.num*conj(h.den);
  r=roots(imag(num));
  w1=real(r(abs(imag(r))<1d-10&real(r)>0)); 
  if w1<>[] then
    a=real((XY)./horner(Hr,%i*w1));
    a=a(a>0&a<1)
    if a==[] then w1=[],end
  else
    a=[]
  end
endfunction
