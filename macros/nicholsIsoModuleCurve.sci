// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [phi,db]=nicholsIsoModuleCurve(mbf,npts)
//mbf : module in dB
//npts :number of curve points (default=100)
//phi (degree),db (dB) :curve coordinates in Nichols complex plane
  if argn(2)<2 then npts=100;end
  lmda=exp(log(10)/20*mbf);
  r=lmda/(lmda^2-1);
  crcl=exp(%i*(-%pi:(2*%pi/npts):%pi));
  lgmt=log(-r*crcl+r*lmda*ones(crcl));
  lgmt=lgmt(:);
  phi=180*(imag(lgmt)/%pi-ones(lgmt));
  db=20/log(10)*real(lgmt);
endfunction
