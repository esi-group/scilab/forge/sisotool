// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAddComplexPoles(win)
  mainH=get_figure_handle(win);
  mainH.event_handler_enable = "off";
  mainH.info_message=_("Click left where you want to add this pole");
  ax=gca()//current axes has been set by sisotoolEditorsEvents
  [btn,pt]=getPointInSubwin(ax) 
  mainH.info_message="";
  if pt==[] then 
    mainH.event_handler_enable = "on";
    return,
  end
  typ=ax.tag;
  editorData=sisotoolGetEditorData(ax);
  compensatorName=editorData.tunable;
  S=sisotoolGetSession(mainH);
  dt=S.Designs(compensatorName).dt;

  ok=%f;
  if typ=='rlocus' then
    pt(2)=abs(pt(2));
    //store only the pole with negative imaginary part
    v=pt(1)-imult(pt(2));
    ok=%t;
  elseif or(typ==["olbm" "olbp" "clbm" "clbp" ]) then
    //add a complex stable pole with a damping factor equal to 0.5 
    u=S.Preferences.units.frequency;
    wn=freqconv(u,"rd/s",-pt(1));
    zetap=0.5
    v=wnzeta2complex(dt,wn,zetap)
    ok=%t;
  elseif typ=="nichols" then
    h=editorData.loci;
    [d,ptp,ind,c]=orthProj(h.data,pt);
    if ind<>[] then
      frq=h.display_function_data.freq;
       u=S.Preferences.units.frequency;
      wn=freqconv(u,"rd/s",frq(ind)+(frq(ind+1)-frq(ind))*c);
      zetap=0.5
      v=wnzeta2complex(dt,wn,zetap)
      ok=%t;
    end
  end
  if ok then
    S=sisotoolAddHistory(S,"Compensator","add",compensatorName,"poles",v);
    S=sisotoolSetCElement(S,compensatorName,"poles",v);
    sisotoolSetSession(S,mainH);   
    sisotoolRedraw(mainH);
    sisotoolUpdateOptimGui(mainH);
  end
  mainH.event_handler_enable = "on";
endfunction
