// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAnalyserChanged()
//TBD pb quand il n'y a pas d'analyser d'ouvert et que l'on selectionne none
  k=gcbo.user_data
  h=gcbo();while h.type<>"Figure" then h=h.parent;end
  
  transferSelectionHandles=h.user_data.transferSelectionHandles;
  viewSelectionHandles=h.user_data.viewSelectionHandles

  if viewSelectionHandles(k).value==1 then 
    //this plot is removed
    //if some next plots are set move them left
    while viewSelectionHandles(k+1).value>1 then //shift left
      viewSelectionHandles(k).value=viewSelectionHandles(k+1).value;
      for l=1:size(transferSelectionHandles,1)
        transferSelectionHandles(l,k).value=transferSelectionHandles(l,k+1).value;
      end
      k=k+1;
    end
    
    viewSelectionHandles(k).value=1;
    viewSelectionHandles(k+1).visible="off"
    transferSelectionHandles(:,k).visible="off"
    transferSelectionHandles(:,k).value=0
    k=k-1
  elseif viewSelectionHandles(k+1).value==1 then
    viewSelectionHandles(k+1).visible="on"
  end
  if k==0 then
    return
  end
  transferSelectionHandles(:,k).enable=bool2s(viewSelectionHandles(k).value>1)
  transferSelectionHandles(:,k).visible=bool2s(viewSelectionHandles(k).value>1)

endfunction
