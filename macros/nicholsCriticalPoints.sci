function v=nicholsCriticalPoints(mn,mx)
//mn min value of the phase's frequency response (in degrees)
//mx max value of the phase's frequency response (in degrees)
//v the array 360*k+180 for k values which cover the [mn mx] interval
  vmn=ceil((mn-180)/360)*360+180
  vmx=floor((mx-180)/360)*360+180
  if vmx<vmn then
    //there is no k such that 360*k+180 is between mn and mx
    if vmn-mx>mn-vmx then
      v=vmx
    else
      v=vmn
    end
  else
    v=vmn:360:vmx;
  end
endfunction
