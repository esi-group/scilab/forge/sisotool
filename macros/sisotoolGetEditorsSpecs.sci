// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function r=sisotoolGetEditorsSpecs()
  OL_editor_names=["Root Locus","Bode plot","Black/Nichols"] ;
  OL_editor_types=["rlocus", "bode","nichols"];
  //TBD est-ce reellement necessaire de distinguer filter et bode?
  CL_editor_names=["Closed loop Bode plot"]
  CL_editor_types=["clbode"]
  r=struct("OL_editor_names",OL_editor_names,"OL_editor_types",OL_editor_types,...
           "CL_editor_names",CL_editor_names,"CL_editor_types",CL_editor_types)
endfunction
