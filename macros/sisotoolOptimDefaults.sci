// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function optimSettings=sisotoolOptimDefaults(Designs,EditorsSettings,AnalysersSettings)
  // The controller components 
  tunable=sisotoolGetArchiSpecs(Designs.archi, "tunable")
  ControllerSettings=struct()
  tp=["eltvalue","enabled","style","initial","lowbound","highbound"]
  for k=1:size(tunable,"*")
     block=S.Designs(tunable(k))
     gain=mlist(tp,%f,1,block.gain,0,%inf);
     //poles
     n=size(block.poles,2)
     kreal=find(imag(block.poles)==0)
     lb=[-%inf;0]*ones(1,n);
     hb=[%inf;%inf]*ones(1,n);
     hb(2,kreal)=0;
     Poles=mlist(tp,%f(ones(1,n)),ones(1,n),[real(block.poles);imag(block.poles)],lb,hb)
    
     //zeros
     n=size(block.zeros,2)
     kreal=find(imag(block.zeros)==0)
     lb=[-%inf;0]*ones(1,n);
     hb=[%inf;%inf]*ones(1,n);
     hb(2,kreal)=0;
     Zeros=mlist(tp,%f(ones(1,n)),ones(1,n),[real(block.zeros);imag(block.zeros)],lb,hb)
   
     //leads and lags
     n=size(block.leadslags,2)
     lb=[-%inf;-%inf]*ones(1,n);
     hb=[%inf;%inf]*ones(1,n);
     LeadsLags=mlist(tp,%f(ones(1,n)),ones(1,n),block.leadslags,lb,hb)
    
     //notches
     n=size(block.notches,2)
     lb=[0;-1;-1]*ones(1,n);
     hb=[%inf;1;1]*ones(1,n);
     Notches=mlist(tp,%f(ones(1,n)),ones(1,n),block.notches,lb,hb)
     ControllerSettings(tunable(k))=struct("gain",gain,"poles",Poles,"zeros",Zeros,"leadslags",LeadsLags,"notches",Notches);
  end
  //The Editors requirements
  Esettings=list()
  subWins=EditorsSettings.subWins
  for k=1:size(subWins,"*")
    subWin=subWins(k)
    reqProps=subWin.reqProps
    ind=sisotoolDefinedReqs(reqProps);
    s=struct()
    for i=1:size(ind,"*")
      s(reqProps(ind(i)).type)=%f;
    end  
    Esettings(k)=s
  end
    
  //The Analysers requirements
  Asettings=list()
  subWins=AnalysersSettings.subWins
  for k=1:size(subWins,"*")
    subWin=subWins(k)
    reqProps=subWin.reqProps
    ind=sisotoolDefinedReqs(reqProps);
    s=struct()
    for i=1:size(ind,"*")
      s(reqProps(ind(i)).type)=%f(ones(subWin.transferSelection));
    end
     Asettings(k)=s
  end
  optimSettings=struct("Controller",ControllerSettings,"Editors",Esettings,"Analysers",Asettings) 
endfunction
