function sisotoolPIDCallback()
  //Set parameters' uicontrol visibility
  frame=gcbo.parent
  frames=frame.parent.children(2:$)// [Method Type Compensator]
  //Set parameters' uicontrol visibility
  parameter_handles=frames(1).children(1:$-1);

  select frame.tag
  case "PIDType"
    N_handles=parameter_handles(7:8);
    N_handles.visible=gcbo.tag=="PIDF" ;
  case "PIDMethod"
    method=gcbo.user_data(gcbo.value);
    PP_handles=parameter_handles(3:6);//wn,zeta
    PP_handles.visible=method=="PP";
    CHR_handles=parameter_handles(1:2); //type,overshoot
    CHR_handles.visible=method=="CHR";
  end

endfunction
