// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function f=IMC_filter(dt,lambda,m,Pi)
//design IMC filter in case of a step input 
  //
// dt :    time domain
// lambda: parameter  
// m :     zeros-poles excess
// Pi:     array of closed RHP poles for P and V
// f :     the filter
// "Robust Process Control" M. Morari, E. Zafiriou, Prentice Hall 
//http://control.ee.ethz.ch/index.cgi?action=details&id=1759&page=publications
  
 
  k=size(Pi,"*");
  //Find nearly multiple poles in Pi and their multiplicity
  [Pu,mult]=nearly_multiples(Pi,1e-6)
  nm=max(mult)-1;
  if dt=="c" then //continuous time case. part I, P: 97-98
    m=max(m,1)
    if k==0 then 
      //no instable poles
      //f(s)=(1/lambda^(m-1))/((lambda*s+1)^(m-1)
      kf=1/lambda;
      pf=[zeros(m-1,1);-1/lambda];
      //f=syslin(dt,kf,real(poly(pf,"z","r")))
      f=zpk([],pf,kf,dt)
      return
    end
    //f(s)=(a_0+a_1*s+...+a_(k_1)*s^(k-1))/((lambda*s+1)^(m+k-1)
    N=%s^(0:k-1);
    //compute the numerators of the derivatives of f(s)
    for i=1:nm
      N=[N;
         derivat(N($,:))*(lambda*%s+1)-(m+k-2+i)*lambda*N($,:)];
    end
    A=[];B=[];
    for i=1:size(Pu,1)
       //equations f(Pu(i)) = 1
      A=[A; horner(N(1,:),Pu(i))];
      B=[B;(lambda*Pu(i)+1).^(m+k-1)];
      for j=1:mult(i)-1
        //equations df^(j)/d^(j)z (Pu(i)) = 0
        A=[A;horner(N(j+1,:),Pu(i))];  
        B=[B;0];
      end
    end
    //solve for numerator coefficients
    a=real(A\B)
    pf=-ones(m+k-1,1)/lambda
    zf=roots(poly(a/a($),"x","c"))
    kf=real(a($)/lambda^(m+k-1));
  else //discrete time case. part II,p: 193
    //Pi the unstable roots (including z=1) of the least common
    //denominator of P and V (p 189)
    if k==0 then 
      //no unstable poles. In this case the filter is given by
      //f(z)=(1-lambda)*z/((z-lambda)
      kf=1-lambda
      f=zpk(0,lambda,kf,dt)
      return
    end 
    //the filter is given by
    //f(z)=(a_0*z+...+a_(k-1)*z^k)*(1-lambda)/((z-lambda)*z^(k-1))
    // we want d^j f/dz^j(f(z)) =0 for z=Pi(i) and j=1:mult(i)-1

//    N=%z^(0:k-1)*(1-lambda);
    N=%z^(1:k)*(1-lambda);
    //compute the numerator of the derivatives
    for i=1:nm
      N=[N;
         derivat(N($,:))*%z*(lambda-%z)-N($,:)*((k-1+i)*(lambda-%z)-%z*i)];
    end
    //Form the linear equations relative to the unknowns a_0 ... a_(k-1)
    A=[];B=[];
    for i=1:size(Pu,1)
      //equations f(Pu(i)) = 1
      A=[A;horner(N(1,:),Pu(i))];
      B=[B;(Pu(i)-lambda)*Pu(i)^(k-1)];
      for j=1:mult(i)-1
        //equations df^(j)/d^(j)z (Pu(i)) = 0
        A=[A;horner(N(j+1,:),Pu(i))];  
        B=[B;0];
      end
    end
    //solve it
    a=real(A\B);
    //form the filter
    kf=real(a($)*(1-lambda))
    zf=[0;roots(real(poly(a/a($),"z","c")))];
    pf=[zeros(m+k-1,1);lambda];
  end
  f=zpk(zf,pf,kf,dt)
endfunction

