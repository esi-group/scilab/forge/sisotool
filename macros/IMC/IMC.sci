// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [C,Q,Qtild,f]=IMC(P,lambda,V)
//Robust Process Control" by Morari and Zafiriou 1989
//P:   the plant model (siso)
//C:   the controller 
//Reference
//  Robust Process Control" by Morari and Zafiriou 1989
  //tranformer ici P et Z en zpk
    
  if or(typeof(P)==["state-space","rational"]) then
    P=zpk(P)
  elseif typeof(P)<>"zpk" then 
    error(msprintf(_("%s: Wrong type for input argument #%d: Linear system expected.\n"),"IMC",1))
  end

  dt=P.dt
  if argn(2)<3 then
    if dt=="c" then
      nz=size(find(P.P{1}==0),"*")
      V=zpk([],zeros(nz+1,1),1,"c");
    else
      nz=size(find(P.P{1}==1),"*")
      V=zpk([],ones(nz+1,1),1,P.dt);
   
    end
  else
    if or(typeof(V)==["state-space","rational"]) then
      V=zpk(V)
    elseif typeof(V)<>"zpk" then 
      error(msprintf(_("%s: Wrong type for input argument #%d: Linear system expected.\n"),"IMC",2))
    end
    if P.dt<>V.dt then
      error(msprintf(_("%s: Incompatible input arguments #%d and #%d: Same time domain expected.\n"),"IMC",1,3))
    end
  end
  //Computes the H2 optimal controller of the plant P for an additive
  //perturbation signal V acting at the output of the plant.
  Qh=H2OptimalController(P,V)
  //Compute Qtild
  pP=P.P{1}
  pV=V.P{1}
  if dt=="c" then
    //Chapter 5
    Qtild=Qh
    Pi=pP(real(pP)>0)
    Pi=[Pi;pV(pV==0)]
    //if or(pV==0)&and(abs(Pi)>0) then Pi=[Pi;0];end
    m=size(Qtild.Z{1},"*")-size(Qtild.P{1},"*")
    if m<0 then //Qtild is strictly proper
      m=-1
    end
  else
    //Pi the unstable roots (including z=1) of the least common
    //denominator of P and V (p 189). V has at least as many poles at Z=1
    //as P and each strictly unstable poles of V is a pole of P
    pV=V.P{1};
    Pi=[pP(abs(pP)>1);pV(abs(pV)==1)]
    m=0
    Qtild= IMC_Controller(Qh,Pi)
  end
  
  f=IMC_filter(dt,lambda,m,Pi)
  Q=simplify_zp(Qtild*f,1e-8)
  PQ=simplify_zp(P*Q,1e-8)
  //take care of denominator zeros
  p1=real(poly(PQ.P{1},"x","r"));
  p2=PQ.K*real(poly(PQ.Z{1},"x","r"));
  d=max(degree(p1),degree(p2));
  p=coeff(p1-p2,0:d);
  kz=find(abs(p)<abs(coeff(p1+p2,0:d))*%eps/2);
  p(kz)=0;
  p=poly(p,"x","c");
  C=simplify_zp(Q*zpk(PQ.P{1},roots(p),1,Q.dt),1e-8)
endfunction


function Qtild= IMC_Controller(Qh,Pi)
// Qh: H2 Optimal Controller
// Pi: unstable roots (including z=1) of the least common denominator of
//     P and V. Set Pi=ones(m,1) for a stable system of type m 
//
// Assumptions:
//   V has at least as many poles at z=1 as P
//   each strictly unstable poles of V is also a a pole of P
 //set Pi=ones(m,1) for a stable system of type m 
// section 2.2.2,page  188-189
  Qh_poles=Qh.P{1}
  //kappa Qh poles with negative real part
  kappa=Qh_poles(real(Qh_poles)<0)
  Qm=zpk(kappa(:),zeros(kappa(:)),real(1/prod(1-kappa)),Qh.dt)
  //compute B
  [Piu,mult]=nearly_multiples(Pi,1e-4);
  if Piu<>[] then
    nm=max(mult);
    //numerator of Qm(1/z)
    num=real(poly(1.0./Qm.Z{1},"z","r")*Qm.K*prod(-Qm.Z{1}))
    N=%z^(0:sum(mult)-1)*num;
    for k=1:nm-1
      N=[N;derivat(N($,:))];
    end
    A=horner(N(1,:),1.0./Piu);b=ones(Piu);
    for i=1:size(Piu,"*")
      A=[A;horner(N(2:mult(i)-1,:),1.0./Piu(i))];
      b=[b;zeros(mult(i)-2,1)];
    end
    B=real(A\b)
    if size(B,"*")>1 then
      B=zpk(syslin(Qh.dt,horner(poly(B,"l","c"),1/%z)))
    end
    Qtild=Qh*(Qm*B)
  else
    Qtild=Qh*Qm
  end
  Qtild=simplify_zp(Qtild)
endfunction

