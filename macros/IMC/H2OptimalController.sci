// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function Qh=H2OptimalController(P,V)
//Computes the H2 optimal controller of the plant P for an additive
//perturbation signal V acting at the output of the plant.
// P: Plant model
// V: model of the additive signal acting at the output of the plant
  
// Assumptions:
//   V has at least as many integrator poles as P
//   each strictly unstable poles of V is also  a pole of P
//Reference:
// "Robust Process Control" by Morari and Zefiriou 1989
  
// Attention : 
//   pour P instable et non minimum phase, Matlab prend V=P/s
//   (perturbation en echelon a l'entree du plant au lieu de la sortie)
//   sinon V=1/s
  
  if or(typeof(P)==["state-space","rational"]) then
    P=zpk(P);
  elseif typeof(P)<>"zpk" then 
    error(msprintf(_("%s: Wrong type for input argument #%d: Linear dynamical system  expected.\n"),"H2OptimalController",1))
  end
  dt=P.dt;
  if argn(2)<2 then
    if dt=="c" then
      nz=size(find(P.P{1}==0),"*")
      V=zpk([],zeros(nz+1,1),1,"c");
    else
      nz=size(find(P.P{1}==1),"*")
      V=zpk([],ones(nz+1,1),1,P.dt);
    end
  else
    if or(typeof(V)==["state-space","rational"]) then
      V=zpk(V);
    elseif typeof(V)<>"zpk" then 
      error(msprintf(_("%s: Wrong type for input argument #%d: Linear system expected.\n"),"H2OptimalController",2))
    end

    if P.dt<>V.dt then
      error(msprintf(_("%s: Wrong type for input argument #%d: Linear system expected.\n"),"H2OptimalController",1,2))
    end
  end
  [zPa,pPa,kPa,zPm,pPm,kPm,kPi]=factorModel(P);
  [zVa,pVa,kVa,zVm,pVm,kVm,kVi]=factorModel(V);
  Pi=pPm(kPi) //strictly unstable poles of P
  Vi=pVm(kVi) //strictly unstable poles of V
  dt=P.dt
  if dt=="c" then
    //section 5.2.1 p92
    //  Qh=bp*(Pm*bv*Vm)^(-1)*{(bp*Pa)^(-1)*bv*Vm)}_*
    //can be rewritten as
    //  Qh=X*Pm^(-1)*{(X*Pa)^-1}_*
    //with 
    //  X=bp*bv^(-1)*Vm^(-1)
    //  bp*bv^(-1)=(-1)^size(Pi,"*")*poly(Pi,"s","r")/poly(-conj(Pi),"s","r")
    //  where
    //  Pi contains the common strictly unstable poles of P which are not in V
    
    //shortcut in special cases
    if pPa==[] then // P is minimum phase
      //Pa=1,Pm=P 
      //Qh = X*Pm^(-1)*{(X)^-1}_* = 1/Pm
      Qh=zpk(pPm,zPm,1/kPm,dt)
    else
      //I) form X= X=bp*bv^(-1)*Vm^(-1) assuming:
      //   V has at least as many integrator poles as P
      //   each strictly unstable poles of V is also  a pole of P
      if sum(abs(pPm)==0)>sum(abs(pVm)==0) then
        error(msprintf(_("%s: Wrong value for input argument #%d: at least as many integrators as the plant expected.\n"),"H2OptimalController",2))
      end 
      //remove P and V common strictly instable poles
      [Pi,Vi]=simplify_zp(Pi,Vi,1e-8);
      if size(Vi,"*")<>0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: Unstable poles must be a subset of instable poles of the plant.\n"),"H2OptimalController",2))
      end
      //form X=bp*bv^(-1)*Vm^(-1)
      zX=[Pi;pVm];pX=[-conj(Pi);zVm];kX=(-1)^size(Pi,"*")/kVm
      //II) Evaluate  Qh=X*Pm^(-1)*{(X*Pa)^-1}_*
      //Evaluate {(X*Pa)^(-1)}_*
      [z,p,k]=starOp([pX;pPa],zX,zPa,1/(kX*kPa),dt)
      //multiply by X*Pm^(-1)
      Qh=zpk([z;zX;pPm],[p;pX;zPm],k*kX/kPm,dt)
      //Cancel zero/pole pairs
      Qh=simplify_zp(Qh,1e-10)
    end
  else
    //section 9.2.1 p 185
    //  Qh=z*bp*(Pm*bv*Vm)^(-1)*{(z*bp*Pa)^(-1)*bv*Vm)}_*
    //can be rewritten as
    //  Qh=X*Pm^(-1)*{(X*Pa)^-1}_*
    //with 
    //  X=z*bp*bv^(-1)*Vm^(-1)
    //  bp*bv^(-1)=(prod(1-1.0./conj(Pi)/prod(1-Pi))*poly(Pi,"z","r")/poly(1.0./conj(Pi),"sz","r")
    //  where
    //  Pi contains the common strictly unstable poles of P which are not
    //  in V
    if and(abs(P.P{1})<1) then // P is stable
      //Pa=1/z,Pm=z*P 
      //Qh = X*Pm^(-1)*{(X)^-1}_* = 1/Pm
      Qh=zpk(pPm,zPm,1/kPm,dt)
    else
      //I) form X assuming:
      //   V has at least as many integrator poles as P
      //   each strictly unstable poles of V is also  a pole of P
      if sum(abs(pPm)==1)>sum(abs(pVm)==1) then
        error(msprintf(_("%s: Wrong value for input argument #%d: at least as many integrators as the plant expected.\n"),"H2OptimalController",2))
      end
      //remove P and V common instable poles
      [Pi,Vi]=simplify_zp(Pi,Vi,1e-8);
      if size(Vi,"*")<>0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: Unstable poles must be a subset of instable poles of the plant.\n"),"H2OptimalController",2))
      end
      if Pi==[] then //bp*bv^(-1)=1
        zX=[0;pVm];pX=zVm;kX=1/kVm
      else
        zX=[0;Pi;pVm];pX=[-conj(Pi);zVm];kX=(prod(1-1.0./conj(Pi))/prod(1-Pi))/kVm
      end
      [zX,pX]=simplify_zp(zX,pX,1e-8)
        //II) Evaluate  Qh=X*Pm^(-1)*{(X*Pa)^-1}_*
      //Evaluate {(X*Pa)^(-1)}_*
      if zPa==[] then
        z=[pX;pPa],p=zX;k=1/(kX*kPa);
      else
        [z,p,k]=starOp([pX;pPa],zX,zPa,1/(kX*kPa),dt)
      end
      //multiply by X*Pm^(-1)
      Qh=zpk([z;zX;pPm],[p;pX;zPm],k*kX/kPm,dt)
      //Cancel zero/pole pairs
      Qh=simplify_zp(Qh,1e-10)
       
    end
  end
endfunction

function [z1,p1,k1]=starOp(z,p1,p2,k,dt)
//Splits the given system in a sum of two terms  the second one
//containing the last n poles.
// If p=[p1 p2], p1 and p2 must  have no common value  
  if intersect(p1,p2)<>[] then
    error(msprintf(_("%s: Incompatible input arguments #%d and #%d: arrays must not have common value.\n"),"starOp",2,3))
  end
  //The algorithm below assumes that zp2ss returns a triangular system 
  //with poles order unchanged
  [A,B,C,D]=abcd(zpk2ss(zpk(z,[p1(:);p2(:)],k,dt)))
 
  //A is a bloc diagonal matrix, A=[A11 A12;0 A22]
  n1=size(p1,"*");
  i1=1:n1;i2=n1+1:$
  //Compute a base change U=[I X;0 I] which bloc diagonalize A
  X=sylv(A(i1,i1),-A(i2,i2),-A(i1,i2),"c");
  //Apply the base change and throw away the 2,2 component as well as the
  //semi proper part
  [z1,p1,k1]=ss2zp(syslin(dt,A(i1,i1),B(i1,:)-X*B(i2,:),C(:,i1)));
endfunction

function [zerosPa,polesPa,gainPa,zerosPm,polesPm,gainPm,ki]=factorModel(P)  
// factor the plant model P=Pa*Pm 
// Pa contains all strictly unstable zeros  of P and is all pass.
// Pm is minimum phase,  
  
// P  : the plant model
// zerosPa : the Pa's zeros
// polesPa : the Pa's poles
// gainPa : the Pa's gain
// zerosPm : the Pm's zeros
// polesPm:  the Pm's poles
// gainPm : the Pm's gain
// ki     : indices of the strictly unstable poles of P in polesPm  

  select typeof(P)
  case "state-space" then
    [z,p,k]=ss2zp(P)
  case "rational" then
    [z,p,k]=tf2zp(P)
  case "zpk" then  
    z=P.Z{1};p=P.P{1},k=P.K(1)
  end 
 
  dt=P.dt
  nz=size(z,"*");
  np=size(p,"*");
  if nz>np then
    error(msprintf(_("%s: Wrong value for input argument #%d: Proper system expected.\n"),"factorModel",1))
  end
  if dt=="c" then //continuous time case
    kzm=find(real(z)<0|abs(z)==0) //indices of marginaly stable zeros
    ki=find(real(p)>0) //indices of strictly unstable poles
    if size(kzm,"*")== nz then //Plant is minimum phase
      zerosPa=[];polesPa=[];gainPa=1;//
      zerosPm=z;polesPm=p;gainPm=k;
    else
      zerosPa=z;zerosPa(kzm)=[];
      polesPa=-zerosPa;
      gainPa=(-1)^size(zerosPa,"*")
      //Pm
      zerosPm=[z(kzm);polesPa];
      polesPm=p;
      gainPm=k/gainPa;
    end
  else //discrete time case
    kzm=find(abs(z)<1|z==1)//indices of marginaly stable zeros
    ki=find(abs(p)>1)// indices of strictly unstable  poles
    if size(kzm,"*")== nz then //Plant is minimum phase
      zerosPa=[];polesPa=zeros(np-nz,1);gainPa=1;
      zerosPm=[z;polesPa];polesPm=p;gainPm=k;
    else
      zerosPa=z;zerosPa(kzm)=[];
      polesPa=[1.0/zerosPa;zeros(np-nz,1)];
      gainPa=real(prod(1-polesPa))/real(prod(1-zerosPa));
      //Pm
      zerosPm=[z(kzm);polesPa];
      polesPm=p;
      gainPm=k/gainPa;
    end
  end
endfunction
