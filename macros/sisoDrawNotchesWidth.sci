// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function e=sisoDrawNotchesWidth(x,y)
  //draw the markers of the image of the Controller poles
  black=1
  xpoly(x,y,"marks")
  e=gce();
  e.mark_size_unit="point";
  e.mark_size=4;
  e.mark_style=2;
  e.mark_foreground=black;
  e.thickness=2;
endfunction
