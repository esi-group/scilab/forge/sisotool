// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolArchiApply()
  gui=gcbo.parent
  ud=gui.user_data
  mainH=ud.mainH
  l=gui.children(2); //listbox
  archinames=l.user_data
  item=l.value
  S=sisotoolGetSession(mainH)
  sys=S.Designs
  if  archinames(item)==sys.archi then return;end
  if S.History.current<> S.History.ref then
    if messagebox(_("System has been modified do you want to save it?"),...
                  "Sisotool message","info",[_("Yes"),_("No")],"modal")==1 then
      sisotoolSaveSession(mainH)
    end
  end
  
  oldSpecs=sisotoolGetArchiSpecs(sys.archi)
  newSpecs=sisotoolGetArchiSpecs(archinames(item))
  [v,ko,kn]=intersect(oldSpecs.tunable,newSpecs.tunable)
  plant=newSpecs.plant
  tunable=newSpecs.tunable
  
  
  newsys=struct();
  newsys.archi=archinames(item)
  [v,ko,kn]=intersect(oldSpecs.tunable,tunable)
  for k=1:size(tunable,'*')
    ke=find(k==kn)
    if ke==[] then
      dt=sys(tunable(1)).dt
      newsys(tunable(k))=sisotoolNewController(dt);
    else
      newsys(tunable(k))=sys(tunable(k))
    end
  end    
  
  [v,ko,kn]=intersect(oldSpecs.plant,plant);
  for k=1:size(plant,'*')
    ke=find(k==kn)
    if ke==[] then
      newsys(plant(k))=syslin("c",1,1);
    else
      newsys(plant(k))=sys(plant(k))
    end
  end   
 
  S.Designs=newsys
  //try to keep the same editors
  out_of=newSpecs.out_of
  subWins=S.EditorsSettings.subWins
  handles=S.EditorsSettings.handles
  for k=1:size(subWins,"*")
    subWin=subWins(k)
    if or(subWin.type==["clbm" "clbp"]) then
      if and(subWin.in<>newSpecs.in) then
        subWin.in=newSpecs.in(1)
      end
     if and(subWin.out<>newSpecs.out) then
        subWin.out=newSpecs.out(1)
     end
     if and(subWin.tunable<>newSpecs.tunable) then
        subWin.tunable=newSpecs.tunable(1)
     end
    else
      if and(subWin.out_of<>out_of) then
        subWin.out_of=out_of(1)
      end
    end
    //update subWin title
    select subWin.type
    case "olbm"
      handles(k).title.text= msprintf(_("Bode plot editor for open loop (%s)"),subWin.out_of)
    case "rlocus"
      handles(k).title.text= msprintf(_("Root locus editor for open loop (%s)"),subWin.out_of)
    case "nichols"
      handles(k).title.text= msprintf(_("Black/Nichols editor for open loop (%s)"),subWin.out_of)
    case "clbm"
      handles(k).title.text= msprintf(_("Bode editor for closed loop from %s to %s, tunable:%s"),...
                                  subWin.in, subWin.out,subWin.tunable)
    end
    subWins(k)=subWin
    editorData=sisotoolGetEditorData(handles(k))
    if or(subWin.type==["clbm" "clbp"]) then
      editorData.tunable=subWin.tunable
    else 
      editorData.tunable=subWin.out_of
    end
    sisotoolSetEditorData(editorData,handles(k))
  end
  S.EditorsSettings.subWins=subWins
  sisotoolSetSession(S,mainH)
  


  if S.OptimGui<>[] then delete(S.OptimGui),end
  if S.PrefGui<>[] then delete(S.PrefGui),end

  if S.AnalysersSelector<>[] then //reset AnalysersGui if opened
    sisotoolAnalysersGui(mainH.figure_id)
  end
  if S.Analysers<>[] then
    delete(S.Analysers.children(S.Analysers.children.type=="Axes"))
    sisotoolCreateAnalysers(S.Analysers)
  end

  //Textual Editor
  if S.CompensatorEditor<>[] then
    sisotoolCreateCGUI(mainH.figure_id)
  end
  //Optim settings
  S.OptimSettings=sisotoolOptimDefaults(S.Designs,S.EditorsSettings,S.AnalysersSettings)
  sisotoolSetSession(S,mainH)
  sisotoolRedraw(mainH)
endfunction

