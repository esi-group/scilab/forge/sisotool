// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function handles=sisotoolManageCharacteristics(subWinIndex,cKey,handles,job)
  //S is defined in the calling context

  ax=S.AnalysersSettings.handles(subWinIndex)
  if job=="delete" then
    if or(cKey==fieldnames(handles)) then
      delete(handles(cKey))
      handles(cKey)=null()
    end
    return
  end
  units=S.Preferences.units
  if job=="create" then
    H=[];
  else
    if or(cKey==fieldnames(handles)) then
      H=handles(cKey);
    else
      return
    end
  end
 
  analyserType=ax.tag;  
  ax.parent.immediate_drawing="off";
  select analyserType
  case "step"
    loci=sisotoolGetEditorData(ax).loci;
    select cKey
    case "peak_response"
      H=managePeakResponse(loci,H)
    case "settling_time"
      H=manageSettlingTime(loci,H)
    case "rise_time"
      H=manageRiseTime(loci,H)
    end
  case "impulse"
    loci=sisotoolGetEditorData(ax).loci;
    select cKey
    case "peak_response"
      H=managePeakResponse(loci,H)
    case "settling_time"
      H=manageSettlingTime(loci,H)
    end
  case "gainplot" 
    axm=ax
    axp=S.AnalysersSettings.handles(subWinIndex+1)
    loci_m=sisotoolGetEditorData(axm).loci
    loci_p=sisotoolGetEditorData(axp).loci
    select cKey
    case "peak_response"
      H=manageBodePeakMagnitude(loci_m,H)
    case "min_margin"
      H=manageBodeMargins(loci_m,loci_p,1,H);
    case "all_margins"
      H=manageBodeMargins(loci_m,loci_p,-1,H);
    end
  case "phaseplot" 
   //Should not be called see sisotoolCharacteristicsCallback
  case "nyquist"
    loci=sisotoolGetEditorData(ax).loci;
    select cKey
    case "peak_response"
       H=manageNyquistPeakMagnitude(loci,H);
    case "min_margin"
       H=manageNyquistMargins(loci,H,1);
    case "all_margins"
       H=manageNyquistMargins(loci,H,-1);
    end
  case "nichols"
    loci=sisotoolGetEditorData(ax).loci;
    select cKey
    case "peak_response"
       H=manageNicholsPeakMagnitude(loci,H);
    case "min_margin"
       H=manageNicholsMargins(loci,H,1);
    case "all_margins"
       H=manageNicholsMargins(loci,H,-1);
    end
  case "pole/zero"

  end
  handles(cKey)=H
  ax.parent.immediate_drawing="on";
endfunction

function H=managePeakResponse(loci,H)
  n_old=size(H,1)
  n=size(loci,"*")
  if n_old>n then
    delete(H(n+1:$,:))
    H=H(1:n,:);
  end
  for l=n_old+1:n
 
    xpoly([],[])
    t=gce();
    t.clip_state="clipgrf"
    t.line_style=2,t.foreground=loci(l).foreground
    xpoly([],[])
    v=gce();
    v.clip_state="clipgrf"
    v.line_style=2,v.foreground=loci(l).foreground
    
    xpoly(0,0);m=gce();
    dfd=loci(l).display_function_data
    dfd.tipType=[analyserType "peakResponse"]
    set(m,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolTRTips",...
        "mark_style",6,... //for tips
        "mark_size_unit","point",...
        "mark_size",9,...
        "mark_foreground",loci(l).foreground)
    sisotoolDatatipCreate(m,1)
 
    H=[H;[m,t,v]];
  end
  if n_old>0 then //update
    bnds=loci(1).parent.data_bounds
    //make sure the lines reach the axes
    ymin=bnds(1,2)-diff(bnds(:,2))/2 
    xmin=bnds(1,1)-diff(bnds(:,1))/2
    for l=1:size(loci,"*")
      d=loci(l).data
      [m,i]=max(d(:,2))
      H(l,1).data=d(i,:)
      H(l,1).display_function_data.units=loci(l).display_function_data.units
      updateDatatips(H(l,1))//bug 14788 workaround
      H(l,2).data=[d(i,1)*ones(2,1),[d(i,2);ymin]]
      H(l,3).data=[[xmin;d(i,1)],d(i,2)*ones(2,1)]
      H(l,1).user_data=loci(l).user_data //finalValue
    end
  end
endfunction

function H=manageSettlingTime(loci,H)
  tol=2/100; //TBD to be made customizable
  n_old=size(H,1)
  n=size(loci,"*")
  if n_old>n then
    delete(H(n+1:$,:))
    H=H(1:n,:);
  end
  for l=n_old+1:n
    //create the entities

    xpoly([],[]);
    upper=gce();
    upper.clip_state="clipgrf"
    upper.line_style=2,upper.foreground=loci(l).foreground
    xpoly([],[])
    lower=gce();
    lower.clip_state="clipgrf"
    lower.line_style=2,lower.foreground=loci(l).foreground
    xpoly([],[])
    timeLoc=gce()
    timeLoc.clip_state="clipgrf"
    timeLoc.line_style=2,timeLoc.foreground=loci(l).foreground

    xpoly(0,0);m=gce()
    dfd=loci(l).display_function_data
    dfd.tipType=[analyserType "settlingTime"]
    dfd.tol=tol
    set(m,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolTRTips",...
        "mark_style",5,...
        "mark_size_unit","point",...
        "mark_size",9,...
        "mark_foreground",loci(l).foreground);
    sisotoolDatatipCreate(m,1)
    H=[H;[m,upper lower timeLoc]];
  end
    
  if n_old>0 then //update
    bnds=loci(1).parent.data_bounds
    ymin=bnds(1,2)-diff(bnds(:,2))/2 
    xmax=bnds(2,1)+diff(bnds(:,1))/2
    for l=1:size(loci,"*")
      loc=loci(l)
      finalValue=loci(l).user_data
      d=loc.data
      if finalValue<>[] then
        i=find(abs(d($:-1:1,2)-finalValue)>tol*abs(finalValue),1)
        if i<>[] then
          i=size(d,1)-i+1
          H(l,1).data=[d(i,1),d(i,2)]
          H(l,1).display_function_data.units=loci(l).display_function_data.units
          updateDatatips(H(l,1))//bug 14788 workaround
          H(l,2).data=[0 ((1+tol)*finalValue);xmax ((1+tol)*finalValue)];
          H(l,3).data=[0 ((1-tol)*finalValue);xmax ((1-tol)*finalValue)]
          H(l,4).data=[d(i,1)*ones(2,1) [d(i,2);ymin]];
        else
          H(l,1).data=[];H(l,2).data=[];H(l,3).data=[];H(l,4).data=[];
        end
      end
    end
  end
endfunction

function H=manageRiseTime(loci,H)

  from=10/100;//TBD to be made customizable
  to=90/100
  n_old=size(H,1)
  n=size(loci,"*")
  if n_old>n then
    delete(H(n+1:$,:))
    H=H(1:n,:);
  end
  for l=n_old+1:n
    //create the entities
    xpoly([],[]);
    value=gce();
    value.clip_state="clipgrf"
    value.line_style=2,value.foreground=loci(l).foreground
    
    xpoly([],[])
    time1=gce()
    time1.clip_state="clipgrf"
    time1.line_style=2,time1.foreground=loci(l).foreground
    xpoly([],[])
    time2=gce()
    time2.clip_state="clipgrf"
    time2.line_style=2,time2.foreground=loci(l).foreground
    
    xpoly(0,0); 
    m=gce()
    dfd=loci(l).display_function_data
    dfd.tipType=[analyserType "riseTime"]
    dfd.to=to
   
    set(m,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolTRTips",...
        "mark_style",14,...
        "mark_size_unit","point",...
        "mark_size",11,...
        "mark_foreground",loci(l).foreground);
    sisotoolDatatipCreate(m,1)
   
    
    H=[H;[m,value time1 time2]];
  end
 
  if n_old>0 then //update  
    bnds=loci(1).parent.data_bounds
    ymin=bnds(1,2)-diff(bnds(:,2))/2 
    for l=1:size(loci,"*")
      loc=loci(l)
      finalValue=loci(l).user_data
      t=loc.data(:,1)
      y=loc.data(:,2)
      if finalValue<>[] then
        kfrom=find(y>finalValue*from,1)
        kto=find(y>finalValue*to,1)
        H(l,1).data=[t(kto),y(kto)]
        H(l,1).display_function_data.units=loci(l).display_function_data.units
        updateDatatips(H(l,1))//bug 14788 workaround
        H(l,2).data=[bnds(1,1),y(kto);t(kto),y(kto)];
        H(l,3).data=[t(kfrom),ymin;t(kfrom),y(kto)]
        H(l,4).data=[t(kto),ymin;t(kto),y(kto)]
      else
        H(l,1).data=[];H(l,2).data=[];H(l,3).data=[];H(l,4).data=[];
      end
     
    end
  end
endfunction

function H=manageBodePeakMagnitude(loci,H);
  n_old=size(H,1)
  sca(loci(1).parent)
  n=size(loci,"*")
  if n_old>n then
    delete(H(n+1:$,:))
    H=H(1:n,:);
  end
  for l=n_old+1:n
    xpoly([],[])
    t=gce();
    t.clip_state="clipgrf"
    t.line_style=2,t.foreground=loci(l).foreground
    xpoly([],[])
    v=gce();
    v.clip_state="clipgrf"
    v.line_style=2,v.foreground=loci(l).foreground
    
    xpoly(1,1);m=gce();
    dfd=loci(l).display_function_data
    dfd.tipType="peak"
    
    set(m,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolBodeTips",...
        "mark_style",6,...
        "mark_size_unit","point",...
        "mark_size",9,...
        "mark_foreground",loci(l).foreground);
    sisotoolDatatipCreate(m,1)
    H=[H;[m,t,v]];
  end
  if n_old>0 then //update  
    bnds=loci(1).parent.data_bounds
    //make sure the lines reach the axes
    ymin=bnds(1,2)-diff(bnds(:,2))/2 
    xmin=bnds(1,1)-diff(bnds(:,1))/2
    for l=1:size(loci,"*")
      d=loci(l).data
      [m,i]=max(d(:,2))
      H(l,1).data=d(i,:)
      H(l,1).display_function_data.units=loci(l).display_function_data.units;
      updateDatatips(H(l,1));//bug 14788 workaround

      H(l,2).data=[d(i,1)*ones(2,1),[d(i,2);ymin]]
      H(l,3).data=[[xmin;d(i,1)],d(i,2)*ones(2,1)]
    end
  end
endfunction

function H=manageNyquistPeakMagnitude(loci,H);
  n_old=size(H,1)
  sca(loci(1).parent)
  n=size(loci,"*")
  if n_old>n then
    delete(H(n+1:$,:))
    H=H(1:n,:)
  end
  for l=n_old+1:n
    
    xpoly([],[])
    r=gce();
    r.clip_state="clipgrf"
    r.line_style=2,t.foreground=loci(l).foreground

    xpoly(1,1);m=gce();
    dfd=loci(l).display_function_data;dfd.freq=0;
    dfd.tipType="peak"
    set(m,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolNyqTips",...
        "mark_style",6,...
        "mark_size_unit","point",...
        "mark_size",9,...
        "mark_foreground",loci(l).foreground);
     sisotoolDatatipCreate(m,1)
    H=[H;[m,r]];
  end
  if n_old>0 then //update  
    for l=1:size(loci,"*")
      d=loci(l).data(1:$/2,:);
      //d=d(d(:,2)>=0,:);
      RI=complex(d(:,1),d(:,2));
      [m,i]=max(abs(RI))
      H(l,1).data=d(i,:)
      H(l,1).display_function_data.freq=loci(l).display_function_data.freq(i)
      updateDatatips(H(l,1));//bug 14788 workaround
      H(l,2).data=[[d(i,1);0],[d(i,2);0]]
    end
  end
endfunction

function H=manageNicholsPeakMagnitude(loci,H);
  n_old=size(H,1)
  n=size(loci,"*")
  if n_old>n then
    delete(H(n+1:$,:))
    H=H(1:n,:);
  end

  sca(loci(1).parent)
  for l=n_old+1:size(loci,"*")
    xpoly([],[])
    t=gce();
    t.clip_state="clipgrf"
    t.line_style=2,t.foreground=loci(l).foreground
    xpoly([],[])
    v=gce();
    v.clip_state="clipgrf"
    v.line_style=2,v.foreground=loci(l).foreground

    xpoly(0,0);m=gce();
    dfd=loci(l).display_function_data;dfd.freq=0;
    dfd.tipType="peak"
    set(m,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolNicTips",...
        "mark_style",6,...
        "mark_size_unit","point",...
        "mark_size",9,...
        "mark_foreground",loci(l).foreground);
    sisotoolDatatipCreate(m,1)
    H=[H;[m,t,v]];
  end
  if n_old>0 then //update  
    bnds=loci(1).parent.data_bounds
    //make sure the lines reach the axes
    ymin=bnds(1,2)-diff(bnds(:,2))/2 
    xmin=bnds(1,1)-diff(bnds(:,1))/2
    for l=1:size(loci,"*")
      d=loci(l).data
      [m,i]=max(d(:,2))
      H(l,1).data=d(i,:)
      H(l,1).display_function_data.freq=loci(l).display_function_data.freq(i)
      updateDatatips(H(l,1));//bug 14788 workaround
      H(l,2).data=[d(i,1)*ones(2,1),[d(i,2);ymin]]
      H(l,3).data=[[xmin;d(i,1)],d(i,2)*ones(2,1)]
    end
  end
endfunction

function H=manageNicholsMargins(loci,H,opt)
  n_old=size(H,1)
  n=size(loci,"*")
  if n_old>n then
    delete(H(n+1:$,:))
    H=H(1:n,:);
  end

  sca(loci(1).parent)
  for l=n_old+1:n
    //gain margins
    xpoly(0,0);mg=gce();
    dfd=loci(l).display_function_data;dfd.freq=0;
    dfd.tipType="gainMargin"
    set(mg,"line_mode","off","mark_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolNicTips",...
        "mark_style",0,...
        "mark_foreground",loci(l).foreground,...
        "mark_size_unit","point",...
        "mark_size",8);
    sisotoolDatatipCreate(mg,1)
    xsegs([0;0],[0;0],1);// xsegs([],[]) does not work
    g=gce();
    g.thickness=2;
    g.segs_color=loci(l).foreground;
    g.clip_state="clipgrf";
    g.line_style=1;
    
    //phase margins
    xpoly(0,0);mp=gce();
    dfd.tipType="phaseMargin"
    set(mp,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolNicTips",...
        "mark_style",4,...
        "mark_foreground",loci(l).foreground,...
        "mark_size_unit","point",...
        "mark_size",9);
     sisotoolDatatipCreate(mp,1)
    xsegs([0;0],[0;0],1);// xsegs([],[]) does not work
    p=gce();
    p.segs_color=loci(l).foreground;
    p.thickness=2;
    p.clip_state="clipgrf";
    p.line_style=1;
    
    //circle
    t=linspace(0,2*%pi,150)'
    xpoly([],[]);c=gce();c.line_style=4;c.clip_state="clipgrf";
    c.data=[sin(t),cos(t)];
    H=[H;[mg,g,mp,p]];
  end
    
  if n_old>0 then //update  
    v180=phaseconv(_("degrees"),units.phase,-180);
    v0=magconv("dB",units.magnitude,0);
    for l=1:size(loci,"*")
      frq=loci(l).display_function_data.freq
      //gain margin
      gm=loci(l).display_function_data.gmargin
      if gm<>[] then
        f=freqconv("Hz",units.frequency,gm(1,:)');
        g=magconv("dB",units.magnitude,gm(2,:)');
        phig=sisotoolShiftPhase(f,v180*ones(f),frq,loci.data(:,1),-2*v180)
        nf=size(f,"*");
        if opt==1 then i=1;else i=1:nf;end
        H(l,1).data=[phig(i)  -g(i)];H(l,1).visible="on";
        for j=i(2:$),sisotoolDatatipCreate(H(l,1),j);end
        H(l,1).display_function_data.freq=f(i)
        updateDatatips(H(l,1));//bug 14788 workaround
        H(l,2).data=[matrix([phig(i)';phig(i)'],-1,1),...
                     matrix([-g(i)';v0*ones(i)],-1,1)];
        H(l,2).segs_color=ones(i)*loci(l).foreground;
        H(l,2).visible="on"
      else
        H(l,1).data=[];H(l,1).visible="off"
        H(l,2).data=[1,1;1,1]; H(l,2).segs_color=1;H(l,2).visible="off"
      end

      //phase margin
      pm=loci(l).display_function_data.pmargin
      
      if pm<>[] then
        frq=loci(l).display_function_data.freq
        twopi=phaseconv(_("degrees"),units.phase,360);
        f=freqconv("Hz",units.frequency,pm(1,:)');
        p1=phaseconv(_("degrees"),units.phase,pm(2,:)');
        p=sisotoolShiftPhase(f,v180+p1,frq,loci.data(:,1),twopi)
        if opt==1 then i=1;else i=1:size(f,"*");end
      
        H(l,3).data=[p(i) v0*ones(i)' ];H(l,3).visible="on"
        for j=i(2:$),sisotoolDatatipCreate(H(l,3),j);end
        H(l,3).display_function_data.freq=f(i)
        updateDatatips(H(l,3));//bug 14788 workaround
        H(l,4).data=[matrix([p(i)';(p(i)-p1(i))'],-1,1),...
                     matrix(v0*[ones(i);ones(i)],-1,1)];
        H(l,4).segs_color=ones(i)*loci(l).foreground;
        H(l,4).visible="on"
      else
        H(l,3).data=[];H(l,3).visible="off"
        H(l,4).data=[0 0;0 0]; H(l,4).segs_color=1;H(l,4).visible="off"
      end
    end
  end
endfunction

function H=manageNyquistMargins(loci,H,opt)
  n_old=size(H,1)
  sca(loci(1).parent)
  n=size(loci,"*")
  if n_old>n then
    delete(H(n+1:$,:))
    H=H(1:n,:);
  end
  for l=n_old+1:size(loci,"*")
    
    //gain margins
    xsegs([0;0],[0;0],1);// xsegs([],[]) does not work
    g=gce();
    g.segs_color=loci(l).foreground;
    g.thickness=2;
    g.clip_state="clipgrf";
    g.line_style=1;

    //phase margins
    xsegs([0;0],[0;0],1);// xsegs([],[]) does not work
    p=gce();
    p.segs_color=loci(l).foreground;
    p.thickness=2;
    p.clip_state="clipgrf";
    p.line_style=1;
    
    //circle
    t=linspace(0,2*%pi,150)'
    xpoly([],[]);c=gce();c.line_style=4;c.clip_state="clipgrf";
    c.data=[sin(t),cos(t)];
    
    xpoly(0,0);mg=gce();
    dfd=loci(l).display_function_data;dfd.freq=0;
    dfd.tipType="gainMargin"
    set(mg,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolNyqTips",...
        "mark_style",0,...
        "mark_foreground",loci(l).foreground,...
        "mark_size_unit","point",...
        "mark_size",8);
    sisotoolDatatipCreate(mg,1);

   
    xpoly(0,0);mp=gce();
    dfd.tipType="phaseMargin";
    set(mp,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolNyqTips",...
        "mark_style",4,...
        "mark_foreground",loci(l).foreground,...
        "mark_size_unit","point",...
        "mark_size",9);
     sisotoolDatatipCreate(mp,1);
    H=[H;[mg,g,mp,p,c]];
  end
    
  if n_old>0 then //update  
    for l=1:size(loci,"*")
      
      //gain margin
      gm=loci(l).display_function_data.gmargin
      frq=loci(l).display_function_data.freq
      if gm<>[] then
        f=freqconv("Hz",units.frequency,gm(1,:)');
        g=magconv("dB",units.magnitude,gm(2,:)');
        rf=gm(3,:)';
        if opt==1 then i=1;else i=1:size(f,"*");end 
        H(l,1).data=[real(rf(i)) imag(rf(i))];H(l,1).visible="on";
        for j=i(2:$),sisotoolDatatipCreate(H(l,1),j);end
        H(l,1).display_function_data.freq=f(i)
        updateDatatips(H(l,1));//bug 14788 workaround
        H(l,2).data=[matrix([real(rf(i))';-ones(i)],-1,1),matrix([zeros(i);zeros(i)],-1,1)];
        H(l,2).segs_color=ones(i)*loci(l).foreground;H(l,2).visible="on";
      else
        H(l,1).data=[];H(l,1).visible="off"
        H(l,2).data=[1,1;1,1]; H(l,2).segs_color=1;H(l,2).visible="off"
      end
      //phase margin
      pm=loci(l).display_function_data.pmargin
      if pm<>[] then
        f=freqconv("Hz",units.frequency,pm(1,:)');
        p=magconv("dB",units.magnitude,pm(2,:)');
        rf=pm(3,:)';
        if opt==1 then i=1;else i=1:size(f,"*");end 
        H(l,3).data=[real(rf(i)) imag(rf(i))];H(l,3).visible="on";
        for j=i(2:$),sisotoolDatatipCreate(H(l,3),j);end
        H(l,3).display_function_data.freq=f(i)
        updateDatatips(H(l,3));//bug 14788 workaround
        H(l,4).data=[matrix([real(rf(i))';zeros(i)],-1,1),matrix([imag(rf(i))';zeros(i)],-1,1)];
        H(l,4).segs_color=ones(i)*loci(l).foreground;H(l,4).visible="on";
      else
        H(l,3).data=[];H(l,3).visible="off";
        H(l,4).data=[0 0;0 0]; H(l,4).segs_color=1;H(l,4).visible="off";
      end
    end
  end
endfunction


function H=manageBodeMargins(loci_m,loci_p,opt,H)
  n_old=size(H,1)
  sca(loci_m(1).parent)
  n=size(loci_m,"*")
  if n_old>n then
    delete(H(n+1:$,:))
    H=H(1:n,:);
  end

  for l=n_old+1:n
    //gain margins
    sca(loci_m(1).parent)
    xsegs([1;1],[1;1],1);g=gce();
    g.segs_color=loci_m(l).foreground;
    g.thickness=2;
    g.clip_state="clipgrf";
    g.line_style=1;
    
    xpoly([],[]);mag=gce(); //0db grid
    mag.foreground=loci_m(l).foreground;
    mag.clip_state="clipgrf";
    mag.line_style=4;

    xpoly(1,1);mg=gce();
    dfd=loci_m(l).display_function_data;
    dfd.tipType="gainMargin"
   
    set(mg,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolBodeTips",...
        "mark_style",0,...
        "mark_foreground",loci_m(l).foreground,...
        "mark_size_unit","point",...
        "mark_size",8);
    sisotoolDatatipCreate(mg,1);
    //phase margins

    sca(loci_p(1).parent)
    xsegs([1;1],[1;1],1);p=gce();
    p.segs_color=loci_p(l).foreground;
    p.thickness=2;
    p.clip_state="clipgrf";
    p.line_style=1;
    
    
    xpoly([],[]);phi=gce(); //-180 degre grid
    phi.foreground=loci_p(l).foreground;
    phi.line_style=4;
    phi.clip_state="clipgrf";
   
    
    xpoly(1,1);mp=gce();
    dfd=loci_p(l).display_function_data;
    dfd.tipType="phaseMargin"
    set(mp,"mark_mode","off",...
        "line_mode","off",...
        "datatip_display_mode","mouseclick",...
        "display_function_data",dfd,...
        "display_function","sisotoolBodeTips",...
        "mark_style",4,...
        "mark_foreground",loci_p(l).foreground,...
        "mark_size_unit","point",...
        "mark_size",9);
    sisotoolDatatipCreate(mp,1);
  
    H=[H;[mg,g,mag,mp,p,phi]];
  end

  if n_old>0 then //update  
    frq=loci_m(1).data(:,1)
    
    fmin=frq(1)/10
    fmax=frq($)*10;
    v180=phaseconv(_("degrees"),units.phase,180)
    v0=magconv("dB",units.magnitude,0)

    for l=1:size(loci_m,"*")
      H(l,3).data=[fmin v0;fmax v0];//mag
      H(l,6).data=[fmin v180;fmax v180];//phi
      
      //gain margin
      gm=loci_m(l).display_function_data.gmargin

      if gm<>[] then
        f=freqconv("Hz",units.frequency,gm(1,:));
        g=magconv("dB",units.magnitude,gm(2,:));
        nf=size(f,"*");
        if opt==1 then i=1;else i=1:nf;end
        H(l,1).data=[f(i)' -g(i)'];H(l,1).visible="on";
        for j=i(2:$),sisotoolDatatipCreate(H(l,1),j);end
        H(l,1).display_function_data.freq=f(i);
        updateDatatips(H(l,1));//bug 14788 workaround
        H(l,2).data=[matrix([f(i);f(i)],-1,1),...
                     matrix([-g(i);v0*ones(i)],-1,1)];
        H(l,2).segs_color=ones(i)*loci_m(l).foreground;
        H(l,2).visible="on";
      else
        H(l,1).data=[];H(l,1).visible="off"
        H(l,2).data=[1,1;1,1]; H(l,2).segs_color=1;H(l,2).visible="off"
      end
      
      //phase margin
      pm=loci_p(l).display_function_data.pmargin
      if pm<>[] then
        f=freqconv("Hz",units.frequency,pm(1,:));
        p1=phaseconv(_("degrees"),units.phase,pm(2,:));
        p=sisotoolShiftPhase(f,v180+p1,frq,loci_p.data(:,2),2*v180)
        pref=p-p1
        nf=size(f,"*");
        if opt==1 then i=1;else i=1:nf;end
        H(l,4).data=[f(i)' p(i)'];H(l,3).visible="on";
        for j=i(2:$),sisotoolDatatipCreate(H(l,4),j);end
        H(l,4).display_function_data.freq=f(i);
        updateDatatips(H(l,4));//bug 14788 workaround
        H(l,5).data=[matrix([f(i);f(i)],-1,1),...
                     matrix([pref(i);p(i)],-1,1)];
        H(l,5).segs_color=ones(i)*loci_p(l).foreground;
        H(l,5).visible="on";
        H(l,6).data=[fmin pref(1);fmax pref(1)];//phi
      else
        H(l,4).data=[];H(l,4).visible="off"
        H(l,5).data=[0 0;0 0]; H(l,5).segs_color=1;H(l,4).visible="off"
      end
    end
  end
endfunction


function gmarginTip(p,i)
//p  :polyline handle
//point index
  d=sisotoolDatatipCreate(p,i)
  set(d,"mark_style",0,...
      "mark_foreground",p.foreground,...
      "mark_size_unit","point",...
      "mark_size",8,...
      "user_data",i)
endfunction

function pmarginTip(p,i)
//p  :polyline handle
//point index
  d=sisotoolDatatipCreate(p,i)
  c=p.foreground
  set(d,"mark_style",4,...
      "mark_size_unit","point",...
      "mark_size",9,...
      "mark_foreground",c,...
      "display_function",p.display_function,...
      "user_data",i)
 endfunction


function settlingTimeTip(p,i)
//p  :polyline handle
//point index
  d=sisotoolDatatipCreate(p,i)
  c=p.foreground
  set(d,"mark_style",5,...
      "mark_size_unit","point",...
      "mark_size",9,...
      "mark_background",c,...
      "mark_foreground",c,...
      "user_data",i)
 endfunction
function riseTimeTip(p,i)
//p  :polyline handle
//point index
  d=sisotoolDatatipCreate(p,i)
  c=p.foreground
  set(d,"mark_style",14,...
      "mark_size_unit","point",...
      "mark_size",11,...
      "mark_background",c,...
      "mark_foreground",c,...
      "user_data",i)
 endfunction
