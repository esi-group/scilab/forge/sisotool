// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [v,isStable]=sisotoolFinalValue(sys,inputType)
  if sys.dt=="c" then
    if inputType=="impulse" then sys=sys*%s;end
    select typeof(sys)
    case "rational"
      isStable=and(real(roots(sys.den))<0)
      if ~isStable then 
        v=[]
      else
        v=horner(sys,0)
      end
    case "state-space"
      sys=minss(sys)
      isStable=and(real(spec(sys.A))<0)
      if ~isStable then 
        v=[]
      else
        v=(sys.C/(-sys.A))*sys.B
      end
    case "zpk"
      isStable=and(real(sys.P{1})<0)
      if ~isStable then 
        v=[]
      else
        v=real(prod(-sys.Z{1})/prod(-sys.P{1}))*sys.K
      end
    end
  else
    if inputType=="impulse" then sys=sys*(%z-1);end
    select typeof(sys)
    case "rational"
      isStable=and(abs(roots(sys.den))<1)
      if ~isStable then 
        v=[]
      else
         v=horner(sys,1)
      end
    case "state-space"
      isStable=and(abs(spec(sys.A))<1)
      if ~isStable then 
        v=[]
      else
         v=(sys.C/(eye(sys.A)-sys.A))*sys.B
      end
    case "zpk"
      isStable=and(abs(sys.P{1})<1)
      if ~isStable then 
        v=[]
      else
        v=real(prod(1-sys.Z{1})/prod(1-sys.P{1}))*sys.K
      end
    end
  end
endfunction
