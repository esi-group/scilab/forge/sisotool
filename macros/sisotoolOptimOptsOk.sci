// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolOptimOptsOk()
  ud=gcbo.user_data
  mainH=ud(1); H=ud(2);
  S=sisotoolGetSession(mainH);
  options=S.OptimSettings.Options
 
  answers=H.string
  options.MaxIter=evstr(answers(1))
  options.MaxFunEvals=evstr(answers(2))
  options.TolFun=evstr(answers(3))
  options.TolX=evstr(answers(4))
  if stripblanks(answers(5))=="" then
    options.OutputFcn=[]
  else
    options.OutputFcn=answers(5)
  end
  //TBD faut-il gerer l'historique ???
  S=sisotoolAddHistory(S,"OptimSettings","options",S.OptimSettings.Options)

  S.OptimSettings.Options=options;
  sisotoolSetSession(S,mainH);
  delete(gcbo.parent)
endfunction
