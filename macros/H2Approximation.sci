function Gm=H2Approximation(G,nn,nd,withdelay,G0)
// Looks for a H2 approximation of a stable dynmaical system
  if typeof(G)=="iodelay" then
    delay=G.iodelay
    G=G.H
  else
    delay=0
  end
  //ajouter tests G: siso, stable,....
  if typeof(G)=="state-space" then
    G=ss2tf(G)
  end
  if G.dt=="c" then
    K=horner(G,0)
  else
    K=horner(G,1)
  end
  G=G/K; //normalisation
  vn=varn(G.den)
  if argn(2)<5 then
    G0=syslin(G.dt,poly(-0.5*ones(1,nn),vn,"r")/...
              poly(-0.8*ones(1,nd),vn,"r"));
  else
    if G.dt=="c" then
      G0=G0/horner(G,0)
    else
      G0=G0/horner(G,1)
    end
  end
  if withdelay then tau=0.1;else tau=[];end
  x0=[coeff(G0.num,0:nn)'
      coeff(G0.den,1:nd)'
      tau];
  [fopt, x,g] = optim (list(H2cost,G,nn,nd), x0);

   Gm=K*syslin(G.dt,poly(x(1:nn+1),vn,"c")/poly([x(1);x(nn+2:nn+1+nd)],vn,"c"))
   if withdelay then delay=x($)+delay;end
   pause
   if delay<>0 then 
     Gm=iodelay(Gm,delay)
   end
endfunction
function [f, g, ind] = H2cost(x, ind, G,nn,nd)
  if size(x,"*")>nn+1+nd&x($)<0 then 
    disp +;ind=-1,f=0;g=zeros(x);return,
  end
  f=H2Error(x,G,nn,nd);
  disp(f)
  g=zeros(x);
  if f<0 then ind=-1,g=zeros(x);return,end
  if (ind == 3 | ind == 4) then
    g = numderivative(list(H2Error,G,nn,nd), x);
  end
endfunction
function e=H2Error(x,G,nn,nd)
  vn=varn(G)
  Gm=syslin(G.dt,poly(x(1:nn+1),vn,"c")/poly([x(1);x(nn+2:nn+1+nd)],vn,"c"))
  if size(x,"*")>nn+1+nd then 
    tau=max(0,x($));
    if tau<>0 then
    if G.dt=="c" then
      //approximation du retard
      Gm=Gm*pade(tau,3)
    else
      Gm=Gm/(%z^round(tau/G.dt))
    end
    end
  end
  e=G-Gm
  if max(real(roots(e.den)))>=-1e-8 then
    //unstable
    e=-1
  else
    e=h2norm(e)
  end
endfunction

