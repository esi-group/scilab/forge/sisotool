// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolMoveResponsePlot(mainH,editorData,entityType,i,k,pt,loc)
  ax=gca();//current axes has been set by sisotoolEventHandler or sisotoolAnalysersEvents
  figureHandle=ax.parent
  S=sisotoolGetSession(mainH)
  
  if entityType=="requirements" then
    //see sisotoolRequirementArea
    req=ax.children($).children(i)
    ind=loc(1)
    if or(req.tag==["upbound" "lowbound"]) then
      ds=req.data//keep it in case of no move
      d=ds
      np=size(d,1)

      if loc(2)<0.1|loc(2)>0.9 then
        //a vertex has been selected move it
        if loc(2)>0.9 then ind=ind+1;end//right point
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          d(ind,1:2)=rep(1:2)
          //insure we have a function
          if ind>2 then d(ind,1)=max(d(ind,1),d(ind-1,1));end
          if ind<np-2 then d(ind,1)=min(d(ind,1),d(ind+1,1));end  
          if ind==2 then d(1,1)=d(ind,1);end
          if ind==np-2 then d(np-1,1)=d(ind,1);end
          req.data=d;
        end 
      elseif d(ind,1)==d(ind+1,1) then
        // a vertical segment selected translate it horizontally
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          d(ind:ind+1,1)=rep(1)
          req.data=d;
        end 
      else
        //a  segment selected translate it vertically
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          dd=rep(2)-d(ind,2)
          d(ind:ind+1,2)= d(ind:ind+1,2)+dd //vertical translation
          req.data=d;
        end 
      end
      if or(d<>ds) then
        requirementIndex=i;
        req.user_data=d(2:$-2,:)
        subWinIndex=editorData.subWinIndex
        windowType=ax.parent.tag;
        Settings= windowType+"Settings"
        reqProp= S(Settings).subWins(subWinIndex).reqProps(requirementIndex)
        S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=d(2:$-2,:)
      
        opt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type);
        S=sisotoolAddHistory(S,"Requirements","update",...
                             windowType,subWinIndex,requirementIndex,reqProp,opt);
        sisotoolSetSession(S,mainH)
      end
    elseif req.tag=="tunnel" then
      subWinIndex=editorData.subWinIndex
      windowType=ax.parent.tag;
      Settings= windowType+"Settings"
      requirementIndex=i
      params=S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data
     // params=req.user_data //[final, risetime,%rise,settlingtime,%settling,%overshoot,%undershoot]
      Plow=req.children(1)
      Phigh=req.children(2)
      sel=loc($)
      if sel==1 then //lowbound polyline selected
        P1=Plow;P2=Phigh
      else //highbound polyline selected
        P1=Phigh;P2=Plow
      end
      ds=P1.data//keep it in case of no move
      d=ds
      np=size(d,1)
      if d(ind,1)==d(ind+1,1) then
        // a vertical segment selected translate it horizontally
        //set constraints
        if sel==1 then
          if ind==3 then //rise time
            bnds=[0 params(4)]
          else //settling time
            bnds=[params(2) %inf]
          end
        else
          bnds=[params(2) %inf]
        end
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          new=min(max(bnds(1),rep(1)),bnds(2))
          d(ind:ind+1,1)=new
          P1.data=d;
          if ind==3&sel==2 then //see sisotoolRequirementArea
            P2.data(5:6,1)=new;
          elseif ind==5&sel==1 then
            P2.data(3:4,1)=new;
          end
        end 
      else
        //a horizontal segment selected translate it vertically
        final=params(1)
        //set constraints
        if sel==1 then //low bound
          if ind==6 then // final-settling
            bnds=[d(4,2) final]
          elseif ind==4 //rise<final-settling
            bnds=[0 d(6,2)]
           elseif ind==2 //undershoot
            bnds=[-%inf 0] 
          end
        else// high bound
          settling=d(4,2)-final
          if ind==4 then // final+settling
            bnds=[final min(d(2,2),2*final-Plow.data(4,2))]
          elseif ind==2 //overshoot
            bnds=[d(4,2) %inf]
          end
        end
        while %t
          rep=xgetmouse([%t %t]);
          if rep(3)<>-1 then break,end
          new=min(max(bnds(1),rep(2)),bnds(2))

          dd=new-d(ind,2)
          d(ind:ind+1,2)= d(ind:ind+1,2)+dd 
          P1.data=d;
          if ind==np-3 then //see sisotoolRequirementArea
            P2.data($-3:$-2,2)=P2.data($-3:$-2,2)-dd;
          end
        end 
      end
      if or(d<>ds) then
        requirementIndex=i;//???voir sisotoolMoveRootLocus
        //params=[final, risetime,%rise,settlingtime,%settling,%overshoot,%undershoot]
        final=params(1);
        params(2)=Plow.data(3,1); //risetime
        params(3)=100*Plow.data(4,2)/final; //%rise
        params(4)=Plow.data(5,1); //settlingtime
        params(5)=100*(final-Plow.data(6,2))/final; //%settling
        params(6)=100*(Phigh.data(2,2)/final-1)
        params(7)=abs(100*Plow.data(2,2)/final)
        req.user_data=params
        subWinIndex=editorData.subWinIndex
        windowType=ax.parent.tag
        Settings= windowType+"Settings"
        reqProp= S(Settings).subWins(subWinIndex).reqProps(requirementIndex);
        S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=params
        //S=sisotoolAddHistory(S,["Requirements" windowType,ax.tag],list(subWinIndex,i,reqProp))
       
        opt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type);
        S=sisotoolAddHistory(S,"Requirements","update",...
                             windowType,subWinIndex,requirementIndex,reqProp,opt);
        sisotoolSetSession(S,mainH)
      end
    end
   
  end
  figureHandle.info_message="";
endfunction
