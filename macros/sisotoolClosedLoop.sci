// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function CL=sisotoolClosedLoop(sys,inout)
  if argn(2)<2 then inout=["r","y"],end
  archiSpecs=sisotoolGetArchiSpecs(sys.archi)
  for t=archiSpecs.tunable
    execstr(t+"=sisotoolC2ss(sys."+t+");")
    //execstr(t+"=sisotoolC2zp(sys."+t+");")
  end
  for p=archiSpecs.plant 
    execstr(p+"=sys."+p+";")
  end
  ki=find(archiSpecs.in==inout(1));
  lo=find(archiSpecs.out==inout(2));
  CL=evstr(archiSpecs.CL(lo,ki));
  try
    CL=minreal(CL);
  catch
    CL=minss(CL,1e-10)
     disp("Unstable closed loop system");
  end
endfunction
