// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [OL,Kc]=sisotoolSys2OL(sys,out_of,exclude)
  archiSpecs=sisotoolGetArchiSpecs(sys.archi)
  i=find(archiSpecs.out_of==out_of)
  if argn(2)==3 then opt=",exclude"; else opt="";end
  execstr("Kc=sys."+out_of+".gain")
  for s=[archiSpecs.tunable archiSpecs.plant]
    execstr(s+"=sys(s);if typeof("+s+")==""sisoC"" then "+s+"=sisotoolC2ss("+s+opt+");end")
  end
  //sign change because rlocus supposes a negative feedback
  if i==[] then
    //prefilter case
    OL=evstr(out_of)
  else
    OL=-evstr(archiSpecs.OL(i))
  end
  //handle degenerated cases
  if type(OL)==1 then OL=syslin("c",OL,1);end
  if OL.dt==[] then OL.dt="c";end
endfunction
