// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolCheckSelection()
//callback for sisotoolExportGui
  guiHandle=gcbo.parent.parent
  if  or(guiHandle.user_data.exportselection.value==1) then
    //enable export buttons
    guiHandle.children(2:3).enable="on"
  else
    //disable export buttons
    guiHandle.children(2:3).enable="off"
  end
endfunction
