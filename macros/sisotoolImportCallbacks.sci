// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolImportCallbacks(%_what,%_win,%_dt)
  select %_what
  case "Radio" then //the Radio button selection changed
    radioCallback(%_dt)
  case "Browse" then
    browseCallback(%_dt) //The browse file button (...) pressed
  case "Import" then
    importCallback(%_win,%_dt) //the Import button pressed
  case "Edit" then
    editCallback(%_dt) // one of the edit uicontrol modified
  end
endfunction

function editCallback(%_dt)
  fig=gcbo.Parent
  tag=gcbo.tag
  %_ud=fig.user_data 
  %_file_edit=%_ud.handles(4);
  %_expr_edit=%_ud.handles(5);
  %_avail_list=%_ud.handles(7);
  %_import_btn=%_ud.handles(9);
  select gcbo.tag
  case "File" then
    %_file_edit.string=stripblanks(%_file_edit.string)
    if %_file_edit.string<>"" then
      %_L=sisotoolAvailableSystems(%_dt,%_file_edit.string)
      if %_L<>[] then
        %_avail_list.string=%_L,
        %_avail_list.value=1;
        %_import_btn.enable=%t
        sisotoolSetError(%_file_edit,%f)
      else
        %_msg=msprintf(_("No dynamical system available in %s"),%_file_edit.string)
        sisotoolSetError(%_file_edit,%t,%_msg)
        %_import_btn.enable=%f
        return
      end
    end
  case "Workspace" then 
    %_L=sisotoolAvailableSystems(%_dt)//do not use H(8).string because it may have changed
    if %_L<>[] then
      %_avail_list.string=%_L
      %_import_btn.enable=%t
    else
      %_import_btn.enable=%f
    end
  case "Expression" then
    if execstr("%_T="+%_expr_edit.string,"errcatch")==0&sisotoolIsSISO(%_T,%_dt) then
      sisotoolSetError(%_expr_edit,%f)
      %_import_btn.enable=%t
    else
       %_msg=_("Invalid expression");
       sisotoolSetError(%_expr_edit,%t,%_msg)
       %_import_btn.enable=%f
       return
    end
  end
endfunction

function browseCallback(%_dt)
  %_ud=get(gcbo.Parent,"user_data");
  %_file_edit=%_ud.handles(4);
  %_f=uigetfile("*.dat")
  %_file_edit.string=%_f;
  editCallback(%_dt)
endfunction

  
function radioCallback(dt)
  fig=gcbo.Parent
  ud=fig.user_data;H=ud.handles //[R1 R2 R3 E1  E3 Browse Avail Model]
  //next instruction no more needed (use of groupname property)
  //Radio=H(1:3)
  //Radio.value=0
  //gcbo.value=1
  file_edit=H(4)
  expr_edit=H(5);
  browse_btn=H(6);
  avail_list=H(7);
  model_list=H(8)
  import_btn=H(9);
  select gcbo.tag
  case "File" then 
    file_edit.enable="on"
//    editCallback(1)
    expr_edit.enable="off"
    browse_btn.enable="on"
    avail_list.enable="on"
    avail_list.value=[];
  case "Workspace" then 
    file_edit.enable="off"
    expr_edit.enable="off"
    browse_btn.enable="off"
    L=sisotoolAvailableSystems(%_dt)
    if L<>[] then
      avail_list.string=L,
      avail_list.value=1;
      import_btn.enable=%t;
    else
      avail_list.string=""
      avail_list.value=[];
      import_btn.enable=%f;
    end
    avail_list.enable="on"
  case "Expression" then 
    file_edit.enable="off"
    expr_edit.enable="on"
    browse_btn.enable="off"
    avail_list.string=""
    avail_list.enable="off"
  end
endfunction

function importCallback(win,dt)
//callback of the Import button
  fig=gcbo.Parent
  ud=fig.user_data;
  file_edit=ud.handles(4)
  expr_edit=ud.handles(5);
  avail_list=ud.handles(7);
  model_list=ud.handles(8);
  import_btn=ud.handles(9);
  mname=model_list.user_data(model_list.value)
  kfrom=find(ud.handles(1:3).value==1)// selected radio button
  select ud.handles(kfrom).tag
  case "File" then //from file
    if avail_list.string==[]|avail_list.value==[] then
      sisotoolSetError(file_edit,%t)
      return
    end
    vname=avail_list.string(avail_list.value)
    if execstr("load(file_edit.string,vname)","errcatch")<>0 then
      msg=msprintf(_("Variable %s does not exist in %s"),vname,file_edit.string);
      sisotoolSetError(file_edit,%t,msg)
      return
    end
    execstr("T="+vname)
    if ~sisotoolIsSISO(T,%_dt)  then
      //it may occur if the file has been changed after it has been tested
       msg=_("variable is not a siso system")
      sisotoolSetError(file_edit,%t,msg);
      return
    end
    sisotoolSetError(file_edit,%f);
  case "Workspace" then
    if avail_list.string==[]|avail_list.value==[] then
      sisotoolSetError(file_edit,%t)
      return
    end
    vname=avail_list.string(avail_list.value)
    if execstr("T="+vname,"errcatch")<>0 then
      msg=_("Variable no more exists")
      sisotoolSetError(expr_edit,%t,msg)
      return
    end
    if ~sisotoolIsSISO(T,%_dt)  then
      //it may occur if the user changes the variable definition
      msg=_("Given system is not siso or  has an icompatible time domain")
      sisotoolSetError(expr_edit,%t,msg)
      return
    end
    sisotoolSetError(expr_edit,%f)
   
  case "Expression" then 
     if execstr("T="+expr_edit.string,"errcatch")<>0 then
       msg=_("Expression is invalid")
       sisotoolSetError(expr_edit,%t,msg)
       return
     end
     if ~sisotoolIsSISO(T,%_dt)  then
      msg=_("Given system is not siso or  has an icompatible time domain")
      sisotoolSetError(expr_edit,%t,msg)
      return
    end
    sisotoolSetError(expr_edit,%f)
  end
  mainH=get_figure_handle(win)
  S=sisotoolGetSession(mainH)
  tunables=sisotoolGetArchiSpecs(S.Designs.archi,"tunable");
  if or(mname==tunables) then //
    T=sisotooltf2C(T)
  elseif typeof(T)=="constant" then   
    T=syslin("c",[],[],[],T)
  end
  
  S=sisotoolAddHistory(S,mname,S.Designs(mname)) 
  S.Designs(mname)=T
  sisotoolSetSession(S,mainH)
  sisotoolRedraw(mainH)
endfunction
