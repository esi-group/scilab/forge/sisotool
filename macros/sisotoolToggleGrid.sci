// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolToggleGrid(win)
  figHandle=get_figure_handle(win)
  if figHandle.tag=="Editors" then
    mainH=figHandle
    ax=gca()// current axes has been set by sisotoolEditorsEvents
    S=sisotoolGetSession(mainH)
    EditorsSettings=S.EditorsSettings
    EditorsHandles=EditorsSettings.handles
    //TBD on peut ne pas stocker les EditorsHandles  en utilisant subWins
    //    et la liste des axes enfants de la figure
    selected=[]
    for k=1:size(EditorsHandles)
      EditorHandle=EditorsHandles(k)
      if or(EditorHandle==ax) then //For bode plots there are 2 handles
        EditorsSettings.gridded(k)=abs(EditorsSettings.gridded(k)-1)
        S.EditorsSettings=EditorsSettings
        sisotoolSetSession(S,mainH)
        sisotoolRedrawEditors(mainH,%f,k)
        break
      end
    end
  else
    mainH=figHandle.user_data.mainH
     ax=gca()// current axes has been set by  sisotoolAnalysersEvents
    S=sisotoolGetSession(mainH)
    AnalysersSettings=S.AnalysersSettings
    AnalysersHandles=AnalysersSettings.handles
    selected=[]
    for k=1:size(AnalysersHandles)
      EditorHandle=AnalysersHandles(k)
      if or(EditorHandle==ax) then //For bode plots there are 2 handles
        AnalysersSettings.gridded(k)=abs(AnalysersSettings.gridded(k)-1)
        S.AnalysersSettings=AnalysersSettings
        sisotoolSetSession(S,mainH)
        sisotoolRedrawAnalysers(mainH,%f,k)
        break
      end
    end
  end

endfunction
