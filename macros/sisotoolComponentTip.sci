function str=sisotoolComponentTip(h)
 
  c=h.parent;
  dfd=c.display_function_data;
  u=dfd.units.frequency
  dt=dfd.dt

  i=find(h.data(1)==c.data(:,1)&h.data(2)==c.data(:,2))
 
  if i==[]|dfd.data==[] then str="";return;end
  select dfd.tipType
  case "pole"
    p=dfd.data(i)
  
    [wn,zeta] = damp(p,dfd.dt)
    wn=freqconv("rd/s",u,wn);
    if isreal(p,0) then
      str=msprintf(_("Compensator real pole\nValue: %.3g\nNatural pulsation: %.3g%s"),p,wn,u)
    else
      str=msprintf(_("Compensator complex pole\nDamping: %.3gdB\nNatural pulsation: %.3g%s"),zeta,wn,u)
    end
  case "zero"
    z=dfd.data(i)
    [wn,zeta] = damp(z,dfd.dt);
     wn=freqconv("rd/s",u,wn);
    if isreal(z,0) then
      str=msprintf(_("Compensator real zero\nValue: %.3g\nNatural pulsation: %.3g%s"),z,wn,u)
    else
      str=msprintf(_("Compensator complex zero\nDamping: %.3gdB\nNatural pulsation: %.3g%s"),zeta,wn,u)
    end
  case "leadlag"
    z=dfd.data(1,i)
    p=dfd.data(2,i)
    if dfd.dt=="c" then
      if abs(z)>=abs(p)
        str=msprintf(_("Compensator lag\nZero: %.3g\nPole: %.3g"),z,p)
      else
        str=msprintf(_("Compensator lead\nZero: %.3g\nPole: %.3g"),z,p)
      end
    else
      if abs(z)<=abs(p)
        str=msprintf(_("Compensator lag\nZero: %.3g\nPole: %.3g"),z,p)
      else
        str=msprintf(_("Compensator lead\nZero: %.3g\nPole: %.3g"),z,p)
      end
    end
  case "notch"
    z=dfd.data(1,i)
    p=dfd.data(2,i)
    [wn,zz] = damp(z,dfd.dt)
    [wn,zp] = damp(p,dfd.dt)
    wn=freqconv("rd/s",u,wn);
    str=msprintf(_("Compensator notch\nDamping: [%.3g,%.3g]dB\nNatural pulsation: %.3g%s"),zz,zp,wn,u)
  end

endfunction
