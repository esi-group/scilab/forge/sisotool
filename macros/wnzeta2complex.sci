// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function z=wnzeta2complex(dt,wn,zeta) 
//converts the natural frequency, demaping factor representation to
//standard complex representation
  if dt=="c" then
    z=wn.*complex(-zeta,-max(1e-30,sqrt(1-zeta.^2)));
  else
    sgn=sign(zeta)
    if abs(zeta)==1 then //z is real >0
      z=exp(-sgn.*wn*dt)
    else
      z=exp(dt*wn.*complex(-zeta,-max(1e-30,sqrt(1-zeta.^2))));
    end
  end
endfunction
