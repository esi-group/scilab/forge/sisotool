// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolSetPIDGui(gui)
//resets the PID GUI values if it is opened
  if type(gui)<>9 then return;end  
  methods=[
      _("Approximate MIGO step response") "AMIGOSR"
      _("Ziegler Nichols step response") "ZNSR"
      _("Chien Hrones Reswick") "CHR";
      _("Cohen Coon") "CC";
      _("Ziegler Nichols frequency response") "ZNFR"
      _("Approximate MIGO frequency response") "AMIGOFR"
      _("Pole placement") "PP"
          ];
  applicable=%t(ones(methods(:,1)))
 
  ud=get(gui,"user_data")
  mainH=ud.mainH

  
  S=sisotoolGetSession(ud.mainH);
  PIDSettings=S.PIDSettings
  sys=S.Designs;
  out_of=sisotoolGetArchiSpecs(sys.archi,"out_of");
  out_of=out_of(PIDSettings.out_of_index)
  dt=sys.dt
  //set compensator to unit gain
  sys.out_of=sisotoolNewController(dt)
  P=sisotoolSys2OL(sys,out_of)
  p=poles(P)
  np=size(p,"*")
  if P.dt=="c" then
    istable=and(real(p)<=0)
    intcount=size(find(real(p)==0),"*")
  else
    istable=and(abs(p)<=1)
    intcount=size(find(abs(p)==1),"*")
  end
  applicable($)=np<=2
  applicable(1:$-1)=istable&&np>1
  applicable(1:3)=applicable(1:3)&intcount<=1
  if applicable(4) then
    [gm,fr]=g_margin(P)
    applicable(4:5)=applicable(4:5)&gm<>[]
  end

  frames=gui.children(2:$)// [Method Type Compensator]
  C=frames(3).children
  C(2).string=out_of(:)
  C(2).value=PIDSettings.out_of_index;
  C(1).string=sisotoolC2tex(sys(out_of(PIDSettings.out_of_index)));
  
  T=frames(2).children;
  T=T(T.style=="radiobutton");
  k=find(T.tag==PIDSettings.PIDType)
  T(k).value=1;

  
  M=frames(1).children;
  M($).string=methods(applicable,1)
  M($).user_data=methods(applicable,2)
  i=find(methods(applicable,2)==PIDSettings.method)
  if i==[] then i=1;end
  M($).value=i
  meth=methods(applicable(i),2)
  if PIDSettings.N<>[] then
     M(7:8).visible="on"
     M(8).string=sci2exp(PIDSettings.N)
  else
     M(7:8).visible="off"
  end
  pars=PIDSettings.parameters

  M(1:6).visible="off"
  if meth=="PP" then
    M(3:6).visible="on"
    M(3).string=sci2exp(pars(2))//zeta
    M(5).string=sci2exp(pars(1))//wn
  elseif meth=="CHR" then
    M(1).value=sci2exp(pars(2))
    M(2).value=sci2exp(pars(1))
  end
endfunction
