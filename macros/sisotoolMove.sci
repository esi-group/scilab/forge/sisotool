// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolMove(mainH,ax,x,y)
//mainH handle on the main editor window
//what?
  figureHandle=ax.parent
  editorData=sisotoolGetEditorData(ax)
  typ=ax.tag
  [entityType,i,k,pt,loc]=sisotoolFindSelected(editorData,[x,y])
  if entityType==[] then return,end //nothing selected
  
  if figureHandle.tag=="Analysers"&entityType<>"requirements" then return;end
  if and(entityType<>["zeros" "poles" "gain" "loci" "leadslags" "notches" ...
                      "notches_width" "requirements"]) then
    warning(msprintf(_("%s action not yet implemented for %s\n"),"Move",sci2exp(entityType,0)))
    return
  end
  if entityType<>"requirements" then
    lab=convstr(sisotoolEntityType2Label(entityType))
    figureHandle.info_message=msprintf(_("Move this %s to the desired location"),lab)
  else
    figureHandle.info_message=msprintf(_("Move this requirement limit to the desired location"))
  end

  select typ
  case "rlocus"
    sisotoolMoveRootLocus(mainH,editorData,entityType,i,k,pt,loc)
  case "olbm" 
    sisotoolMoveGainPlot(mainH,editorData,entityType,i,k,pt,loc)
  case "gainplot" //TBD voir si on peut remplacer gainplot par olbm
    sisotoolMoveGainPlot(mainH,editorData,entityType,i,k,pt,loc)
  case "olbp" 
    sisotoolMovePhasePlot(mainH,editorData,entityType,i,k,pt,loc)
  case "nichols" then
    sisotoolMoveBlack(mainH,editorData,entityType,i,k,pt,loc)
  case "clbm" then
    sisotoolMoveGainPlot(mainH,editorData,entityType,i,k,pt,loc)
  case "clbp" then
    sisotoolMovePhasePlot(mainH,editorData,entityType,i,k,pt,loc)
    return
  case "phaseplot" then
    sisotoolMovePhasePlot(mainH,editorData,entityType,i,k,pt,loc)
    return
  case "step" then
    sisotoolMoveResponsePlot(mainH,editorData,entityType,i,k,pt,loc)
    return
  case "impulse" then
    sisotoolMoveResponsePlot(mainH,editorData,entityType,i,k,pt,loc)
    return
  end
  sisotoolRedraw(mainH);
  if entityType<>"requirements" then
    sisotoolUpdateOptimGui(mainH,"eltchange")
  else
  end

endfunction
