// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolSaveFigure(id)
  fig=scf(id)
  fil=uigetfile("*.scg")
  if fil=="" then return;end
 
  evh=fig.event_handler;fig.event_handler="";
  evhe=fig.event_handler_enable;fig.event_handler_enable="off"
  cf=fig.closerequestfcn;fig.closerequestfcn=""
  
  //clean the user-data fields which may contain graphic handles.
   ud=fig.user_data;fig.user_data=[];
  udsub=list()
  for k=1:size(fig.children,"*")
    udsub(k)=fig.children(k).user_data
    fig.children(k).user_data=[];
  end

  save(fil,"fig")

  fig.user_data=ud;
  fig.event_handler=evh
  fig.event_handler_enable=evhe
  fig.closerequestfcn=cf;
  for k=1:size(fig.children,"*")
    fig.children(k).user_data=udsub(k);
  end

endfunction
