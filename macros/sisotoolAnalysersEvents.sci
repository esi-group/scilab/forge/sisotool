// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAnalysersEvents(win,x,y,ibut)
  if ibut<0 then return,end
  if ibut>1000 then
    select ascii(ibut-1000)
    case "z" //CTRL+z
      sisotoolUndo(win)
    case "y" //CTRL+y
      sisotoolRedo(win)
    case "s" // CTRL+s 
      sisotoolSaveSession(win,%f)
     case "o" // CTRL+s   
      sisotoolLoadSession(win)
    end
    return
  end
  analysersHandle=get_figure_handle(win)
  mainH=analysersHandle.user_data.mainH
  scf(analysersHandle);
 // analysersHandle.event_handler_enable = "off"
  [ax,sub]=findSubwin(x,y)
  ax.rotation_angles=[0 270];
  if ax==[] then analysersHandle.event_handler_enable = "on";return,end
  if ibut==0 then //left press ???
    sisotoolMove(mainH,ax,x,y)
  elseif or(ibut==[2 5]) then
    sisotoolAnalysersCMenu(ax,x,y,sub)
  end
  analysersHandle.event_handler_enable = "on"
endfunction

