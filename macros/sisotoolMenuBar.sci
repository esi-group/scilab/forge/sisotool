// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolMenuBar(mainH)
 //create the sisotool ones
 sisotoolFileMenu(mainH)
 sisotoolEditMenu(mainH)
 sisotoolEditorsMenu(mainH)
 sisotoolAnalysisMenu(mainH)
 sisotoolToolsMenu(mainH)

endfunction
