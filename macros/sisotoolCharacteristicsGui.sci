// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolCharacteristicsGui(win,cType)
  ax=gca()//current axes has been set by sisotoolEventHandler
  analyserData=sisotoolGetEditorData(ax)
  analyserType=ax.tag
  //TBD
  select analyserType
  case "step"
    select cType
    case _("Peak response")
    case _("Settling time")
    case _("Rise time")
    case _("Steady state")
    end
  case "impulse"
    select cType 
    case _("Peak response")
    case _("Settling time")
    end
  case "bode" //pas ok (olbm,....)
    select cType 
    case _("Peak response")
    case _("Min stability margin")
    case _("All stability margins")
    end
  case "nyquist"
    select cType 
    case _("Peak response")
    case _("Min stability margin")
    case _("All stability margins")
    end
  case "nichols"
    select cType 
    case _("Peak response")
    case _("Min stability margin")
    case _("All stability margins")
    end
  case "pole/zero"

  end 

endfunction
