// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAddComponent()
  gui=gcbo.parent
  guiHandle=gui.parent; //handle on the dialog figure
  win=get_figure_handle(guiHandle);
  win=gcbo.parent.parent.figure_id; 
  componentTypes=["Real Pole" "Complex Pole" "Integrator" "Real Zero" ...
                  "Complex Zero"  "Differentiator" "Lead" "Lag" "Notch"]
  componentLabels= [_("Real Pole")  _("Complex Pole")  _("Integrator")  _("Real Zero") ...
                   _("Complex Zero")   _("Differentiator")  _("Lead")  _("Lag")  _("Notch")]
 
  listOfMenus = list()
  for k=1:size(componentLabels,"*"),  
    listOfMenus($+1)=[componentLabels(k) "sisotoolAddGUIComponent("+string(win)+","+sci2exp(componentTypes(k))+")"];
  end
  sisotoolCreatePopupMenu(listOfMenus)
 
 // Cmenu=sisotoolCreatePopupMenu(listOfMenus)
 // k=find(Cmenu==componentLabels)
 // if k<>[] then
 //   sisotoolAddGUIComponent(gui,componentTypes(k))
 // end
endfunction
