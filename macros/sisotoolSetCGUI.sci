// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolSetCGUI(gui)
//Update the compensator editor GUI with current values
  PartChanged=argn(2)<1
  if PartChanged then
    // sisotoolSetCGUI called as a callback of the compensatorParts listbox
    gui=gcbo.parent.parent
  end

  ud=get(gui,"user_data")
  S=sisotoolGetSession(ud.mainH)
  sys=S.Designs
  units=S.Preferences.units
  selectionHandles=ud.selectionHandles;
  DisplayHandle=selectionHandles.display;
  compensatorDisplay=DisplayHandle.children(1);
  compensatorParts=DisplayHandle.children(2)

  compensatorName=compensatorParts.string(compensatorParts.value);
 

  EditorFrame=selectionHandles.editor;
  if PartChanged then //reset component Editor
    delete(EditorFrame.children);
    EditorFrame.user_data=[];
  end
  viewHandles=selectionHandles.views;
  ind=viewHandles(1).value;
  ComponentData=EditorFrame.user_data;
  C=sys(compensatorName);
  nzeros=size(C.zeros,"*");
  npoles=size(C.poles,"*");
  nleadslags=size(C.leadslags,2);
  
  nnotches=size(C.notches,2);
  nt=nzeros+npoles+nleadslags+nnotches;
  compensatorDisplay.visible="off"; //see bug report 13909
  compensatorDisplay.string=sisotoolC2tex(C)
  compensatorDisplay.visible="on";
  types=[];
  locations=[];
  dampings=[];
  frequencies=[];
    
  //gain
  K=C.gain
  locations=[locations; sisotoolFormatPZ(K)]
  types=[types;"Gain"];
  dampings=[dampings;" "];
  frequencies=[frequencies;" "];
  
  //zeros
  z=C.zeros.';
  if z<>[] then
    [wn,zeta]=damp(z,C.dt);
    l=sisotoolFormatPZ(z);
    locations=[locations;l];
    types=[types;emptystr(l)+"Zero"];
    dampings=[dampings;sisotoolFormatPZ(zeta)];
    frequencies=[frequencies;sisotoolFormatPZ(freqconv("rd/s",units.frequency,wn))];
  end
  //poles
  z=C.poles.';
  if z<>[] then
    [wn,zeta]=damp(z,C.dt);
    l=sisotoolFormatPZ(z);
    locations=[locations;l];
    types=[types;emptystr(l)+"Pole"];
    dampings=[dampings;sisotoolFormatPZ(zeta)];
    frequencies=[frequencies;sisotoolFormatPZ(freqconv("rd/s",units.frequency,wn))];
  end
  //Leads and lags
  if nleadslags>0 then
    n=nleadslags
    z=C.leadslags(1,:).'
    p=C.leadslags(2,:).'
    t=emptystr(n,1);
    
    if C.dt=="c" then
      t(abs(z)>abs(p))="Lag" 
      t(abs(z)<=abs(p))="Lead"
    else
      t(abs(z)<abs(p))="Lag" 
      t(abs(z)>=abs(p))="Lead"
    end

    types=[types;t]
    locations=[locations;sisotoolFormatPZ([z,p])]
    dampings=[dampings;emptystr(n,1)+"1"];
    frequencies=[frequencies;
                 sisotoolFormatPZ(freqconv("rd/s",units.frequency,damp([z,p])))];
  end
  if nnotches>0 then
    n=nnotches
    z=C.notches(1,1:2:$).'
    p=C.notches(2,1:2:$).'
    types=[types;emptystr(n,1)+"Notch"];
    locations=[locations;"["+sisotoolFormatPZ(z)+"; "+sisotoolFormatPZ(p)+"]"]
    [wn,zetaz]=damp(z,C.dt);
    [wn,zetap]=damp(p,C.dt);
    dampings=[dampings;"["+sisotoolFormatPZ(zetaz)+"; "+sisotoolFormatPZ(zetap)+"]"]
    frequencies=[frequencies;sisotoolFormatPZ(freqconv("rd/s",units.frequency,wn))];
  end

  for k=1:4
     viewHandles(k).value=ind;
  end
   
  viewHandles(1).string=types
  viewHandles(2).string=locations;//locations
  viewHandles(3).string=frequencies;//natural frequencies
  viewHandles(4).string=dampings; //damping
 
  if type(ComponentData)<>1 then //a component is currently edited
     entityType=ComponentData.type
     entityIndex=ComponentData.entityIndex
     H=ComponentData.handle
     if entityType=="gain" then
       H(1).string=string(C.gain)
     elseif or(entityType==["zeros" "poles"]) then
       z=C(entityType)(entityIndex)
       [wn,zeta]=damp(z,C.dt)
       H(1).string=string(real(z));
       if imag(z)<>0 then
         H(2).string=string(abs(imag(z)));
         H(3).string=string(freqconv("rd/s",units.frequency,wn));
         H(4).string=string(zeta); 
       end
     elseif entityType=="leadslags" then
       z=C(entityType)(1,entityIndex)
       p=C(entityType)(2,entityIndex)
       [wmax,phimax]=LeadLag2Properties(z,p,C.dt)
       H(1).string=string(z);
       H(2).string=string(p);
       H(3).string=string(phaseconv(_("rd"),units.phase,phimax));// degrees
       H(4).string=string(freqconv("rd/s",units.frequency,wmax));
     elseif or(entityType=="notches") then 
       z=C(entityType)(1,entityIndex);
       p=C(entityType)(2,entityIndex);
       //compute wn zetaz zetap depth width
       [wn,zetaz] = damp(z,C.dt)
       [wn,zetap] = damp(p,C.dt)
       //Zetaz should be less than zetap
       depth=zetaz/zetap;
       width=sisotoolNotchWidth(zetaz,zetap)
       H(1).string=string(freqconv("rd/s",units.frequency,wn));
       H(2).string=string(zetaz);
       H(3).string=string(zetap);
       H(4).string=string(magconv("absolute",units.magnitude,depth)); //Db
       H(5).string=string(log10(width)); //log10
     end
   end
   
endfunction

