// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolUndo(win)
  mainH=get_figure_handle(win);
  
  if mainH.tag<>"Editors" then
    figHandle=mainH;
    mainH=mainH.user_data.mainH;
  else
    figHandle=mainH;
  end
  windowType=figHandle.tag;
  Settings= windowType+"Settings"
  S=sisotoolGetSession(mainH);
  current=S.History.current;
  if current<=0 then return;end
  prev=S.History.stack(current);
  args=prev.args
  kind=prev.kind
  op=prev.op

  select kind
  case "Compensator"
    compensatorName=args(1);
    entityType=args(2);
    select op
    case "add" //undo add component
      S=sisotoolDeleteCElement(S,compensatorName,entityType,$);
    case "delete" //undo delete component
      value=args(4)
      S=sisotoolSetCElement(S,compensatorName,entityType,value);
    case "update"  //undo update component
      if entityType=="all" then //TBD verifier
        cur=list(S.Designs(compensatorName))
        for i=1:size(compensatorName,"*")
          S.Designs(compensatorName(i))=args(3)(i)
        end
      else
        index=args(3)
        if entityType=="gain" then index=1;end
        value=args(4)
        cur=sisotoolGetCElement(S,compensatorName,entityType,index);
        S=sisotoolSetCElement(S,compensatorName,entityType,value,index);
        S.History.stack(current).args(4)=cur;
      end
    end
  case "Requirements"
    windowType=args(1);
    Settings=windowType+"Settings";
    subWinIndex=args(2);
    requirementIndex=args(3);
    reqProp=args(4);
    opt=args(5)
    select op
    case "add" //undo add requirement
      args(4).data= S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data
      args(5).data=S.OptimSettings(windowType)(subWinIndex)(reqProp.type)
      S.History.stack(current).args=args
      S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=[];
      S.OptimSettings(windowType)(subWinIndex)(reqProp.type)=%f(ones(opt));
    case "delete" //undo delete requirement
      S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=reqProp.data;
      S.OptimSettings(windowType)(subWinIndex)(reqProp.type)=opt;
      args(4).data=[]
      args(5).data=%f(ones(opt))
      S.History.stack(current).args=args
    case "update"  //undo update requirement
      cur=S(Settings).subWins(subWinIndex).reqProps(requirementIndex)
      S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=reqProp.data;
      curopt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type)
      S.OptimSettings(windowType)(subWinIndex)(reqProp.type)=opt;
      args(4)=cur;args(5)=curopt;
      S.History.stack(current).args=args;
    end
  case "OptimSettings"
    kind=args(1);
    tag=op
    select tag
    case "req"
      sub=args(2);req_type=args(3);requirementIndex=args(4);v=args(5);
      cur= S.OptimSettings(kind)(sub)(req_type)(requirementIndex)
      S.OptimSettings(kind)(sub)(req_type)(requirementIndex)=v
      S.History.stack(current).args(5)=cur;
    case "enabled"
      tunable=args(2);typ=args(3);index=args(4);v=args(5);
      cur=S.OptimSettings(kind)(tunable)(typ)(tag)(index)
      S.OptimSettings(kind)(tunable)(typ)(tag)(index)=v;
      S.History.stack(current).args(5)=cur;
    case "style"
      guiHandle=S.OptimGui
      tunable=args(2);typ=args(3);index=args(4);v=args(5);
      frame=guiHandle.children(3)
      rtyp=frame.children(4).user_data
      cur= S.OptimSettings(kind)(tunable)(typ)(tag)(index)
      initial=S.OptimSettings(kind)(tunable)(typ).initial(:,index);
      lowbound=S.OptimSettings(kind)(tunable)(typ).lowbound(:,index);
      highbound=S.OptimSettings(kind)(tunable)(typ).highbound(:,index);
      dt=S.Designs(tunable).dt
      [initial,lowbound,highbound]=sisotoolChangeEltFormat(dt,rtyp,v,initial,lowbound,highbound);
      S.OptimSettings(kind)(tunable)(typ).initial(:,index)=initial;
      S.OptimSettings(kind)(tunable)(typ).lowbound(:,index)=lowbound;
      S.OptimSettings(kind)(tunable)(typ).highbound(:,index)=highbound;
      S.OptimSettings(kind)(tunable)(typ)(tag)(index)=v;
      S.History.stack(current).args(5)=cur;
    case "options"
      cur= S.OptimSettings.Options
      S.OptimSettings.Options=args(1)
      S.History.stack(current).args(1)=cur;
    else //initial lowbound or highbound
      tunable=args(2);typ=args(3);index=args(4);v=args(5);
      i=find(tag==[ "highbound" "lowbound" "initial"])
      cur=S.OptimSettings(kind)(tunable)(typ)(tag)(:,index)
      S.OptimSettings(kind)(tunable)(typ)(tag)(:,index)=v;
      S.History.stack(current).args(5)=cur;
    end
  end
  S.History.current=current-1;
  sisotoolSetSession(S,mainH);
  if kind<>"OptimSettings" then sisotoolRedraw(mainH);end
  sisotoolUpdateOptimGui(mainH);
endfunction
