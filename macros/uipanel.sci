// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function h=uipanel(varargin)
  border = createBorder("line", "white",1)
  for k=1:2:size(varargin)
    if convstr(varargin(k))=='title' then
      fnt=createBorderFont("",0,"bold")
      border = createBorder("titled",border, varargin(k+1),"","",fnt)
      varargin(k+1)=null()
      varargin(k)=null()
      break
    end
  end
  h=uicontrol("Style","frame","relief","groove","border",border,varargin(:))
endfunction

