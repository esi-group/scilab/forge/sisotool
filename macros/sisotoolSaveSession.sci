// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolSaveSession(win,opt)
 
  if type(win)==1 then
    mainH=get_figure_handle(win);
  else
    mainH=win
  end
  S=sisotoolGetSession(mainH);
  if argn(2)<2|opt then
    path=uiputfile("*.siso")
    if path=="" then return,end
    S.Path=path
    [path, fname] = fileparts(path)
    mainH.figure_name=msprintf(_("%s: Graphical compensator editors (%%d)"),fname)
  else
    path=S.Path
  end
  tools=["EditorsSelector","CompensatorEditor",...
         "AnalysersSelector","ArchitectureSelector",...
         "Analysers","OptimGui","PrefGui","IMCGui","PIDGui"]
  for tool=tools 
    //do not save the handles but recall that they were opened
    if S(tool)<>[] then S(tool)="opened"; end
  end
  S.EditorsSettings.handles=[];
  S.AnalysersSettings.handles=[];

  //do not save the history
  S.History=struct("stack",list(),"current",0,"ref",0);
  save(S.Path,"S")
  S=sisotoolGetSession(mainH)
  S.Path=path
  S.History.ref=S.History.current
  
  sisotoolSetSession(S,mainH)
endfunction

