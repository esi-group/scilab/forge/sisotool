// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function S=sisotoolCreateEditors(mainH,S)
 
  mainH.immediate_drawing="off"
  subwins=[];
  subWins=S.EditorsSettings.subWins;
  dt=S.Designs.dt
  units=S.Preferences.units
  nsubwins=size(subWins,'*');
  if nsubwins==0 then return;end
  //Workaround for bug 14908
  A=mainH.children;
  if A<>[] then   A=A(A.type=="axes");end
  [wsplit,wmargin]=sisotoolSubwinSettings(list2vec(subWins.type));
  EditorHandles=list();
  for sub=1:nsubwins
    subWin=subWins(sub);
    select subWin.type
    case "rlocus"
      ax=newaxes(mainH);
      ax.tag="rlocus";
      ed=sisotoolNewEditorData(sub);
      ax.axes_visible="on";
      ax.clip_state='clipgrf';
      ax.data_bounds = [-1,-1;0,1];
      ax.title.text=msprintf(_("Root locus editor for open loop (%s)"),subWin.out_of)
      ax.axes_bounds=wsplit(sub,:)
      ax.margins=wmargin(sub,:)
      ax.background=-2;
      ax.tight_limits = ["on","on","off"];
      ax.x_label.text=_("Real axis")
      ax.y_label.text=_("Imaginary axis")
       //the entities must be drawn in this order: requirements, grid, evans,marks
      //Requirements
      ed.reqTypes=sisotoolRequirementTypes(ax.tag)
      ed.tunable=subWin.out_of
      rtyps=list2vec(ed.reqTypes.type)
      rtyps(rtyps=="overshoot")=[] //overshoot is similar to damping with different unit
      ed.requirements=sisoCreateRequirements(rtyps,ones(rtyps));

      //Place holder for grid
      xpoly([],[])
      h=glue(gce());//h.tag="grid" 
      
      //Place Holder for mainGrid
      xpoly([],[]);h=gce();set(h,"foreground",2,"line_style",3);h.tag="h"
      xpoly([],[]);v=gce();set(v,"foreground",2,"line_style",3);v.tag="v"
      if S.Designs.dt<>"c" then
        //unit circle
        t=linspace(0,2*%pi,100)
        xpoly(sin(t),cos(t));c=gce();set(c,"foreground",2,"line_style",3);
        ed.mainGrid=[h,v,c]
      else
        ed.mainGrid=[h,v]
      end

      //Place holder for Evans plot
      xpoly([],[])
      h=glue(gce());//h.tag="rlocus" 
      ed.loci=h; 
   
      //poles and zeros handles
      ed.zeros=sisoCreateZeros(dt)
      ed.poles=sisoCreatePoles(dt)
      ed.leadslags=sisoCreateLeadsLags(dt)
      ed.notches=sisoCreateNotches(dt)
      //gain
      ed.gain=sisoCreateGain(dt)     
      
      
      sisotoolSetEditorData(ed,ax);
    
    case "olbm" then ////////////////////////////////
      //Open Loop gainplot
      axolbm=newaxes(mainH);axolbm.tag="olbm";
      sca(axolbm)
      axolbm.title.text=msprintf(_("Bode plot editor for open loop (%s)"),subWin.out_of)
      axes_bounds=wsplit(sub,:);
      margins=wmargin(sub,:);
      sca(axolbm)
      axolbm.axes_visible="on";
      axolbm.axes_bounds=axes_bounds;
      axolbm.margins=margins
      axolbm.background=-2;
      axolbm.clip_state='clipgrf'
      axolbm.x_label.text='';
      axolbm.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
      axolbm.data_bounds = [1e-3,-1;1e3,1];
      axolbm.log_flags = "lnn";
      axolbm.tight_limits=["on" "on" "off"];
      axolbm.x_ticks.labels=emptystr(axolbm.x_ticks.labels);      

      edm=sisotoolNewEditorData(sub);
      edm.reqTypes=sisotoolRequirementTypes(axolbm.tag)
      edm.tunable=subWin.out_of

                                            
      //create graphic entities
      rtyps=list2vec(edm.reqTypes.type);//rtyps=rtyps(1:2)//TBD margins?
      edm.requirements=sisoCreateRequirements(rtyps,[1 1 2]);
      xpoly([],[]);edm.loci=gce();
      dfd=struct("TF_name",subWin.out_of,"units",units,"tipType",[]);
      set(edm.loci,"display_function_data",dfd,...
          "display_function","sisotoolBodeTips");
      //poles and zeros handles
      edm.zeros=sisoCreateZeros(dt)
      edm.poles=sisoCreatePoles(dt)
      edm.leadslags=sisoCreateLeadsLags(dt)
      edm.notches=sisoCreateNotches(dt)
      edm.notches_width=[sisoCreateWidth(dt);sisoCreateWidth(dt)];

      //gain margin handles
      e0=sisoCreateRuler(dt);
      e1=sisoCreateRuler(dt);
      e2=sisoCreateMargin(dt)
      edm.gmargin=[e0 e1 e2];
      edm.pmargin=sisoCreateRuler(dt);
      sisotoolSetEditorData(edm,axolbm)
      ax=axolbm;
    case "olbp" then ////////////////////////////////
      //Open Loop phaseplot
      axolbp=newaxes(mainH);axolbp.tag="olbp";
      axes_bounds=wsplit(sub,:);
      margins=wmargin(sub,:);
      sca(axolbp)
      axolbp.axes_visible="on";
      axolbp.axes_bounds=axes_bounds;
      axolbp.margins=margins;
      axolbp.background=-2;
      axolbp.clip_state='clipgrf';
      axolbp.x_label.text=msprintf(_("Frequency (%s)"),units.frequency);
      axolbp.y_label.text=msprintf(_("Phase (%s)"),units.phase);
      axolbp.data_bounds = [1e-3,-90;1e3,90];
      axolbp.log_flags = "lnn";
      axolbp.tight_limits=["on" "on" "off"];
      edp=sisotoolNewEditorData(sub);
      edp.reqTypes=sisotoolRequirementTypes(axolbp.tag)
      edp.tunable=subWin.out_of
      //create graphic entities
                                                 
      //create graphic entities
      rtyps=list2vec(edp.reqTypes.type);
      edp.requirements=sisoCreateRequirements(rtyps,[2]);
      xpoly([],[]);edp.loci=gce();
      dfd=struct("TF_name",subWin.out_of,"units",units,"tipType",[]);
      set(edp.loci,"display_function_data",dfd,...
          "display_function","sisotoolBodeTips");

      //poles and zeros handles
      edp.zeros=sisoCreateZeros(dt)
      edp.poles=sisoCreatePoles(dt)
      edp.leadslags=sisoCreateLeadsLags(dt)
      edp.notches=sisoCreateNotches(dt)
      //gain margin handle
      edp.gmargin=sisoCreateRuler(dt);
      //phase margin handles
      e0=sisoCreateRuler(dt);
      e1=sisoCreateRuler(dt);
      e2=sisoCreateMargin(dt)
      edp.pmargin=[e0 e1 e2];
      sisotoolSetEditorData(edp,axolbp);
      ax=axolbp;
    case "nichols" then ////////////////////////////////
      ax=newaxes(mainH);ax.tag="nichols";
      ed=sisotoolNewEditorData(sub);
      ed.tunable=subWin.out_of
      ed.reqTypes=sisotoolRequirementTypes(ax.tag)
      ax.title.text=msprintf(_("Black/Nichols editor for open loop (%s)"),subWin.out_of)
      ax.axes_bounds=wsplit(sub,:)
      ax.margins=wmargin(sub,:)
      ax.axes_visible="on";
      ax.clip_state='clipgrf';
      ax.tight_limits = ["on","on","off"];
      ax.x_label.text=msprintf(_("Phase (%s)"),units.phase);
      ax.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
  
      ax.data_bounds = [-1,-1;0,1];
      ax.background=-2;
      sca(ax)
      
      rtyps=list2vec(ed.reqTypes.type)
      //take care the order matter
      ed.requirements=sisoCreateRequirements(rtyps,[2 2 1 1 1]);
      //  2.3 db curve
       //main grids
      xpoly([],[]),e1=gce();e1.line_style=4;e1.clip_state="clipgrf";
      xpoly([],[]),e2=gce();e2.line_style=4;e2.clip_state="clipgrf";
      xpoly(phaseconv(_("degrees"),units.phase,-180),magconv("dB",units.magnitude,0),"marks")
      e3=gce();
      set(e3,"mark_style",1,"mark_foreground",5,"mark_size",1,"thickness",2);
      ed.mainGrid=[e1,e2,e3];
           
      //loci
      xpoly([],[]);ed.loci=gce();
      dfd=struct("TF_name","Open loop out of "+subWin.out_of,"units",units,"tipType",[]);
      set(ed.loci,"display_function_data",dfd,...
          "display_function","sisotoolNicTips");

      ed.zeros=sisoCreateZeros(dt);
      ed.poles=sisoCreatePoles(dt);
      ed.leadslags=sisoCreateLeadsLags(dt);
      ed.notches=sisoCreateNotches(dt);
      ed.notches_width=[sisoCreateWidth(dt);sisoCreateWidth(dt)];
      //gain
      ed.gain=sisoCreateGain(dt)
      //gain margin
      ed.gmargin=sisoCreateMargin(dt)
      //phase margin
      ed.pmargin=sisoCreateMargin(dt)
      sisotoolSetEditorData(ed,ax);
      
    case "clbm" //Closed Loop bode from in to out
       axclbm=newaxes(mainH);axclbm.tag="clbm"
      
       axclbm.title.text=msprintf(_("Bode editor for closed loop from %s to %s, tunable:%s"),...
                                  subWin.in, subWin.out,subWin.tunable)
       axes_bounds=wsplit(sub,:);
       margins=wmargin(sub,:);
       sca(axclbm)
       axclbm.axes_visible="on";
       axclbm.axes_bounds=axes_bounds;
       axclbm.margins=margins
       axclbm.background=-2;
       axclbm.clip_state='clipgrf'
       axclbm.x_label.text='';
       axclbm.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
       axclbm.data_bounds = [1e-3,-1;1e3,1];
       axclbm.log_flags = "lnn";
       axclbm.tight_limits=["on" "on" "off"];
       axclbm.x_ticks.labels=emptystr(axclbm.x_ticks.labels);      

       edm=sisotoolNewEditorData(sub);
       edm.reqTypes=sisotoolRequirementTypes(axclbm.tag)
       edm.tunable=subWin.tunable
       //create graphic entities
       rtyps=list2vec(edm.reqTypes.type);
       edm.requirements=sisoCreateRequirements(rtyps,[1 1 2]);
       xpoly([],[]);e1=gce();e1.foreground=15;
       dfd=struct("TF_name",subWin.tunable,"units",units,"tipType",[]);
       set(e1,"display_function_data",dfd,...
          "display_function","sisotoolBodeTips");

       xpoly([],[]);e2=gce();e2.foreground=6;
       TF_name=msprintf(_("Closed loop %s to %s"),subWin.in,subWin.out)
       dfd=struct("TF_name",TF_name,"units",units,"tipType",[]);
       set(e2,"display_function_data",dfd,...
           "display_function","sisotoolBodeTips");

       edm.loci=[e1 e2];//gain locus

       //poles and zeros handles
       edm.zeros=sisoCreateZeros(dt)
       edm.poles=sisoCreatePoles(dt)
       edm.leadslags=sisoCreateLeadsLags(dt);
       edm.notches=sisoCreateNotches(dt);
      
       //gain margin handles
       e0=sisoCreateRuler(dt);
       e1=sisoCreateRuler(dt);
       e2=sisoCreateMargin(dt)
       edm.gmargin=[e0 e1 e2];
       edm.pmargin=sisoCreateRuler(dt);
       sisotoolSetEditorData(edm,axclbm)
       ax=axclbm;
    case "clbp" //Phase plot bode from in to out  
      axclbp=newaxes(mainH);axclbp.tag="clbp"
       axes_bounds=wsplit(sub,:);
       margins=wmargin(sub,:);
     
       sca(axclbp)
       axclbp.axes_visible="on";
       axclbp.axes_bounds=axes_bounds;
       axclbp.margins=margins;
       axclbp.background=-2;
       axclbp.clip_state='clipgrf';
       axclbp.x_label.text=msprintf(_("Frequency (%s)"),units.frequency);
       axclbp.y_label.text=msprintf(_("Phase (%s)"),units.phase);
       axclbp.data_bounds = [1e-3,-90;1e3,90];
       axclbp.log_flags = "lnn";
       axclbp.tight_limits=["on" "on" "off"];
       edp=sisotoolNewEditorData(sub);
       
       
       edp.reqTypes=sisotoolRequirementTypes(axclbp.tag)
       edp.tunable=subWin.tunable
       //create graphic entities
       rtyps=list2vec(edp.reqTypes.type);
       edp.requirements=sisoCreateRequirements(rtyps,2);
       
       xpoly([],[]);e1=gce();e1.foreground=15;
       dfd=struct("TF_name",subWin.tunable,"units",units,"tipType",[]);
       set(e1,"display_function_data",dfd,...
          "display_function","sisotoolBodeTips");
       xpoly([],[]);e2=gce();e2.foreground=6;
       TF_name=msprintf(_("Closed loop %s to %s"),subWin.in,subWin.out)
       dfd=struct("TF_name",TF_name,"units",units,"tipType",[]);
       set(e2,"display_function_data",dfd,"display_function","sisotoolBodeTips");
       edp.loci=[e1 e2];//gain locus
       
       //poles and zeros handles
       edp.zeros=sisoCreateZeros(dt)
       edp.poles=sisoCreatePoles(dt)
       edp.leadslags=sisoCreateLeadsLags(dt);
       edp.notches=sisoCreateNotches(dt);
       //gain margin handle
       edp.gmargin=sisoCreateRuler(dt);
       //phase margin handles
       e0=sisoCreateRuler(dt);
       e1=sisoCreateRuler(dt);
       e2=sisoCreateMargin(dt)
       edp.pmargin=[e0 e1 e2];
       
       sisotoolSetEditorData(edp,axclbp);
       
       ax=axclbp;
    end
    EditorHandles(sub)=ax;
  end //end of loop en subwindows
  S.EditorsSettings.handles=EditorHandles;
  mainH.immediate_drawing="on"
delete(A)
endfunction
function e=sisoCreateRuler(dt)
  xpoly([],[],"lines");e=gce();
  set(e,"foreground",27,"thickness",1,"line_style",4,"tag","ruler")//browndashed
endfunction
function e=sisoCreateMargin(dt)
  xpoly([],[],"lines");e=gce();
  set(e,"foreground",27,"thickness",2,"tag","margin")//brown
endfunction
function e=sisoCreatePoles(dt)
//create the markers entity of the image of the Controller poles
  xpoly([],[],"marks");e=gce();
  set(e,"datatip_display_mode","mouseclick",...
      "display_function","sisotoolComponentTip",...
      "display_function_data",struct("tipType","pole","data",[],"dt",dt,"units",units),...
      "mark_size_unit","point",...
      "mark_size",7,...
      "mark_style",2,...
      "mark_foreground",5,...
      "thickness",2,...
      "tag","poles"); 
endfunction
function e=sisoCreateZeros(dt)
//Create the markers entity of the image of the Controller zeros
  xpoly([],[],"marks");e=gce();
  set(e,"datatip_display_mode","mouseclick",...
      "display_function","sisotoolComponentTip",...
      "display_function_data",struct("tipType","zero","data",[],"dt",dt,"units",units),...
      "mark_size_unit","point",...
      "mark_size",7,...
      "mark_style",9,...
      "mark_foreground",5,...
      "thickness",2,...
      "tag","zeross");
endfunction

function e=sisoCreateLeadsLags(dt)
//create the markers entities of the image of the Controller leads
  
  //zeros
  xpoly([],[],"marks");e1=gce();
  set(e1,"datatip_display_mode","mouseclick",...
      "display_function","sisotoolComponentTip",...
      "display_function_data",struct("tipType","leadlag","data",[],"dt",dt,"units",units),...
      "mark_size_unit","point",...
      "mark_size",7,...
      "mark_style",9,...
      "mark_foreground",5,...
      "thickness",2,...
      "tag","leadslags zeros");
  
  //poles
  xpoly([],[],"marks");e2=gce();
  set(e2,"datatip_display_mode","mouseclick",...
      "display_function","sisotoolComponentTip",...
      "display_function_data",struct("tipType","leadlag","data",[],"dt",dt,"units",units),...
      "mark_size_unit","point",...
      "mark_size",7,...
      "mark_style",2,...
      "mark_foreground",5,...
      "thickness",2,...
      "tag","leadslags poles");
  e=[e1;e2];
endfunction


function e=sisoCreateNotches(dt)
//create the markers entities of the image of the Controller notches
  
  //zeros
  xpoly([],[],"marks");e1=gce();
  set(e1,"datatip_display_mode","mouseclick",...
      "display_function","sisotoolComponentTip",...
      "display_function_data",struct("tipType","notch","data",[],"dt",dt,"units",units),...
      "mark_size_unit","point",...
      "mark_size",7,...
      "mark_style",9,...
      "mark_foreground",5,...
      "thickness",2,...
      "tag","notches zeros");
  
  //poles
  xpoly([],[],"marks");e2=gce();
  set(e2,"datatip_display_mode","mouseclick",...
      "display_function","sisotoolComponentTip",...
      "display_function_data",struct("tipType","notch","data",[],"dt",dt,"units",units),...
      "mark_size_unit","point",...
      "mark_size",7,...
      "mark_style",2,...
      "mark_foreground",5,...
      "thickness",2,...
      "tag","notches poles");
  e=[e1;e2];
endfunction



function e=sisoCreateGain(dt)
  xpoly([],[],"marks")
  e=gce();
  e.display_function="sisotoolRlocusGainTip"
  e.display_function_data=struct("loci",[],"dt",dt,"units",units);
  e.datatip_display_mode="mouseclick";
  e.mark_size_unit="point";
  e.mark_size=6;
  e.mark_style=11;
  e.mark_foreground=6;
  e.mark_background=6;
endfunction

function e=sisoCreateWidth(dt)
  //draw the markers of the image of the Controller poles
  xpoly([],[],"marks")
  e=gce();
  e.mark_size_unit="point";
  e.mark_size=7;
  e.mark_style=4;
  e.thickness=2;
endfunction

