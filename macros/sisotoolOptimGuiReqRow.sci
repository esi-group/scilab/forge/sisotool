// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function h=sisotoolOptimGuiReqRow(frameH,y,TF_name,req_name,enabled, data)
  margin_x     = 5;  
  label_h      = 20;
  //see also sisotoolOptimGuiEltRow
  x0=margin_x;w0=label_h
  x1=x0+w0+margin_x/3;w1=50
  x2=x1+w1+margin_x/3;w2=100;
  x3=x2+w2+margin_x/3;w3=150;
  x4=x3+w3+margin_x/3;w4=150;
  x5=x4+w4+margin_x/3;w5=150;
  
  
  c=uicontrol( ...
      "parent"              , frameH,...
      "style"               , "checkbox",...
      "position"            , [x0,y,w0,label_h ],...
      "value"               , bool2s(enabled),...
      "background"          , bg, ...
      "user_data"           , data,...
      "callback"            ,"sisotoolUpdateOptSetting()",...
      "visible"             , "on");
  TF_name=uicontrol( ...
      "parent"              , frameH,...
      "style"               , "text",...
      "string"              , TF_name,...
      "position"            , [x1,y,w1+w2,label_h ],...
      "fontsize"            , 12,...
      "horizontalalignment" , "left", ...
      "visible"             , "on");
  req_name=uicontrol( ...
      "parent"              , frameH,...
      "style"               , "text",...
      "string"              , req_name,...
      "position"            , [x3,y,w3+w4+w5,label_h ],...
      "fontsize"            , 12,...
      "horizontalalignment" , "left", ...
      "visible"             , "on");
  h=[c TF_name req_name] //TBD TF_name et req_name probablement inutiles
endfunction
