// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolMoveRootLocus(mainH,editorData,entityType,i,k,pt,loc)
  ax=gca(); //current axes set by eventhandler
  figureHandle=ax.parent
  S=sisotoolGetSession(mainH)

  if entityType=="requirements" then

    req=ax.children($).children(i)
    ud=req.user_data
    p=ud //keep it in case of no move
    select req.tag
    case "settlingtime"
       while %t
         rep=xgetmouse([%t %t]);
         if rep(3)<>-1 then break,end
         p=-4/rep(1)//Ts
         d=sisotoolRequirementArea("rlocus","settlingtime",p,axesbounds(ax));
         figureHandle.info_message=msprintf(_("Maximum settling time =%.3g (s)"),p)
         req.data=d;

      end
    case "damping"
      while %t
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        p=-rep(1)/norm(rep(1:2))//zeta
        d=sisotoolRequirementArea("rlocus","damping",p,axesbounds(ax));
        figureHandle.info_message=msprintf(_("Mimimum damping factor=%.3g "),p)
        req.data=d;
      end

    case "wmax"
       while %t
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        p=norm(rep(1:2))//wn
        d=sisotoolRequirementArea("rlocus","wmax",p,axesbounds(ax));
        figureHandle.info_message=msprintf(_("Upper natural frequency limit=%.3g (rd/s)"),p)
        req.data=d;
      end
    case "wmin"
      while %t
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        p=norm(rep(1:2));//wn
        d=sisotoolRequirementArea("rlocus","wmin",p,axesbounds(ax));
        figureHandle.info_message=msprintf(_("Lower natural frequency limit=%.3g (rd/s)"),p)
        req.data=d;
      end
    else
      mprintf("sisotoolMoveRootLocus unknown tag:"+ req.tag)
    end
    req.user_data=p
    figureHandle.info_message="";
    if or(p<>ud) then
      windowType=ax.parent.tag
      Settings= windowType+"Settings"
      subWinIndex=editorData.subWinIndex
      requirementIndex=find(list2vec(S(Settings).subWins(subWinIndex).reqProps.type)==req.tag)
      reqProp= S(Settings).subWins(subWinIndex).reqProps(requirementIndex);
      S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=p;
      opt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type);
      S=sisotoolAddHistory(S,"Requirements","update",...
                         windowType,subWinIndex,requirementIndex,reqProp,opt)
      sisotoolSetSession(S,mainH)
    end
    return
  end

  sys=S.Designs
  editorData=sisotoolGetEditorData(ax)
  compensatorName=editorData.tunable
  C=sys(compensatorName)
  if entityType=="loci" then return;end
  cur=sisotoolGetCElement(S,compensatorName,entityType,k);
  S=sisotoolAddHistory(S,"Compensator","update",compensatorName, ...
                       entityType,k,cur);
  if or(entityType==["zeros" "poles"]) then
    if entityType=="zeros" then
      info_message=_("Drag this zero  to the desired location.")+" "+ ...
          _("Current location:%s")
    else
      info_message=_(" Drag this pole  to the desired location.")+" "+ ...
          _("Current location:%s")
    end
    figureHandle.info_message=msprintf(info_message, ...
                                       string(C(entityType)(k)))
   
    if imag(C(entityType)(k))==0 then //real case
      while %t
        set("current_axes", ax);
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        C(entityType)(k)=rep(1);
        figureHandle.info_message=msprintf(info_message,string(C(entityType)(k)))
        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
      end
    else //pair of complex case
      while %t
        set("current_axes", ax);
        [rep,w]=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        C(entityType)(k)=complex(rep(1),-abs(rep(2)));
        figureHandle.info_message=msprintf(info_message,string(C(entityType)(k)))
        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
      end
    end
    figureHandle.info_message=""
    if rep==-1000 then sys=[];end //the tool has been closed
  elseif entityType=="gain" then
    //retreive the parameter K along the loci
    //The gain images in the root locus are drawn at the
    //locations of the closed loop poles
    info_message=_("Drag this mark along the locus to adjust the loop gain.")+" "+ ...
        _("Current gain:%s")
    figureHandle.info_message=msprintf(info_message,string(C.gain))
    h=editorData.loci

    kk=h(1).display_function_data; //they are all the same
    //find the selected branch (a closed loop pole)
    dmin=%inf;imin=0,indmin=0
    for i=1:size(h,'*')
      [d,ptp,ind,c]=orthProj(h(i).data,pt)
      if d<dmin then dmin=d,imin=i;end
    end
    if imin==0 then imin=1,end //None branch selected
    data=h(imin).data
    while %t
      //move the closed loop pole along the selected locus
       set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      //interpolate the associated loop gain
      [d,ptp,ind,c]=orthProj(data,rep(1:2))
      if ind<>[] then
        C.gain=kk(ind)+(kk(ind+1)-kk(ind))*c;
        figureHandle.info_message=msprintf(info_message,string(C.gain))
        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
      end
    end
    figureHandle.info_message=""
    k=1
    if rep==-1000 then sys=[];end //the tool has been closed
  elseif entityType=="leadslags" then
    //move only the selected pole or zero
    if i==1 then
      info_message=_("Drag this real zero  to the desired location.")+" "+ ...
          _("Current value:%s")
    else
      info_message=_(" Drag this real pole  to the desired location.")+" "+ ...
          _("Current value:%s")
    end
    figureHandle.info_message=msprintf(info_message, string(C(entityType)(i,k)))
    while %t
      set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      C(entityType)(i,k)=rep(1);
      figureHandle.info_message=msprintf(info_message,string(C(entityType)(i,k)))
      sys(compensatorName)=C;
      sisotoolSetSys(sys,mainH);
      sisotoolUpdateResponses(mainH);
    end
    figureHandle.info_message=""
    if rep==-1000 then sys=[];end //the tool has been closed  
    sys(compensatorName)=C;
  
  elseif entityType=="notches" then
    //keep equal natural frequency wn for poles and zeros and
    //ensure that |zetaz|<|zetap|

    if i==1 then //zero
      info_message=_("Drag this zero to adjust notch.")+" "+ ...
          _("Current value:%s")
      while %t
        figureHandle.info_message=msprintf(info_message,string(C(entityType)(i,k)))

        set("current_axes", ax);
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        z=complex(rep(1),-abs(rep(2)));//new zero position
        p=C(entityType)(2,k); //old pole position
        [wn,zetaz]=damp(z,C.dt);
        if zetaz>=0 then
          [wnold,zetap]=damp(p,C.dt);
          zetap=max(zetaz,zetap);
          p=wnzeta2complex(C.dt,wn,zetap)
          C(entityType)(:,k)=[z;p];
          sys(compensatorName)=C;
          sisotoolSetSys(sys,mainH);
          sisotoolUpdateResponses(mainH);
        end
      end
    else //pole
      info_message=_("Drag this pole to adjust notch.")+" "+ ...
          _("Current value:%s")
      while %t
        set("current_axes", ax);
        figureHandle.info_message=msprintf(info_message,string(C(entityType)(i,k)))
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        p=complex(rep(1),-abs(rep(2)));//new pole position
        z=C(entityType)(1,k);//old zero position
        [wn,zetap]=damp(p,C.dt);
        if zetap>=0 then
          [wnold,zetaz]=damp(z,C.dt);
          zetaz=min(zetaz,zetap);
          z=wnzeta2complex(C.dt,wn,zetaz)
          C(entityType)(:,k)=[z;p];
          sys(compensatorName)=C;
          sisotoolSetSys(sys,mainH);
          sisotoolUpdateResponses(mainH);
        end
      end
    end
    figureHandle.info_message=""
  end
  //update optimSettings
  S=sisotoolUpdateCElement(S,compensatorName,entityType,k,C(entityType)(:,k))
  sisotoolSetSession(S,mainH);

endfunction

