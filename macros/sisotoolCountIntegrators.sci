// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function ni=sisotoolCountIntegrators(sys)
  if sys.dt=="c" then
    select typeof(sys)
    case "rational" then
      P1=sys.den
      ni=degree(P1)-find(coeff(P1)<>0,1)+1
    case "state space"
      A=sys.A;n=size(A,1);
      A1=eye(A);n1=n;
      while %t 
        A1=A1*A;
        r= rank(A1);
        if r==n1 then break;end
        n1=r;
      end
      ni=n-n1
    case "zpk"
      ni=sum(sys.P{1}==0)
    end
  else
    select typeof(sys)
    case "rational" then
      P1=sys.den
      ni=0;
      while %t
        if abs(sum(coeff(P1)))>sum(abs(coeff(P1)))*%eps then
          break
        end
        ni=ni+1;
        P1=derivat(P1);
      end
    case "state space"
      A=sys.A-eye();n=size(A,1);
      A1=eye(A);n1=n;
      while %t 
        A1=A1*A;
        r= rank(A1);
        if r==n1 then break;end
        n1=r;
      end
      ni=n-n1
    case "zpk"
      ni=sum(sys.P{1}==1)
    end
  end
endfunction
