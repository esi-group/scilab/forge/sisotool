function d=dBdist2isogain(att,db,phi)
  //ecart minimal entre une courbe d'iso gain et une courbe (phi,db)
  w=linspace(-%pi,0,200)';
  y=10^(att/20)*exp(%i*w);
  y(y==1)=[];//remove singular point if any
  rf=y./(1-y);
  [m, p]=dbphi(rf);
  p=[-360-p($:-1:1);p];
  m=[m($:-1:1);m]; 
  n=size(p,"*");
  
  while min(phi)<p(1) then
    p=[p(1:n-1)-360;p];
    m=[m(1:n-1)    ;m];
  end
  while max(phi)>p($)
    p=[p;p($-n+2:$)+360];
    m=[m;m($-n+2:$)];
  end

  d=-%inf
  
  for k=1:size(phi,"*")
    ind=find(phi(k)>=p(1:$-1)&phi(k)<p(2:$))
    if ind<>[] then
      [mm,i]=min(m(ind))
      d=max(d,db(k)-min(m(ind)))
    end
  end
endfunction
