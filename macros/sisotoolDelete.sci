// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolDelete(win) 
  
  //A REVOIR pour utiliser les handle de editordata
  mainH=get_figure_handle(win)
  mainH.info_message=_("Click left on the pole/zero/lead/lag you want to delete");
  ax=gca()//current axes has been set by sisotoolEditorsEvents
  [btn,pt]=getPointInSubwin(ax) 
  editorData=sisotoolGetEditorData(ax)
 
  compensatorName=editorData.tunable
  [xi,yi]=xchange(pt(1),pt(2),'f2i')
  [entityType,i,k,pt,loc]=sisotoolFindSelected(editorData,[xi,yi])
  if entityType==[]|entityType=="loci" then return,end
  mainH.info_message="";
  S=sisotoolGetSession(mainH)
  cur=sisotoolGetCElement(S,compensatorName,entityType,k);
  S=sisotoolAddHistory(S,"Compensator","delete",compensatorName,entityType,k,cur)
  //S=sisotoolAddHistory(S,compensatorName,S.Designs(compensatorName)) TBD verifier
  S=sisotoolDeleteCElement(S,compensatorName,entityType,k)
  sisotoolSetSession(S,mainH);
  sisotoolRedraw(mainH);
  sisotoolUpdateOptimGui(mainH)
endfunction
