// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function %_L=sisotoolAvailableSystems(%_dt,%_f)
  //use of %_ to avoid name conflits
  %_L=[];
  if argn(2)==2 then 
    if execstr("load(%_f)","errcatch")<>0 then return,end
    [%_N,w] = listvarinfile(%_f)
  else
    %_N=who_user(%f)
    %_N(grep(%_N,"/^%.*/",'r'))=[];
  end
  for %_name=matrix(%_N,1,-1)
     execstr(["if sisotoolIsSISO("+%_name+","+sci2exp(%_dt)+") then"
              "  %_L=[%_L %_name]"
              "end"]);
  end
endfunction
