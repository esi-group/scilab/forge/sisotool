// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolLQGTuning()
  guiHandle=gcbo.parent
  ud=guiHandle.user_data;
  mainH=ud.mainH;
  values=evstr(ud.handles.string) //,[Pe,Me,Xe,Ue,Ie]
  frameC=guiHandle.children(4);
  Popup=frameC.children(2)
 
  S=sisotoolGetSession(mainH);
  S.LQGSettings= [values Popup.value];//update settings
  sys=S.Designs
  [plants,out_of]=sisotoolGetArchiSpecs(sys.archi,["plant","out_of"]);
  
  out_of=out_of(Popup.value)
  dt=sys(plants(1)).dt
  sys.F=sisotoolNewController(dt);
  sys(out_of)=sisotoolNewController(dt);
  P=sisotoolSys2OL(sys,out_of)
  if typeof(P)=="rational" then P=tf2ss(P);end
  ns=size(P.A,1);
  [ny,nu]=size(P)

  //LQ estimator
 
  Wy = values(2)*eye(ny,ny);
  Wu = values(1)*eye(nu,nu);

  Swv=zeros(nu,ny);
  Qww=P.B*Wu*P.B';
  Rvv=Wy+P.D*Swv + Swv'*P.D+P.D*Wu*P.D';
  Swv=P.B*Wu*P.D'+Swv;
  Qwv=[Qww Swv;Swv' Rvv];
  
   
  //LQ compensator
  Qxx=P.C'*values(3)*P.C
  Quu=values(4)*eye(nu,nu);
  Qi=values(5);
  K=lqg(P,sysdiag(Qxx,Quu),Qwv,Qi,1)

  //Update controller data structure 
  sys(out_of)=sisotooltf2C(K)
  S.Designs=sys
  sisotoolSetSession(S,mainH);
  sisotoolRedraw(mainH);
endfunction
