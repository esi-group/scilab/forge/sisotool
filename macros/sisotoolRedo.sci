// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolRedo(win)
  mainH=get_figure_handle(win);
  if mainH.tag<>"Editors" then
    figHandle=mainH
    mainH=mainH.user_data.mainH
  else
    figHandle=mainH
  end
  S=sisotoolGetSession(mainH);
  current=S.History.current

  if current+1>size(S.History.stack) then return;end
  next=S.History.stack(current+1);
  args=next.args
  kind=next.kind
  op=next.op
  S.History.current=current+1
  select kind
  case "Compensator"
    compensatorName=args(1);
    entityType=args(2)
    select op
    case "add" //redo add component
       S=sisotoolSetCElement(S,compensatorName,entityType,args(3))
    case "delete" //redo delete component
       S=sisotoolDeleteCElement(S,compensatorName,entityType,args(3))
    case "update"  //redo update component 
      if entityType=="all" then //TBD verifier
        cur=list(S.Designs(compensatorName))
        for i=1:size(compensatorName,"*")
          S.Designs(compensatorName(i))=args(3)(i)
        end
      else
        index=args(3)
        if entityType=="gain" then index=1;end
        value=args(4)
        cur=sisotoolGetCElement(S,compensatorName,entityType,index);
        S=sisotoolSetCElement(S,compensatorName,entityType,value,index);
      end
      next.args(4)=cur
      S.History.stack(current+1)=next;
    end
  case "Requirements"
    windowType=args(1);
    Settings=windowType+"Settings";
    subWinIndex=args(2);
    requirementIndex=args(3);
    reqProp=args(4);
    opt=args(5)
   
    select op
    case "add" //redo add requirement
      args(4).data=[]
      args(5)=%f(ones(opt))
      S.History.stack(current+1).args=args
      S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=reqProp.data;
      S.OptimSettings(windowType)(subWinIndex)(reqProp.type)=opt;
    case "delete" //redo delete requirement
      S.History.stack(current+1).args(4).data= ...
          S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data
      S.History.stack(current+1).args(5)=S.OptimSettings(windowType)(subWinIndex)(reqProp.type)
      S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=[];
      S.OptimSettings(windowType)(subWinIndex)(reqProp.type)=%f(ones(opt));
    case "update"  //redo update requirement 
      cur=S(Settings).subWins(subWinIndex).reqProps(requirementIndex)
      curopt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type)
      S(Settings).subWins(subWinIndex).reqProps(requirementIndex).data=reqProp.data;
      S.OptimSettings(windowType)(subWinIndex)(reqProp.type)=opt;
      S.History.stack(current+1).args(4)=cur;
      S.History.stack(current+1).args(5)=curopt;
    end
  case "OptimSettings"
     kind=args(1);
     tag=op
    select op
    case "req"
      sub=args(2);req_type=args(3);requirementIndex=args(4);v=args(5);
      cur= S.OptimSettings(kind)(sub)(req_type)(requirementIndex)
      S.OptimSettings(kind)(sub)(req_type)(requirementIndex)=v
      S.History.stack(current+1).args(5)=cur;
    case "enabled"
      tunable=args(2);typ=args(3);index=args(4);v=args(5);
      cur=S.OptimSettings(kind)(tunable)(typ)(tag)(1,index)
      S.OptimSettings(kind)(tunable)(typ)(tag)(1,index)=v;
      S.History.stack(current+1).args(5)=cur;
    case "style"
      guiHandle=S.OptimGui
      frame=guiHandle.children(3)
      rtyp=frame.children(4).user_data
      cur= S.OptimSettings(kind)(tunable)(typ)(tag)(1,index)
      tunable=args(2);typ=args(3);index=args(4);v=args(5);
      initial=S.OptimSettings(kind)(tunable)(typ).initial(:,index);
      lowbound=S.OptimSettings(kind)(tunable)(typ).lowbound(:,index);
      highbound=S.OptimSettings(kind)(tunable)(typ).highbound(:,index);
      [initial,lowbound,highbound]=sisotoolChangeEltFormat(rtyp,style,initial,lowbound,highbound);
      S.OptimSettings(kind)(tunable)(typ).initial(:,index)=initial;
      S.OptimSettings(kind)(tunable)(typ).lowbound(:,index)=lowbound;
      S.OptimSettings(kind)(tunable)(typ).highbound(:,index)=highbound;
      S.OptimSettings(kind)(tunable)(typ)(tag)(1,index)=style;
      S.History.stack(current+1).args(5)=cur;
    case "options"
      cur= S.OptimSettings.Options
      S.OptimSettings.Options=args(1)
      S.History.stack(current+1).args(1)=cur;
    else //initial lowbound or highbound
      tunable=args(2);typ=args(3);index=args(4);v=args(5);
      i=find(tag==[ "highbound" "lowbound" "initial"])
      cur=S.OptimSettings(kind)(tunable)(typ)(tag)(:,index)
      S.OptimSettings(kind)(tunable)(typ)(tag)(:,index)=v;
      S.History.stack(current+1).args(5)=cur;
    end
  end 
  sisotoolSetSession(S,mainH);
  if kind<>"OptimSettings" then sisotoolRedraw(mainH);end
  sisotoolUpdateOptimGui(mainH);
endfunction 
