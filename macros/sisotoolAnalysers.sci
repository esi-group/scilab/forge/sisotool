// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAnalysers(mainWin)
  mainH=get_figure_handle(mainWin);
  S=sisotoolGetSession(mainH)
  analysersHandle=S.AnalysersSettings.winHandle
  if analysersHandle<>[] then return,end
  archi=S.Designs.archi
  Analysers=S.AnalysersSettings.Analysers
  analysersHandle=figure("dockable", "on", ...
                 "menubar", "figure", ...
                 "toolbar", "none", ...
                 "infobar_visible", "on",  ...
                 "default_axes","off",...
                 "background",lightgray()...
                );
  id=analysersHandle.figure_id
  analysersHandle.figure_name=_("Analysers  selector")+" ("+string(id)+")";
  AnalysersMenuBar(analysersHandle) 
  N=size(Analysers,"*")
  [axes_bounds,margins]=sisotoolSubwinSettings(N)
  handles=list()
  for ka=1:N
    bounds=axes_bounds(ka,:);
    margins=margins(ka,:);
    if Analysers(ka).type=="bode" then
      //module
      bounds(4)=bounds(4)/2;
      margins(3)=margins(3)+2/8;
      margins(4)=0.05;
      axm=newaxes(analysersHandle);
      axm.axes_bounds=bounds;
      axm.margins=margins; 
      //phase
      margins=margins(k,:);
      bounds(2)=bounds(2)+bounds(4);
      margins(4)=margins(4)+2/8;
      margins(3)= 0.05;
      axp=newaxes(analysersHandle);
      
      axp.axes_bounds=bounds;
      axp.margins=margins;
      Analysers(ka).handle=[axm,axp]
    else
      ax=newaxes(analysersHandle);
      ax.axes_bounds=bounds;
      ax.margins=margins;
      Analysers(ka).handle=ax;
    end
    //TBD recreer les graphiques
  end
  S.AnalysersSettings.Analysers=Analysers
  S.AnalysersSettings.winHandle=analysersHandle
  set(analysersHandle,"user_data",struct("mainH",mainH))
  sisotoolSetSession(S,mainH)
  analysersHandle.event_handler="sisotoolAnalysersEvents"
  analysersHandle.event_handler_enable = "on"
endfunction

function AnalysersMenuBar(analysersHandle) 
  view=uimenu("parent",  analysersHandle,"label", _("Settings"))
  
  uimenu("parent", view, ...
         "label", _("Add a sub-window"),...
         "callback" ,list(4,"sisotoolAddSubwin("+string(id)+")"))
  
  uimenu("parent", view, ...
         "label", _("Delete a sub-window ..."),...
         "callback" ,list(4,"sisotoolDeleteSubwin("+string(id)+")"))
endfunction
  
