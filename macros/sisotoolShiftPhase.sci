// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function phi=sisotoolShiftPhase(f,phi,fc,phic,twopi)
// shift the phase phi, to make the point (f,phi) be on the curve (fc,phic)
// f a scalar, the point frequency
// phi a scalar, the point phase
// fc an array, the curve frequencies
// phic an array the curve phases
  for i=1:size(f,"*")
    k=find(fc(1:$-1)<=f(i)&fc(2:$)>f(i));
    if k<>[] then
      [m,k]=min(abs(fc-f(i)))
      phi(i)=phi(i)+round((phic(k)-phi(i))/(twopi))*(twopi)
    end
  end
endfunction
