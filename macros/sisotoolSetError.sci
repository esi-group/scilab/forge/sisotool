// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function r=sisotoolSetError(h,r,msg)
//Show erroneous edited value
  if argn(2)<3 then msg="";end
  if ~r then 
    h.backgroundcolor=[1 1 1]
    h.tooltipstring=""
  else
    h.backgroundcolor=[1 0 0]
    h.tooltipstring=msg
    mprintf("%s\n",msg)
  end
endfunction
