// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function wloc=sisotoolNotchWidthLoc(notches,dt)
//compute the frequency  (in Hz) position of the notch witdh markers
//freqloc is a 2 by n array. Each column gives the frequency locations of the
//notch markers
//  
//The notch transfer function is given by
// H(s)=(1+2*zetaz*s/w+s^2/w^2)/(1+2*zetap*s/w+s^2/w^2)
//the width markers are located at 25% of the max depth of the notch
//given in dB
  
//the notch depth is the notch modulus at w=wn. So depth=zetaz/zetap
//let find the frequency values such that
//  20*log10(|H(i*w)|)=df*20*log10(zetaz/zetap)
// or equivalently
//  |H(iw)|^2=(zetaz/zetap)^(df*2)
//  |H(iw)|^2 =((1-r)^2+4*r*zetaz^2)/((1-r)^2+4*r*zetap^2) with r=w^2/wn^2
//so r is given by a second order equation.
//  (1-r)^2+4*r*zetaz^2-((1-r)^2+4*r*zetap^2)*(zetaz/zetap)^(df*2)=0
//  (1-dr)+r*(-2+4*zetaz^2-(-2+4*zetap^2)*dr)+r^2*(1-dr)  
//with
//  dr=(zetaz/zetap)^(df*2)
  [wn,zeta]=damp(notches(:,:),dt)
  df=0.25;
  dr=(zeta(1,:)./zeta(2,:)).^(df*2);//depth ratio
  a=1-dr
  b=-2+4*zeta(1,:).^2-(-2+4*zeta(2,:).^2).*dr
  wloc=zeros(notches)
  for i=1:size(zeta,2)
    P=poly([a(i),b(i),a(i)],"r","c");
    wloc(:,i)=gsort(sqrt(real(roots(P)))*wn(i),"g","i");
  end
 wloc=wloc/(2*%pi);
endfunction
