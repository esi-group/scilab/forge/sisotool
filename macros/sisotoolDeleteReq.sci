// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolDeleteReq(win,editorType,requirementType,requirementIndex,subWinIndex)
  figHandle=get_figure_handle(win);
  windowType=figHandle.tag;
  if windowType<>"Editors" then
    //called by the analysers window
    mainH=figHandle.user_data.mainH;
  else
    mainH=figHandle;
  end
  editors=figHandle.children(figHandle.children.type=="Axes");
  ax=editors(editors.tag==editorType)
  Settings=windowType+"Settings"
  S=sisotoolGetSession(mainH);
  subWin=S(Settings).subWins(subWinIndex)  //subWins defined for sisotoolUpdateOptimGui
  reqProps=subWin.reqProps
  reqProp=reqProps(requirementIndex);
 
  sisotoolDrawRequirement(ax,requirementType,[]);
  opt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type)
  S=sisotoolAddHistory(S,"Requirements","delete", windowType,subWinIndex,requirementIndex,reqProp,opt)
  reqProp.data=[]
  reqProps(requirementIndex)=reqProp
  S(Settings).subWins(subWinIndex).reqProps=reqProps;
  
  
  //update  OptimSettings
  s=S.OptimSettings(windowType)(subWinIndex);
  s(reqProp.type)=null();
  S.OptimSettings(windowType)(subWinIndex)=s;


  sisotoolUpdateOptimGui(mainH)
  sisotoolSetSession(S,mainH)
  
endfunction
