// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolRequirementOk()
  guiHandle=gcbo.parent
  ud=guiHandle.user_data

  ax=ud.axes
 
  windowType=ax.parent.tag
  mainH=ud.mainH
  valueHandle=ud.valueHandle
  editorData=sisotoolGetEditorData(ax)
  
  subWinIndex=editorData.subWinIndex
  S=sisotoolGetSession(mainH)
  units=S.Preferences.units
  Settings= windowType+"Settings"
 
  subWins=S(Settings).subWins
  requirementIndex=ud.ind
  subWin=subWins(subWinIndex)
  
  reqProp=subWin.reqProps(requirementIndex)
  reqProp_old=reqProp
  requirementType=reqProp.type;

  reqs=ax.children($).children
  req=reqs(find(reqs.tag==requirementType))
  if or(reqProp.dlgType==["scalar" "damping"]) then
    valueHandle=ud.valueHandle
    requirementData=evstr(valueHandle.string)
  elseif reqProp.dlgType=="table" then
    tableFrame=ud.valueHandle
    ncols=tableFrame.user_data;
    H=matrix(tableFrame.children($:-1:1),ncols+2,-1)'//the last 2 column contains the handles on + - buttons
    requirementData=matrix(evstr(H(:,1:2).string),-1,2)
  elseif  reqProp.dlgType=="tunnel" then
    valueHandle=ud.valueHandle
    requirementData=evstr(valueHandle.string)
  end
  added=req.user_data==[]// true if the requirement  has been added,false if modified
  reqProp.data=requirementData;
  
  req.user_data=requirementData;//for sisotoolUpdateReqs
  if added then
    if windowType=="Editors" then 
      nt=1 //TBD a verifier pour le cas filter
    else
      nt=size(subWin.transferSelection,"*")
    end
    op="add"
    opt=%f(ones(1,nt));
    S.OptimSettings(windowType)(subWinIndex)(reqProp.type)=opt
  else
    op="update"
    opt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type)
  end
  S=sisotoolAddHistory(S,"Requirements",op,...
                       windowType,subWinIndex,requirementIndex,reqProp_old,opt)
  sisotoolDrawRequirement(ax,reqProp.type,requirementData)
  
  S(Settings).subWins(subWinIndex).reqProps(requirementIndex)=reqProp

  sisotoolSetSession(S,mainH)
  if added then sisotoolUpdateOptimGui(mainH);end

  delete(guiHandle)
endfunction
